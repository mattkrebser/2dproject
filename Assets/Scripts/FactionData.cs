﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Contains static information about how faction members dress
/// </summary>
[System.Serializable]
public class FactionData : MonoBehaviour
{
    [SerializeField]
    public List<FactionStyle> Factions = new List<FactionStyle>();

    [System.Serializable]
    public class FactionStyle
    {
        [Tooltip("This is Just a label")]
        public Factions faction;
        [SerializeField]
        public List<string> FactionShirtsMale = new List<string>();
        [SerializeField]
        public List<string> FactionShirtsFemale = new List<string>();
        [SerializeField]
        public List<string> FactionPants = new List<string>();
        [SerializeField]
        public List<string> FactionShoes = new List<string>();
        [SerializeField]
        public List<string> FactionGlasses= new List<string>();
        [SerializeField]
        public List<string> FactionLWeapon = new List<string>();
        [SerializeField]
        public List<string> FactionRWeapon = new List<string>();
        [SerializeField]
        public List<string> FactionHats = new List<string>();

        //all shirts and all pants does not include nude

        public bool all_weaponsL, all_weaponsR, allHats, allGlasses, allshoes, allpants, allshirts, eye_whites;
        public bool nude_only;
    }
}

