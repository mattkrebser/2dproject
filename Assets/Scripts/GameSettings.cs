﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameSettings : MonoBehaviour
{
    private static bool _Running = false;
    /// <summary>
    /// Global spawn modifier
    /// </summary>
    public static bool Running
    {
        get
        {
            return _Running;
        }
    }
    /// <summary>
    /// Is the player currently in the lobby?
    /// </summary>
    public static bool InLobby = false;
    /// <summary>
    /// Is the lobby running? (This value is very similar to 'InLobby', but is changed a few frames slower
    /// </summary>
    public static bool LobbyRunning = false;

    private static bool _inGame = false;
    /// <summary>
    /// Are we in the Main menu or the game?
    /// </summary>
    public static bool IsInGame
    {
        get
        {
            return _inGame;
        }
    }

    public static GameSettings Instance;

    void Awake()
    {
        GetComponent<CoroutineHelper>().Init();
        Instance = this;
        Application.targetFrameRate = 120;
        PeopleData.Init();

#if UNITY_EDITOR
        Profiler.maxNumberOfSamplesPerFrame += 1000;
#endif
        StartCoroutine(LateDisplayRoutine());
    }

    /// <summary>
    /// Display the lobby scene after waiting a few frames
    /// </summary>
    /// <returns></returns>
    IEnumerator LateDisplayRoutine()
    {
        yield return 0; yield return 0; yield return 0; yield return 0;
        yield return 0; yield return 0; yield return 0; yield return 0;
        yield return 0; yield return 0; yield return 0; yield return 0;

        //get the persistent object
        GameObject p = GameObject.FindWithTag("Persistant");
        DontDestroyOnLoad(p);

        //loadgame
        if (p.GetComponent<PersistantData>().LoadGame)
            LoadGame(p.GetComponent<PersistantData>().LoadName);
        //newgame
        else if (p.GetComponent<PersistantData>().NewGame)
            NewGame(p.GetComponent<PersistantData>().firstName, p.GetComponent<PersistantData>().lastName,
                p.GetComponent<PersistantData>().isMale);
        //display lobby
        else
            DisplayLobbyScene();
    }

    public void DisplayLobbyScene()
    {
        string SaveName = DiskOp.LobbySave;

        _inGame = false;
        InLobby = true;

        GameVariables gv = DiskOp.LoadObject<GameVariables>(SaveName + "Game.dat");
        List<Faction> fDat = DiskOp.LoadObject<List<Faction>>(SaveName + "Faction.dat");
        List<PlayerInfo> pInfo = DiskOp.LoadObject<List<PlayerInfo>>(SaveName + "Character.dat");
        List<VehicleInfo> vInfo = DiskOp.LoadObject<List<VehicleInfo>>(SaveName + "Vehicles.dat");
        PlayerInfo2 pi2 = DiskOp.LoadObject<PlayerInfo2>(SaveName + "Player.dat");
        List<PlayerInfo> statChars = DiskOp.LoadObject<List<PlayerInfo>>(SaveName + "StaticCharacter.dat");
        List<ZoneDat> zones = DiskOp.LoadObject<List<ZoneDat>>(SaveName + "Zone.dat");
        List<SaveStateDat> objDat = DiskOp.LoadObject<List<SaveStateDat>>(SaveName + "Obj.dat");

        //initialize
        InitGameVar(gv);
        InitFDat(fDat);
        InitVehicles(vInfo);
        InitCharacters(pInfo, pi2);
        InitStaticCharacters(statChars);
        InitZones(zones);
        SaveStateObject.SetDat(objDat);

        _Running = false;
        LobbyRunning = true;

        //Hibernate all maps but the one the player is in
        MapData active = MapData.maps[pi2.ActiveMap];
        foreach (MapData m in MapData.maps.Values)
        {
            if (m.ZoneID != active.ZoneID)
                HibernateMap(m);
        }

        StartCoroutine(MainMenuInit());
    }
    /// <summary>
    /// Set the character to immortal in the lobby scene.
    /// </summary>
    /// <returns></returns>
    IEnumerator MainMenuInit()
    {
        yield return 0;
        CharacterMove.CharacterTransform.GetComponent<CharacterData>().IsImmortal = true;
    }

    /// <summary>
    /// Get rid of the lobby scene. (The lobby is actually just a saved game being played with an invisible player)
    /// </summary>
    /// <param name="p"></param>
    /// <param name="name"></param>
    /// <param name="settings"></param>
    public void HideLobbyScene(PersistantData p, string name, NewGameMenu settings = null)
    {
        InLobby = false;
        LobbyRunning = false;
        if (settings == null)
        {
            p.LoadGame = true;
            p.LoadName = name;
        }
        else
        {
            p.NewGame = true;
            string first = String.IsNullOrEmpty(settings.FirstName.text.Trim()) ? "Jean" : settings.FirstName.text;
            string last = String.IsNullOrEmpty(settings.LastName.text.Trim()) ? "" : settings.LastName.text;
            p.firstName = first;
            p.lastName = last;
            p.isMale = settings.IsMale;
        }
        QuitGameNoSave();
    }

    /// <summary>
    /// Called from main menu, makes a new game
    /// </summary>
    public void NewGame(NewGameMenu newSettings)
    {
        //format inputs
        string first = String.IsNullOrEmpty(newSettings.FirstName.text.Trim()) ? "Jean" : newSettings.FirstName.text;
        string last = String.IsNullOrEmpty(newSettings.LastName.text.Trim()) ? "" : newSettings.LastName.text;

        string SaveName = first + " " + last + " -AutoSave";

        GameObject p = GameObject.FindWithTag("Persistant");

        if (p.GetComponent<PersistantData>().NewGame == false)
            HideLobbyScene(p.GetComponent<PersistantData>(), SaveName, newSettings);
    }
    /// <summary>
    /// Secondary function, called after scene reload
    /// </summary>
    /// <param name="firstName"></param>
    /// <param name="lastName"></param>
    /// <param name="isMale"></param>
    public void NewGame(string firstName, string lastName, bool isMale)
    {
        //format inputs
        string first = firstName;
        string last = lastName;

        GameObject p = GameObject.FindWithTag("Persistant");

        PersistantData pd = p.GetComponent<PersistantData>();
        pd.NewGame = false;
        pd.firstName = "";
        pd.lastName = "";
        pd.isMale = false;
        StartCoroutine(WaitNewGame(first, last, isMale));

    }
    /// <summary>
    /// Initialize new game after waiting one frame
    /// </summary>
    /// <param name="firstName"></param>
    /// <param name="lastName"></param>
    /// <param name="isMale"></param>
    /// <returns></returns>
    IEnumerator WaitNewGame(string firstName, string lastName, bool isMale)
    {
        yield return 0;

        //initialize
        InitGameVar(null);
        InitFDat(null);
        InitVehicles(null);

        //format inputs
        string first = firstName;
        string last = lastName;

        string SaveName = first + " " + last + " -AutoSave";

        InitCharacters(null, null, isMale,
            first, last);

        _inGame = true;
        _Running = true;

        SaveGame(SaveName);

        StartCoroutine(DelayedHibernateAll());
        Quest.LateInitQuests();
    }

    /// <summary>
    /// called from main menu, loads an exisintg game
    /// </summary>
    /// <param name="SaveName"></param>
    public void LoadGame(string SaveName)
    {
        GameObject p = GameObject.FindWithTag("Persistant");

        if (p.GetComponent<PersistantData>().LoadGame == false)
            HideLobbyScene(p.GetComponent<PersistantData>(), SaveName);
        else
        {
            p.GetComponent<PersistantData>().LoadName = "";
            p.GetComponent<PersistantData>().LoadGame = false;
            StartCoroutine(WaitLoad(SaveName));
        }
    }
    /// <summary>
    /// Load the game in after waiting one frame
    /// </summary>
    /// <param name="SaveName"></param>
    /// <returns></returns>
    IEnumerator WaitLoad(string SaveName)
    {
        yield return 0; 
        _inGame = true;

        GetComponent<UIMenus>().MainMenu.SetActive(false);

        GameVariables gv = DiskOp.GetGameVars(SaveName);
        List<Faction> fDat = DiskOp.GetFactionDat(SaveName);
        List<PlayerInfo> pInfo = DiskOp.GetAllCharacters(SaveName);
        List<VehicleInfo> vInfo = DiskOp.GetAllVehicles(SaveName);
        PlayerInfo2 pi2 = DiskOp.GetPlayerDat(SaveName);
        List<PlayerInfo> statChars = DiskOp.GetStaticChars(SaveName);
        List<ZoneDat> zones = DiskOp.GetZoneDat(SaveName);
        List<SaveStateDat> objDat = DiskOp.GetObjDat(SaveName);

        //initialize
        InitGameVar(gv);
        InitFDat(fDat);
        InitVehicles(vInfo);
        InitCharacters(pInfo, pi2);
        InitStaticCharacters(statChars);
        InitZones(zones);
        SaveStateObject.SetDat(objDat);

        _Running = true;

        //Hibernate all maps but the one the player is in
        StartCoroutine(DelayedHibernateAll());
    }

    /// <summary>
    /// Called from in game, saves the current game
    /// </summary>
    /// <param name="SaveName"></param>
    public void SaveGame(string SaveName, bool DontReset = false)
    {
        GameVariables vb = GetLiveGameVar();
        List<Faction> facDat = GetLiveFactionsDat();
        List<VehicleInfo> vDat = GetLiveVehicles();
        List<PlayerInfo> pDat = GetLiveCharacters();
        PlayerInfo2 pI2 = GetPlayerSpecificInformation();
        List<PlayerInfo> staticChars = GetStaticCharData();
        List<ZoneDat> z = GetZones();
        List<SaveStateDat> s = SaveStateObject.GetDat();

        DiskOp.SaveCharData(pDat, SaveName);
        DiskOp.SaveFactionDat(facDat, SaveName);
        DiskOp.SaveGameVars(vb, SaveName);
        DiskOp.SaveVehicles(vDat, SaveName);
        DiskOp.SavePlayerDat(pI2, SaveName);
        DiskOp.SaveStaticChars(staticChars, SaveName);
        DiskOp.SaveZoneDat(z, SaveName);
        DiskOp.SaveObjDat(s, SaveName);

        if (!DontReset)
        {
            //Hibernate all maps but the one the player is in
            StartCoroutine(DelayedHibernateAll());
        }
    }
    /// <summary>
    /// Saves and quits game
    /// </summary>
    public void QuitGame()
    {
        try
        {
            CharacterData player = CharacterMove.CharacterTransform.GetComponent<CharacterData>();

            SaveGame(player.first_name + " " + player.last_name + " -AutoSave", true);
            _inGame = false;
            _Running = false;

            SaveStateObject.ClearObjects();
            ZoneSettings.ResetAllZones();
            foreach (CharacterData c in CharacterData.AllCharacters)
            {
                if (c.IsPlayer)
                    Destroy(c.gameObject);
                if (!c.IsPlayer && !c.GetComponent<AIMove>().IsStatic)
                    Destroy(c.gameObject);
            }
            foreach (VehicleScript v in VehicleScript.All_Vehicles.Values)
            {
                Destroy(v.gameObject);
            }
        }
        catch (Exception e)
        {
            if (Application.isEditor)
                Debug.Log(e);
        }

        SceneManager.LoadScene("CityScene");
    }
    public void QuitGameNoSave()
    {
        try
        {
            _inGame = false;
            _Running = false;

            SaveStateObject.ClearObjects();
            ZoneSettings.ResetAllZones();
            if (CharacterData.AllCharacters != null)
                foreach (CharacterData c in CharacterData.AllCharacters)
                {
                    if (c.IsPlayer)
                        Destroy(c.gameObject);
                    if (!c.IsPlayer && !c.GetComponent<AIMove>().IsStatic)
                        Destroy(c.gameObject);
                }
            if (VehicleScript.All_Vehicles != null)
                foreach (VehicleScript v in VehicleScript.All_Vehicles.Values)
                {
                    Destroy(v.gameObject);
                }
        }
        catch (Exception e)
        {
            if (Application.isEditor)
                Debug.Log(e);
        }

        SceneManager.LoadScene("CityScene");
    }
    /// <summary>
    /// Application.Quit()
    /// </summary>
    public void QuitApplication()
    {
        Application.Quit();
    }

    //All of the Get functions directly below turn a bunch of live GameObjetcs into neatly organized lists of serializable objects
    //that can be save to the disk

    GameVariables GetLiveGameVar()
    {
        GameVariables new_gv = new GameVariables();
        new_gv.Difficulty = Constants.Difficulty;
        return new_gv;
    }
    List<Faction> GetLiveFactionsDat()
    {
        return FactionSimulation.All_Factions;
    }
    List<VehicleInfo> GetLiveVehicles()
    {
        MapData NonHibernatingMap = MapData.ActiveMap;
        List<VehicleInfo> vList = new List<VehicleInfo>();
        foreach (VehicleScript v in VehicleScript.All_Vehicles.Values)
        {
            VehicleInfo vI = new VehicleInfo();
            vI.Health = v.Health;
            vI.PathNumber = v.PathID;
            vI.Position = v.transform.position;
            vI.RotationEuler = v.transform.eulerAngles;
            vI.Spawner = v.SpawnedFrom.zone_name;
            vI.stationaryVehicle = v.IsStationary;
            vI.VehicleID = v.VehicleID;
            vI.vehicleSkin = v.VehicleSkin;
            vI.VehicleSpeed = v.speed;
            vI.vehicleNeedsReset = v.vehicle_needs_reset;
            vList.Add(vI);
        }

        foreach (MapData m in MapData.maps.Values)
        {
            if (m != NonHibernatingMap)
            {
                if (m.MyPlayers != null)
                    vList.AddRange(m.MyVehicles);
            }
        }

        return vList;
    }
    List<VehicleInfo> GetLiveVehicles(MapData InThisMap)
    {
        List<VehicleInfo> vList = new List<VehicleInfo>();
        foreach (VehicleScript v in VehicleScript.All_Vehicles.Values)
        {
            if (v.MyZone == null || v.MyZone.ZoneID != InThisMap.ZoneID)
                continue;

            VehicleInfo vI = new VehicleInfo();
            vI.Health = v.Health;
            vI.PathNumber = v.PathID;
            vI.Position = v.transform.position;
            vI.RotationEuler = v.transform.eulerAngles;
            vI.Spawner = v.SpawnedFrom.zone_name;
            vI.stationaryVehicle = v.IsStationary;
            vI.VehicleID = v.VehicleID;
            vI.vehicleSkin = v.VehicleSkin;
            vI.VehicleSpeed = v.speed;
            vI.vehicleNeedsReset = v.vehicle_needs_reset;
            vList.Add(vI);
        }
        return vList;
    }
    List<PlayerInfo> GetLiveCharacters()
    {
        MapData NonHibernatingMap = MapData.ActiveMap;

        List<PlayerInfo> pList = new List<PlayerInfo>();
        foreach (CharacterData c in CharacterData.AllCharacters)
        {
            //static don't get saved here
            if (!c.IsPlayer && c.GetComponent<AIMove>().IsStatic)
                continue;

            PlayerInfo pI = new PlayerInfo();
            pI.faction = c.faction;
            pI.firstName = c.first_name;
            pI.Health = c.Health;
            pI.HealthPacks = c.NumHealthPacks;
            pI.isPlayer = c.IsPlayer;
            pI.lastName = c.last_name;
            pI.Money = c.Money;
            pI.isImmortal = c.IsImmortal;
            pI.ForcedGunsOut = c.ForcedGunsOut;
            pI.personality = c.personality;
            pI.Position = c.transform.position;
            pI.RotationEuler = c.RotationEuler;
            pI.arms = c.Arms;
            pI.earing = c.Earings;
            pI.eyecolor = c.Eyecolor;
            pI.face = c.Facecolor;
            pI.glasses = c.Glasses;
            pI.hair = c.Hair;
            pI.hat = c.Hat;
            pI.lipstick = c.Lipstick;
            pI.LWeapon = c.L_weapon;
            pI.RWeapon = c.R_weapon;
            pI.nose = c.Nose;
            pI.outereye = c.Outer_eye;
            pI.pants = c.Pants;
            pI.shirt = c.Shirt;
            pI.shoes = c.Shoes;

            if (c.InVehicle)
            {
                pI.VehicleID = c.InVehicle.VehicleID;
                pI.isPassenger = ReferenceEquals(c.InVehicle.passenger, c);
            }

            if (!c.IsPlayer)
            {
                AIMove aim = c.GetComponent<AIMove>();
                pI.PathNumber = aim.PathID;
                pI.SpawnedInVehicle = aim.SpawnedInVehicle;
                pI.Spawner = c.SpawnedFrom.zone_name;
                pI.ReverseDirection = aim.reverse_direction;
                pI.Destination = aim.Destination;
                pI.CanSpeak = c.GetComponent<NPCAI>().DisabledNormalConversation;
                pI.CurrentZone = c.GetComponent<AIMove>().MyZone.ZoneName;
            }
            pList.Add(pI);
        }

        foreach (MapData m in MapData.maps.Values)
        {
            if (m != NonHibernatingMap)
            {
                if (m.MyPlayers != null)
                    pList.AddRange(m.MyPlayers);
            }
        }
        return pList;
    }
    List<PlayerInfo> GetLiveCharacters(MapData InThisMap)
    {
        List<PlayerInfo> pList = new List<PlayerInfo>();
        foreach (CharacterData c in CharacterData.AllCharacters)
        {
            //Only collecting non static, non character npcs in the input map
            if (c.IsPlayer)
                continue;
            AIMove aim = c.GetComponent<AIMove>();
            if (aim == null || aim.IsStatic || aim.MyZone == null ||
                aim.MyZone.ZoneID != InThisMap.ZoneID)
                continue;
            PlayerInfo pI = new PlayerInfo();
            pI.faction = c.faction;
            pI.firstName = c.first_name;
            pI.Health = c.Health;
            pI.HealthPacks = c.NumHealthPacks;
            pI.isPlayer = c.IsPlayer;
            pI.lastName = c.last_name;
            pI.Money = c.Money;
            pI.personality = c.personality;
            pI.Position = c.transform.position;
            pI.RotationEuler = c.RotationEuler;
            pI.arms = c.Arms;
            pI.earing = c.Earings;
            pI.eyecolor = c.Eyecolor;
            pI.face = c.Facecolor;
            pI.glasses = c.Glasses;
            pI.hair = c.Hair;
            pI.hat = c.Hat;
            pI.lipstick = c.Lipstick;
            pI.LWeapon = c.L_weapon;
            pI.RWeapon = c.R_weapon;
            pI.nose = c.Nose;
            pI.outereye = c.Outer_eye;
            pI.pants = c.Pants;
            pI.shirt = c.Shirt;
            pI.shoes = c.Shoes;
            pI.isImmortal = c.IsImmortal;
            pI.ForcedGunsOut = c.ForcedGunsOut;

            if (c.InVehicle)
            {
                pI.VehicleID = c.InVehicle.VehicleID;
                pI.isPassenger = ReferenceEquals(c.InVehicle.passenger, c);
            }

            if (!c.IsPlayer)
            {
                pI.PathNumber = aim.PathID;
                pI.SpawnedInVehicle = aim.SpawnedInVehicle;
                pI.Spawner = c.SpawnedFrom.zone_name;
                pI.ReverseDirection = aim.reverse_direction;
                pI.Destination = aim.Destination;
                pI.CanSpeak = c.GetComponent<NPCAI>().DisabledNormalConversation;
                pI.CurrentZone = c.GetComponent<AIMove>().MyZone.ZoneName;
            }
            pList.Add(pI);
        }
        return pList;
    }
    PlayerInfo2 GetPlayerSpecificInformation()
    {
        if (CharacterMove.CharacterTransform == null)
        {
            return GameObject.Find("Character(Clone)").GetComponent<PlayerDat>().playerInfo2;
        }
        return CharacterMove.CharacterTransform.GetComponent<PlayerDat>().playerInfo2;
    }

    /// <summary>
    /// Put all of the active characters/objects on a map into a list of serialized (non-active) objects.
    /// By non-active, I mean that these objects are not calling Unity functions (like Update). It is just a list of
    /// data-holding objects
    /// </summary>
    /// <param name="ThisMap"></param>
    public void HibernateMap(MapData ThisMap)
    {
        ZoneSettings.ResetMapZones(ThisMap);
        ThisMap.MyPlayers = GetLiveCharacters(ThisMap);
        ThisMap.MyVehicles = GetLiveVehicles(ThisMap);
        foreach (CharacterData c in CharacterData.AllCharacters)
        {
            if (c.IsPlayer)
                continue;
            AIMove aim = c.GetComponent<AIMove>();
            if (aim == null || aim.IsStatic || aim.MyZone == null ||
                aim.MyZone.ZoneID != ThisMap.ZoneID)
                continue;

            Destroy(c.gameObject);
        }
        foreach (CharacterData c in CharacterData.StaticCharacters.Values)
        {
            if (c.gameObject.activeSelf)
            {
                AIMove aim = c.GetComponent<AIMove>();
                //add active statics in this zone
                if (aim.MyZone.ZoneID == ThisMap.ZoneID)
                {
                    if (ThisMap.AwakeStaticPlayers == null)
                        ThisMap.AwakeStaticPlayers = new List<GameObject>();
                    ThisMap.AwakeStaticPlayers.Add(aim.gameObject);
                    aim.gameObject.SetActive(false);
                }
            }
        }
        foreach (VehicleScript v in VehicleScript.All_Vehicles.Values)
        {
            if (v.MyZone.ZoneID == ThisMap.ZoneID)
                Destroy(v.gameObject);
        }
        ThisMap.isAsleep = true;
    }
    /// <summary>
    /// Hibernate the map after waiting a few frames
    /// </summary>
    /// <returns></returns>
    IEnumerator DelayedHibernateAll()
    {
        yield return 0; yield return 0; yield return 0; yield return 0; yield return 0;
        //Hibernate all maps but the one the player is in
        MapData active = MapData.ActiveMap;
        foreach (MapData m in MapData.maps.Values)
        {
            if (m.ZoneID != active.ZoneID)
                HibernateMap(m);
        }
    }
    /// <summary>
    /// Wake up a map that has been hibernated
    /// </summary>
    /// <param name="ThisMap"></param>
    public void WakeUpMap(MapData ThisMap)
    {
        InitVehicles(ThisMap.MyVehicles);
        InitCharacters(ThisMap.MyPlayers);
        ThisMap.MyPlayers = null;
        ThisMap.MyVehicles = null;
        ThisMap.isAsleep = false;

        //reset static characters
        if (ThisMap.AwakeStaticPlayers != null)
        {
            foreach (GameObject g in ThisMap.AwakeStaticPlayers)
            {
                AIMove aim = g.GetComponent<AIMove>();
                if (aim != null && aim.IsStatic && aim.MyZone.ZoneID == ThisMap.ZoneID)
                {
                    if (!aim.gameObject.activeSelf)
                    {
                        aim.gameObject.SetActive(true);
                    }
                }
            }
            ThisMap.AwakeStaticPlayers = null;
        }
    }
    /// <summary>
    /// Wake up all static GameObjects that have been hibernated
    /// </summary>
    static void WakeStatics()
    {
        foreach (MapData m in MapData.maps.Values)
        {
            if (m.AwakeStaticPlayers != null)
            {
                foreach (GameObject g in m.AwakeStaticPlayers)
                {
                    AIMove aim = g.GetComponent<AIMove>();
                    if (aim != null && aim.IsStatic && aim.MyZone.ZoneID == m.ZoneID)
                    {
                        if (!aim.gameObject.activeSelf)
                        {
                            aim.gameObject.SetActive(true);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Get a list of all zones in the game. This list contains serializable objects and is used for saving.
    /// </summary>
    /// <returns></returns>
    List<ZoneDat> GetZones()
    {
        List<ZoneDat> zones = new List<ZoneDat>();
        foreach (ZoneSettings z in ZoneSettings.All_ZoneSettings.Values)
        {
            if (z.SpawnCap != 0 || !z.IsSpawning)
            {
                zones.Add(z.GetDat());
            }
        }
        return zones;
    }
    void InitZones(List<ZoneDat> zones)
    {
        foreach (ZoneDat z in zones)
        {
            ZoneSettings zs = ZoneSettings.GetZone(z.ZoneName);
            zs.SetDat(z);
        }
    }

    /// <summary>
    /// Initialize settings for each game save. I never implemented settings. Oh well.
    /// </summary>
    /// <param name="v"></param>
    void InitGameVar(GameVariables v)
    {
        if (v == null)
        {
            Constants.Difficulty = 3;
            return;
        }

        if (v.Difficulty < 1) v.Difficulty = 3;
        if (v.Difficulty > 10) v.Difficulty = 3;

        Constants.Difficulty = v.Difficulty;
    }
    /// <summary>
    /// Initialize faction data. (Faction data is used to make groups of NPCs attack each other)
    /// </summary>
    /// <param name="fdat"></param>
    void InitFDat(List<Faction> fdat)
    {
        FactionSimulation.SetFactions(fdat);
    }
    /// <summary>
    /// Spawn Characters based off data svaed to the disk
    /// </summary>
    /// <param name="pInfo"></param>
    void InitCharacters(List<PlayerInfo> pInfo, PlayerInfo2 pInfo2, bool MalePlayer = false,
        string PlayerFirstName = "Jean", string PlayerLastName = "")
    {
        if (pInfo == null || pInfo.Count == 0 || pInfo2 == null)
        {
            //Dont have to initialize any NPC's, just the character
            GameObject player = Prefabs.GetNewPrefab("Prefabs/Character");
            CharacterData c = player.GetComponent<CharacterData>();
            PlayerInfo p;
            PeopleData.InitializeCharacter(c, p = PlayerInfo.Default(MalePlayer, PlayerFirstName, PlayerLastName));
            player.GetComponent<PlayerDat>().playerInfo = p;
            player.GetComponent<PlayerDat>().playerInfo2 = PlayerInfo2.Default;
            player.transform.position = p.Position;
            player.GetComponent<CharacterData>().RotationEuler = p.RotationEuler;
        }
        else
        {
            foreach (PlayerInfo p in pInfo)
            {
                //init player
                if (p.isPlayer)
                {
                    GameObject player= Prefabs.GetNewPrefab("Prefabs/Character");
                    CharacterData c = player.GetComponent<CharacterData>();
                    PeopleData.InitializeCharacter(c, p);
                    player.GetComponent<PlayerDat>().playerInfo = p;
                    player.GetComponent<PlayerDat>().playerInfo2 = pInfo2;
                    player.transform.position = p.Position;
                    player.GetComponent<CharacterData>().RotationEuler = p.RotationEuler;
                    if (p.VehicleID != 0)
                    {
                        VehicleScript MyVehicle = VehicleScript.All_Vehicles[p.VehicleID];
                        MyVehicle.EnterVehicle(c, !p.isPassenger, p.isPassenger);
                    }
                }
                //otherwise make npc
                else
                {
                    if (String.IsNullOrEmpty(p.Spawner))
                    {
                        Debug.Log("Error!");
                    }
                    else
                    {
                        ZoneSettings MySettings = ZoneSettings.GetZone(p.Spawner);
                        MySettings.MakeNPC(p);
                    }
                }
                
            }
        }
    }

    /// <summary>
    /// SPawn vehicles based off data saved to the disk
    /// </summary>
    /// <param name="vInfo"></param>
    void InitVehicles(List<VehicleInfo> vInfo)
    {
        if (vInfo == null || vInfo.Count == 0)
        {
            //nothing to do if nothing to load. Game will just auto spawn everything
        }
        else
        {
            foreach (VehicleInfo v in vInfo)
            {
                if (String.IsNullOrEmpty(v.Spawner))
                {
                    Debug.Log("Error!");
                }
                else
                {
                    ZoneSettings MySettings = ZoneSettings.GetZone(v.Spawner);
                    MySettings.MakeVehicle(v);
                }
            }
        }
    }

    /// <summary>
    /// Only Initializes Specific NPC's, used for Waking up a hibernating Map
    /// </summary>
    /// <param name="pInfo"></param>
    void InitCharacters(List<PlayerInfo> pInfo)
    {
        foreach (PlayerInfo p in pInfo)
        {
            if (String.IsNullOrEmpty(p.Spawner))
            {
                Debug.Log("Error!");
            }
            else
            {
                ZoneSettings MySettings = ZoneSettings.GetZone(p.Spawner);
                MySettings.MakeNPC(p);
            }
        }
    }

    /// <summary>
    /// Initialize static characters based on the input data
    /// </summary>
    /// <param name="static_chars"></param>
    public static void InitStaticCharacters(List<PlayerInfo> static_chars)
    {
        foreach (PlayerInfo p in static_chars)
        {
            CharacterData c = CharacterData.StaticCharacters[p.StaticKey];
            PeopleData.InitializeCharacter(c, p);

            c.transform.position = p.Position;
            c.RotationEuler = p.RotationEuler;

            c.gameObject.SetActive(p.isActive);

            ZoneSettings z = null;
            if (!String.IsNullOrEmpty(p.Spawner))
                z = ZoneSettings.GetZone(p.Spawner);

            c.GetComponent<NPCAI>().DisabledNormalConversation = p.CanSpeak;
            c.GetComponent<AIMove>().MyZone = MapData.maps[p.CurrentZone];

            if (z != null)
            {
                AIMove aim = c.GetComponent<AIMove>();
                int num = p.PathNumber;
                aim.PathID = num;

                aim.points = z.avaiable_paths[num].path;
                aim.spawner_position = z.transform.position;

                //If the NPC belongs to a vehicle, then reversing is not allowed
                if (z.avaiable_paths[num].allow_reverse && !p.SpawnedInVehicle)
                {
                    aim.reverse_direction = p.ReverseDirection;
                }
                aim.Init(p, z.RandomlySpawn, dontmove: true, newDestination: p.Destination);
            }

            //enter vehicle
            if (p.VehicleID != 0)
            {
                VehicleScript v = VehicleScript.All_Vehicles[p.VehicleID];
                v.EnterVehicle(c, !p.isPassenger, p.isPassenger);
            }
        }
    }
    /// <summary>
    /// Get a list of information about all static characters
    /// </summary>
    /// <returns></returns>
    public static List<PlayerInfo> GetStaticCharData()
    {
        WakeStatics();
        List<PlayerInfo> pList = new List<PlayerInfo>();
        foreach (CharacterData c in CharacterData.StaticCharacters.Values)
        {
            //if this character is static...
            if (!c.IsPlayer && c.GetComponent<AIMove>().IsStatic)
            {
                PlayerInfo pI = new PlayerInfo();
                pI.faction = c.faction;
                pI.firstName = c.first_name;
                pI.Health = c.Health;
                pI.HealthPacks = c.NumHealthPacks;
                pI.isPlayer = c.IsPlayer;
                pI.lastName = c.last_name;
                pI.Money = c.Money;
                pI.isImmortal = c.IsImmortal;
                pI.ForcedGunsOut = c.ForcedGunsOut;
                pI.personality = c.personality;
                pI.Position = c.transform.position;
                pI.RotationEuler = c.RotationEuler;
                pI.arms = c.Arms;
                pI.earing = c.Earings;
                pI.eyecolor = c.Eyecolor;
                pI.face = c.Facecolor;
                pI.glasses = c.Glasses;
                pI.hair = c.Hair;
                pI.hat = c.Hat;
                pI.lipstick = c.Lipstick;
                pI.LWeapon = c.L_weapon;
                pI.RWeapon = c.R_weapon;
                pI.nose = c.Nose;
                pI.outereye = c.Outer_eye;
                pI.pants = c.Pants;
                pI.shirt = c.Shirt;
                pI.shoes = c.Shoes;

                pI.StaticKey = c.gameObject.name;
                pI.isActive = c.gameObject.activeSelf;
                pI.CurrentZone = c.GetComponent<AIMove>().MyZone.ZoneName;

                if (c.InVehicle)
                {
                    pI.VehicleID = c.InVehicle.VehicleID;
                    pI.isPassenger = ReferenceEquals(c.InVehicle.passenger, c);
                }

                if (!c.IsPlayer)
                {
                    AIMove aim = c.GetComponent<AIMove>();
                    pI.PathNumber = aim.PathID;
                    pI.SpawnedInVehicle = aim.SpawnedInVehicle;
                    pI.ReverseDirection = aim.reverse_direction;
                    pI.Destination = aim.Destination;
                    pI.CanSpeak = c.GetComponent<NPCAI>().DisabledNormalConversation;

                    if (c.SpawnedFrom != null)
                        pI.Spawner = c.SpawnedFrom.zone_name;
                }

                pList.Add(pI);
            }
        }
        return pList;
    }
}

[System.Serializable]
public class GameVariables
{
    public int Difficulty = 3;
}
