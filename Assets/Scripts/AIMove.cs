﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class AIMove : MonoBehaviour {

    /// <summary>
    /// List of points on the path.
    /// </summary>
    [System.NonSerialized]
    public List<Transform> points;
    /// <summary>
    /// The path being used
    /// </summary>
    [System.NonSerialized]
    public int PathID;

    private int destPoint = 0;
    public int Destination
    {
        get
        {
            return destPoint;
        }
    }

    private NavMeshAgent _agent;
    public NavMeshAgent agent
    {
        get
        {
            if (_agent == null)
                _agent = GetComponent<NavMeshAgent>();
            return _agent;
        }
        set
        {
            _agent = value;
        }
    }

    private CharacterData _cdat;
    /// <summary>
    /// Reference to character data
    /// </summary>
    private CharacterData char_dat
    {
        get
        {
            if (_cdat == null)
                _cdat = GetComponent<CharacterData>();
            return _cdat;
        }
        set
        {
            _cdat = value;
        }
    }

    /// <summary>
    /// Continuousy loop on the supplied path?
    /// </summary>
    [Tooltip("true=Continuously loop over the same path")]
    public bool repeat_path = true;

    /// <summary>
    /// Non-zero=distance the agent will venture from its path to randomly selected traversal points within this radius
    /// </summary>
    [Tooltip("Non-zero=distance the agent will venture from its path to randomly selected traversal points within this radius")]
    public float zone_patrol_radius = 0.0f;

    /// <summary>
    /// A target besides one on the path.
    /// </summary>
    private Transform target;
    /// <summary>
    /// Allowed to go this far from the preset path.
    /// </summary>
    private float dist_allowed_from_path;

    [System.NonSerialized]
    /// <summary>
    /// Is the controller current walking? Applys for cars as well.
    /// </summary>
    public bool isWalking = true;

    /// <summary>
    /// Traverse path in opposite direction?
    /// </summary>
    [System.NonSerialized]
    public bool reverse_direction = false;

    /// <summary>
    /// External slow
    /// </summary>
    [System.NonSerialized]
    public float slow_speed = 1.0f;

    /// <summary>
    /// Location of Zone Settings that spawned this NPC
    /// </summary>
    [System.NonSerialized]
    public Vector3 spawner_position;

    /// <summary>
    /// If an NPC spawned in a vehicle, and then they exited that vehicle, then the NPC needs
    /// to be destroyed
    /// </summary>
    [System.NonSerialized]
    public bool SpawnedInVehicle;

    /// <summary>
    /// Time of last checkpoint reached
    /// </summary>
    float TimeOfLastCheckPoint = 0.0f;
    float accumulated_time = 0.0f;

    /// <summary>
    /// Time spent waiting at the current node.
    /// </summary>
    float TimeSpentWaiting = 0.0f;
    bool isWaiting = false, finishedWaiting = false;
    AIPathAction currentAction;

    /// <summary>
    /// The NPC always exists and is not spawned by a spawner
    /// </summary>
    [Tooltip("Set this flag to true if the NPC is not spawned from a ZoneSettings spawner.")]
    public bool IsStatic = false;
    [Tooltip("Set this flag to true if an component besides AIMove is moving this NPC. Eg BossAI")]
    public bool IsOutsideControlled = false;
    /// <summary>
    /// Is this static character allowed to move? This value only applys if the character is static
    /// </summary>
    private bool StaticCanMove = false;
    private CapsuleCollider StaticCollider; NavMeshObstacle statObstacle;

    /// <summary>
    /// Initialize Agent
    /// </summary>
    public void Init(bool SpawnRandom)
    {
        agent = GetComponent<NavMeshAgent>();
        char_dat = GetComponent<CharacterData>();
        agent.stoppingDistance = 0.35f;

        //Initialize
        if (points != null && points.Count > 0)
        {
            int p = 0;
            agent.Warp(points[p = SpawnRandom ? Random.Range(0, points.Count) :
                GetClosestPoint(spawner_position)].position);
            if (!reverse_direction)
                destPoint = p + 1 >= points.Count ? 0 : p + 1;
            else
                destPoint = p - 1 < 0 ? points.Count - 1 : p - 1;
            agent.destination = points[destPoint].position;
        }

        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        agent.autoBraking = false;
    }

    /// <summary>
    /// Initialize Agent
    /// </summary>
    /// <param name="pI"></param>
    public void Init(PlayerInfo pI, bool SpawnRandom, bool dontmove=false, int newDestination = -1)
    {
        agent = GetComponent<NavMeshAgent>();
        char_dat = GetComponent<CharacterData>();
        agent.stoppingDistance = 0.35f;

        SpawnedInVehicle = pI.SpawnedInVehicle;
        
        //Initialize
        if (points != null && points.Count > 0)
        {
            int p = SpawnRandom ? Random.Range(0, points.Count) : GetClosestPoint(pI.Position);
            agent.Warp(pI.Position);

            if (newDestination == -1)
            {
                if (!reverse_direction)
                    destPoint = p + 1 >= points.Count ? 0 : p + 1;
                else
                    destPoint = p - 1 < 0 ? points.Count - 1 : p - 1;
            }
            else
                destPoint = newDestination;

            //if the input dontmove is false
            if (!dontmove)
            {
                //this can sometimes happen for NPCs that got saved while walking to their destroy point
                //(They don't reset, so their destPoint value is greater than what is allowed)
                if (SpawnedInVehicle && destPoint >= points.Count)
                {
                    if (!reverse_direction)
                        destPoint = p + 1 >= points.Count ? 0 : p + 1;
                    else
                        destPoint = p - 1 < 0 ? points.Count - 1 : p - 1;
                }
                else
                    agent.destination = points[destPoint].position;
            }
        }

        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        agent.autoBraking = false;
    }

    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Count == 0)
            return;

        //stop if not repeating a path
        if (!repeat_path)
        {
            if (!reverse_direction && destPoint + 1 >= points.Count)
            {
                agent.autoBraking = true;
                destPoint = points.Count - 2;
            }
            else if (reverse_direction && destPoint - 1 < 0)
            {
                agent.autoBraking = true;
                destPoint = 1;
            }
        }

        //Record time
        TimeOfLastCheckPoint = Time.realtimeSinceStartup;

        bool assigned_next = false;
        //if we aren't ignoring path modifiers
        if (IgnorePathModifiersTime < 0.01f)
        {
            //Determine if this node will move the NPC to a random point
            AIPathAction aip;
            aip = points[destPoint].GetComponent<AIPathAction>();
            //otherwise
            if (aip != null)
            {
                //if enforece rotation is enabled..
                if (aip.EnforceThisTransformRotation)
                {
                    transform.eulerAngles = aip.transform.eulerAngles;
                }

                //if we need to wait on arriving ot this node...
                if (aip != null && aip.WaitOnArrive > 0.05f)
                {
                    if (finishedWaiting)
                    {
                        TimeSpentWaiting = 0.0f;
                        finishedWaiting = false;
                        isWaiting = false;
                        currentAction = null;
                    }
                    else
                    {
                        currentAction = aip;
                        isWaiting = true;
                        return;
                    }
                }

                //if move to random node is enabled
                if (aip.MoveToRandNodeOnLeave)
                {
                    destPoint = Random.Range(0, points.Count);
                }
                else
                {
                    // Choose the next point in the array as the destination,
                    // cycling to the start if necessary.
                    if (!reverse_direction)
                        destPoint = destPoint + 1 >= points.Count ? 0 : destPoint + 1;
                    else
                        destPoint = destPoint - 1 < 0 ? points.Count - 1 : destPoint - 1;
                }

                assigned_next = true;
            }
        }

        //if we havent yet assign a new point
        if (!assigned_next)
        {
            // Choose the next point in the array as the destination,
            // cycling to the start if necessary.
            if (!reverse_direction)
                destPoint = destPoint + 1 >= points.Count ? 0 : destPoint + 1;
            else
                destPoint = destPoint - 1 < 0 ? points.Count - 1 : destPoint - 1;
        }

        //set destination
        agent.destination = points[destPoint].position;
    }
    void GotoNextPointVehicle()
    {
        NavMeshAgent agent = char_dat.InVehicle.vehicleAgent;
        List<Transform> points = char_dat.InVehicle.vehicleRoute;

        //vehicles dont do zones/inverted paths/path loops
        //upon reaching the end of their path (out of bounds), they get warped to the start

        // Returns if no points have been set up
        if (points.Count == 0)
            return;

        if (destPoint + 1 >= points.Count)
        {
            //Get a random path, only cars do this automatically
            char_dat.InVehicle.vehicleRoute = char_dat.InVehicle.SpawnedFrom.avaiable_paths[Random.Range(0,
                 char_dat.InVehicle.SpawnedFrom.avaiable_paths.Count)].path;

            char_dat.InVehicle.vehicleAgent.Warp(char_dat.InVehicle.vehicleRoute[0].position);
            EnteredVehicle();
        }

        destPoint = destPoint + 1 >= points.Count ? 0 : destPoint + 1;

        TimeOfLastCheckPoint = Time.realtimeSinceStartup;

        agent.destination = points[destPoint].position;
    }

    [Tooltip("The zone in which this NPC is currently in")]
    public MapData MyZone;

    /// <summary>
    /// If non-zero, then Path modifiers will be ignored until this value reaches zero (in seconds)
    /// </summary>
    float IgnorePathModifiersTime = 0.0f;

    bool isPaused = false;

    void Update()
    {
        //if AImove is not controlling this object. (Rare case)
        if (IsOutsideControlled)
            return;

        //If this is far away, then do nothing
        if (CharacterMove.CharacterTransform == null || char_dat == null || (this.agent != null && char_dat.InVehicle == null &&
            (transform.position - CharacterMove.CharacterTransform.position).sqrMagnitude > 256))
        {
            if (!isPaused && _agent != null)
                agent.Stop();
            isPaused = true;
        }
        else
        {
            if (isPaused && _agent != null)
                agent.Resume();
            isPaused = false;
        }

        //Assign path modifer timer
        IgnorePathModifiersTime -= Time.deltaTime;
        IgnorePathModifiersTime = IgnorePathModifiersTime <= 0 ? 0 : IgnorePathModifiersTime;

        //fix rotation
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        //Fix static Position
        if (IsStatic)
        {
            StaticNPCAction();
            return;
        }

        //Wait 2 seconds to move
        accumulated_time += Time.deltaTime;
        if (accumulated_time < 2.0f)
            return;

        if (char_dat.InVehicle == null)
        {
            //set speed
            if (isWalking && agent.speed != 0.5f)
                agent.speed = Constants.AIWalkSpeed;
            else if (!isWalking && agent.speed != Constants.AIRunSpeed)
                agent.speed = Constants.AIRunSpeed;

            if (SpawnedInVehicle)
            {
                if (agent.remainingDistance < 0.5f)
                {
                    //we don't notify the spawner of the Death because vehicle spawners don't count the drivers
                    Destroy(gameObject);
                }
                return;
            }

            // Returns if no points have been set up
            if (points.Count == 0)
                return;

            //if we aren't following/moving towards a target
            if (target == null)
            {
                //if we are currently waiting...
                if (isWaiting)
                {
                    TimeSpentWaiting += Time.deltaTime;
                    if (TimeSpentWaiting > currentAction.WaitOnArrive)
                    {
                        finishedWaiting = true;
                        GotoNextPoint();
                    }
                }
                else
                {
                    // Choose the next destination point when the agent gets
                    // close to the current one.
                    if (agent.remainingDistance < 0.35f)
                        GotoNextPoint();
                    //taking too long to reach goal, must be stuck
                    else if (Time.realtimeSinceStartup - TimeOfLastCheckPoint > 60)
                    {
                        agent.ResetPath();
                        agent.Resume();
                        GotoNextPoint();
                    }
                }
            }
            //we are following a target
            else
            {
                //If too far from path...
                if ((transform.position - points[destPoint].position).sqrMagnitude > dist_allowed_from_path * dist_allowed_from_path)
                {
                    GoBackToPath();
                    GotoNextPoint();
                }
                else
                {
                    if (agent.remainingDistance < agent.stoppingDistance)
                        agent.Stop();
                    else if (agent.remainingDistance > agent.stoppingDistance)
                        agent.Resume();

                    agent.destination = target.position;
                }
            }
        }
        //vehicle code
        else
        {
            NavMeshAgent agent = char_dat.InVehicle.vehicleAgent;

            if (agent == null)
                return;

            List<Transform> points = char_dat.InVehicle.vehicleRoute;

            //set speed
            if (isWalking && agent.speed != 1.0f)
                agent.speed = Constants.VehicleWalkSpeed;
            else if (!isWalking && agent.speed != char_dat.InVehicle.speed)
                agent.speed = char_dat.InVehicle.speed;

            //If this is the passenger, then no movement is needed
            if (ReferenceEquals(char_dat.InVehicle.passenger, char_dat))
            {
                return;
            }

            // Returns if no points have been set up
            if (points.Count == 0)
                return;

            if (target == null)
            {
                // Choose the next destination point when the agent gets
                // close to the current one.
                if (agent.remainingDistance < 0.4f)
                    GotoNextPointVehicle();
            }
            else
            {
                //If too far from path...
                if ((transform.position - points[destPoint].position).sqrMagnitude > dist_allowed_from_path * dist_allowed_from_path)
                {
                    GoBackToPath();
                    GotoNextPointVehicle();
                }
                else
                {
                    if (agent.remainingDistance < agent.stoppingDistance)
                        agent.Stop();
                    else if (agent.remainingDistance > agent.stoppingDistance)
                        agent.Resume();

                    agent.destination = target.position;
                }
            }
        }
    }

    /// <summary>
    /// Static character movement
    /// </summary>
    public void StaticNPCAction()
    {
        if (PlayerIsNotInZone())
            return;


        //make it so that the static character either can or cannot move
        if (StaticCanMove)
        {
            if (StaticCollider != null)
            {
                Destroy(StaticCollider);
                StaticCollider = null;
            }

            if (agent == null)
            {
                NavMeshAgent n = gameObject.AddComponent<NavMeshAgent>();
                n.height = 0.2f; n.autoBraking = false;
                n.avoidancePriority = 50; n.obstacleAvoidanceType = ObstacleAvoidanceType.GoodQualityObstacleAvoidance;
                n.speed = 0.5f; n.acceleration = 8; n.stoppingDistance = 0.35f; n.baseOffset = 0;
                n.radius = 0.23f;
                agent = n;
            }

            if (statObstacle != null)
            {
                Destroy(statObstacle);
                statObstacle = null;
            }

        }
        else
        {
            if (StaticCollider == null)
            {
                StaticCollider = gameObject.AddComponent<CapsuleCollider>();
                StaticCollider.center = new Vector3(0, 0.23f, 0);
                StaticCollider.radius = 0.18f;
                StaticCollider.height = 0.23f;
            }

            if (agent != null)
            {
                Destroy(agent);
                agent = null;
            }

            if (statObstacle == null)
                (statObstacle = gameObject.AddComponent<NavMeshObstacle>()).radius = 0.25f;
        }

        return;
    }
    /// <summary>
    /// This function Disables some components on static
    /// Characters when they are not in the same zone as the player
    /// </summary>
    /// <returns></returns>
    public bool PlayerIsNotInZone()
    {
        if (!IsStatic)
            return false;

        if (char_dat.InVehicle == null)
        {
            if (MapData.ActiveMap != null && MyZone != null && MyZone.ZoneID != MapData.ActiveMap.ZoneID)
            {
                if (agent != null && agent.enabled)
                {
                    agent.enabled = false;
                }
                if (agent != null && char_dat.enabled)
                {
                    char_dat.enabled = false;
                }
                return true;
            }
            else
            {
                if (agent != null && !agent.enabled)
                {
                    agent.enabled = true;
                }
                if (agent != null && !char_dat.enabled)
                {
                    char_dat.enabled = true;
                }
                return false;
            }
        }
        else
        {
            NavMeshAgent agent = char_dat.InVehicle.vehicleAgent;
            if (agent == null)
                return true;
            if (MapData.ActiveMap != null && MyZone != null && MyZone.ZoneID != MapData.ActiveMap.ZoneID)
            {
                if (agent != null && agent.enabled)
                    agent.enabled = false;
                return true;
            }
            else
            {
                if (agent != null && !agent.enabled)
                    agent.enabled = true;
                return false;
            }
        }
    }

    /// <summary>
    /// Stops the agent from moving
    /// </summary>
    public void Pause()
    {
        agent.Stop();
    }
    /// <summary>
    /// Resume movement after stoping
    /// </summary>
    public void Resume()
    {
        agent.Resume();
    }

    /// <summary>
    /// Go to a specific target and stay within a distance from them. Also go back to input path
    /// if the distance from the path is too great.
    /// </summary>
    /// <param name="t"></param>
    /// <param name="max_distance_allowed_from_path"></param>
    /// <param name="minimum_distance_from_target"></param>
    public void GotoTarget(Transform t, float max_distance_allowed_from_path, float minimum_distance_from_target)
    {
        if (IsStatic)
            return;

        if (char_dat.InVehicle == null)
        {
            target = t;
            dist_allowed_from_path = max_distance_allowed_from_path;
            agent.stoppingDistance = minimum_distance_from_target;
        }
    }
    /// <summary>
    /// Stop following a target and go back to path
    /// </summary>
    public void GoBackToPath()
    {
        if (target != null)
        {
            if (char_dat.InVehicle == null)
            {
                target = null;
                dist_allowed_from_path = 0;
                agent.stoppingDistance = 0.35f;
                destPoint = GetClosestPoint();
                agent.destination = points[destPoint].position;
                agent.Resume();
            }
        }
    }

    /// <summary>
    /// Returns a random point within zone_patrol_radius of the input point
    /// </summary>
    /// <param name="near_this"></param>
    /// <returns></returns>
    Vector3 GetRandPoint(Vector3 near_this)
    {
        float minx = near_this.x - zone_patrol_radius * 0.8f;
        float maxx = near_this.x + zone_patrol_radius * 0.8f;
        float minz = near_this.z - zone_patrol_radius * 0.8f;
        float maxz = near_this.z + zone_patrol_radius * 0.8f;

        minx = minx < maxx ? minx : maxx;
        maxx = maxx >= minx ? maxx : minx;

        minz = minz < maxz ? minz : maxz;
        maxz = maxz >= minz ? maxz : minz;

        return new Vector3(Random.Range(minx, maxx), near_this.y, Random.Range(minz, maxz));
    }
    /// <summary>
    /// Returns indx of point closest to the NPC, returns -1 if there is no path
    /// </summary>
    /// <returns></returns>
    int GetClosestPoint()
    {
        int index = -1;
        float dist = float.MaxValue;
        for(int i = 0; i < points.Count; i++)
        {
            if ((points[i].position - transform.position).sqrMagnitude < dist)
            {
                dist = (points[i].position - transform.position).sqrMagnitude;
                index = i;
            }
        }
        return index;
    }
    /// <summary>
    /// Returns indx of point closest to the input position, returns -1 if there is no path
    /// </summary>
    /// <returns></returns>
    int GetClosestPoint(Vector3 closest_to_this)
    {
        int index = -1;
        float dist = float.MaxValue;
        for (int i = 0; i < points.Count; i++)
        {
            if ((points[i].position - closest_to_this).sqrMagnitude < dist)
            {
                dist = (points[i].position - closest_to_this).sqrMagnitude;
                index = i;
            }
        }
        return index;
    }
    /// <summary>
    /// Get closest point on the attatched vehicle path
    /// </summary>
    /// <param name="closest_to_this"></param>
    /// <returns></returns>
    int GetVehicleClosestPoint(Vector3 closest_to_this)
    {
        List<Transform> points = char_dat.InVehicle.vehicleRoute;

        int index = -1;
        float dist = float.MaxValue;
        for (int i = 0; i < points.Count; i++)
        {
            if ((points[i].position - closest_to_this).sqrMagnitude < dist)
            {
                dist = (points[i].position - closest_to_this).sqrMagnitude;
                index = i;
            }
        }
        return index;
    }

    /// <summary>
    /// Call this after entering a vehicle, updates various properties
    /// </summary>
    public void EnteredVehicle()
    {
        if (char_dat.InVehicle.vehicleRoute != null && char_dat.InVehicle.vehicleRoute.Count > 0)
        {
            destPoint = GetVehicleClosestPoint(char_dat.InVehicle.transform.position);
            char_dat.InVehicle.vehicleAgent.destination = char_dat.InVehicle.vehicleRoute[destPoint].position;
        }
    }
    /// <summary>
    /// Called after exiting a vehicle, updates various properties
    /// </summary>
    public void ExitedVehicle()
    {
        //if the NPC spawned in a vehicle, then it needs to be removed to avoid memory
        //issues in long running games
        if (SpawnedInVehicle)
        {
            //set destination to car start point, upon destination reached, the NPC will be destroyed
            agent.destination = char_dat.InVehicle.vehicleRoute[0].position;
        }
        //otherwise, the NPC goes back to its normal Route
        else
        {
            if (points != null && points.Count > 0)
            {
                destPoint = GetClosestPoint(transform.position);
                agent.destination = points[destPoint].position;
            }
        }
    }

    /// <summary>
    /// Move to the furthest point on the path from the input character
    /// </summary>
    /// <param name="fromMe"></param>
    public void RunAway(CharacterData fromMe)
    {
        //does not handle cars, as they don't stop anways
        if (agent.GetComponent<VehicleScript>() != null)
            return;

        int p = GetFurthestPoint(fromMe.transform.position);
        destPoint = p;
        agent.destination = points[destPoint].position;
        IgnorePathModifiersTime = 15.0f;
    }

    int GetFurthestPoint(Vector3 fromThisPosition)
    {
        int index = -1;
        float dist = float.MinValue;
        for (int i = 0; i < points.Count; i++)
        {
            if ((points[i].position - fromThisPosition).sqrMagnitude > dist)
            {
                dist = (points[i].position - fromThisPosition).sqrMagnitude;
                index = i;
            }
        }
        return index;
    }

    /// <summary>
    /// Move this object to the input world position
    /// </summary>
    /// <param name="position"></param>
    public void SetPosition(Vector3 position)
    {
        if (agent == null)
            transform.position = position;
        else
            agent.Warp(position);
    }
}
