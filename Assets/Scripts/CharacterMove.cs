﻿using UnityEngine;
using System;

public class CharacterMove : MonoBehaviour {

    [System.NonSerialized]
    public Rigidbody rbod;
    /// <summary>
    /// Returns an attatched car if there is one
    /// </summary>
    public VehicleScript Car
    {
        get
        {
            return GetComponent<CharacterData>().InVehicle;
        }
    }

    public float speed = 3.5f;

    /// <summary>
    /// Reference to animator
    /// </summary>
    Animator animator;

    //current animation being played
    string current_animation;
    /// <summary>
    /// Get/Set current animation being played
    /// </summary>
    public string CurrentAnimation
    {
        get
        {
            return current_animation;
        }
        set
        {
            if (current_animation == value)
                return;
            current_animation = value;
            animator.Play(value);
        }
    }

    /// <summary>
    /// Used for lava + water pits (external slowing)
    /// </summary>
    [NonSerialized]
    public float slow_speed = 1.0f;

    /// <summary>
    /// Camera for the player
    /// </summary>
    public Camera cam;

    /// <summary>
    /// The direction the player is facing, transform.rotation is constant
    /// </summary>
    [System.NonSerialized]
    public Vector3 LookDirection;

    /// <summary>
    /// There is only one player, and this is his transform
    /// </summary>
    public static Transform CharacterTransform;

    /// <summary>
    /// The player's characdata object. Cache the result of this get()
    /// </summary>
    public static CharacterData Player
    {
        get
        {
            return CharacterTransform == null ? null : CharacterTransform.GetComponent<CharacterData>();
        }
    }

    /// <summary>
    /// Returns true if the squared distance from the player to the input object is less than the
    /// input distance
    /// </summary>
    /// <param name="nearThis"></param>
    /// <param name="withDistSqr"></param>
    /// <returns></returns>
    public static bool NearPlayer(Transform nearThis, float withDistSqr)
    {
        if (CharacterMove.CharacterTransform == null)
            return false;
        return (CharacterTransform.position - nearThis.position).sqrMagnitude < withDistSqr;
    }

    void Start()
    {
        rbod = GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();
        CharacterTransform = transform;
        cam = CameraFollow.cam;
        GetComponent<CharacterActions>().cam = cam;
        cam.GetComponent<CameraFollow>().follow_this = gameObject;
        cam.transform.position = new Vector3(transform.position.x, 2.5f, transform.position.z);
    }

    void OnDestroy()
    {
        if (cam != null)
            cam.GetComponent<CameraFollow>().GoBack();
    }

    bool MoveFwd = false, MoveBck = false, MoveLft = false, MoveRht = false;
    void FixedUpdate()
    {
        //enfore character position
        if (transform.position.y < 0.22f)
            transform.position = new Vector3(transform.position.x, 0.23f, transform.position.z);
        //Enfore Character rotation
        transform.rotation = Quaternion.Euler(0, 0, 0);
        //frozen?
        bool Frozen = GetComponent<PlayerDat>().playerInfo2.isFrozen;

        if (Car == null)
        {
            //Raycats only against the level floor
            RaycastHit hit;
            if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 100,
                1 << LayerMask.NameToLayer("Ground")))
            {
                Vector3 look_direction = new Vector3(hit.point.x - transform.position.x, 0,
                    hit.point.z - transform.position.z).normalized;
                Vector3 direction = Vector3.zero;
                if (MoveFwd)
                {
                    direction += Vector3.forward;
                }
                if (MoveBck)
                {
                    direction += Vector3.back;
                }
                if (MoveLft)
                {
                    direction += Vector3.left;
                }
                if (MoveRht)
                {
                    direction += Vector3.right;
                }

                direction = direction.normalized;

                if (!Frozen)
                    rbod.velocity = speed * direction * slow_speed;
                else
                    rbod.velocity = Vector3.zero;

                SetAnimation(look_direction, rbod.velocity);

                LookDirection = look_direction;
            }
        }
        //otherwise, the player is in a vehicle
        else
        {
            VehicleScript car = Car;

            //if passenger, then just obtain enough information to set the animation
            if (car.passenger == GetComponent<CharacterData>())
            {
                RaycastHit hitp;
                //do raycast from mouse
                if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hitp, 100,
                    1 << LayerMask.NameToLayer("Ground")))
                {
                    //look direction is hitpoint - character point
                    Vector3 look_direction = new Vector3(hitp.point.x - transform.position.x, 0,
                        hitp.point.z - transform.position.z).normalized;

                    SetAnimation(look_direction, Vector3.zero);
                }
                return;
            }

            Rigidbody car_bod = car.GetComponent<Rigidbody>();
            RaycastHit hit;

            //enforce character position
            transform.position = car.driver_position.position;
            //enforce car rotation
            car_bod.transform.eulerAngles = new Vector3(0, car_bod.transform.eulerAngles.y, 0);

            if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 100,
                1 << LayerMask.NameToLayer("Ground")))
            {
                Vector3 look_direction = new Vector3(hit.point.x - transform.position.x, 0,
                    hit.point.z - transform.position.z).normalized;

                //If this is the driver
                if (ReferenceEquals(car.driver, GetComponent<CharacterData>()))
                {
                    float turn_amount = Input.GetAxis("Horizontal") * (car_bod.velocity.magnitude / car.speed)
                        * 200 * Time.deltaTime;

                    if (MoveFwd)
                    {
                        Vector3 dir = car.transform.rotation * Vector3.forward;
                        Vector3 velocity = car_bod.velocity + (dir * Time.deltaTime * 10);
                        if (velocity.magnitude > car.speed)
                            velocity = ConvertToMagnitude(velocity, car.speed);

                        if (!Frozen)
                            car_bod.velocity = velocity * slow_speed;
                    }
                    else if (MoveBck)
                    {
                        Vector3 dir = car.transform.rotation * Vector3.back;
                        Vector3 velocity = car_bod.velocity + (dir * Time.deltaTime * 10);

                        if (velocity.magnitude > car.speed)
                            velocity = ConvertToMagnitude(velocity, car.speed);

                        if (!Frozen)
                            car_bod.velocity = velocity * slow_speed;

                        turn_amount = -turn_amount;
                    }

                    if (car_bod.velocity.magnitude > 0)
                        car.transform.Rotate(0, turn_amount, 0);
                }

                SetAnimation(look_direction, car_bod.velocity);

                LookDirection = look_direction;
            }
        }
    }

    /// <summary>
    /// Returns true if the player is moving
    /// </summary>
    public bool IsMoving
    {
        get
        {
            return MoveFwd || MoveBck || MoveLft || MoveRht;
        }
    }

    void Update()
    {
        if (!GameSettings.Running)
            return;

        CharacterData c = GetComponent<CharacterData>();
        if (c.IsDead)
        {
            MoveFwd = false; MoveBck = false; MoveLft = false; MoveRht = false;
            return;
        }

        VehicleScript car = Car;
        //character movement
        if (car == null)
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                MoveFwd = true;
            }
            else
                MoveFwd = false;
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                MoveBck = true;
            }
            else
                MoveBck = false;
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                MoveLft = true;
            }
            else
                MoveLft = false;
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                MoveRht = true;
            }
            else
                MoveRht = false;
        }
        //car movement
        else
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                MoveFwd = true;
                MoveBck = false;
            }
            else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                MoveFwd = false;
                MoveBck = true;
            }
            else
            {
                MoveFwd = false;
                MoveBck = false;
            }
        }
    }

    /// <summary>
    /// Converts the input vector to have the input magnitude
    /// </summary>
    /// <param name="v"></param>
    /// <param name="target_magnitude"></param>
    /// <returns></returns>
    public static Vector3 ConvertToMagnitude(Vector3 v, float target_magnitude)
    {
        return v == Vector3.zero ? v :
            v * Mathf.Sqrt((target_magnitude * target_magnitude) / (v.x * v.x + v.y * v.y + v.z * v.z));
    }

    /// <summary>
    /// Set player's animation
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="velocity"></param>
    void SetAnimation(Vector3 direction, Vector3 velocity)
    {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
        {
            bool l = false, r = false;
            r = direction.x > 0.0001f;
            l = direction.x < -0.0001f;
            if (l) { CurrentAnimation = velocity.magnitude > 0.01f ? "WalkLeft" : "IdleLeft"; return; }
            if (r) { CurrentAnimation = velocity.magnitude > 0.01f ? "WalkRight" : "IdleRight"; return; }
        }
        else
        {
            bool f = false, b = false;
            f = direction.z > 0.0001f;
            b = direction.z < -0.0001f;
            if (f) { CurrentAnimation = velocity.magnitude > 0.01f ? "WalkForward" : "IdleForward"; return; }
            if (b) { CurrentAnimation = velocity.magnitude > 0.01f ? "Walk" : "Idle"; return; }
        }
    }
}
