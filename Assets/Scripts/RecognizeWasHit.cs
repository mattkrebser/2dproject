﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class should be attatched to character/vehicle objects who have the Projectile collider on them
/// </summary>
public class RecognizeWasHit : MonoBehaviour
{
    public CharacterData attatchedCharacter;
    public VehicleScript attatchedVehicle;
}
