using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Global Inout Handler, no local stuff
/// </summary>
public static class InputHandler
{
    private static Dictionary<KeySet, List<Action>> global_input = new Dictionary<KeySet, List<Action>>();

    private static float scrl_whl;
    private static Vector2 ms_chg;
    public static Vector2 MouseDelta
    {
        get
        {
            return ms_chg;
        }
    }

    /// <summary>
    /// Object under the mouse. Null if there is none.
    /// </summary>
    public static GameObject Active3DObject;

    /// <summary>
    /// Add a global input. This input delegate will be called whenever the specified Input set is activated
    /// </summary>
    /// <param name="input"></param>
    /// <param name="f"></param>
    public static void AddGlobalInput(KeySet input, Action f)
    {
        //include keyset into inputevent logic
        if (KeySet.IsMultiKey(input))
            InputEvents.TryAddKeyCombo(input);

        List<Action> l = null;
        if (global_input.TryGetValue(input, out l))
        {
            l.Add(f);
        }
        else
        {
            l = new List<Action>(); l.Add(f);
            global_input.Add(input,l);
        }
    }

    /// <summary>
    /// Remove the specified input
    /// </summary>
    /// <param name="input"></param>
    /// <param name="f"></param>
    public static void RemoveGlobalInput(KeySet input, Action f)
    {
        List<Action> l = null;
        if (global_input.TryGetValue(input, out l))
        {
            l.Remove(f);
        }

        //no need to look for keysets that arent being used
        if (l.Count == 0)
            InputEvents.remove_key_combo(input);
    }

    /// <summary>
    /// Runs all functions for the input keyset
    /// </summary>
    /// <param name="input"></param>
    public static void RunInputCheck(KeySet input)
    {
        List<Action> l;
        if (global_input.TryGetValue(input, out l))
        {
            foreach (Action d in l)
            {
                d();
            }
        }
    }

    public static void AssignScrollWheelChange(float t)
    {
        scrl_whl = t;
    }
    public static float GetScrollWheelChange()
    {
        return scrl_whl;
    }
    public static void AssignMouseChange(float x, float y)
    {
        ms_chg = new Vector2(x, y);
    }
}

/// <summary>
/// Types of events that get recognized as inputs. 
/// </summary>
public enum GameEventTypes
{
    none,
    /// <summary>
    /// pressed any type of input down and held down
    /// </summary>
	inputKeyDown,
    /// <summary>
    /// released any button
    /// </summary>
	inputKeyUp,
    /// <summary>
    /// on mousewheel scroll up
    /// </summary>
	scrollUp,
    /// <summary>
    /// on mousewheel scroll down
    /// </summary>
	scrollDown,
    /// <summary>
    /// Not supported.
    /// </summary>
	pointerEnter,
    /// <summary>
    /// Not supported.
    /// </summary>
	pointerExit,
    /// <summary>
    /// Not supported.
    /// </summary>
	hover,
    /// <summary>
    /// Begin dragging mouse
    /// </summary>
	beginDrag,
    /// <summary>
    /// Stopped dragging mouse
    /// </summary>
	endDrag,
    /// <summary>
    /// Not supported
    /// </summary>
    valueChange,
    /// <summary>
    /// Not supported
    /// </summary>
    endEdit,
    /// <summary>
    /// Not supported
    /// </summary>
    dropped,
    /// <summary>
    /// the instant a multi key combo is pressed down once. Use 'OnInputKeyDown' for keysets that do not contain more than one key
    /// </summary>
    multiKey,
    /// <summary>
    /// The mouse was moved.
    /// </summary>
    mouseMove,
    /// <summary>
    /// Called once, the first frame a key is pushed down. Use 'multikey' event if the keyset has multiple non-None keys.
    /// </summary>
    OnInputKeyDown
}

/// <summary>
/// Data type for input, can contain 3 keys and a type
/// </summary>
public struct KeySet
{
    public KeyCode key1;
    public KeyCode key2;
    public KeyCode key3;
    public GameEventTypes type1;

    public KeySet(KeyCode k1, KeyCode k2, KeyCode k3, GameEventTypes t1)
    {
        key1 = k1;
        key2 = k2;
        key3 = k3;
        type1 = t1;
    }

    public KeySet(KeyCode k1)
    {
        key1 = k1;
        key2 = default(KeyCode);
        key3 = default(KeyCode);
        type1 = GameEventTypes.none;
    }

    public KeySet(KeyCode k1, GameEventTypes t1)
    {
        key1 = k1;
        key2 = default(KeyCode);
        key3 = default(KeyCode);
        type1 = t1;
    }

    public static readonly KeySet noInputHover = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.hover);
    public static readonly KeySet zero = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.none);
    public static readonly KeySet ValueChange = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.valueChange);
    public static readonly KeySet EndEdit = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.endEdit);
    public static readonly KeySet Mouse0Up = new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp);
    public static readonly KeySet EndDrag = new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.endDrag);
    public static readonly KeySet BeginDrag = new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.beginDrag);
    public static readonly KeySet PointerEnter = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.pointerEnter);
    public static readonly KeySet PointerExit = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.pointerExit);
    public static readonly KeySet Dropped = new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.dropped);
    public static readonly KeySet ScrollUp = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.scrollUp);
    public static readonly KeySet ScrollDown = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.scrollDown);
    public static readonly KeySet MoveMouse = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.mouseMove);

    /// <summary>
    /// Returns default KeySet for input event enum
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static KeySet GetDefault(GameEventTypes type)
    {
        if (type == GameEventTypes.beginDrag)
        {
            return KeySet.BeginDrag;
        }
        else if (type == GameEventTypes.endDrag)
        {
            return KeySet.EndDrag;
        }
        else if (type == GameEventTypes.dropped)
        {
            return KeySet.Dropped;
        }
        else if (type == GameEventTypes.endEdit)
        {
            return KeySet.EndEdit;
        }
        else if (type == GameEventTypes.hover)
        {
            return KeySet.noInputHover;
        }
        else if (type == GameEventTypes.inputKeyDown)
        {
            return new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown);
        }
        else if (type == GameEventTypes.inputKeyUp)
        {
            return KeySet.Mouse0Up;
        }
        else if (type == GameEventTypes.none)
        {
            return KeySet.zero;
        }
        else if (type == GameEventTypes.pointerEnter)
        {
            return KeySet.PointerEnter;
        }
        else if (type == GameEventTypes.pointerExit)
        {
            return KeySet.PointerExit;
        }
        else if (type == GameEventTypes.scrollDown)
        {
            return new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.scrollDown);
        }
        else if (type == GameEventTypes.scrollUp)
        {
            return new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.scrollUp);
        }
        else if (type == GameEventTypes.valueChange)
        {
            return KeySet.ValueChange;
        }
        else
            return KeySet.zero;
    }

    /// <summary>
    /// Returns true if the input Key isForbidden (Won't trigger a valid input key event)
    /// </summary>
    /// <param name="inputKey"></param>
    /// <returns></returns>
    public static bool IsForbiddenKey(KeyCode inputKey)
    {
        if (inputKey == KeyCode.RightShift)
            return true;
        return false;
    }

    /// <summary>
    /// Returns true if the input keyset contains multiple keys
    /// </summary>
    /// <param name="inputKey"></param>
    /// <returns></returns>
    public static bool IsMultiKey(KeySet inputKey)
    {
        int count = 0;
        if (inputKey.key1 != KeyCode.None)
            count++;
        if (inputKey.key2 != KeyCode.None)
            count++;
        if (inputKey.key3 != KeyCode.None)
            count++;

        if (count > 1)
            return true;
        return false;
    }

    public override bool Equals(object obj)
    {
        return obj is KeySet ? equals((KeySet)obj) : false;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 29;
            hash = key1.GetHashCode() + hash * 23;
            hash = key2.GetHashCode() + hash * 23;
            hash = key3.GetHashCode() + hash * 23;
            hash = type1.GetHashCode() + hash * 23;
            return hash;
        }
    }

    public override string ToString()
    {
        object[] stuff = { key1, key2, key3, type1 };
        return String.Format("Key1: {0}, Key2: {1}, Key3: {2}, Event: {3}", stuff);
    }

    /// <summary>
    /// Determines if a KeySet is equal to another. Ordering in keys do not matter (they are sets).
    /// </summary>
    /// <param name="k"></param>
    /// <returns></returns>
    private bool equals(KeySet k)
    {
        if (k.type1 != type1)
            return false;

        if (k.key1 == key1)
        {
            if (k.key2 == key2)
            {
                if (k.key3 == key3)
                    return true;
            }
            else if (k.key2 == key3)
            {
                if (k.key3 == key2)
                    return true;
            }
        }
        else if (k.key1 == key2)
        {
            if (k.key2 == key1)
            {
                if (k.key3 == key3)
                    return true;
            }
            else if (k.key2 == key3)
            {
                if (k.key3 == key1)
                    return true;
            }
        }
        else if (k.key1 == key3)
        {
            if (k.key2 == key1)
            {
                if (k.key3 == key2)
                    return true;
            }
            else if (k.key2 == key2)
            {
                if (k.key3 == key1)
                    return true;
            }
        }
        return false;
    }

    public static bool operator ==(KeySet k1, KeySet k2)
    {
        return k1.Equals(k2);
    }

    public static bool operator !=(KeySet k1, KeySet k2)
    {
        return !(k1 == k2);
    }
}

public enum KeyStatus
{
    UP = 0,
    DOWN = 1
}




















