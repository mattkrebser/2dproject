﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using I18N.West;
using I18N;

/// <summary>
/// Used for loading and saving things
/// </summary>
public static class DiskOp
{
    /// <summary>
    /// Current Game Name being used to save games
    /// </summary>
    private static string CurrentGameName = "";
    /// <summary>
    /// Director Seperator
    /// </summary>
    public static readonly string Slash = Path.DirectorySeparatorChar.ToString();
    /// <summary>
    /// Where all saves are
    /// </summary>
    public static readonly string SaveDirectory = Application.persistentDataPath + Slash + "GameSaves";
    /// <summary>
    /// SaveDirecty + slash + gameName + slash
    /// </summary>
    private static string SaveDir
    {
        get
        {
            return SaveDirectory + Slash + CurrentGameName + Slash;
        }
    }

    /// <summary>
    /// Path to lobby save + slash
    /// </summary>
    public static string LobbySave
    {
        get
        {
            return Application.dataPath + Slash + "LobbySave" + Slash;
        }
    }

    /// <summary>
    /// Return saved player-specific information
    /// </summary>
    /// <param name="GameName"></param>
    /// <returns></returns>
    public static PlayerInfo2 GetPlayerDat(string GameName)
    {
        CurrentGameName = GameName;
        if (File.Exists(SaveDir + "Player.dat"))
        {
            PlayerInfo2 pi2 = LoadObject<PlayerInfo2>(SaveDir + "Player.dat");
            Quest.AllQuestsDat = pi2.MyQuests;
            Quest.LateInitQuests();
            pi2.MyQuests = null;
            return pi2;
        }

        return null;
    }
    /// <summary>
    /// load all characters including player from disk
    /// </summary>
    /// <returns></returns>
    public static List<PlayerInfo> GetAllCharacters(string GameName)
    {
        CurrentGameName = GameName;
        if (File.Exists(SaveDir + "Character.dat"))
        {
            return LoadObject<List<PlayerInfo>>(SaveDir + "Character.dat");
        }

        return null;
    }
    /// <summary>
    /// load game dependent variables
    /// </summary>
    /// <returns></returns>
    public static GameVariables GetGameVars(string GameName)
    {
        CurrentGameName = GameName;
        if (File.Exists(SaveDir + "Game.dat"))
        {
            return LoadObject<GameVariables>(SaveDir + "Game.dat");
        }

        return null;
    }
    /// <summary>
    /// load vehicles
    /// </summary>
    /// <returns></returns>
    public static List<VehicleInfo> GetAllVehicles(string GameName)
    {
        CurrentGameName = GameName;
        if (File.Exists(SaveDir + "Vehicles.dat"))
        {
            return LoadObject<List<VehicleInfo>>(SaveDir + "Vehicles.dat");
        }

        return null;
    }
    /// <summary>
    /// Loads Faction Dat From disk for the current game
    /// </summary>
    /// <returns></returns>
    public static List<Faction> GetFactionDat(string GameName)
    {
        CurrentGameName = GameName;
        if (File.Exists(SaveDir + "Faction.dat"))
        {
            return LoadObject<List<Faction>>(SaveDir + "Faction.dat");
        }
        else
            return null;
    }
    /// <summary>
    /// Save player specific information
    /// </summary>
    /// <param name="p"></param>
    /// <param name="GameName"></param>
    public static void SavePlayerDat(PlayerInfo2 p, string GameName)
    {
        CurrentGameName = GameName;
        TryMakeSaveDir();
        p.MyQuests = Quest.AllQuestsDat;
        SaveObject<PlayerInfo2>(p, SaveDir + "Player.dat");
    }
    /// <summary>
    /// Save all characters
    /// </summary>
    /// <param name="p"></param>
    public static void SaveCharData(List<PlayerInfo> p, string GameName)
    {
        CurrentGameName = GameName;
        TryMakeSaveDir();
        SaveObject<List<PlayerInfo>>(p, SaveDir + "Character.dat");
    }
    /// <summary>
    /// Save Game Variables
    /// </summary>
    /// <param name="gv"></param>
    public static void SaveGameVars(GameVariables gv, string GameName)
    {
        CurrentGameName = GameName;
        TryMakeSaveDir();
        SaveObject<GameVariables>(gv, SaveDir + "Game.dat");
    }
    /// <summary>
    /// Save vehicles
    /// </summary>
    /// <param name="v"></param>
    public static void SaveVehicles(List<VehicleInfo> v, string GameName)
    {
        CurrentGameName = GameName;
        TryMakeSaveDir();
        SaveObject<List<VehicleInfo>>(v, SaveDir + "Vehicles.dat");
    }
    /// <summary>
    /// Save factions
    /// </summary>
    /// <param name="fD"></param>
    public static void SaveFactionDat(List<Faction> fD, string GameName)
    {
        CurrentGameName = GameName;
        TryMakeSaveDir();
        SaveObject<List<Faction>>(fD, SaveDir + "Faction.dat");
    }

    public static List<PlayerInfo> GetStaticChars(string GameName)
    {
        CurrentGameName = GameName;
        if (File.Exists(SaveDir + "StaticCharacter.dat"))
        {
            return LoadObject<List<PlayerInfo>>(SaveDir + "StaticCharacter.dat");
        }

        return null;
    }
    /// <summary>
    /// Save all characters
    /// </summary>
    /// <param name="p"></param>
    public static void SaveStaticChars(List<PlayerInfo> p, string GameName)
    {
        CurrentGameName = GameName;
        TryMakeSaveDir();
        SaveObject<List<PlayerInfo>>(p, SaveDir + "StaticCharacter.dat");
    }

    /// <summary>
    /// load all zone data
    /// </summary>
    /// <param name="GameName"></param>
    /// <returns></returns>
    public static List<ZoneDat> GetZoneDat(string GameName)
    {
        CurrentGameName = GameName;
        if (File.Exists(SaveDir + "Zone.dat"))
        {
            return LoadObject<List<ZoneDat>>(SaveDir + "Zone.dat");
        }

        return null;
    }
    /// <summary>
    /// Save all zones
    /// </summary>
    /// <param name="p"></param>
    public static void SaveZoneDat(List<ZoneDat> p, string GameName)
    {
        CurrentGameName = GameName;
        TryMakeSaveDir();
        SaveObject<List<ZoneDat>>(p, SaveDir + "Zone.dat");
    }

    /// <summary>
    /// load all zone data
    /// </summary>
    /// <param name="GameName"></param>
    /// <returns></returns>
    public static List<SaveStateDat> GetObjDat(string GameName)
    {
        CurrentGameName = GameName;
        if (File.Exists(SaveDir + "Obj.dat"))
        {
            return LoadObject<List<SaveStateDat>>(SaveDir + "Obj.dat");
        }

        return null;
    }
    /// <summary>
    /// Save all zones
    /// </summary>
    /// <param name="p"></param>
    public static void SaveObjDat(List<SaveStateDat> p, string GameName)
    {
        CurrentGameName = GameName;
        TryMakeSaveDir();
        SaveObject<List<SaveStateDat>>(p, SaveDir + "Obj.dat");
    }

    /// <summary>
    /// Trys to make the save directories, if they dont exist
    /// </summary>
    static void TryMakeSaveDir()
    {
        if (!Directory.Exists(SaveDirectory))
        {
            Directory.CreateDirectory(SaveDirectory);
        }
        if (!Directory.Exists(SaveDirectory + Slash + CurrentGameName))
        {
            Directory.CreateDirectory(SaveDirectory + Slash + CurrentGameName);
        }
    }
    /// <summary>
    /// Returns a list of All Save names
    /// </summary>
    /// <returns></returns>
    public static List<string> GetAllSaveNames()
    {
        if (!Directory.Exists(SaveDirectory))
        {
            Directory.CreateDirectory(SaveDirectory);
        }
        List<string> list = new List<string>();
        string[] dirs = Directory.GetDirectories(SaveDirectory);
        return dirs == null ? list : new List<string>(dirs);
    }
    /// <summary>
    /// Returns true if the inputr save exists
    /// </summary>
    /// <param name="SaveName"></param>
    /// <returns></returns>
    public static bool SaveExists(string SaveName)
    {
        if (!Directory.Exists(SaveDirectory))
        {
            Directory.CreateDirectory(SaveDirectory);
            return false;
        }
        return Directory.Exists(SaveDirectory + Slash + SaveName);
    }

    /// <summary>
    /// Load an object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="path"></param>
    /// <returns></returns>
    public static T LoadObject<T>(string path)
    {
        XmlSerializer xs = new XmlSerializer(typeof(T));
        using (FileStream fs = File.Open(path, FileMode.Open))
        {
            using (StreamWriter sw = new StreamWriter(fs, I18N.Common.ByteEncoding.UTF8))
            {
                T new_obj = (T)xs.Deserialize(fs);
                return new_obj;
            }
        }
    }

    /// <summary>
    /// Save an object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="path"></param>
    public static void SaveObject<T>(T obj, string path)
    {
        XmlSerializer xs = new XmlSerializer(typeof(T));
        using (FileStream fs = File.Open(path, FileMode.Create))
        {
            using (StreamWriter sw = new StreamWriter(fs, I18N.Common.ByteEncoding.UTF8))
            {
                xs.Serialize(fs, obj);
            }
        }
    }
}
