﻿using UnityEngine;
using System.Collections;

public class NPCAnimator : MonoBehaviour
{
    /// <summary>
    /// Agent Reference
    /// </summary>
    public AIMove agent;

    /// <summary>
    /// Reference to animator
    /// </summary>
    public Animator animator;

    /// <summary>
    /// Character Data Reference
    /// </summary>
    public CharacterData char_dat;

    //current animation being played
    string current_animation;
    /// <summary>
    /// Get/Set current animation being played
    /// </summary>
    public string CurrentAnimation
    {
        get
        {
            return current_animation;
        }
        set
        {
            if (current_animation == value)
                return;
            current_animation = value;
            animator.Play(value);
        }
    }

    void Start()
    {
        StartCoroutine(lateRotationFix());
    }
    IEnumerator lateRotationFix()
    {
        yield return 0; yield return 0; yield return 0; yield return 0;
        foreach (Transform t in GetComponentsInChildren<Transform>())
        {
            if (t != transform)
                t.localRotation = Quaternion.Euler(new Vector3(90, 0, 0));
        }
    }

    bool isPlaying = true;
    // Update is called once per frame
    void Update()
    {
        if (CharacterMove.CharacterTransform == null)
            return;

        //if is far away, then do nothing
        if ((transform.parent.position - CharacterMove.CharacterTransform.position).sqrMagnitude > 64)
        {
            if (isPlaying)
            {
                isPlaying = false;
                animator.enabled = false;
            }
            return;
        }
        //otherwise resume
        else
        {
            if (!isPlaying)
            {
                isPlaying = true;
                animator.enabled = true;
            }
        }

        //set animator to walk speed if walkinh
        if (agent.isWalking && animator.speed != 0.75f)
            animator.speed = 0.75f;

        //in vehicle animation
        if (char_dat.InVehicle != null)
        {
            char_dat.transform.localEulerAngles= new Vector3(char_dat.InVehicle.transform.eulerAngles.x,
                360 - char_dat.InVehicle.transform.eulerAngles.y, char_dat.InVehicle.transform.eulerAngles.z);
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            SetAnimation(char_dat.InVehicle.transform.rotation * Vector3.forward, char_dat.InVehicle.vehicleAgent == null ?
                Vector3.zero : char_dat.InVehicle.vehicleAgent.velocity);
        }
        //not in vehicle animation, only adjust rotation
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            SetAnimation(agent.transform.rotation * Vector3.forward,
                agent.agent == null ? Vector3.zero : agent.agent.velocity);
        }
    }

    /// <summary>
    /// Set player's animation
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="velocity"></param>
    void SetAnimation(Vector3 direction, Vector3 velocity)
    {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.z))
        {
            bool l = false, r = false;
            r = direction.x > 0.0001f;
            l = direction.x < -0.0001f;
            if (l) { CurrentAnimation = velocity.magnitude > 0.01f ? "WalkLeft" : "IdleLeft"; return; }
            if (r) { CurrentAnimation = velocity.magnitude > 0.01f ? "WalkRight" : "IdleRight"; return; }
        }
        else
        {
            bool f = false, b = false;
            f = direction.z > 0.0001f;
            b = direction.z < -0.0001f;
            if (f) { CurrentAnimation = velocity.magnitude > 0.01f ? "WalkForward" : "IdleForward"; return; }
            if (b) { CurrentAnimation = velocity.magnitude > 0.01f ? "Walk" : "Idle"; return; }
        }
    }
}
