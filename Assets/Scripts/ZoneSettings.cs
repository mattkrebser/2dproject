﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Defines how NPC spawning takes place at the point. Thinkof 'ZoneSettings' as a spawner
/// that spawn NPC's based off the settings
/// </summary>
public class ZoneSettings : MonoBehaviour
{
    [Tooltip("What is this zone called?")]
    public string zone_name;
    [Tooltip("WHich faction owns this zone? None Means random")]
    public Factions faction_owner;
    [Tooltip("Set the personality on the NPCs spawned by this, none=Random")]
    public Personality EnforcedPersonality = Personality.none;
    [Tooltip("Should NPC's have the same last names?")]
    public bool shared_lastname;
    [Tooltip("Should NPC's look similar?")]
    public bool shared_ancestry;
    [Tooltip("Max Number of residents this zone can have active at a time")]
    public int num_residents;
    [Tooltip("How fats NPC's get spawned (if not at max)")]
    public float resident_spawn_rate;
    [Tooltip("Initial spawn amount of NPCs")]
    public int initial_number_of_residents;

    [Tooltip("Only make male/female/or both")]
    public MaleFemale EnforcedGenderOnly = MaleFemale.both;

    [Tooltip("One of the paths in this array will be randomly selected on character spawn")]
    public List<AIPath> avaiable_paths;

    /// <summary>
    /// Spawn characters?
    /// </summary>
    [Tooltip("Enable/Disable spawning")]
    public bool IsSpawning= true;

    [Tooltip("NPC's will spawn at random points on the path")]
    public bool RandomlySpawn = false;

    private bool Initialized = false;

    /// <summary>
    /// If true, all Npcs from this zone will attack the player
    /// </summary>
    [System.NonSerialized]
    public bool AttackPlayer = false;

    /// <summary>
    /// Are we spawning vehicles?
    /// </summary>
    [Tooltip("Should this spawner spawn Vehicles?")]
    public bool SpawnVehicles = false;
    /// <summary>
    /// Should Vehicles have people in them?
    /// </summary>
    [Tooltip("SHould the vehicles spawned by this spawner be empty?")]
    public bool EmptyVehicles = false;
    /// <summary>
    /// Rotation of spawned objects.
    /// </summary>
    public Vector3 SpawnedRotationEuler;

    /// <summary>
    /// Number of live NPCS
    /// </summary>
    private int num_live_npcs = 0;

    /// <summary>
    /// How far away must the player be before spawning is allowed
    /// </summary>
    [Tooltip("how far away the player must be before spawning is allowed")]
    public float distFromPlayer_until_can_spawn = 9.0f;

    /// <summary>
    /// This Spawner belongs to this zone...
    /// </summary>
    [Tooltip("The zone(MapData object) that this spawner is in")]
    public MapData MyZone;

    /// <summary>
    /// Maximum number of NPCs this zone will spawn over time, once this number is reached, the zone will stop spawning
    /// </summary>
    [Tooltip("The maximum number of NPCs over time the zone can spawn before it stops spawning. " +
        "This only applys if greater than 0")]
    public int SpawnCap;

    private int tot_spawn; private int tot_dead;
    /// <summary>
    /// Total number of NPCs that have died from this spawner
    /// </summary>
    private int TotalDiedAmount
    {
        get
        {
            return tot_dead;
        }
        set
        {
            tot_dead = value;
            if (SpawnCap > 0 && tot_dead >= SpawnCap)
            {
                SendMessage("OnSpawnerDefeat", SendMessageOptions.DontRequireReceiver);
            }
        }
    }
    /// <summary>
    /// Total number of NPCs this Zone has spawned
    /// </summary>
    private int TotalSpawnAmount
    {
        get
        {
            return tot_spawn;
        }
        set
        {
            tot_spawn = value;
        }
    }
    /// <summary>
    /// Returns true if this zone has reached its' spawn cap
    /// </summary>
    public bool ZoneReachedSpawnLimit
    {
        get
        {
            return SpawnCap == 0 ? false : TotalSpawnAmount >= SpawnCap;
        }
    }

    public static Dictionary<string, ZoneSettings> All_ZoneSettings = new Dictionary<string, ZoneSettings>();

    /// <summary>
    /// Get the zone with the input name.
    /// </summary>
    /// <param name="zone_name"></param>
    /// <returns></returns>
    public static ZoneSettings GetZone(string zone_name)
    {
        return All_ZoneSettings[zone_name];
    }
    /// <summary>
    /// Reset all Zones
    /// </summary>
    public static void ResetAllZones()
    {
        foreach (ZoneSettings z in All_ZoneSettings.Values)
        {
            z.ResetZone();
        }
    }

    /// <summary>
    /// Destroys all NPC's spawned by this spawner and marks the spawner as defeated.
    /// It will be bloody.
    /// </summary>
    public void AutoDefeatThisSpawner()
    {
        if (MyZone.isAsleep)
        {
            List<PlayerInfo> pList = new List<PlayerInfo>();
            foreach (PlayerInfo p in MyZone.MyPlayers)
            {
                if (p.Spawner != zone_name)
                    pList.Add(p);
            }
            MyZone.MyPlayers = pList;
        }
        else
        {
            foreach (CharacterData c in CharacterData.AllCharacters)
            {
                if (c.SpawnedFrom == this)
                {
                    c.SetCharHealth(null, 0);
                }
            }
        }
        TotalSpawnAmount = SpawnCap;
        TotalDiedAmount = SpawnCap;
    }

    public void ResetZone()
    {
        num_live_npcs = 0;
        tot_spawn = 0;
        tot_dead = 0;
        IsSpawning = true;
        Initialized = false;
    }

    /// <summary>
    /// Resets the zones in the input map.
    /// </summary>
    /// <param name="MyZones"></param>
    public static void ResetMapZones(MapData MyZones)
    {
        foreach (ZoneSettings z in All_ZoneSettings.Values)
        {
            if (z.MyZone.ZoneID == MyZones.ZoneID)
            {
                z.num_live_npcs = 0;
                z.Initialized = false;
            }
        }
    }
    
    /// <summary>
    /// Returns true if this zone is hibernating
    /// </summary>
    public bool IsHibernating
    {
        get
        {
            return !(MapData.ActiveMap == null ? false : MapData.ActiveMap.ZoneID == MyZone.ZoneID);
        }
    }

    void Start()
    {
        All_ZoneSettings.Add(zone_name, this);
        //clamp between 0 and 1
        resident_spawn_rate = resident_spawn_rate <= 0.1f ? 0.1f : resident_spawn_rate;
        StartCoroutine(respawnRoutine());
    }

    void OnDestroy()
    {
        All_ZoneSettings.Remove(zone_name);
    }

    public IEnumerator respawnRoutine()
    {
        yield return 0; yield return 0;
        while (true)
        {
            if (ZoneReachedSpawnLimit && num_live_npcs + TotalDiedAmount < SpawnCap && IsSpawning && !IsHibernating)
            {
                Debug.Log("Error " + zone_name  + ", " + TotalDiedAmount + ", " + TotalSpawnAmount + ", " + IsSpawning);
            }
            //initial spawning
            if (IsSpawning && !Initialized && !ZoneReachedSpawnLimit &&
                (GameSettings.Running || GameSettings.LobbyRunning) && !IsHibernating)
            {
                Initialized = true;
                for (int i = 0; i < initial_number_of_residents; i++)
                {
                    if (!ZoneReachedSpawnLimit && num_live_npcs < num_residents)
                    {
                        if (!SpawnVehicles)
                        {
                            MakeNPC();
                        }
                        else
                        {
                            MakeVehicle();
                        }
                    }
                }

            }
            //normal spawning
            else if (IsSpawning && !ZoneReachedSpawnLimit && (GameSettings.Running || GameSettings.LobbyRunning)
                && !IsHibernating)
            {
                if (CharacterMove.CharacterTransform != null &&
                    !((CharacterMove.CharacterTransform.position - transform.position).magnitude < 
                    distFromPlayer_until_can_spawn))
                {
                    //if there needs to be more NPCs spawned...
                    if (num_live_npcs < num_residents)
                    {
                        if (SpawnVehicles)
                        {
                            MakeVehicle();
                        }
                        else
                        {
                            MakeNPC();
                        }
                    }
                }
            }
            yield return new WaitForSeconds(Mathf.Abs(resident_spawn_rate));
        }
    }

    /// <summary>
    /// InstaSpawn 'Amount' NPCs.
    /// </summary>
    /// <param name="Amount"></param>
    public void InstaSpawn(int Amount)
    {
        if (IsSpawning && !ZoneReachedSpawnLimit && (GameSettings.Running || GameSettings.LobbyRunning)
                && !IsHibernating)
        {
            for (int i = 0; i < Amount; i++)
            {
                if (!ZoneReachedSpawnLimit && num_live_npcs < num_residents)
                {
                    if (!SpawnVehicles)
                    {
                        MakeNPC();
                    }
                    else
                    {
                        MakeVehicle();
                    }
                }
            }
        }
    }

    CharacterData MakeNPC()
    {
        //make new character
        GameObject new_npc = Instantiate(PeopleData.NPC_PREFAB);
        new_npc.SetActive(true);
        CharacterData c = new_npc.GetComponent<CharacterData>();
        c.SpawnedFrom = this;
        //fill data values
        PeopleData.GetNPC(faction_owner, c, Gender:EnforcedGenderOnly, personality:EnforcedPersonality);

        //get character's path
        if (avaiable_paths != null && avaiable_paths.Count > 0)
        {
            AIMove aim = new_npc.GetComponent<AIMove>();
            int num = 0;
            aim.points = avaiable_paths[num = UnityEngine.Random.Range(0, avaiable_paths.Count)].path;
            aim.PathID = num;
            aim.spawner_position = transform.position;
            if (avaiable_paths[num].allow_reverse)
            {
                aim.reverse_direction = UnityEngine.Random.Range(0, 2) == 1;
            }
            aim.MyZone = MyZone;
            aim.Init(RandomlySpawn);
        }

        TrySetZoneConfig(c);

        num_live_npcs++;
        TotalSpawnAmount++;

        //if attack flag
        if (AttackPlayer)
            new_npc.GetComponent<NPCAI>().Attack(CharacterMove.CharacterTransform.GetComponent<CharacterData>());

        return c;
    }
    CharacterData MakeNPC(Factions fac)
    {
        //make new character
        GameObject new_npc = Instantiate(PeopleData.NPC_PREFAB);
        new_npc.SetActive(true);
        CharacterData c = new_npc.GetComponent<CharacterData>();
        c.SpawnedFrom = this;
        //fill data values
        PeopleData.GetNPC(fac, c, Gender: EnforcedGenderOnly, personality: EnforcedPersonality);

        //get character's path
        if (avaiable_paths != null && avaiable_paths.Count > 0)
        {
            AIMove aim = new_npc.GetComponent<AIMove>();
            int num = 0;
            aim.points = avaiable_paths[num = UnityEngine.Random.Range(0, avaiable_paths.Count)].path;
            aim.PathID = num;
            aim.spawner_position = transform.position;
            if (avaiable_paths[num].allow_reverse)
            {
                aim.reverse_direction = UnityEngine.Random.Range(0, 2) == 1;
            }
            aim.MyZone = MyZone;
            aim.Init(RandomlySpawn);
        }

        num_live_npcs++;
        TotalSpawnAmount++;

        //if attack flag
        if (AttackPlayer)
            new_npc.GetComponent<NPCAI>().Attack(CharacterMove.CharacterTransform.GetComponent<CharacterData>());

        return c;
    }

    Factions RandFaction
    {
        get
        {
            Factions f;
            while (true)
            {
                f = PeopleData.RandFaction;
                if (f != Factions.Dave && f != Factions.TheLaw)
                    return f;
            }
        }
    }
    VehicleScript MakeVehicle()
    {
        int orig_num_npc = num_live_npcs;
        int origTotal = TotalSpawnAmount;

        //assign random number of passengers
        int num_drivers = UnityEngine.Random.Range(0, 2);
        //get random faction
        Factions fac = faction_owner == Factions.none ? RandFaction : faction_owner;

        //get new vehicle
        VehicleScript v = VehicleScript.GetNewRandVehicle(fac);

        //Assign transform
        v.transform.position = transform.position;
        v.transform.eulerAngles = SpawnedRotationEuler;

        //Stationary vehicle?
        v.IsStationary = EmptyVehicles;

        //Assign parent reference
        v.SpawnedFrom = this;

        //Assign ID
        v.VehicleID = v.UniqueVehicleID;

        v.MyZone = MyZone;

        //Stationary vehicles dont have drivers or paths
        if (!v.IsStationary)
        {
            int pathnum;
            //Assign a route
            v.vehicleRoute = avaiable_paths[pathnum = UnityEngine.Random.Range(0, avaiable_paths.Count)].path;
            v.PathID = pathnum;

            CharacterData c;
            //add a driver
            v.EnterVehicle(c = MakeNPC(fac));

            c.GetComponent<AIMove>().SpawnedInVehicle = true;

            //50-50 chance of passenger
            if (num_drivers == 1)
            {
                v.EnterVehicle(c = MakeNPC(fac));
                c.GetComponent<AIMove>().SpawnedInVehicle = true;
            }
        }
        else
        {
            v.PathID = -1;
        }
        //Increment number of live NPC's count
        num_live_npcs = orig_num_npc + 1;
        TotalSpawnAmount = origTotal + 1;

        v.Init();

        return v;
    }

    /// <summary>
    /// Makes an NPC and initializes the Spawner
    /// </summary>
    /// <param name="p"></param>
    public void MakeNPC(PlayerInfo p)
    {
        GameObject new_npc = Instantiate(PeopleData.NPC_PREFAB);
        new_npc.SetActive(true);
        CharacterData c = new_npc.GetComponent<CharacterData>();
        c.SpawnedFrom = this;
        PeopleData.InitializeCharacter(c, p);
        //we dont need to set rotation (and shouldn't) if a character
        //was not in a vehicle
        if (p.VehicleID != 0)
            c.transform.eulerAngles = p.RotationEuler;

        new_npc.GetComponent<NPCAI>().DisabledNormalConversation = p.CanSpeak;

        AIMove aim = new_npc.GetComponent<AIMove>();
        aim.MyZone = MapData.maps[p.CurrentZone];
        //get character's path
        if (avaiable_paths != null && avaiable_paths.Count > 0)
        {
            int num = p.PathNumber;
            aim.points = avaiable_paths[num].path;
            aim.PathID = num;
            aim.spawner_position = transform.position;

            //If the NPC belongs to a vehicle, then reversing is not allowed
            if (avaiable_paths[num].allow_reverse && !p.SpawnedInVehicle)
            {
                aim.reverse_direction = p.ReverseDirection;
            }
            aim.Init(p, RandomlySpawn, newDestination:p.Destination);
        }

        //If the NPC was spawned in a vehicle, then don't increment the count
        if (!p.SpawnedInVehicle)
        {
            num_live_npcs++;
        }

        //enter vehicle
        if (p.VehicleID != 0)
        {
            VehicleScript v = VehicleScript.All_Vehicles[p.VehicleID];
            v.EnterVehicle(c, !p.isPassenger, p.isPassenger);
        }

        //if attack flag
        if (AttackPlayer)
            StartCoroutine(LateAttackRoutine(new_npc));

        Initialized = true;
    }
    IEnumerator LateAttackRoutine(GameObject new_npc)
    {
        while (CharacterMove.CharacterTransform == null)
            yield return 0;
        //if attack flag
        if (AttackPlayer)
            new_npc.GetComponent<NPCAI>().Attack(CharacterMove.CharacterTransform.GetComponent<CharacterData>());
    }
    /// <summary>
    /// Makes a vehicle and Initializes thre spawner
    /// </summary>
    /// <param name="v"></param>
    public void MakeVehicle(VehicleInfo v)
    {
        //get new vehicle
        VehicleScript vS = VehicleScript.GetNewRandVehicle(Factions.none, v.vehicleSkin, v.Health, v.VehicleSpeed);

        //Assign transform
        vS.transform.position = v.Position;
        vS.transform.eulerAngles = v.RotationEuler;

        //Stationary vehicle?
        vS.IsStationary = v.stationaryVehicle;

        //Assign parent reference
        vS.SpawnedFrom = this;

        //Assign reset Var
        vS.vehicle_needs_reset = v.vehicleNeedsReset;

        //Assign ID
        vS.VehicleID = v.VehicleID;
        vS.MyZone = MyZone;


        //Stationary vehicles dont have drivers or paths
        if (!vS.IsStationary)
        {
            int pathnum = v.PathNumber;
            //Assign a route
            vS.vehicleRoute = avaiable_paths[pathnum].path;
            vS.PathID = pathnum;
        }
        else
        {
            vS.PathID = -1;
        }
        //Increment number of live NPC's count
        num_live_npcs++;

        vS.Init();

        Initialized = true;
    }

    /// <summary>
    /// Call this if an NPC belonging to this spawner has died.
    /// </summary>
    public void ReportDeath()
    {
        num_live_npcs--;
        TotalDiedAmount++;
    }

    public void SetDat(ZoneDat z)
    {
        tot_spawn = z.tot_spawn;
        tot_dead = z.tot_dead;
        IsSpawning = z.IsSpawning;
        AttackPlayer = z.AttackPlayer;
    }
    public ZoneDat GetDat()
    {
        ZoneDat z = new ZoneDat();
        z.tot_spawn = tot_spawn;
        z.tot_dead = tot_dead;
        z.IsSpawning = IsSpawning;
        z.ZoneName = zone_name;
        z.AttackPlayer = AttackPlayer;
        return z;
    }

    /// <summary>
    /// Attempts to find a zone configuration for this object, and applys it if one is found
    /// </summary>
    void TrySetZoneConfig(CharacterData c)
    {
        ZoneConfiguration zc = GetComponent<ZoneConfiguration>();
        if (zc != null)
        {
            if (!String.IsNullOrEmpty(zc.FirstName))
            {
                c.first_name = zc.FirstName == "None" ? "" : zc.FirstName;
            }
            if (!String.IsNullOrEmpty(zc.LastName))
            {
                c.last_name = zc.LastName == "None" ? "" : zc.LastName;
            }
            if (!String.IsNullOrEmpty(zc.LWeapon))
            {
                c.L_weapon = zc.LWeapon == "None" ? "" : zc.LWeapon;
            }
            if (!String.IsNullOrEmpty(zc.RWeapon))
            {
                c.R_weapon = zc.RWeapon == "None" ? "" : zc.RWeapon;
            }
            if (!String.IsNullOrEmpty(zc.Arms))
            {
                c.Arms = zc.Arms == "None" ? "" : zc.Arms;
            }
            if (!String.IsNullOrEmpty(zc.EyeColor))
            {
                c.Eyecolor = zc.EyeColor == "None" ? "" : zc.EyeColor;
            }
            if (!String.IsNullOrEmpty(zc.Face))
            {
                c.Facecolor = zc.Face == "None" ? "" : zc.Face;
            }
            if (!String.IsNullOrEmpty(zc.Shirt))
            {
                c.Shirt = zc.Shirt == "None" ? "" : zc.Shirt;
            }
            if (!String.IsNullOrEmpty(zc.Shoes))
            {
                c.Shoes = zc.Shoes == "None" ? "" : zc.Shoes;
            }
            if (!String.IsNullOrEmpty(zc.legs))
            {
                c.Pants = zc.legs == "None" ? "" : zc.legs;
            }
            if (!String.IsNullOrEmpty(zc.OuterEye))
            {
                c.Outer_eye = zc.OuterEye == "None" ? "" : zc.OuterEye;
            }
            if (!String.IsNullOrEmpty(zc.Hat))
            {
                c.Hat = zc.Hat == "None" ? "" : zc.Hat;
            }
            if (!String.IsNullOrEmpty(zc.Hair))
            {
                c.Hair = zc.Hair == "None" ? "" : zc.Hair;
            }
            if (!String.IsNullOrEmpty(zc.Glasses))
            {
                c.Glasses = zc.Glasses == "None" ? "" : zc.Glasses;
            }

            if (zc.AttacksPlayerOnSpawn)
                c.GetComponent<NPCAI>().Attack(CharacterMove.CharacterTransform.GetComponent<CharacterData>());
        }
    }
}

[System.Serializable]
public enum MaleFemale
{
    both,
    male,
    female
}