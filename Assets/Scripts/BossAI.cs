﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossAI : MonoBehaviour {

    [Tooltip("Spawn minions?")]
    public MinionAbility SpawnMinions;
    public RocketBarrageAbility RocketBarrage;
    public GrenadeRainAbility GrenadeRain;
    public HealStationsAbility HealStations;
    public TeleportPadsAbility TeleportPads;
    public PushPlayerAbility Push;
    public PullPlayerAbility Pull;

    [Tooltip("Play this music when at 30% or lower health")]
    public AudioClip LowHealthMusic;

    [Tooltip("Single Dialogue. Randomly selects one dialogue to choose from the list of strings and says it randomly in combat")]
    public Dialogue Taunts;

    [Tooltip("Path to walk on. The nodes to move to are selected randomly")]
    public AIPath BossPathNodes;

    public int BossMaxHealth = 3000;

    public float BossAttackRange = 5.0f;

    public float BossSpeed = 2.2f;

    [Tooltip("How often this NPC used one of its abilities. Randomly selected")]
    public float AbilityFrequency = 10.0f;

    private CharacterData MyCharacter;

    /// <summary>
    /// Is this boss in combat with the player?
    /// </summary>
    public bool InCombatWithPlayer
    {
        get
        {
            if (MyCharacter == null)
                return false;
            return MyCharacter.ForcedGunsOut;
        }
        set
        {
            if (MyCharacter == null)
                return;
            MyCharacter.ForcedGunsOut = value;
        }
    }

    /// <summary>
    /// Destination in the AIPath list
    /// </summary>
    private int destination = 0;
    /// <summary>
    /// The desired position as designated by the 'destination' variable
    /// </summary>
    private Vector3 DesiredPosition
    {
        get
        {
            return BossPathNodes.path[destination].position;
        }
    }

    /// <summary>
    /// NavMeshAgent for this boss
    /// </summary>
    NavMeshAgent agent;
    /// <summary>
    /// Distance to the next point
    /// </summary>
    float distanceToPoint
    {
        get
        {
            return (BossPathNodes.path[destination].position - transform.position).magnitude;
        }
    }
    /// <summary>
    /// Distance from this boss to the player
    /// </summary>
    float distanceToPlayer
    {
        get
        {
            if (CharacterMove.CharacterTransform == null)
                return 0;
            return (CharacterMove.CharacterTransform.position - transform.position).magnitude;
        }
    }
    /// <summary>
    /// Current target
    /// </summary>
    Transform Target;

    /// <summary>
    /// Used to randomly move the boss to places
    /// </summary>
    float RandomMoveBehaviourTimer = 0.0f;
    /// <summary>
    /// If true, the boss will follow the target
    /// </summary>
    bool FollowTarget = true;
    /// <summary>
    /// The amount of time spent waiting after eaching the destination on the current path
    /// </summary>
    float TimeWaitingOnPath = 0.0f;
    /// <summary>
    /// Is the boss currently using an ability?
    /// </summary>
    public bool InAbility
    {
        get
        {
            return _inAbility;
        }
        set
        {
            _inAbility = value;
        }
    }
    bool _inAbility = false;
    /// <summary>
    /// Time of last ability use
    /// </summary>
    private float timeOfLastAbility = 0.0f;
    /// <summary>
    /// time of the last taunt
    /// </summary>
    private float timeOfLastTaunt = 0.0f;
    /// <summary>
    /// Did we play a music track?
    /// </summary>
    private bool PlayedMusic = false;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        MyCharacter = GetComponent<CharacterData>();
        agent.speed = BossSpeed;
        timeOfLastAbility = Time.realtimeSinceStartup;
        RandomMoveBehaviourTimer = Time.realtimeSinceStartup;
    }

    void OnDisable()
    {
        InCombatWithPlayer = false;
        if (PlayedMusic)
            MusicPlayer.ResumeNormalPlayer();
    }

    void OnDestroy()
    {
        if (PlayedMusic)
            MusicPlayer.ResumeNormalPlayer();
    }

	// Update is called once per frame
	void Update ()
    {
	    if (InCombatWithPlayer && CharacterMove.CharacterTransform != null)
        {
            //if the player is very far away, then regen health
            if (!CharacterMove.NearPlayer(transform, 256))
            {
                MyCharacter.SetCharHealth(null, MyCharacter.Health + 1);
            }

            //reassign avoidance priority
            if (agent.obstacleAvoidanceType != ObstacleAvoidanceType.GoodQualityObstacleAvoidance)
            {
                agent.obstacleAvoidanceType = ObstacleAvoidanceType.GoodQualityObstacleAvoidance;
                agent.avoidancePriority = 25;
            }

            //decide movement pattern
            if (Time.realtimeSinceStartup - RandomMoveBehaviourTimer > 10.0f)
            {
                FollowTarget = Random.Range(0, 2) == 1;
                RandomMoveBehaviourTimer = Time.realtimeSinceStartup;
            }

            //assign target
            Target = CharacterMove.CharacterTransform;

            //try to do an ability
            TryRandAbility();

            //if not using an ability (standard movement)
            if (!InAbility)
            {
                //follow the player if the player is too far or follow flag
                if (FollowTarget || distanceToPlayer > 14)
                {
                    if (agent.stoppingDistance < 2.6f)
                        agent.stoppingDistance = 2.6f;
                    GotoTarget();
                }
                //move to destination if there is an incorrect destination or arrived at destination
                else if (distanceToPoint < 1.0f || !ApproximateEqual(DesiredPosition, agent.destination))
                {
                    TimeWaitingOnPath += Time.deltaTime;
                    //if waiting for over 2.5 seconds... (It could be more or less depending on
                    //if the npc followed the player)
                    if (TimeWaitingOnPath > 2.5f)
                    {
                        if (agent.stoppingDistance > 0.75f)
                            agent.stoppingDistance = 0.75f;
                        GotoRandomDestination();
                        TimeWaitingOnPath = 0.0f;
                    }

                }
            }
            //Using an ability...
            else
            {
                //nothing, handled on ability class methods
            }

            //attempt to shoot the target
            TryShootTarget();
            //attempt to taunt
            TryTaunt();
            //attempt to play boss music
            TryPlayMusic();
        }
        //not in combat
        else
        {
            //reset music if needed
            if (MyCharacter.Health > BossMaxHealth * 0.3f && PlayedMusic)
            {
                MusicPlayer.ResumeNormalPlayer();
                PlayedMusic = false;
            }

            //reset ability counter
            timeOfLastAbility = Time.realtimeSinceStartup;

            //not in combat, reset health
            if (MyCharacter.Health != BossMaxHealth)
                MyCharacter.SetCharHealth(null, BossMaxHealth);

            //reassign avoidance priority
            if (agent.obstacleAvoidanceType != ObstacleAvoidanceType.NoObstacleAvoidance)
            {
                agent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
                agent.avoidancePriority = 0;
            }
        }
	}

    /// <summary>
    /// Trys to play music for this boss
    /// </summary>
    void TryPlayMusic()
    {
        //play music
        if (MyCharacter.Health < BossMaxHealth * 0.3f && !PlayedMusic)
        {
            MusicPlayer.PlayTrack(LowHealthMusic, 10000000f, 1.0f);
            PlayedMusic = true;
        }
        else if (MyCharacter.Health > BossMaxHealth * 0.3f && PlayedMusic)
        {
            MusicPlayer.ResumeNormalPlayer();
            PlayedMusic = false;
        }
    }
    /// <summary>
    /// try to do a taunt dialogue
    /// </summary>
    void TryTaunt()
    {
        if (AbilityFrequency <= 2)
            return;

        if (Time.realtimeSinceStartup - timeOfLastAbility < AbilityFrequency - 2  &&
            Time.realtimeSinceStartup - timeOfLastAbility > 2)
        {
            if (Time.realtimeSinceStartup - timeOfLastTaunt > 5.0f)
            {
                timeOfLastTaunt = Time.realtimeSinceStartup;
                if (Taunts != null && Taunts.ThisDialogue != null && Taunts.ThisDialogue.Count > 0)
                    Dialogue.SingleRandomDialogueSelect(Taunts, MyCharacter);
            }
        }
    }
    /// <summary>
    /// Assign a random position
    /// </summary>
    public void GotoRandomDestination()
    {
        destination = Random.Range(0, BossPathNodes.path.Count);
        agent.destination = DesiredPosition;
    }
    /// <summary>
    /// Goto target position
    /// </summary>
    public void GotoTarget()
    {
        if (Target != null)
            agent.destination = Target.position;
    }
    /// <summary>
    /// Attempt to shoot at the target
    /// </summary>
    public void TryShootTarget()
    {
        float distToTarget = (Target.position - transform.position).magnitude;

        Vector3 lookDir = (Target.position - transform.position); lookDir.y = 0; lookDir = lookDir.normalized;
        MyCharacter.RotationEuler = Quaternion.LookRotation(lookDir, Vector3.up).eulerAngles;
        
        if (distToTarget <= BossAttackRange)
        {
            if (!MyCharacter.GunsOut)
                MyCharacter.GunsOut = true;

            //determine if missed
            bool NPC_Missed = Random.Range(0.0f, 1.0f) < Constants.NPCMissRate;
            //fire weapons if there are any weapons
            if (MyCharacter.R_weapon != null)
            {
                ItemInteract.Shoot(MyCharacter, true, Target.position, true, NPC_Missed);
            }
            if (MyCharacter.L_weapon != null)
                ItemInteract.Shoot(MyCharacter, false, Target.position, true, NPC_Missed);
        }
    }

    bool ApproximateEqual(Vector3 v1, Vector3 v2)
    {
        return Mathf.Abs(v1.x - v2.x) < 0.1f && Mathf.Abs(v1.y - v2.y) < 0.1f && Mathf.Abs(v1.z - v2.z) < 0.1f;
    }
    /// <summary>
    /// Try to activate a random ability
    /// </summary>
    void TryRandAbility()
    {
        if (InAbility)
            timeOfLastAbility = Time.realtimeSinceStartup;

        if (Time.realtimeSinceStartup - timeOfLastAbility > AbilityFrequency && !InAbility)
        {
            timeOfLastAbility = Time.realtimeSinceStartup;

            //get list of abilities to be used
            List<BossAbility> ActiveAbilities = new List<BossAbility>();
            if (SpawnMinions.IsUseable) ActiveAbilities.Add(SpawnMinions);
            if (RocketBarrage.IsUseable) ActiveAbilities.Add(RocketBarrage);
            if (GrenadeRain.IsUseable) ActiveAbilities.Add(GrenadeRain);
            if (HealStations.IsUseable) ActiveAbilities.Add(HealStations);
            if (TeleportPads.IsUseable) ActiveAbilities.Add(TeleportPads);
            if (Push.IsUseable) ActiveAbilities.Add(Push);
            if (Pull.IsUseable) ActiveAbilities.Add(Pull);

            //do the ability
            if (ActiveAbilities.Count > 0)
            {
                ActiveAbilities[Random.Range(0, ActiveAbilities.Count)].DoAbility(this);
            }
        }
    }
}

[System.Serializable]
public class BossAbility
{
    /// <summary>
    /// Do the ability whatever it is
    /// </summary>
    /// <param name="boss"></param>
    public virtual void DoAbility(BossAI boss)
    {

    }

    /// <summary>
    /// Can the ability be used?
    /// </summary>
    public virtual bool IsUseable
    {
        get
        {
            return false;
        }
    }
}

[System.Serializable]
public class MinionAbility : BossAbility
{
    [Tooltip("Boss Spawns Minions from these spawners. Doesn't use this ability if list is empty")]
    public List<ZoneSettings> MinionSpawners;
    [Tooltip("Spawn minions for x seconds")]
    public int MinionSpawnAmountPerZone = 3;
    [Tooltip("Says this dialogue on activate. Only one of the strings in the list of dialogue strings will be randomly used.")]
    public Dialogue SaysOnActivate;

    public override bool IsUseable
    {
        get
        {
            return MinionSpawners != null && MinionSpawners.Count > 0;
        }
    }

    public override void DoAbility(BossAI boss)
    {
        //If there is dialogue....
        if (SaysOnActivate != null && SaysOnActivate.ThisDialogue != null && SaysOnActivate.ThisDialogue.Count > 0)
            Dialogue.SingleRandomDialogueSelect(SaysOnActivate, boss.GetComponent<CharacterData>());

        //Spawn NPCs
        foreach (ZoneSettings z in MinionSpawners)
        {
            z.IsSpawning = true;
            z.InstaSpawn(MinionSpawnAmountPerZone);
            z.IsSpawning = false;
        }
    }
}

[System.Serializable]
public class RocketBarrageAbility : BossAbility
{
    [Tooltip("Set to zero to disable ability")]
    public int NumRocketsPerSecond = 0;
    public float Duration = 0.0f;
    public float DamagePerRocket = 0.0f;
    [Tooltip("Says this dialogue on activate. Only the first dialogue is used. No responses.")]
    public Dialogue SaysOnActivate;

    public override bool IsUseable
    {
        get
        {
            return NumRocketsPerSecond > 0;
        }
    }
}

[System.Serializable]
public class GrenadeRainAbility : BossAbility
{
    [Tooltip("set to zero to disbale ability")]
    public int NumGrenadesSecond = 0;
    public float Duration = 0.0f;
    public float DamagePerGrenade = 0.0f;
    [Tooltip("Says this dialogue on activate. Only the first dialogue is used. No responses.")]
    public Dialogue SaysOnActivate;

    public override bool IsUseable
    {
        get
        {
            return NumGrenadesSecond > 0;
        }
    }
}

[System.Serializable]
public class HealStationsAbility : BossAbility
{
    
}

[System.Serializable]
public class TeleportPadsAbility : BossAbility
{

}

[System.Serializable]
public class PushPlayerAbility : BossAbility
{
    [Tooltip("Set to zero to disbale ability")]
    public float PushStrength = 0.0f;
    [Tooltip("Says this dialogue on activate. Only the first dialogue is used. No responses.")]
    public Dialogue SaysOnActivate;

    public override bool IsUseable
    {
        get
        {
            return PushStrength > 0.01f;
        }
    }
}

[System.Serializable]
public class PullPlayerAbility : BossAbility
{
    [Tooltip("Set to zero to disable ability")]
    public float PullWithinRange = 0.0f;
    [Tooltip("Says this dialogue on activate. Only the first dialogue is used. No responses.")]
    public Dialogue SaysOnActivate;

    public override bool IsUseable
    {
        get
        {
            return PullWithinRange > 0.001f;
        }
    }
}