﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerInfo
{
    public int Health;
    public int HealthPacks;

    public Factions faction;
    public Personality personality;

    public string hat, face, hair, lipstick, nose, earing, shirt, pants, shoes, arms, glasses, eyecolor, outereye;
    public string LWeapon, RWeapon;

    public string firstName, lastName;
    public string StaticKey;

    public int Money;
    public int VehicleID;
    public bool isPassenger;
    public bool ReverseDirection;
    public int PathNumber;
    public string Spawner;
    public string CurrentZone;
    public int Destination;

    public bool SpawnedInVehicle;

    /// <summary>
    /// This value is only used for static characters
    /// </summary>
    public bool isActive;

    public bool ForcedGunsOut;
    public bool isImmortal;
    public bool isPlayer;
    public bool CanSpeak;

    public Vector3 Position;
    public Vector3 RotationEuler;

    /// <summary>
    /// Returns a new PlayerInfo, that contains the default settings.
    /// This is only used for the Player. Also changes difficulty to 3.
    /// </summary>
    public static PlayerInfo Default(bool isMale, string FirstName, string LastName)
    {
        PlayerInfo p = new PlayerInfo();

        p.faction = Factions.Character;
        p.personality = Personality.AwesomeSauce;

        p.Health = Constants.PlayerMaxHealth;
        p.HealthPacks = 0;

        p.firstName = FirstName; p.lastName = LastName;

        p.Money = 100;

        if (isMale)
        {
            p.pants = "Legs-Blue"; p.shirt = "MaleTorso-Colorless"; p.face = "Face-MediumSkin";
            p.arms = "RegularArms-MediumSkin"; p.shoes = "Feet-Black"; p.hair = "Hair-Guy-Spiked-Black";
            p.outereye = "OuterEye"; p.eyecolor = "EyeColor-Green";
        }
        else
        {
            p.pants = "Legs-Blue"; p.shirt = "FemaleTorso-Colorless"; p.face = "Face-MediumSkin";
            p.arms = "RegularArms-MediumSkin"; p.shoes = "Feet-Black"; p.hair = "Hair-Girl-PonyTail-Red";
            p.outereye = "OuterEye"; p.eyecolor = "EyeColor-LightBlue";
        }

        p.Position = new Vector3(30.0f, 0.23f, 2.5f);

        Constants.Difficulty = 3;

        return p;
    }
}

[System.Serializable]
public class VehicleInfo
{
    public int Health;
    public string vehicleSkin;
    public bool stationaryVehicle;
    public float VehicleSpeed;
    public Vector3 Position;
    public Vector3 RotationEuler;
    public int PathNumber;
    public string Spawner;
    public int VehicleID;
    public bool vehicleNeedsReset;
}