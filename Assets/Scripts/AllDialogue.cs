﻿using UnityEngine;
using System.Collections.Generic;

public class AllDialogue : MonoBehaviour
{
    static readonly int E_len = System.Enum.GetValues(typeof(DialogueEventType)).Length;
    /// <summary>
    /// All dialogues in an array for which each element (list) pertains to Dialogues relating to an event
    /// </summary>
    static List<Dialogue>[] dialogues_by_event;

    public Dialogue foreign1, foreign2;

    /// <summary>
    /// use this list when searching
    /// </summary>
    private static List<Dialogue> search_list = new List<Dialogue>();

    private static AllDialogue instance;

	// Use this for initialization
	void Awake ()
    {
        instance = this;

        dialogues_by_event = new List<Dialogue>[E_len];

        for (int i = 0; i < E_len; i++)
        {
            dialogues_by_event[i] = new List<Dialogue>();
        }

        Dialogue[] all_d = GetComponentsInChildren<Dialogue>();
        foreach (Dialogue d in all_d)
        {
            //not sure how....
            if ((int)d.ThisDialogueEventType >= E_len)
                continue;
            dialogues_by_event[(int)d.ThisDialogueEventType].Add(d);
        }
	}

    /// <summary>
    /// Returns a random Dialogue. Could return null based on inputs. The input Character's personality is taken
    /// into account.
    /// </summary>
    /// <param name="myDialogue"> The character who will speak. </param>
    /// <param name="eventType"> Restrict dialogue options to only this event</param>
    /// <param name="onlyThisFactionDialogue"> Restrict DIalogue options to only this faction </param>
    /// <returns></returns>
    public static Dialogue GetRandDialogue(Personality character_personality = Personality.none,
        DialogueEventType eventType = DialogueEventType.General,
        Factions onlyThisFactionDialogue = Factions.none)
    {
        search_list.Clear();
        //for all dialogues in this event
        foreach (Dialogue d in dialogues_by_event[(int)eventType])
        {
            //if a dialogue matches the inputs, or if the inputs are generic
            if ((d.PersonalityType == character_personality || character_personality == Personality.none ||
                d.PersonalityType == Personality.none) &&
                (onlyThisFactionDialogue == Factions.none || onlyThisFactionDialogue == d.FactionDialogue ||
                d.FactionDialogue == Factions.none))
            {
                search_list.Add(d);
            }
        }
        return search_list.Count == 0 ? null : search_list[Random.Range(0, search_list.Count)];
    }

    /// <summary>
    /// Will always return a non-null Dialogue unless no dialogues exist.
    /// </summary>
    /// <param name="character_personality"></param>
    /// <param name="eventType"></param>
    /// <param name="onlyThisFactionDialogue"></param>
    /// <returns></returns>
    public static Dialogue GetRandDialogueMustReturn(Personality character_personality = Personality.none,
    DialogueEventType eventType = DialogueEventType.General,
    Factions onlyThisFactionDialogue = Factions.none)
    {
        search_list.Clear();
        //for all dialogues in this event
        foreach (Dialogue d in dialogues_by_event[(int)eventType])
        {
            //if a dialogue matches the inputs, or if the inputs are generic
            if ((d.PersonalityType == character_personality || character_personality == Personality.none) &&
                (onlyThisFactionDialogue == Factions.none || onlyThisFactionDialogue == d.FactionDialogue))
            {
                search_list.Add(d);
            }
        }

        if (search_list.Count == 0)
        {
            //for all dialogues in this event
            foreach (Dialogue d in dialogues_by_event[(int)eventType])
            {
                search_list.Add(d);
            }

            if (search_list.Count == 0)
            {
                for (int i = 0; i < E_len; i++)
                    foreach (Dialogue d in dialogues_by_event[i])
                    {
                        search_list.Add(d);
                    }

                if (search_list.Count == 0)
                {
                    Debug.Log("No Dialogues exist.");
                    return null;
                }
            }
        }
        return search_list[Random.Range(0, search_list.Count)];
    }

    /// <summary>
    /// Returns random foreigner dialogue
    /// </summary>
    /// <returns></returns>
    public static Dialogue RandForeignerDialogue()
    {
        return Random.Range(0, 2) == 1 ? instance.foreign1 : instance.foreign2;
    }
}
