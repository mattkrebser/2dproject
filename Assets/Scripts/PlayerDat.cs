﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class PlayerDat : MonoBehaviour
{
    public PlayerInfo playerInfo;
    public PlayerInfo2 playerInfo2;

    /// <summary>
    /// Set loadout based on Key Number ie, the keyboard keys 1,2,3,4,5,6,7,8,9,0
    /// </summary>
    /// <param name="KeyNumberPressed"></param>
    public void SetLoadout(int KeyNumberPressed)
    {
        if (!GameSettings.IsInGame)
            return;

        if (!playerInfo2.GunsAllowed)
            return;

        CharacterData c = GetComponent<CharacterData>();

        if (c.IsDead)
            return;

        if (KeyNumberPressed == 0)
        {
            c.L_weapon = null;
            c.R_weapon = null;
            playerInfo2.ActiveLWeapon = 9;
            playerInfo2.ActiveRWeapon = 9;
            return;
        }

        if (KeyNumberPressed > 9)
            return;

        if (playerInfo2.RWeaponsLoadOut[KeyNumberPressed - 1] != null ||
            playerInfo2.LWeaponsLoadOut[KeyNumberPressed - 1] != null)
        {
            c.L_weapon = playerInfo2.LWeaponsLoadOut[KeyNumberPressed - 1];
            c.R_weapon = playerInfo2.LWeaponsLoadOut[KeyNumberPressed - 1];
            playerInfo2.ActiveLWeapon = KeyNumberPressed - 1;
            playerInfo2.ActiveRWeapon = KeyNumberPressed - 1;
        }
    }

    public void SetWeaponAmmo(string Weapon, int newValue)
    {
        if (String.IsNullOrEmpty(Weapon))
            return;
        if (Weapon.Contains("Bazooka"))
            playerInfo2.Rockets = newValue;
        else if (Weapon.Contains("Grenade"))
            playerInfo2.Grenades = newValue;
        else
            playerInfo2.Ammo = newValue;
    }
    public int GetWeaponAmmo(string Weapon)
    {
        if (String.IsNullOrEmpty(Weapon))
            return 0;
        if (Weapon.Contains("Bazooka"))
            return playerInfo2.Rockets;
        else if (Weapon.Contains("Grenade"))
            return playerInfo2.Grenades;
        else
            return playerInfo2.Ammo;
    }

    public bool OwnsWeapon(string Weapon, bool isRight)
    {
        if (string.IsNullOrEmpty(Weapon))
            return false;
        if (!isRight)
        {
            for (int i = 0; i < playerInfo2.LWeapons.Count; i++)
                if (playerInfo2.LWeapons[i].Contains(Weapon))
                    return true;
        }
        else
        {
            for (int i = 0; i < playerInfo2.RWeapons.Count; i++)
                if (playerInfo2.RWeapons[i].Contains(Weapon))
                    return true;
        }
        return false;
    }
    public void AddWeapon(string Weapon, bool IsRight)
    {
        if (string.IsNullOrEmpty(Weapon))
            return;

        if (IsRight)
        {
            playerInfo2.RWeapons.Add(Weapon);
        }
        else
        {
            playerInfo2.LWeapons.Add(Weapon);
        }
    }

    int GetWeaponPosition(List<string> weapons, string weapon)
    {
        for (int i = 0; i < weapons.Count; i++)
        {
            if (weapons[i] == weapon)
                return i;
        }
        return -1;
    }

    public void GetNextLoadoutUp()
    {
        if (!GameSettings.IsInGame)
            return;

        if (!playerInfo2.GunsAllowed)
            return;

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();

        if (c.IsDead)
            return;

        string[] weapons = playerInfo2.RWeaponsLoadOut;
        string[] lweapons = playerInfo2.LWeaponsLoadOut;
        int startHere = playerInfo2.ActiveRWeapon;

        int Max = weapons.Length;
        int i = 0;
        while (i < Max)
        {
            startHere = Clamp(startHere + 1, Max);
            if (startHere == 9)
            {
                c.L_weapon = null; c.R_weapon = null;
                playerInfo2.ActiveLWeapon = playerInfo2.ActiveRWeapon = startHere;
                return;
            }
            else if (!String.IsNullOrEmpty( weapons[startHere]) || !String.IsNullOrEmpty(lweapons[startHere]))
            {
                c.R_weapon = weapons[startHere]; c.L_weapon = lweapons[startHere];
                playerInfo2.ActiveLWeapon = playerInfo2.ActiveRWeapon = startHere;
                return;
            }
            i++;
        }
    }
    public void GetNextLoadoutDown()
    {
        if (!GameSettings.IsInGame)
            return;

        if (!playerInfo2.GunsAllowed)
            return;

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();

        if (c.IsDead)
            return;

        string[] weapons = playerInfo2.RWeaponsLoadOut;
        string[] lweapons = playerInfo2.LWeaponsLoadOut;
        int startHere = playerInfo2.ActiveRWeapon;

        int Max = weapons.Length;
        int i = 0;
        while (i < Max)
        {
            startHere = ClampDown(startHere - 1, Max - 1);
            if (startHere == 9)
            {
                c.L_weapon = null; c.R_weapon = null;
                playerInfo2.ActiveLWeapon = playerInfo2.ActiveRWeapon = startHere;
                return;
            }
            else if (!String.IsNullOrEmpty(weapons[startHere]) || !String.IsNullOrEmpty(lweapons[startHere]))
            {
                c.R_weapon = weapons[startHere]; c.L_weapon = lweapons[startHere];
                playerInfo2.ActiveLWeapon = playerInfo2.ActiveRWeapon = startHere;
                return;
            }
            i++;
        }
    }

    /// <summary>
    /// Can the character shoot his/her gun based on ammo
    /// </summary>
    /// <param name="isRight"></param>
    /// <returns></returns>
    public bool CanShoot(bool isRight)
    {
        if (!playerInfo2.GunsAllowed)
            return false;

        CharacterData c = GetComponent<CharacterData>();
        string weapon;
        if (isRight)
            weapon = c.R_weapon;
        else
            weapon = c.L_weapon;
        int ammo = GetWeaponAmmo(weapon);
        return ammo > 0;
    }

    int Clamp(int val, int Max)
    {
        if (val >= Max)
            return 0;
        else
            return val;
    }
    int ClampDown(int val, int Max)
    {
        if (val < 0)
            return Max;
        else
            return val;
    }

    public void SetGunsAllowed(bool value)
    {
        if (value == false)
            SetLoadout(0);
        playerInfo2.GunsAllowed = value;
    }

    public static void AddMoney(int amount)
    {
        if (CharacterMove.CharacterTransform == null)
            return;

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();

        c.Money += amount;
    }
    public static void AddAmmo(string weapon, int amount)
    {
        if (CharacterMove.CharacterTransform == null)
            return;

        PlayerDat p = CharacterMove.CharacterTransform.GetComponent<PlayerDat>();

        p.SetWeaponAmmo(weapon, p.GetWeaponAmmo(weapon) + amount);
    }
    public static void AddRandomAmmo()
    {
        int r = UnityEngine.Random.Range(0, 10);
        if (r <= 1)
        {
            AddAmmo("Bazooka", UnityEngine.Random.Range(1, 4));
        }
        else if (r <= 3)
        {
            AddAmmo("Grenade", UnityEngine.Random.Range(1, 3));
        }
        else
        {
            AddAmmo("Gun", UnityEngine.Random.Range(5, 25));
        }
    }
}
