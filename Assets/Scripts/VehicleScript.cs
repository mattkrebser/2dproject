﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class VehicleScript : MonoBehaviour {

    /// <summary>
    /// All vehicles
    /// </summary>
    private static Dictionary<string, Sprite> _vehicles;
    private static Dictionary<string, Sprite> vehicles
    {
        get
        {
            if (_vehicles == null)
            {
                _vehicles = new Dictionary<string, Sprite>();
                Sprite[] s_arr = Resources.LoadAll<Sprite>("VehicleSprites");
                foreach (Sprite s in s_arr)
                {
                    _vehicles.Add(s.name, s);
                }
            }

            return _vehicles;
        }
        set
        {
            _vehicles = value;
        }
    }

    private static List<string> _vehicle_paths;
    private static List<string> vehicle_paths
    {
        get
        {
            if (_vehicle_paths == null)
            {
                _vehicle_paths = new List<string>();
                Sprite[] s_arr = Resources.LoadAll<Sprite>("VehicleSprites");
                foreach (Sprite s in s_arr)
                {
                    vehicle_paths.Add(s.name);
                }
            }
            return _vehicle_paths;
        }
        set
        {
            _vehicle_paths = value;
        }
    }

    /// <summary>
    /// All loaded vehicles
    /// </summary>
    public static Dictionary<int, VehicleScript> All_Vehicles = new Dictionary<int, VehicleScript>();
    /// <summary>
    /// Loaded Prefab
    /// </summary>
    public static GameObject VEHICLE_PREFAB
    {
        get
        {
            if (vp == null)
                vp = Resources.Load("Prefabs/Car") as GameObject;
            return vp;
        }
    }
    private static GameObject vp;

    public SpriteRenderer spriteRef;
    private string _VehicleSkin;
    /// <summary>
    /// The Vehicle sprite
    /// </summary>
    public string VehicleSkin
    {
        set
        {
            if (vehicles.ContainsKey(value))
            {
                spriteRef.color = new Color(1, 1, 1, 1);
                spriteRef.sprite = vehicles[value];
                _VehicleSkin = value;
            }
            else
            {
                spriteRef.color = new Color(0, 0, 0, 0);
            }
        }
        get
        {
            return _VehicleSkin;
        }
    }

    /// <summary>
    /// The current health of the vehicle, do not use this to modify the health,
    /// use 'Health' instead
    /// </summary>
    private int health = 1100;
    /// <summary>
    /// Used to modify Health
    /// </summary>
    private int CurrentHealth
    {
        get
        {
            return health;
        }
        set
        {
            if (isDead)
                return;

            health = value;

            if (health <= 0)
            {
                isDead = true;
                //kill vehicle
                VehicleExplode.MakeCarExplode(this);
                SpawnedFrom.ReportDeath();

                //kill passengers
                if (driver != null)
                {
                    driver.SetCharHealth(null, -1);
                }
                if (passenger != null)
                {
                    passenger.SetCharHealth(null, -1);
                }

                Destroy(gameObject);
            }
        }
    }
    public int Health
    {
        get
        {
            return health;
        }
    }
    bool isDead = false;
    public float speed;

    /// <summary>
    /// Max health for this vehicle
    /// </summary>
    public int MaxHealth
    {
        get
        {
            if (VehicleSkin == "car1" || VehicleSkin == "car2" || VehicleSkin == "car4")
            {
                return 1000;
            }
            else if (VehicleSkin == "car3" || VehicleSkin == "car5" || VehicleSkin == "car6")
            {
                return 1350;
            }
            else if (VehicleSkin == "car7")
            {
                return 1100;
            }
            else if (VehicleSkin == "car8")
            {
                return 1950;
            }
            return 2000;
        }
    }

    public CharacterData driver;

    public CharacterData passenger;

    /// <summary>
    /// Drivers get parented here
    /// </summary>
    public Transform driver_position;
    /// <summary>
    /// Passenger Gets parent here
    /// </summary>
    public Transform passenger_position;

    public Transform driver_exit1;
    public Transform driver_exit2;
    public Transform driver_exit3;

    public Transform passenger_exit1;
    public Transform passenger_exit2;
    public Transform passenger_exit3;

    [System.NonSerialized]
    public ZoneSettings SpawnedFrom;

    /// <summary>
    /// IF the vehicle is NPC controller, then it has an agent
    /// </summary>
    [System.NonSerialized]
    public NavMeshAgent vehicleAgent;
    /// <summary>
    /// The AI path for this vehicle
    /// </summary>
    [System.NonSerialized]
    public List<Transform> vehicleRoute;

    /// <summary>
    /// Radius to check for free space to exit the vehicle
    /// </summary>
    float exit_radius_check = 0.25f;

    /// <summary>
    /// True if vehicle is spawned without NPC's (stationary)
    /// </summary>
    public bool IsStationary = false;

    public BoxCollider ProjectileCollider;

    public PhysicMaterial parked_friction;
    public PhysicMaterial driving_friction;

    /// <summary>
    /// Used for saving/loading. Generated by the Spawner.
    /// </summary>
    public int VehicleID
    {
        get
        {
            return vid;
        }
        set
        {
            vid = value;
        }
    }
    private int vid;
    /// <summary>
    /// Returns an unused vehicle ID
    /// </summary>
    public int UniqueVehicleID
    {
        get
        {
            int randNum = Random.Range(0, int.MaxValue);
            while (All_Vehicles.ContainsKey(randNum) && randNum != 0) randNum = Random.Range(0, int.MaxValue);
            return randNum;
        }
    }
    [System.NonSerialized]
    /// <summary>
    /// Which path on the spawner is being used? If it is negative, then it has no path.
    /// </summary>
    public int PathID;

    /// <summary>
    /// Returns true if both driver and passenger is null
    /// </summary>
    public bool isEmpty
    {
        get
        {
            return driver == null && passenger == null;
        }
    }

    public MapData MyZone;

    /// <summary>
    /// If someone enters the vehicle, then it needs to be reset...
    /// </summary>
    public bool vehicle_needs_reset = false;

    public void SetHealth(CharacterData c, int value)
    {
        if (c == null)
            return;
        if (driver != null && !driver.IsPlayer && value > 0)
            driver.GetComponent<NPCAI>().AttackedBy = c;
        if (passenger != null && !passenger.IsPlayer && value > 0)
            passenger.GetComponent<NPCAI>().AttackedBy = c;

        CurrentHealth = value;
    }

    /// <summary>
    /// Initialize Vehicle
    /// </summary>
    public void Init()
    {
        if (VehicleID == 0)
        {
            Debug.Log("Error, 0 VehicleID variable");
        }
        All_Vehicles.Add(VehicleID, this);
        StartCoroutine(VehicleResetRoutine());
    }

    /// <summary>
    /// Reset vehicles
    /// </summary>
    /// <returns></returns>
    IEnumerator VehicleResetRoutine()
    {
        yield return 0; yield return 0; yield return 0;
        yield return new WaitForSeconds(5.0f);

        float time_out_of_view = 0.0f;

        while (true)
        {
            //only allow for resets if there is no passengers and car is away from the player
            if (vehicle_needs_reset && driver == null &&
                (CameraFollow.cam.transform.position - transform.position).magnitude > 8)
            {
                time_out_of_view += 1.0f;
                //more than 30.0 seconds, reset
                if (time_out_of_view > 30.0f)
                {
                    if (passenger != null)
                        Destroy(passenger);
                    //destroy this and get a new one
                    SpawnedFrom.ReportDeath();
                    break;
                }
            }
            else
            {
                time_out_of_view = 0.0f;
            }
            yield return new WaitForSeconds(1.0f);
        }
        //If the loop is exited, then destroy!
        Destroy(gameObject);
    }

    /// <summary>
    /// Returns a random vehicle unless parameters are supplied
    /// </summary>
    /// <param name="vehicleSkin"></param>
    /// <param name="vehicleHealth"></param>
    /// <param name="vehicleSpeed"></param>
    /// <returns></returns>
    public static VehicleScript GetNewRandVehicle(Factions fac, string vehicleSkin = null, int vehicleHealth = -1, float vehicleSpeed = -1f)
    {
        GameObject new_car = Instantiate(VEHICLE_PREFAB);
        VehicleScript v = new_car.GetComponent<VehicleScript>();

        if (fac == Factions.TheLaw)
        {
            v.VehicleSkin = "car8";
        }
        else if (fac == Factions.Dave)
        {
            v.VehicleSkin = "car7";
        }
        else if (vehicleSkin != null)
        {
            v.VehicleSkin = vehicleSkin;
        }
        else
        {
            v.VehicleSkin = vehicle_paths[Random.Range(0, 6)];
        }

        if (v.VehicleSkin == "car1" || v.VehicleSkin == "car2" || v.VehicleSkin == "car4")
        {
            v.CurrentHealth = 1000;
            v.speed = 3.9f;
        }
        else if (v.VehicleSkin == "car3" || v.VehicleSkin == "car5" || v.VehicleSkin == "car6")
        {
            v.CurrentHealth = 1350;
            v.speed = 3.4f;
        }
        else if (v.VehicleSkin == "car7")
        {
            v.speed = 3.9f;
            v.CurrentHealth = 1100;
        }
        else if (v.VehicleSkin == "car8")
        {
            v.speed = 3.35f;
            v.health = 1950;
        }
        else if (v.VehicleSkin == null)
        {
            Debug.Log("Unkown Car!");
        }

        if (vehicleHealth != -1)
        {
            v.CurrentHealth = vehicleHealth;
        }
        if (vehicleSpeed != -1f)
        {
            v.speed = vehicleSpeed;
        }

        return v;
    }

    public void OnDestroy()
    {
        All_Vehicles.Remove(VehicleID);
    }

    /// <summary>
    /// Enter the vehicle
    /// </summary>
    /// <param name="c"></param>
    public void EnterVehicle(CharacterData c, bool force_driver = false, bool force_passenger = false)
    {
        vehicle_needs_reset = true;
        //if force driver, or there is no driver
        if ((driver == null || force_driver) && !force_passenger)
        {
            GetComponent<CapsuleCollider>().material = driving_friction;

            c.InVehicle = this;

            //Kick out current driver if there is one
            if (driver != null)
            {
                Vector3 pos;
                //cant enter if the driver can't leave. (very rare case)
                if (!can_exit(true, out pos)) return;
                //make current driver leave
                LeaveVehicle(driver);
            }

            //disable collider
            c.CharacterCollider.enabled = false;

            //set driver reference
            driver = c;

            //if this is the character
            if (c.faction == Factions.Character)
            {
                driver_position.localPosition = 
                    new Vector3(driver_position.localPosition.x, -0.15f, driver_position.localPosition.z);

                //remove the rigidbody of the character
                Destroy(c.GetComponent<Rigidbody>());
                c.GetComponent<NavMeshObstacle>().enabled = false;
                if (GetComponent<Rigidbody>() == null)
                {
                    Rigidbody r = gameObject.AddComponent<Rigidbody>();
                    r.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX |
                        RigidbodyConstraints.FreezeRotationZ;
                    r.mass = 12;
                    r.angularDrag = 0.6f;
                }
                GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
            }
            //if this is an NPC
            else
            {
                if (GetComponent<Rigidbody>() != null)
                    Destroy(GetComponent<Rigidbody>());
                c.GetComponent<NavMeshAgent>().enabled = false;
                GetComponent<NavMeshObstacle>().enabled = false;
                NavMeshAgent agent = gameObject.AddComponent<NavMeshAgent>();
                agent.avoidancePriority = 40;
                agent.obstacleAvoidanceType = ObstacleAvoidanceType.MedQualityObstacleAvoidance;
                vehicleAgent = agent;
                agent.radius = 0.5f; agent.autoBraking = false; agent.speed = speed;
                agent.baseOffset = 0.36f;
                c.GetComponent<AIMove>().EnteredVehicle();

                driver_position.localPosition =
                    new Vector3(driver_position.localPosition.x, -0.36f, driver_position.localPosition.z);
            }

            c.transform.SetParent(driver_position);
            c.transform.position = driver_position.position;
        }
        else if (passenger == null || force_passenger)
        {
            c.InVehicle = this;

            //make passenger leave if there is one
            if (passenger != null)
            {
                Vector3 pos;
                //cant enter if the driver can't leave. (very rare case)
                if (!can_exit(false, out pos)) return;
                //make current driver leave
                LeaveVehicle(passenger);
            }

            //disable collider
            c.CharacterCollider.enabled = false;

            //set driver reference
            passenger = c;

            //if this is the character
            if (c.faction == Factions.Character)
            {
                //remove the rigidbody of the character
                Destroy(c.GetComponent<Rigidbody>());
                //disable character's obstacle component
                c.GetComponent<NavMeshObstacle>().enabled = false;
                passenger_position.localPosition =
                    new Vector3(passenger_position.localPosition.x, -0.15f, passenger_position.localPosition.z);
            }
            //if this is an NPC
            else
            {
                c.GetComponent<NavMeshAgent>().enabled = false;
                passenger_position.localPosition = 
                    new Vector3(passenger_position.localPosition.x, -0.36f, passenger_position.localPosition.z);
            }

            c.transform.SetParent(passenger_position);
            c.transform.position = passenger_position.position;

        }
    }

    /// <summary>
    /// Leave the vehicle
    /// </summary>
    /// <param name="c"></param>
    public void LeaveVehicle(CharacterData c)
    {
        vehicle_needs_reset = true;
        //if the input character is the driver..
        if (ReferenceEquals(c, driver))
        {
            Vector3 exit_pos;
            if (can_exit(true, out exit_pos))
            {
                GetComponent<CapsuleCollider>().material = parked_friction;

                //unparent from the car
                c.transform.SetParent(null);
                //assign to exit position that we found
                c.transform.position = exit_pos;

                //if this is the player
                if (c.faction == Factions.Character)
                {
                    //add the rigidbody back
                    Rigidbody rbod = c.gameObject.AddComponent<Rigidbody>();
                    //apply contstraints
                    rbod.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX |
                    RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
                    //assign rbod variable
                    c.gameObject.GetComponent<CharacterMove>().rbod = rbod;
                    //enable obstacle
                    c.GetComponent<NavMeshObstacle>().enabled = true;
                    //reset slow speed
                    c.GetComponent<CharacterMove>().slow_speed = 1.0f;
                    rbod.interpolation = RigidbodyInterpolation.Interpolate;
                    rbod.collisionDetectionMode = CollisionDetectionMode.Continuous;
                }
                else
                {
                    Destroy(GetComponent<NavMeshAgent>());
                    c.GetComponent<NavMeshAgent>().enabled = true;
                    c.GetComponent<AIMove>().ExitedVehicle();
                    c.GetComponent<AIMove>().slow_speed = 1.0f;
                    GetComponent<NavMeshObstacle>().enabled = true;
                    vehicleAgent = null;
                }

                c.CharacterCollider.enabled = true;
                driver = null;

                //The car no longer has a driver, add the rigidbody back
                if (GetComponent<Rigidbody>() == null)
                {
                    Rigidbody r = gameObject.AddComponent<Rigidbody>();
                    r.constraints = RigidbodyConstraints.FreezeRotationX |
                        RigidbodyConstraints.FreezeRotationZ;
                    r.mass = 12;
                    r.angularDrag = 0.6f;
                }

                GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.None;

                //not in a vehicle anymore
                c.InVehicle = null;

                driver_position.localPosition 
                    = new Vector3(driver_position.localPosition.x, -0.36f, driver_position.localPosition.z);
            }
        }
        //otherwise
        else
        {
            Vector3 exit_pos;
            if (can_exit(false, out exit_pos))
            {
                //unparent from the car
                c.transform.SetParent(null);
                //assign to exit position that we found
                c.transform.position = exit_pos;

                //if this is the player
                if (c.faction == Factions.Character)
                {
                    //add the rigidbody back
                    Rigidbody rbod = c.gameObject.AddComponent<Rigidbody>();
                    //apply contstraints
                    rbod.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX |
                    RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
                    //assign rbod variable
                    c.gameObject.GetComponent<CharacterMove>().rbod = rbod;
                    //enable obstacle
                    c.GetComponent<NavMeshObstacle>().enabled = true;
                    //reset slow speed
                    c.GetComponent<CharacterMove>().slow_speed = 1.0f;
                    rbod.interpolation = RigidbodyInterpolation.Interpolate;
                    rbod.collisionDetectionMode = CollisionDetectionMode.Continuous;
                }
                else
                {
                    c.GetComponent<NavMeshAgent>().enabled = true;
                    c.GetComponent<AIMove>().ExitedVehicle();
                    c.GetComponent<AIMove>().slow_speed = 1.0f;
                }

                c.CharacterCollider.enabled = true;
                passenger = null;

                //not in a vehicle anymore
                c.InVehicle = null;

                passenger_position.localPosition = 
                    new Vector3(passenger_position.localPosition.x, -0.36f, passenger_position.localPosition.z);
            }
        }
    }

    /// <summary>
    /// Returns true if the charater can exit the vehicle and outputs the exit point
    /// </summary>
    /// <param name="isDriver"></param>
    /// <param name="exit_pos"></param>
    /// <returns></returns>
    bool can_exit(bool isDriver, out Vector3 exit_pos)
    {
        int layermask = 1 << LayerMask.NameToLayer("Walls") | 1 << LayerMask.NameToLayer("Character") |
            LayerMask.NameToLayer("VehicleMove");

        if (isDriver)
        {
            if (!Physics.CheckSphere(driver_exit1.position, exit_radius_check, layermask))
            {
                exit_pos = driver_exit1.position;
                return true;
            }
            if (!Physics.CheckSphere(driver_exit2.position, exit_radius_check, layermask))
            {
                exit_pos = driver_exit2.position;
                return true;
            }
            if (!Physics.CheckSphere(driver_exit3.position, exit_radius_check, layermask))
            {
                exit_pos = driver_exit3.position;
                return true;
            }
            if (!Physics.CheckSphere(passenger_exit1.position, exit_radius_check, layermask))
            {
                exit_pos = passenger_exit1.position;
                return true;
            }
            if (!Physics.CheckSphere(passenger_exit2.position, exit_radius_check, layermask))
            {
                exit_pos = passenger_exit2.position;
                return true;
            }
            if (!Physics.CheckSphere(passenger_exit3.position, exit_radius_check, layermask))
            {
                exit_pos = passenger_exit3.position;
                return true;
            }
            exit_pos = transform.position;
            return false;
        }
        else
        {
            if (!Physics.CheckSphere(passenger_exit1.position, exit_radius_check, layermask))
            {
                exit_pos = passenger_exit1.position;
                return true;
            }
            if (!Physics.CheckSphere(passenger_exit2.position, exit_radius_check, layermask))
            {
                exit_pos = passenger_exit2.position;
                return true;
            }
            if (!Physics.CheckSphere(passenger_exit3.position, exit_radius_check, layermask))
            {
                exit_pos = passenger_exit3.position;
                return true;
            }
            if (!Physics.CheckSphere(driver_exit1.position, exit_radius_check, layermask))
            {
                exit_pos = driver_exit1.position;
                return true;
            }
            if (!Physics.CheckSphere(driver_exit2.position, exit_radius_check, layermask))
            {
                exit_pos = driver_exit2.position;
                return true;
            }
            if (!Physics.CheckSphere(driver_exit3.position, exit_radius_check, layermask))
            {
                exit_pos = driver_exit3.position;
                return true;
            }
            exit_pos = transform.position;
            return false;
        }
    }
}
