﻿using UnityEngine;
using System.Collections.Generic;

public class AIPath : MonoBehaviour {

    [Tooltip("Drag GameObject transform onto the list to make a path. Then DRag this path" +
        " onto a ZoneSettings to apply it")]
    public List<Transform> path;

    public bool show;
    [Tooltip("50-50 chance the AI will walk its path in reverse")]
    public bool allow_reverse = true;
}
