﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Quest : MonoBehaviour
{
    [System.NonSerialized]
    public static Dictionary<string, Quest> AllQuests = new Dictionary<string, Quest>();

    public string QuestName;
    public bool ResetOnComplete = false;

    public List<QuestEvent> Events = new List<QuestEvent>();


    public int CurrentEvent;
    [HideInInspector]
    public bool IsCompleted;
    [HideInInspector]
    public bool Started;
    [Tooltip("This quest is automatically started")]
    public bool AutoStarted = false;

    [Tooltip("Should progress on this quest be saved to the disk?")]
    public bool SaveQuestProgressOnQuit = true;

    [Tooltip("The interact for events on this quest will override low priority quests. A good use for this is NPC" +
        " Dialogue where some dialogue needs to be overriden")]
    public bool isHighPriorityQuest = true;

    [Tooltip("If true, this quest will appear in the player's Journal, and will be eligable for Way point marking")]
    public bool QuestAppearsInPlayerJournal = false;

    [System.NonSerialized]
    public GameObject MiniMapPointer;

    // Use this for initialization
    void Start ()
    {
        if (SaveQuestProgressOnQuit || QuestAppearsInPlayerJournal)
            AllQuests.Add(QuestName, this);
        if (AutoStarted)
        {
            Started = true;
        }
	}

    void OnDestroy()
    {
        AllQuests.Remove(QuestName);
    }

    /// <summary>
    /// Used to start the quest after being loaded from disk
    /// </summary>
    void Init()
    {
        if (!IsCompleted && Started && Events.Count > 0 && CurrentEvent < Events.Count)
        {
            Events[CurrentEvent].StartEvent();
            if (Events[CurrentEvent].AutomaticAdvance)
                NextEvent();
        }
    }

    /// <summary>
    /// Start the next event.
    /// </summary>
    public void NextEvent()
    {
        CurrentEvent++;
        //if this was the last event..
        if (CurrentEvent >= Events.Count)
        {
            IsCompleted = true;
            //if reset...
            if (ResetOnComplete)
                ResetQuest();
        }
        else
        {
            while (Events[CurrentEvent].AutomaticAdvance)
            {
                Events[CurrentEvent].StartEvent();
                CurrentEvent++;
                //if this was the last event...
                if (CurrentEvent >= Events.Count)
                {
                    IsCompleted = true;
                    //if reset
                    if (ResetOnComplete)
                        ResetQuest();
                    return;
                }
            }
            Events[CurrentEvent].StartEvent();
        }
    }
    /// <summary>
    /// The most recent checkpoint from CurrentEvent(inclusive) to 0(inclusive).
    /// Returns 0 if none of them are checkpoints
    /// </summary>
    int MostRecentCheckPoint
    {
        get
        {
            if (Events == null || Events.Count == 0)
                return 0;
            if (Events.Count <= CurrentEvent || IsCompleted)
                return Events.Count;
            if (Events[CurrentEvent].isCheckPoint || CurrentEvent == 0)
                return CurrentEvent;
            for (int i = CurrentEvent - 1; i >= 0; i--)
            {
                if (Events[i].isCheckPoint)
                    return i;
            }

            //Unless the entire quest chain starting from
            //current event is a non-checkpoint
            return 0;
        }
    }
    QuestDat GetDatForSave()
    {
        QuestDat d = new QuestDat();
        d.Stage = MostRecentCheckPoint;
        d.completed = IsCompleted;
        d.started = Started;
        d.QuestName = QuestName;
        return d;
    }
    void SetDat(QuestDat d)
    {
        CurrentEvent = d.Stage;
        IsCompleted = d.completed;
        Started = d.started;
        if (AutoStarted)
            Started = true;
    }
    /// <summary>
    /// Get/set current quests. This is used for loading/getting disk information
    /// </summary>
    public static List<QuestDat> AllQuestsDat
    {
        get
        {
            List<QuestDat> qd = new List<QuestDat>();
            foreach (Quest q in AllQuests.Values)
            {
                if (q.SaveQuestProgressOnQuit)
                    qd.Add(q.GetDatForSave());
            }
            return qd;
        }
        set
        {
            if (value == null)
                return;

            foreach (QuestDat qd in value)
            {
                AllQuests[qd.QuestName].SetDat(qd);
            }
        }
    }
    /// <summary>
    /// Initialize the Quests, Usually called after loading in a game
    /// </summary>
    public static void LateInitQuests()
    {
        CoroutineHelper.RunCoroutine(LateInitRoutine());
    }
    private static IEnumerator LateInitRoutine()
    {
        yield return 0; yield return 0; yield return 0; yield return 0;
        yield return 0; yield return 0; yield return 0; yield return 0;
        foreach (Quest q in AllQuests.Values)
        {
            q.Init();
        }
    }

    /// <summary>
    /// Resets the quest.
    /// </summary>
    public void ResetQuest()
    {
        if (AutoStarted)
            Started = true;
        else
            Started = false;

        IsCompleted = false;
        CurrentEvent = 0;

        if (Events == null || Events.Count == 0)
            return;

        foreach (QuestEvent qe in Events)
        {
            qe.ResetEvent();
        }

        if (AutoStarted)
            Events[CurrentEvent].StartEvent();
    }
}
