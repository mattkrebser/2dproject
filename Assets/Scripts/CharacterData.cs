﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class CharacterData : MonoBehaviour {

    /// <summary>
    /// All sprites
    /// </summary>
    private static Dictionary<string, Sprite> sprites;
    /// <summary>
    /// All weapon sprites
    /// </summary>
    private static Dictionary<string, Sprite> weapon_sprites;

    /// <summary>
    /// A list containing all characters, inlucind the player
    /// </summary>
    public static List<CharacterData> AllCharacters = new List<CharacterData>();

    /// <summary>
    /// Dictionary containing only static characters, the key is their scene (instanceID)
    /// </summary>
    public static Dictionary<string, CharacterData> StaticCharacters = new Dictionary<string, CharacterData>();

    public Factions faction;
    public Personality personality;
    public string first_name;
    public string last_name;

    private int _NumHealthPacks;
    public int NumHealthPacks
    {
        get
        {
            return _NumHealthPacks;
        }
        set
        {
            _NumHealthPacks = value;
        }
    }
    /// <summary>
    /// Max allowed health.
    /// </summary>
    public int MaxHealth
    {
        get
        {
            if (myBossAI == null)
                return Constants.PlayerMaxHealth;
            else
                return myBossAI.BossMaxHealth;
        }
    }
    private int _health = 100;
    /// <summary>
    /// Current Health left
    /// </summary>
    private int CurrentHealth
    {
        get
        {
            return _health;
        }
        set
        {
            if (isDead)
                return;
            if (IsImmortal)
                return;
            //if health was increased...
            if (value > _health && _health != 0)
                HealFX.MakeHealFX(this);

            //Call attacked AI routine
            if (value < _health && SpawnedFrom != null && value > 0)
                GetComponent<NPCAI>().WasAttacked();

            //clamp
            _health = value > MaxHealth ? MaxHealth : value;

            //if health is lower than zero, DIE
            if (_health <= 0)
            {
                isDead = true;

                if (InVehicle != null)
                    InVehicle.LeaveVehicle(this);

                //do death FX if player is within 12 distance
                if (CharacterMove.CharacterTransform == null ||
                    (CharacterMove.CharacterTransform.position - transform.position).sqrMagnitude < 144)
                {
                    //death fx
                    BloodSplat.KillCharacterFX(this);
                    SoundFX.PlayAtPosition(transform.position, "bloodsplat", 2.5f);
                }

                //remove a red circle object
                RemoveRedCircle();

                //if this instance was not spawned, then it is the character
                if (IsPlayer)
                {
                    GetComponent<PlayerRespawn>().RespawnRoutine();
                    return;
                }
                //otherwise, NPC
                else
                {
                    if (GetComponent<AIMove>().IsStatic)
                    {
                        ResetStaticNPC();
                        return;
                    }
                    //only report death if spawner was a character spawner
                    if (SpawnedFrom != null && !SpawnedFrom.SpawnVehicles)
                        SpawnedFrom.ReportDeath();

                    //give money/ammo tp player as reward if they did the killing
                    if (GetComponent<NPCAI>().AttackedBy == CharacterMove.Player || GetComponent<NPCAI>() == CharacterMove.Player)
                    {
                        PlayerDat.AddMoney(UnityEngine.Random.Range(1, 10));
                        PlayerDat.AddRandomAmmo();
                    }

                    Destroy(gameObject);
                }
            }
        }
    }
    /// <summary>
    /// current health of this character
    /// </summary>
    public int Health
    {
        get
        {
            return _health;
        }
    }
    bool isDead = false;
    /// <summary>
    /// Are we dead yet?
    /// </summary>
    public bool IsDead
    {
        get
        {
            return isDead;
        }
    }
    /// <summary>
    /// Can this character die?
    /// </summary>
    public bool IsImmortal = false;
    /// <summary>
    /// If true, this character will always have their guns out, regardless of combat status
    /// (does not apply for the player).
   ///  For Boss characters, this value represents if the boss is in combat with the player (true=in combat)
    /// </summary>
    public bool ForcedGunsOut = false;

    /// <summary>
    /// Returns true if this character is carrying a weapon.
    /// </summary>
    public bool IsArmed
    {
        get
        {
            return !String.IsNullOrEmpty(L_weapon) || !String.IsNullOrEmpty(R_weapon);
        }
    }

    public int Money = 25;
    [HideInInspector]
    public int PathNumber = 0;

    /// <summary>
    /// Set the health of the character, the health was modified by the input character
    /// </summary>
    /// <param name="changedBy"></param>
    public void SetCharHealth(CharacterData changedBy, int value)
    {
        //set who attacked us
        if (!IsPlayer && changedBy != null && value > 0)
            GetComponent<NPCAI>().AttackedBy = changedBy;
        //set health
        CurrentHealth = value;
    }
    /// <summary>
    /// Use a single health pack
    /// </summary>
    public void UseHealthPack()
    {
        if (NumHealthPacks > 0)
        {
            SetCharHealth(null, Health + Constants.HealthPackHealAmount);
            NumHealthPacks--;
        }
    }

    //shooting info
    [System.NonSerialized]
    public float RemainingCooldownLeft = 0.0f;
    [System.NonSerialized]
    public float RemainingCooldownRight = 0.0f;

    [Tooltip("The attatched vehicle (if there is one")]
    public VehicleScript InVehicle;

    [Tooltip("The Collider used to collider with the world")]
    public Collider CharacterCollider;

    public ZoneSettings SpawnedFrom;

    [Tooltip("Name of the Hat Tile")]
    public string Hat;
    [Tooltip("Name of the Hair Tile")] //etc...
    public string Hair;
    public string Shirt;
    public string Pants;
    public string Shoes;
    public string L_weapon;
    public string R_weapon;
    public string Glasses;
    public string Earings;
    public string Arms;
    public string Lipstick;
    public string Nose;
    public string Eyecolor;
    public string Facecolor;
    public string Outer_eye;
    public string Blood;

    /// <summary>
    /// Parent sprites to this
    /// </summary>
    public GameObject sprite_child;

    Sprite hat;
    SpriteRenderer hat_renderer;
    Sprite hair;
    SpriteRenderer hair_renderer;
    Sprite shirt;
    SpriteRenderer shirt_renderer;
    Sprite pants;
    SpriteRenderer pants_renderer;
    Sprite shoes;
    SpriteRenderer shoes_renderer;
    Sprite l_weapon;
    SpriteRenderer lweapon_renderer;
    Sprite r_weapon;
    SpriteRenderer rweapon_renderer;
    Sprite glasses;
    SpriteRenderer glasses_renderer;
    Sprite earings;
    SpriteRenderer earings_renderer;
    Sprite arms;
    SpriteRenderer arms_renderer;
    Sprite lipstick;
    SpriteRenderer lipstick_renderer;
    Sprite nose;
    SpriteRenderer nose_renderer;
    Sprite eyecolor;
    SpriteRenderer eyecolor_renderer;
    Sprite facecolor;
    SpriteRenderer facecolor_renderer;
    Sprite outer_eye;
    SpriteRenderer outereye_renderer;
    Sprite blood;
    SpriteRenderer blood_renderer;

    /// <summary>
    /// Returns true if this is a male, Also returns true if it can't be determined
    /// </summary>
    public bool isMale
    {
        get
        {
            if (String.IsNullOrEmpty(Shirt))
                return true;
            return Shirt.Contains("Male");
        }
    }
    /// <summary>
    /// Is this the player?
    /// </summary>
    public bool IsPlayer = false;

    /// <summary>
    /// Character animation controller
    /// </summary>
    private RuntimeAnimatorController controller;

    /// <summary>
    /// Reference to base sprite
    /// </summary>
    public SpriteRenderer animated_sprite;

    /// <summary>
    /// This field is null on all NPC's, reference to CharacterMove component
    /// </summary>
    public CharacterMove character_mover;

    /// <summary>
    /// The calculated rotation Euler of this object. 
    /// </summary>
    public Vector3 RotationEuler
    {
        get
        {
            return !IsPlayer ? transform.eulerAngles : character_mover.LookDirection == Vector3.zero ? Vector3.zero :
            Quaternion.LookRotation(character_mover.LookDirection, Vector3.up).eulerAngles;
        }
        set
        {
            if (!IsPlayer) { transform.eulerAngles = value; }
            else { character_mover.LookDirection = Quaternion.Euler(value) * Vector3.forward; }
        }
    }

    private int current_state;
    /// <summary>
    /// Gets the current Animation number, 0 to 11
    /// </summary>
    public int CurrentAnimationState
    {
        get
        {
            return current_state;
        }
    }
    /// <summary>
    /// The direction that the Character(what the player sees) is facing
    /// </summary>
    public Direction CharacterFacingDirection
    {
        get
        {
            if (current_state >= 9)
                return Direction.West;
            else if (current_state >= 6)
                return Direction.North;
            else if (current_state >= 3)
                return Direction.East;
            else
                return Direction.South;
        }
    }

    /// <summary>
    /// Guns out? This does not reflect wether or not weapons are being dislayed.
    /// </summary>
    [System.NonSerialized]
    public bool GunsOut = false;

    /// <summary>
    /// Left weaponammo. Can only set/get for player. NPC's don't use ammo
    /// </summary>
    public int LWeaponAmmo
    {
        set
        {
            if (IsPlayer)
            {
                if (!String.IsNullOrEmpty(L_weapon))
                    GetComponent<PlayerDat>().SetWeaponAmmo(L_weapon, value);
            }
        }
        get
        {
            if (IsPlayer)
            {
                if (!String.IsNullOrEmpty(L_weapon))
                    return GetComponent<PlayerDat>().GetWeaponAmmo(L_weapon);
            }
            return 0;
        }
    }
    /// <summary>
    /// Right WeaponAmmo, only get/set for player. Npc's dont use ammo
    /// </summary>
    public int RWeaponAmmo
    {
        set
        {
            if (IsPlayer)
            {
                if (!String.IsNullOrEmpty(R_weapon))
                    GetComponent<PlayerDat>().SetWeaponAmmo(R_weapon, value);
            }
        }
        get
        {
            if (IsPlayer)
            {
                if (!String.IsNullOrEmpty(R_weapon))
                    return GetComponent<PlayerDat>().GetWeaponAmmo(R_weapon);
            }
            return 0;
        }
    }

    private NPCAI _npcai;
    /// <summary>
    /// If this is an NPC, this is a reference to an attatched AI
    /// </summary>
    private NPCAI myAI
    {
        get
        {
            if (_npcai == null)
                _npcai = GetComponent<NPCAI>();
            return _npcai;
        }
    }

    /// <summary>
    /// The current red circle object under this character
    /// </summary>
    private GameObject RedCircle;

    private BossAI myBossAI;

    void Awake()
    {
        AllCharacters.Add(this);

        //attempt to find a boss cmponent
        myBossAI = GetComponent<BossAI>();

        //if this is a static character...
        if (!IsPlayer && GetComponent<AIMove>().IsStatic)
        {
            StaticCharacters.Add(gameObject.name, this);
        }

        //get animation controller
        controller =
        (RuntimeAnimatorController)Instantiate(Resources.Load("CharacterAnimationController"));

        //initialize sprite array
        if (sprites == null)
        {
            sprites = new Dictionary<string, Sprite>();
            Sprite[] s_arr = Resources.LoadAll<Sprite>("Sprites");
            foreach (Sprite s in s_arr)
            {
                sprites.Add(s.name, s);
            }
        }
        //initialize weapon sprite array
        if (weapon_sprites == null)
        {
            weapon_sprites = new Dictionary<string, Sprite>();
            Sprite[] s_arr = Resources.LoadAll<Sprite>("SpriteWeapons");
            foreach (Sprite s in s_arr)
            {
                weapon_sprites.Add(s.name, s);
            }
        }

        //assign character mover
        character_mover = GetComponent<CharacterMove>();
    }

    void OnDestroy()
    {
        isDead = true;
        AllCharacters.Remove(this);
        StaticCharacters.Remove(gameObject.name);
    }
    /// <summary>
    /// Vehicle draw priority
    /// </summary>
    /// <returns></returns>
    int GetVehiclePriority()
    {
        bool driver_proc = true;
        //The passenger gets draw priority if the vehicles rotation is between 0 and 180 degrees
        if (InVehicle.transform.eulerAngles.y <= 180 && InVehicle.transform.eulerAngles.y > 0)
        {
            driver_proc = false;
        }
        //If the driver has priority
        if (driver_proc && ReferenceEquals(InVehicle.driver, this))
        {
            return 15;
        }
        else if (!driver_proc && ReferenceEquals(InVehicle.passenger, this))
        {
            return 15;
        }
        return 0;
    }
    /// <summary>
    /// Add the input priority to each render. This is useful to make this character appear above other characters
    /// </summary>
    /// <param name="add_proc"></param>
    void SetRenderingPriorities(int add_proc)
    {
        if (hat_renderer != null && hat_renderer.sortingOrder != 9 + add_proc)
            hat_renderer.sortingOrder = 9 + add_proc;
        if (hair_renderer != null && hair_renderer.sortingOrder != 8 + add_proc)
            hair_renderer.sortingOrder = 8 + add_proc;
        if (shirt_renderer != null && shirt_renderer.sortingOrder != 6 + add_proc)
            shirt_renderer.sortingOrder = 6 + add_proc;
        if (pants_renderer != null && pants_renderer.sortingOrder != 3 + add_proc)
            pants_renderer.sortingOrder = 3 + add_proc;
        if (shoes_renderer != null && shoes_renderer.sortingOrder != 3 + add_proc)
            shoes_renderer.sortingOrder = 3 + add_proc;
        if (glasses_renderer != null && glasses_renderer.sortingOrder != 7 + add_proc)
            glasses_renderer.sortingOrder = 7 + add_proc;
        if (earings_renderer != null && earings_renderer.sortingOrder != 6 + add_proc)
            earings_renderer.sortingOrder = 6 + add_proc;
        if (arms_renderer != null && arms_renderer.sortingOrder != 7 + add_proc)
            arms_renderer.sortingOrder = 7 + add_proc;
        if (lipstick_renderer != null && lipstick_renderer.sortingOrder != 10 + add_proc)
            lipstick_renderer.sortingOrder = 10 + add_proc;
        if (nose_renderer != null && nose_renderer.sortingOrder != 11 + add_proc)
            nose_renderer.sortingOrder = 11 + add_proc;
        if (eyecolor_renderer != null && eyecolor_renderer.sortingOrder != 5 + add_proc)
            eyecolor_renderer.sortingOrder = 5 + add_proc;
        if (facecolor_renderer != null && facecolor_renderer.sortingOrder != 4 + add_proc)
            facecolor_renderer.sortingOrder = 4 + add_proc;
        if (outereye_renderer != null && outereye_renderer.sortingOrder != 6 + add_proc)
            outereye_renderer.sortingOrder = 6 + add_proc;
        if (blood_renderer != null && blood_renderer.sortingOrder != 12 + add_proc)
            blood_renderer.sortingOrder = 12 + add_proc;
    }
    /// <summary>
    /// Seperate Weapon render priority
    /// </summary>
    /// <param name="add_proc"></param>
    void FigureWeaponRenderPriority(int add_proc)
    {
        //On start up, the rotation vector is still zero, it needs a frame to be initialized
        if (character_mover != null && character_mover.LookDirection == Vector3.zero)
            return;

        int add_procR = add_proc;
        int add_procL = add_proc;

        //Get eulerangles rotation
        Vector3 dir = RotationEuler;

        //determine which weapon gets higher priority
        if (dir.y <= 180 && dir.y > 0)
        {
            add_procR++;
        }
        else
        {
            add_procL++;
        }

        //set rendering priorities
        if (lweapon_renderer != null && lweapon_renderer.sortingOrder != 13 + add_procL)
            lweapon_renderer.sortingOrder = 13 + add_procL;
        if (rweapon_renderer != null && rweapon_renderer.sortingOrder != 13 + add_procR)
            rweapon_renderer.sortingOrder = 13 + add_procR;
    }

    void LateUpdate()
    {
        if (IsDead)
        {
            NullAll();
            return;
        }

        //if the player is not initialized yet, then just return
        if (CharacterMove.CharacterTransform == null)
            return;

        //get animation controller
        if (controller == null)
            controller = (RuntimeAnimatorController)Instantiate(Resources.Load("CharacterAnimationController"));

        //If this is not the player, and it is far away, then do nothing
        if (character_mover == null && (transform.position - CharacterMove.CharacterTransform.position).sqrMagnitude > 64)
            return;

        //player always got teh guns
        if (IsPlayer)
            GunsOut = true;
        else
            GunsOut = myAI.GunsOut;

        //update item cooldowns
        RemainingCooldownLeft = RemainingCooldownLeft - Time.deltaTime < 0 ? 0 : RemainingCooldownLeft - Time.deltaTime;
        RemainingCooldownRight = RemainingCooldownRight - Time.deltaTime < 0 ? 0 : RemainingCooldownRight - Time.deltaTime;

        //If this character is driving a vehicle (not a passenger),
        //then add draw priority to all character sprites (so that they appear above the passenger)
        int add_Priority = InVehicle == null ? 0 : GetVehiclePriority();
        SetRenderingPriorities(add_Priority);
        FigureWeaponRenderPriority(add_Priority);

        //Get the current state of the parent character mover.
        //See Assets/Animation/animation.png, each number 0 to 11 corrisponds to a state on the sprite
        current_state = System.Int32.Parse(animated_sprite.sprite.name.Substring(animated_sprite.sprite.name.IndexOf("_") + 1));

        //Set sprites
        if (!String.IsNullOrEmpty(Hat))
        {
            if (hat_renderer == null)
                hat_renderer = AddAnimator(9, "Hat");

            //hair bands are handlded differently than hats
            if (Hat.Contains("HairBand"))
            {
                if (current_state >= 6 && current_state <= 8)
                {
                    hat_renderer.color = new Color(0, 0, 0, 0);
                }
                else
                {
                    //clamp states to numbers to be between 1 to 8 (hair band sprite in Resources.Sprites only has 8 states)
                    //this clamping is common for many of the different sprites
                    int num = current_state >= 9 ? current_state - 3 : current_state;
                    string key = GetNewName(Hat, num);
                    if (!sprites.ContainsKey(key))
                        hat_renderer.color = new Color(0, 0, 0, 0);
                    else
                    {
                        hat = sprites[key];
                        hat_renderer.sprite = hat;
                        hat_renderer.color = new Color(1, 1, 1, 1);
                    }
                }
            }
            else
            {
                string key = GetNewName(Hat, current_state);
                if (!sprites.ContainsKey(key))
                    hat_renderer.color = new Color(0, 0, 0, 0);
                else
                {
                    hat = sprites[key];
                    hat_renderer.sprite = hat;
                    hat_renderer.color = new Color(1, 1, 1, 1);
                }
            }
        }
        else if (hat_renderer != null)
            hat_renderer.color = new Color(0, 0, 0, 0);

        if (!String.IsNullOrEmpty(Hair) && (Hat == null || !Hat.Contains("Hat")))
        {
            if (hair_renderer == null)
            {
                hair_renderer = AddAnimator(8, "Hair");
            }
            string key = GetNewName(Hair, current_state);
            if (!sprites.ContainsKey(key))
                hair_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                //this fixes a visual bug with the afro. TODO: Redo afro textures
                //sprites 9,10,11. The middle (10) is offset up one pixel too high which causes jitters
                if (Hair.Contains("Afro"))
                {
                    int num = current_state < 9 ? current_state : 9;
                    hair = sprites[GetNewName(Hair, num)];
                    hair_renderer.sprite = hair;
                    hair_renderer.color = new Color(1, 1, 1, 1);
                }
                else
                {
                    hair = sprites[GetNewName(Hair, current_state)];
                    hair_renderer.sprite = hair;
                    hair_renderer.color = new Color(1, 1, 1, 1);
                }
            }
        }
        else if (hair_renderer != null)
            hair_renderer.color = new Color(0, 0, 0, 0);

        //only render shirt when in a vehicle
        if (InVehicle == null && !String.IsNullOrEmpty(Shirt))
        {
            if (shirt_renderer == null)
                shirt_renderer = AddAnimator(6, "Shirt");
            string key = GetNewName(Shirt, current_state);
            if (!sprites.ContainsKey(key))
                shirt_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                //fixes visual bug that some female hirts have
                if (Shirt.Contains("Female"))
                {
                    int num = 0;
                    if (current_state >= 9)
                        num = 11;
                    else if (current_state >= 6)
                        num = 8;
                    else if (current_state >= 3)
                        num = 5;
                    else
                        num = 2;
                    shirt_renderer.transform.localPosition = 
                        new Vector3(0.012f, shirt_renderer.transform.localPosition.y,
                        shirt_renderer.transform.localPosition.z);
                    shirt = sprites[GetNewName(Shirt, num)];
                    shirt_renderer.sprite = shirt;
                    shirt_renderer.color = new Color(1, 1, 1, 1);
                }
                else
                {
                    shirt_renderer.transform.localPosition =
                        new Vector3(0.0f, shirt_renderer.transform.localPosition.y,
                        shirt_renderer.transform.localPosition.z);
                    shirt = sprites[GetNewName(Shirt, current_state)];
                    shirt_renderer.sprite = shirt;
                    shirt_renderer.color = new Color(1, 1, 1, 1);
                }
            }
        }
        else if (shirt_renderer != null)
            shirt_renderer.color = new Color(0, 0, 0, 0);

        if (InVehicle == null && !String.IsNullOrEmpty(Pants))
        {
            if (pants_renderer == null)
                pants_renderer = AddAnimator(3, "Pants");
            string key = GetNewName(Pants, current_state);
            if (!sprites.ContainsKey(key))
                pants_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                pants = sprites[GetNewName(Pants, current_state)];
                pants_renderer.sprite = pants;
                pants_renderer.color = new Color(1, 1, 1, 1);
            }
        }
        else if (pants_renderer != null)
            pants_renderer.color = new Color(0, 0, 0, 0);

        if (InVehicle == null && !String.IsNullOrEmpty(Shoes))
        {
            if (shoes_renderer == null)
                shoes_renderer = AddAnimator(3, "Shoes");
            string key = GetNewName(Shoes, current_state);
            if (!sprites.ContainsKey(key))
                shoes_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                shoes = sprites[GetNewName(Shoes, current_state)];
                shoes_renderer.sprite = shoes;
                shoes_renderer.color = new Color(1, 1, 1, 1);
            }
        }
        else if (shoes_renderer != null)
            shoes_renderer.color = new Color(0, 0, 0, 0);

        if (!String.IsNullOrEmpty(L_weapon) && (GunsOut || ForcedGunsOut))
        {
            if (lweapon_renderer == null)
                lweapon_renderer = AddAnimator(13, "lweapon");

            //clamp state number
            int num = 0;
            if (current_state >= 9)
                num = 3;
            else if (current_state >= 6)
                num = 2;
            else if (current_state >= 3)
                num = 1;
            else
                num = 0;

            string key = GetNewName(L_weapon, num);
            if (!weapon_sprites.ContainsKey(key))
                lweapon_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                l_weapon = weapon_sprites[GetNewName(L_weapon, num)];
                lweapon_renderer.sprite = l_weapon;
                lweapon_renderer.color = new Color(1, 1, 1, 1);
            }
        }
        else if (lweapon_renderer != null)
            lweapon_renderer.color = new Color(0, 0, 0, 0);

        if (!String.IsNullOrEmpty(R_weapon) && (GunsOut || ForcedGunsOut))
        {
            if (rweapon_renderer == null)
                rweapon_renderer = AddAnimator(13, "rweapon");

            //clamp state number
            int num = 0;
            if (current_state >= 9)
                num = 3;
            else if (current_state >= 6)
                num = 2;
            else if (current_state >= 3)
                num = 1;
            else
                num = 0;

            string key = GetNewName(R_weapon, num);
            if (!weapon_sprites.ContainsKey(key))
                rweapon_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                r_weapon = weapon_sprites[GetNewName(R_weapon, num)];
                rweapon_renderer.sprite = r_weapon;
                rweapon_renderer.color = new Color(1, 1, 1, 1);
            }
        }
        else if (rweapon_renderer != null)
            rweapon_renderer.color = new Color(0, 0, 0, 0);

        if (!String.IsNullOrEmpty(Glasses))
        {
            if (glasses_renderer == null)
                glasses_renderer = AddAnimator(7, "Glasses");
            //glasses only have 8 tiles, this fixes display issues
            if (current_state >= 6 && current_state <= 8)
            {
                glasses_renderer.color = new Color(0, 0, 0, 0);
            }
            else
            {
                //convert states to numbers 1 to 8
                int num = current_state >= 9 ? current_state - 3 : current_state;

                string key = GetNewName(Glasses, num);
                if (!sprites.ContainsKey(key))
                    glasses_renderer.color = new Color(0, 0, 0, 0);
                else
                {
                    glasses = sprites[GetNewName(Glasses, num)];
                    glasses_renderer.sprite = glasses;
                    glasses_renderer.color = new Color(1, 1, 1, 1);
                }
            }
        }
        else if (glasses_renderer != null)
            glasses_renderer.color = new Color(0, 0, 0, 0);

        if (!String.IsNullOrEmpty(Earings))
        {
            if (earings_renderer == null)
                earings_renderer = AddAnimator(6, "Earings");
            string key = GetNewName(Earings, current_state);
            if (!sprites.ContainsKey(key))
                earings_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                earings = sprites[GetNewName(Earings, current_state)];
                earings_renderer.sprite = earings;
                earings_renderer.color = new Color(1, 1, 1, 1);
            }
        }
        else if (earings_renderer != null)
            earings_renderer.color = new Color(0, 0, 0, 0);

        if (InVehicle == null && !String.IsNullOrEmpty(Arms))
        {
            if (arms_renderer == null)
                arms_renderer = AddAnimator(7, "Arms");
            string key = GetNewName(Arms, current_state);
            if (!sprites.ContainsKey(key))
                arms_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                arms = sprites[GetNewName(Arms, current_state)];
                arms_renderer.sprite = arms;
                arms_renderer.color = new Color(1, 1, 1, 1);
            }
        }
        else if (arms_renderer != null)
            arms_renderer.color = new Color(0, 0, 0, 0);

        if (!String.IsNullOrEmpty(Lipstick))
        {
            if (lipstick_renderer == null)
                lipstick_renderer = AddAnimator(10, "lipstick");

            if (current_state >= 6 && current_state <= 8)
            {
                lipstick_renderer.color = new Color(0, 0, 0, 0);
            }
            else
            {
                //convert states to numbers 1 to 8
                int num = current_state >= 9 ? current_state - 3 : current_state;

                string key = GetNewName(Lipstick, num);
                if (!sprites.ContainsKey(key))
                    lipstick_renderer.color = new Color(0, 0, 0, 0);
                else
                {
                    lipstick = sprites[GetNewName(Lipstick, num)];
                    lipstick_renderer.sprite = lipstick;
                    lipstick_renderer.color = new Color(1, 1, 1, 1);
                }
            }
        }
        else if (lipstick_renderer != null)
            lipstick_renderer.color = new Color(0, 0, 0, 0);
        if (!String.IsNullOrEmpty(Nose))
        {
            if (nose_renderer == null)
                nose_renderer = AddAnimator(11, "nose");

            //noses only used states 3-5, 9-11
            //these states have to be clamped to 0-5

            if (current_state <= 2 || (current_state >= 6 && current_state <= 8))
            {
                nose_renderer.color = new Color(0, 0, 0, 0);
            }
            else
            {
                int num = 0;
                if (current_state >= 9)
                    num = current_state - 6;
                else
                    num = current_state - 3;

                string key = GetNewName(Nose, num);
                if (!sprites.ContainsKey(key))
                    nose_renderer.color = new Color(0, 0, 0, 0);
                else
                {
                    nose = sprites[GetNewName(Nose, num)];
                    nose_renderer.sprite = nose;
                    nose_renderer.color = new Color(1, 1, 1, 1);
                }
            }
        }
        else if (nose_renderer != null)
            nose_renderer.color = new Color(0, 0, 0, 0);
        if (!String.IsNullOrEmpty(Eyecolor))
        {
            if (eyecolor_renderer == null)
                eyecolor_renderer = AddAnimator(5, "eyecolor");
            if (current_state >= 6 && current_state <= 8)
            {
                eyecolor_renderer.color = new Color(0, 0, 0, 0);
            }
            else
            {
                //convert states to numbers 1 to 8
                int num = current_state >= 9 ? current_state - 3 : current_state;
                string key = GetNewName(Eyecolor, num);
                if (!sprites.ContainsKey(key))
                    eyecolor_renderer.color = new Color(0, 0, 0, 0);
                else
                {
                    eyecolor = sprites[GetNewName(Eyecolor, num)];
                    eyecolor_renderer.sprite = eyecolor;
                    eyecolor_renderer.color = new Color(1, 1, 1, 1);
                }
            }
        }
        else if (eyecolor_renderer != null)
            eyecolor_renderer.color = new Color(0, 0, 0, 0);
        if (!String.IsNullOrEmpty(Facecolor))
        {
            if (facecolor_renderer == null)
                facecolor_renderer = AddAnimator(4, "facecolor");
            string key = GetNewName(Facecolor, current_state);
            if (!sprites.ContainsKey(key))
                facecolor_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                facecolor = sprites[GetNewName(Facecolor, current_state)];
                facecolor_renderer.sprite = facecolor;
                facecolor_renderer.color = new Color(1, 1, 1, 1);
            }
        }
        else if (facecolor_renderer != null)
            facecolor_renderer.color = new Color(0, 0, 0, 0);
        if (!String.IsNullOrEmpty(Outer_eye))
        {
            if (outereye_renderer == null)
                outereye_renderer = AddAnimator(6, "outereye");
            string key = GetNewName(Outer_eye, current_state);
            if (!sprites.ContainsKey(key))
                outereye_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                outer_eye = sprites[GetNewName(Outer_eye, current_state)];
                outereye_renderer.sprite = outer_eye;
                outereye_renderer.color = new Color(1, 1, 1, 1);
            }
        }
        else if (outereye_renderer != null)
            outereye_renderer.color = new Color(0, 0, 0, 0);

        //Add blood if low health
        if (Health < MaxHealth * 0.5f && String.IsNullOrEmpty(Blood))
        {
            Blood = PeopleData.GetRandomBlood();
        }
        //remove blood if not low health
        else if (Health > MaxHealth * 0.5f && !String.IsNullOrEmpty(Blood))
        {
            Blood = null;
        }

        if (!String.IsNullOrEmpty(Blood))
        {
            if (blood_renderer == null)
                blood_renderer = AddAnimator(11, "blood");

            //clamp state number
            int num = 0;
            if (current_state >= 9)
                num = 3;
            else if (current_state >= 6)
                num = 2;
            else if (current_state >= 3)
                num = 1;
            else
                num = 0;

            string key = GetNewName(Blood, num);
            if (!sprites.ContainsKey(key))
                blood_renderer.color = new Color(0, 0, 0, 0);
            else
            {
                blood = sprites[GetNewName(Blood, num)];
                blood_renderer.sprite = blood;
                blood_renderer.color = new Color(1, 1, 1, 1);
            }
        }
        else if (blood_renderer != null)
            blood_renderer.color = new Color(0, 0, 0, 0);

        //enfore red circle rotation
        if (RedCircle != null)
        {
            RedCircle.transform.eulerAngles = new Vector3(90, 90, 0);
            RedCircle.transform.position = new Vector3(transform.position.x, 0, -0.2f + transform.position.z);
        }
    }

    /// <summary>
    /// Clear all sprites. This does not delete the clothes, they just wont be displayed
    /// in the frame that this is called
    /// </summary>
    void NullAll()
    {
        if (hat_renderer != null) hat_renderer.sprite = null;
        if (hair_renderer != null) hair_renderer.sprite = null;
        if (shirt_renderer != null) shirt_renderer.sprite = null;
        if (pants_renderer != null) pants_renderer.sprite = null;
        if (shoes_renderer != null) shoes_renderer.sprite = null;
        if (lweapon_renderer != null) lweapon_renderer.sprite = null;
        if (rweapon_renderer != null) rweapon_renderer.sprite = null;
        if (glasses_renderer != null) glasses_renderer.sprite = null;
        if (earings_renderer != null) earings_renderer.sprite = null;
        if (arms_renderer != null) arms_renderer.sprite = null;
        if (lipstick_renderer != null) lipstick_renderer.sprite = null;
        if (nose_renderer != null) nose_renderer.sprite = null;
        if (eyecolor_renderer != null) eyecolor_renderer.sprite = null;
        if (facecolor_renderer != null) facecolor_renderer.sprite = null;
        if (outereye_renderer != null) outereye_renderer.sprite = null;
        if (blood_renderer != null) blood_renderer.sprite = null;
    }

    /// <summary>
    /// Try to get a sprite with name. The character does not have to be wearing the sprite.
    /// This function assumes the unity appendage '_#' is not on the input string
    /// </summary>
    /// <param name="spriteName"></param>
    /// <returns></returns>
    public static Sprite GetClothingSprite(string spriteName)
    {
        if (sprites == null || sprites.Count == 0 || spriteName == null)
            return null;
        spriteName += "_1";
        Sprite spr = null;
        sprites.TryGetValue(spriteName, out spr);
        return spr;
    }

    /// <summary>
    /// Add a new Animation object to the character
    /// </summary>
    /// <param name="order"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    SpriteRenderer AddAnimator(int order, string name)
    {
        GameObject new_obj = new GameObject(name);
        new_obj.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        new_obj.transform.SetParent(sprite_child.transform);
        new_obj.transform.localPosition = new Vector3(0, -0.22f, 0.08f);
        new_obj.transform.position = new Vector3(new_obj.transform.position.x, 0, new_obj.transform.position.z);
        new_obj.transform.localRotation = Quaternion.Euler(90, 0, 0);
        new_obj.gameObject.layer = LayerMask.NameToLayer("Characters");

        if (GetComponent<AIMove>() != null)
            new_obj.transform.localPosition = new Vector3(0, 0, 0.08f);

        SpriteRenderer s = new_obj.AddComponent<SpriteRenderer>();
        s.sortingOrder = order;
        Animator m = new_obj.AddComponent<Animator>();
        m.runtimeAnimatorController = controller;
        return s;
    }

    /// <summary>
    /// Format the name
    /// </summary>
    /// <param name="tile_name"></param>
    /// <param name="renderer"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    string GetNewName(string tile_name, int state)
    {
        return tile_name + "_" + state.ToString();
    }

    /// <summary>
    /// This function will be called for this character data if the NPC is static
    /// </summary>
    public void ResetStaticNPC()
    {
        isDead = false;
        _health = MaxHealth;
        _NumHealthPacks = 0;
        IsImmortal = true;
        if (InVehicle != null)
        {
            InVehicle.LeaveVehicle(this);
            InVehicle = null;
        }
        PlaceCharacters.DeactivateStaticCharacter(this);
    }
    
    /// <summary>
    /// Make a red circle under this character
    /// </summary>
    public void MakeRedCircle()
    {
        if (RedCircle == null)
        {
            RedCircle = Prefabs.GetNewPrefab("Prefabs/RedCircle");
            RedCircle.transform.eulerAngles = new Vector3(90, 90, 0);
            RedCircle.transform.position = new Vector3(transform.position.x, 0, -0.2f + transform.position.z);
        }
    }
    /// <summary>
    /// Destroy a red circle object under this character (if there is any)
    /// </summary>
    public void RemoveRedCircle()
    {
        if (RedCircle != null)
        {
            Destroy(RedCircle);
            RedCircle = null;
        }
    }

    /// <summary>
    /// Make the character invisible (not undo-able)
    /// </summary>
    public void DeleteClothes()
    {
        Hat = null;
        Hair = null;
        Shirt = null;
        Pants = null;
        Shoes = null;
        L_weapon = null;
        R_weapon = null;
        Glasses = null;
        Earings = null;
        Arms = null;
        Lipstick = null;
        Nose = null;
        Eyecolor = null;
        Facecolor = null;
        Outer_eye = null;
        Blood = null;
    }

    /// <summary>
    /// Returns the sprite associated with the input string, returns null if none is found.
    /// Only looks at what this character is wearing. Example: GetSprite(this.Shirt).
    /// </summary>
    /// <param name="part"></param>
    /// <returns></returns>
    public Sprite GetSprite(string part)
    {
        if (part == Hat)
        {
            if (hat_renderer != null)
                return hat_renderer.sprite;
            return null;
        }
        if (part == Hair)
        {
            if (hair_renderer != null)
                return hair_renderer.sprite;
            return null;
        }
        if (part == Shirt)
        {
            if (shirt_renderer != null)
                return shirt_renderer.sprite;
            return null;
        }
        if (part == Pants)
        {
            if (pants_renderer != null)
                return pants_renderer.sprite;
        }
        if (part == Shoes)
        {
            if (shoes_renderer != null)
                return shoes_renderer.sprite;
            return null;
        }
        if (part == L_weapon)
        {
            if (lweapon_renderer != null)
                return lweapon_renderer.sprite;
            return null;
        }
        if (part == R_weapon)
        {
            if (rweapon_renderer != null)
                return rweapon_renderer.sprite;
            return null;
        }
        if (part == Glasses)
        {
            if (glasses_renderer != null)
                return glasses_renderer.sprite;
            return null;
        }
        if (part == Earings)
        {
            if (earings_renderer != null)
                return earings_renderer.sprite;
            return null;
        }
        if (part == Arms)
        {
            if (arms_renderer != null)
                return arms_renderer.sprite;
            return null;
        }
        if (part == Lipstick)
        {
            if (lipstick_renderer != null)
                return lipstick_renderer.sprite;
            return null;
        }
        if (part == Nose)
        {
            if (nose_renderer != null)
                return nose_renderer.sprite;
            return null;
        }
        if (part == Eyecolor)
        {
            if (eyecolor_renderer != null)
                return eyecolor_renderer.sprite;
            return null;
        }
        if (part == Outer_eye)
        {
            if (outereye_renderer != null)
                return outereye_renderer.sprite;
            return null;
        }
        if (part == Blood)
        {
            if (blood_renderer != null)
                return blood_renderer.sprite;
            return null;
        }
        if (part == Facecolor)
        {
            if (facecolor_renderer != null)
                return facecolor_renderer.sprite;
            return null;
        }

        return null;
    }
}