﻿using UnityEngine;
using System.Collections.Generic;

public class MapData : MonoBehaviour {
    /// <summary>
    /// Map length
    /// </summary>
    [Tooltip("Map length, only necessary for the city map")]
    public float d_x;
    /// <summary>
    /// map width
    /// </summary>
    [Tooltip("Map Width, only necessary for the city map")]
    public float d_y;

    [Tooltip("This value should be false for every map but the city map")]
    public bool IsPositionAtGlobalZero = false;

    [Tooltip("The top left corner of this map")]
    public Transform topleft;
    [Tooltip("The bottom right corner of this map")]
    public Transform bottomright;

    /// <summary>
    /// The normal Camera height - CamHeightModifier will become the new camera height while the
    /// player is in this map
    /// </summary>
    [Tooltip("Subtract this amount from the normal camera follow height while on this map")]
    public float CamHeightModifer = 0.0f;

    /// <summary>
    /// All maps, stored by ZoneName
    /// </summary>
    public static Dictionary<string, MapData> maps = new Dictionary<string, MapData>();
    static HashSet<int> ZoneIDS = new HashSet<int>();

    private static MapData _activeMap;
    /// <summary>
    /// The map that the player is currently in
    /// </summary>
    public static MapData ActiveMap
    {
        get
        {
            if (_activeMap == null)
            {
                if (CharacterMove.CharacterTransform != null)
                {
                    _activeMap = maps[CharacterMove.CharacterTransform.GetComponent<PlayerDat>().playerInfo2.ActiveMap];
                }
            }
            return _activeMap;
        }
        set
        {
            _activeMap = value;
            CharacterMove.CharacterTransform.GetComponent<PlayerDat>().playerInfo2.ActiveMap = _activeMap.ZoneName;
        }
    }

    void Awake()
    {
        foreach (DoorEnter d in GetComponentsInChildren<DoorEnter>(true))
        {
            d.MyMap = this;
        }
        maps.Add(ZoneName, this);
        zoneID = RandID;
        ZoneIDS.Add(zoneID);
    }

    void OnDestroy()
    {
        maps.Remove(ZoneName);
    }

    [Tooltip("The name of this zone, Must be unique!")]
    public string ZoneName;
    private int zoneID;
    /// <summary>
    /// Random ID assign to the zone at start up
    /// </summary>
    public int ZoneID
    {
        get
        {
            return zoneID;
        }
    }
    private int RandID
    {
        get
        {
            int newID = Random.Range(int.MinValue, int.MaxValue);
            while (ZoneIDS.Contains(newID) || newID == 0)
                newID = Random.Range(int.MinValue, int.MaxValue);
            return newID;
        }
    }

    /// <summary>
    /// Serialized list of vehicles
    /// </summary>
    [System.NonSerialized]
    public List<VehicleInfo> MyVehicles;
    /// <summary>
    /// Serialized list of NPCs
    /// </summary>
    [System.NonSerialized]
    public List<PlayerInfo> MyPlayers;
    /// <summary>
    /// list of static characters belonging to this map that should be awake
    /// </summary>
    [System.NonSerialized]
    public List<GameObject> AwakeStaticPlayers;

    /// <summary>
    /// If asleep, then the NPCs/vehicles on this map are currently Destroyed and are being stored in a serialized array
    /// </summary>
    [System.NonSerialized]
    public bool isAsleep = false;

    public override int GetHashCode()
    {
        return GetInstanceID();
    }

    public override bool Equals(object o)
    {
        return o is UnityEngine.Object ? ((UnityEngine.Object)o).GetInstanceID() == GetInstanceID() : false;
    }

    [Tooltip("How high above the map is the mini-map cam?")]
    public OrthoGraphicCameraZoomLevel ZoomLevel;
    [Tooltip("How much to restrict or allow the minimap to move from the map borders")]
    public float MiniMapPadding = -4f;
}
