﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class BoxManager : MonoBehaviour
{
    static List<RectTransform> boxes = new List<RectTransform>();
    RectTransform rectT;

    private GameObject followthis;
    public GameObject follow_this
    {
        get
        {
            return followthis;
        }
        set
        {
            followthis = value;
            if (followthis != null)
                previous_position = followthis.transform.position;
        }
    }
    private Vector3 previous_position;

	void Awake()
    {
        rectT = GetComponentInChildren<Image>().rectTransform;
        boxes.Add(rectT);
    }
    void OnDestroy()
    {
        boxes.Remove(rectT);
    }

    void Update()
    {
        //fixed rotation
        transform.eulerAngles = new Vector3(90, 0, 0);

        //if not destroyed..
        if (follow_this != null)
        {
            transform.position += follow_this.transform.position - previous_position;
            previous_position = follow_this.transform.position;
        }
    }

    /// <summary>
    /// Reposition the inpt transform so that it doesnt collide with others
    /// </summary>
    /// <param name="repo_this"></param>
    public static void RepositionDialogue(RectTransform repo_this, CharacterData c)
    {
        CoroutineHelper.RunCoroutine(Repo(repo_this));
    }

    static IEnumerator Repo(RectTransform repo_this)
    {
        yield return 0; yield return 0; yield return 0;

        if (repo_this == null)
        {
            yield break;
        }

        repo_this.transform.position = new Vector3(repo_this.transform.position.x, repo_this.transform.position.y,
            repo_this.transform.position.z + repo_this.sizeDelta.y * repo_this.localScale.y);
        foreach (RectTransform rt in boxes)
        {
            if (repo_this != rt)
            {
                float srx = rt.localScale.x, srz = rt.localScale.y;
                float gx1 = rt.anchoredPosition3D.x + rt.transform.position.x;
                float gx2 = rt.anchoredPosition3D.x + rt.sizeDelta.x * srx + rt.transform.position.x;
                //upper left corner parenting, so subtract in y-direction (2d space) zcoord in 3d due to our rotation
                float gz1 = rt.anchoredPosition3D.y + rt.transform.position.z;
                float gz2 = rt.anchoredPosition3D.y - rt.sizeDelta.y * srz + rt.transform.position.z;

                //get corners
                Vector3 rtc1 = new Vector3(gx1, 0.01f, gz1), rtc2 = new Vector3(gx1, 0.01f, gz2),
                        rtc3 = new Vector3(gx2, 0.01f, gz2), rtc4 = new Vector3(gx2, 0.01f, gz1);

                float sx = repo_this.localScale.x, sz = repo_this.localScale.y;
                float rgx1 = repo_this.anchoredPosition3D.x + repo_this.transform.position.x;
                float rgx2 = repo_this.anchoredPosition3D.x + repo_this.sizeDelta.x * sx + repo_this.transform.position.x;
                //upper left corner parenting, so subtract in y-direction (2d space) zcoord in 3d due to our rotation
                float rgz1 = repo_this.anchoredPosition3D.y + repo_this.transform.position.z;
                float rgz2 = repo_this.anchoredPosition3D.y - repo_this.sizeDelta.y * sz + repo_this.transform.position.z;

                //get corners
                Vector3 repo_tc1 = new Vector3(rgx1, 0.01f, rgz1), repo_tc2 = new Vector3(rgx1, 0.01f, rgz2),
                        repo_tc3 = new Vector3(rgx2, 0.01f, rgz2), repo_tc4 = new Vector3(rgx2, 0.01f, rgz1);

                bool intersected = false;
                //rectangle intersect tests
                if (RectanglePointIntersects(rtc1, rtc2, rtc3, rtc4, repo_tc1)) intersected = true;
                if (RectanglePointIntersects(rtc1, rtc2, rtc3, rtc4, repo_tc2)) intersected = true;
                if (RectanglePointIntersects(rtc1, rtc2, rtc3, rtc4, repo_tc3)) intersected = true;
                if (RectanglePointIntersects(rtc1, rtc2, rtc3, rtc4, repo_tc4)) intersected = true;

                if (RectanglePointIntersects(repo_tc1, repo_tc2, repo_tc3, repo_tc4, rtc1)) intersected = true;
                if (RectanglePointIntersects(repo_tc1, repo_tc2, repo_tc3, repo_tc4, rtc2)) intersected = true;
                if (RectanglePointIntersects(repo_tc1, repo_tc2, repo_tc3, repo_tc4, rtc3)) intersected = true;
                if (RectanglePointIntersects(repo_tc1, repo_tc2, repo_tc3, repo_tc4, rtc4)) intersected = true;

                if (intersected)
                {
                    float newz = rt.parent.transform.position.z + repo_this.anchoredPosition3D.y + repo_this.sizeDelta.y * sz;
                    repo_this.parent.transform.position = new Vector3(repo_this.parent.transform.position.x,
                        0.01f, newz);
                }
            }
        }
    }

    static bool RectanglePointIntersects(Vector3 r1, Vector3 r2, Vector3 r3, Vector3 r4, Vector3 p)
    {
        if (p.x > r1.x && p.x < r4.x && p.z > r2.z && p.z < r1.z)
            return true;
        return false;
    }
}
