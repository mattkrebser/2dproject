using System.Collections.Generic;
using UnityEngine;
using System;

public sealed class InputEvents : MonoBehaviour
{
	private static int dragCounter = 0;

    /// <summary>
    /// Are we currently dragging?
    /// </summary>
    public static bool Is_Dragging
    {
        get
        {
            if (dragCounter >= 69)
                return true;
            return false;
        }
    }

    /// <summary>
    /// Current OnGUI event being processed in the OnGUI function
    /// </summary>
    private Event e;

    /// <summary>
    /// set of allowed MultiKey sets that InputEvents class will look for
    /// </summary>
    private static Dictionary<KeyCode, List<KeySet>> allowedMultiKeys = new Dictionary<KeyCode, List<KeySet>>();
    /// <summary>
    /// Dictionary containing Key,KeyStatus pairs (key represents a key, keystataus represents up/down)
    /// </summary>
    private static Dictionary<KeyCode, KeyStatus> key_status = new Dictionary<KeyCode, KeyStatus>();
    /// <summary>
    /// bookkeeping for multikey events so they aren't fired more than once before being released
    /// </summary>
    private static HashSet<KeySet> multiKeyEventFired = new HashSet<KeySet>();
    /// <summary>
    /// set containing keysets that are currently being held down
    /// </summary>
    private static HashSet<KeySet> beingHeldDown = new HashSet<KeySet>();

    void Awake()
    {
        Array enums = System.Enum.GetValues(typeof(KeyCode));
        foreach (KeyCode e in enums)
        {
            //None is always 'DOWN' (being pressed)
            if (e == KeyCode.None)
            {
                key_status.Add(e, KeyStatus.DOWN);
            }
            else if (!key_status.ContainsKey(e))
                key_status.Add(e, default(KeyStatus));
        }
    }

    void OnDestroy()
    {
        key_status.Clear();
    }

    /// <summary>
    /// Try to run a multi key event on the current key
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    private static KeySet try_multi(KeyCode key)
    {
        if (allowedMultiKeys.ContainsKey(key))
        {
            List<KeySet> l = null;
            if (allowedMultiKeys.TryGetValue(key, out l))
            {
                if (l == null)
                    return KeySet.zero;
                //length of l is the number of different combos that this key is involved in,
                //which gets specified by you (using AddKeyCombo(KeySet))
                for (int i = 0; i < l.Count; i++)
                {
                    if (key_status[l[i].key1] == KeyStatus.DOWN && key_status[l[i].key2] == KeyStatus.DOWN &&
                        key_status[l[i].key3] == KeyStatus.DOWN)
                    {
                        return l[i];
                    }
                }
            }
            else
            {
                Debug.Log("Internal Error, KeyCode listed as multi input, but no combos found.");
                return KeySet.zero;
            }
        }
        return KeySet.zero;
    }

    /// <summary>
    /// Returns true if the input key is part of multi-key sets
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    private static bool is_multi(KeyCode key)
    {
        List<KeySet> l = null;
        if (allowedMultiKeys.TryGetValue(key, out l))
            if (l != null && l.Count > 0)
                return true;
        return false;
    }

    /// <summary>
    /// Fires an inputkey up for each multi key combination associated with the input key
    /// </summary>
    /// <param name="key"></param>
    private static void multi_key_up(KeyCode key)
    {
        if (allowedMultiKeys.ContainsKey(key))
        {
            List<KeySet> l = null;
            if (allowedMultiKeys.TryGetValue(key, out l))
            {
                if (l == null)
                    return;
                //length of l is the number of different combos that this key is involved in,
                //which gets specified by you (using AddKeyCombo(KeySet))
                for (int i = 0; i < l.Count; i++)
                {
                    if (beingHeldDown.Contains(l[i]))
                        InputHandler.RunInputCheck(new KeySet(l[i].key1, l[i].key2, l[i].key3, GameEventTypes.inputKeyUp));
                }
            }
            else
            {
                Debug.Log("Internal Error, KeyCode listed as multi input, but no combos found.");
                return;
            }
        }
    }
    private static void remove_multi_key_event(KeyCode key)
    {
        if (allowedMultiKeys.ContainsKey(key))
        {
            List<KeySet> l = null;
            if (allowedMultiKeys.TryGetValue(key, out l))
            {
                if (l == null)
                    return;
                //length of l is the number of different combos that this key is involved in,
                //which gets specified by you (using AddKeyCombo(KeySet))
                for (int i = 0; i < l.Count; i++)
                {
                    if (multiKeyEventFired.Contains(l[i]))
                    {
                        multiKeyEventFired.Remove(l[i]);
                    }
                }
            }
            else
            {
                Debug.Log("Internal Error, KeyCode listed as multi input, but no combos found.");
                return;
            }
        }
    }
    private static void remove_held_down(KeyCode key)
    {
        if (beingHeldDown.Contains(new KeySet(key, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
        {
            beingHeldDown.Remove(new KeySet(key, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
        }

        if (!is_multi(key))
            return;

        if (allowedMultiKeys.ContainsKey(key))
        {
            List<KeySet> l = null;
            if (allowedMultiKeys.TryGetValue(key, out l))
            {
                if (l == null)
                    return;
                //length of l is the number of different combos that this key is involved in,
                //which gets specified by you (using AddKeyCombo(KeySet))
                for (int i = 0; i < l.Count; i++)
                {
                    if (beingHeldDown.Contains(l[i]))
                    {
                        beingHeldDown.Remove(l[i]);
                    }
                }
            }
            else
            {
                Debug.Log("Internal Error, KeyCode listed as multi input, but no combos found.");
                return;
            }
        }
    }
    /// <summary>
    /// Add any multi key combo (from KeyCode enum) of up to three keys. EventType is ignored. Please only use for keysets
    /// that have multiple non-None keys
    /// </summary>
    /// <param name="combo"></param>
    public static void AddKeyCombo(KeySet combo)
    {
        if (combo.key1 == combo.key2 || combo.key1 == combo.key3 || combo.key2 == combo.key3)
        {
            Debug.Log("Error, combinations have to be unique keys!");
            return;
        }
        if (KeySet.IsForbiddenKey(combo.key1) || KeySet.IsForbiddenKey(combo.key2) || KeySet.IsForbiddenKey(combo.key3))
        {
            Debug.Log("Error, a input key in the combination is invalid! Use KeySet.IsForbidden()");
            return;
        }
        if (combo == KeySet.zero)
        {
            Debug.Log("Zero KeySet is not allowed!");
            return;
        }
            List<KeySet> l = null;
        //Add each key/KeySet pair. We don't need to add the key if it is none
        if (combo.key1 != KeyCode.None && allowedMultiKeys.TryGetValue(combo.key1, out l))
        {

            if (l.Contains(combo))
                Debug.Log("Error, already have a multi key combo " + combo.ToString());
            else
                l.Add(combo);
        }
        else if (combo.key1 != KeyCode.None)
        {
            l = new List<KeySet>();
            l.Add(combo);
            allowedMultiKeys.Add(combo.key1, l);
        }

        l = null;
        if (combo.key2 != KeyCode.None && allowedMultiKeys.TryGetValue(combo.key2, out l))
        {

            if (l.Contains(combo))
                Debug.Log("Error, already have a multi key combo " + combo.ToString());
            else
                l.Add(combo);
        }
        else if (combo.key2 != KeyCode.None)
        {
            l = new List<KeySet>();
            l.Add(combo);
            allowedMultiKeys.Add(combo.key2, l);
        }

        l = null;
        if (combo.key3 != KeyCode.None && allowedMultiKeys.TryGetValue(combo.key3, out l))
        {

            if (l.Contains(combo))
                Debug.Log("Error, already have a multi key combo " + combo.ToString());
            else
                l.Add(combo);
        }
        else if (combo.key3 != KeyCode.None)
        {
            l = new List<KeySet>();
            l.Add(combo);
            allowedMultiKeys.Add(combo.key3, l);
        }
    }
    /// <summary>
    /// Same as AddKeyCombo(KeySet), but no already existing key error message.
    /// </summary>
    /// <param name="combo"></param>
    public static void TryAddKeyCombo(KeySet combo)
    {
        Debug.Log(combo);
        if (combo.key1 == combo.key2 || combo.key1 == combo.key3 || combo.key2 == combo.key3)
        {
            Debug.Log("Error, combinations have to be unique keys!");
            return;
        }
        if (KeySet.IsForbiddenKey(combo.key1) || KeySet.IsForbiddenKey(combo.key2) || KeySet.IsForbiddenKey(combo.key3))
        {
            Debug.Log("Error, a input key in the combination is invalid! Use KeySet.IsForbidden()");
            return;
        }
        if (combo == KeySet.zero)
        {
            Debug.Log("Zero KeySet is not allowed!");
            return;
        }
        List<KeySet> l = null;
        //Add each key/KeySet pair. We don't need to add the key if it is none
        if (combo.key1 != KeyCode.None && allowedMultiKeys.TryGetValue(combo.key1, out l))
        {

            if (l.Contains(combo))
            {/*do nothing */}
            else
                l.Add(combo);
        }
        else if (combo.key1 != KeyCode.None)
        {
            l = new List<KeySet>();
            l.Add(combo);
            allowedMultiKeys.Add(combo.key1, l);
        }

        l = null;
        if (combo.key2 != KeyCode.None && allowedMultiKeys.TryGetValue(combo.key2, out l))
        {

            if (l.Contains(combo))
            { }
            else
                l.Add(combo);
        }
        else if (combo.key2 != KeyCode.None)
        {
            l = new List<KeySet>();
            l.Add(combo);
            allowedMultiKeys.Add(combo.key2, l);
        }

        l = null;
        if (combo.key3 != KeyCode.None && allowedMultiKeys.TryGetValue(combo.key3, out l))
        {

            if (l.Contains(combo))
            { }
            else
                l.Add(combo);
        }
        else if (combo.key3 != KeyCode.None)
        {
            l = new List<KeySet>();
            l.Add(combo);
            allowedMultiKeys.Add(combo.key3, l);
        }
    }

    /// <summary>
    /// remove a KeySet
    /// </summary>
    /// <param name="combo"></param>
    public static void remove_key_combo(KeySet combo)
    {
        if (combo.key1 != KeyCode.None)
        {
            List<KeySet> combos = null;
            if (allowedMultiKeys.TryGetValue(combo.key1, out combos))
            {
                if (combos != null)
                {
                    for (int i = combos.Count - 1; i >= 0; i--)
                    {
                        if (combo == combos[i])
                        {
                            combos.RemoveAt(i);
                        }
                    }
                    if (combos.Count == 0)
                        allowedMultiKeys.Remove(combo.key1);
                }
            }
        }
        if (combo.key2 != KeyCode.None)
        {
            List<KeySet> combos = null;
            if (allowedMultiKeys.TryGetValue(combo.key2, out combos))
            {
                if (combos != null)
                {
                    for (int i = combos.Count - 1; i >= 0; i--)
                    {
                        if (combo == combos[i])
                        {
                            combos.RemoveAt(i);
                        }
                    }
                    if (combos.Count == 0)
                        allowedMultiKeys.Remove(combo.key1);
                }
            }
        }
        if (combo.key3 != KeyCode.None)
        {
            List<KeySet> combos = null;
            if (allowedMultiKeys.TryGetValue(combo.key3, out combos))
            {
                if (combos != null)
                {
                    for (int i = combos.Count - 1; i >= 0; i--)
                    {
                        if (combo == combos[i])
                        {
                            combos.RemoveAt(i);
                        }
                    }
                    if (combos.Count == 0)
                        allowedMultiKeys.Remove(combo.key1);
                }
            }
        }
    }

    /// <summary>
    /// Key is DOWN/UP
    /// </summary>
    /// <param name="key"></param>
    /// <param name="stat"></param>
    private static void set_key_status(KeyCode key, KeyStatus stat)
    {
        if (key != KeyCode.None)
            key_status[key] = stat;
    }

    private static int _framesLast = -1;
    private static int FramesSinceLastShiftDown
    {
        get
        {
            return _framesLast;
        }
        set
        {
            if (value > 10)
                _framesLast = 10;
            else
                _framesLast = value;
        }
    }

    //Side note*
    /*

    You can test the behaviour by adding a debug statement in the InputHandler.RunInputCheck function
    Debug(input);
    
    */

    void OnGUI(){
		e = Event.current;
        KeySet multi = KeySet.zero;

        //the events are listed in order of their priority in this if/else (this only effects multi key mouse/keyboard conflicts)
            
        //on key up event
        if (e.type == EventType.KeyUp)
        {
            if (is_multi(e.keyCode))
            {
                multi_key_up(e.keyCode);
                remove_multi_key_event(e.keyCode);
            }
            //key up gets called for everything (don't want to miss any key up)
            InputHandler.RunInputCheck(new KeySet(e.keyCode, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            //remove key down because that is what we added
            remove_held_down(e.keyCode);
            set_key_status(e.keyCode, KeyStatus.UP);
        }

        //on keyhelddown event
        else if (e.type == EventType.KeyDown && e.keyCode != KeyCode.None)
        {
            set_key_status(e.keyCode, KeyStatus.DOWN);
            multi = try_multi(e.keyCode);
            //If this current key is involved in a multi-key event, only the multi key event gets called for KeyDown events
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(e.keyCode, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(e.keyCode, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(e.keyCode, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown));
                InputHandler.RunInputCheck(new KeySet(e.keyCode, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
        }

        HandleShift(e, ref multi);

        if (e.type == EventType.MouseDown)
        {
            TryMouseDown(e, ref multi);
        }

        else if (e.type == EventType.MouseUp)
        {
            TryMouseUp(e, ref multi);
        }

        //handle drags. Drag end calls on mouse release. Drag events only use the mouse
        if (e.type == EventType.mouseDrag)
        {
            if (dragCounter == 0)
            {
                InputHandler.RunInputCheck(KeySet.BeginDrag);
                dragCounter = 69;
            }
        }
        else if (e.type == EventType.MouseUp && dragCounter >= 69)
        {
            InputHandler.RunInputCheck(KeySet.EndDrag);
            dragCounter = 0;
        }
    }

    /// <summary>
    /// If the application looses focues, then release anything being held down
    /// </summary>
    /// <param name="focusStatus"></param>
    void OnApplicationFocus(bool focusStatus)
    {
        beingHeldDown.Clear();
    }

    void Update()
    {

        //handle mouse input
        float dx = Input.GetAxis("Mouse X");
        float dy = Input.GetAxis("Mouse Y");
        if (dx != 0 || dy != 0)
        {
            InputHandler.AssignMouseChange(dx, dy);
            InputHandler.RunInputCheck(KeySet.MoveMouse);
        }
        else
            InputHandler.AssignMouseChange(0, 0);

        //handle Shift button (allow for a max of one shift button down event per frame)
        if (FramesSinceLastShiftDown != -1)
            FramesSinceLastShiftDown++;

        //call delegates for each button being held down
        foreach (KeySet set in beingHeldDown)
        {
            InputHandler.RunInputCheck(new KeySet(set.key1, set.key2, set.key3, GameEventTypes.inputKeyDown));
        }

        InputHandler.AssignScrollWheelChange(Input.GetAxis("Mouse ScrollWheel"));
        if (InputHandler.GetScrollWheelChange() != 0)
            InputHandler.RunInputCheck(InputHandler.GetScrollWheelChange() < 0 ? KeySet.ScrollDown : KeySet.ScrollUp);
    }

    /// <summary>
    /// Check for each mouse button
    /// </summary>
    /// <param name="e"></param>
    /// <param name="multi"></param>
    void TryMouseDown(Event e, ref KeySet multi)
    {
        if (e.button == 0)
        {
            set_key_status(KeyCode.Mouse0, KeyStatus.DOWN);
            multi = try_multi(KeyCode.Mouse0);
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
            
        }
        else if (e.button == 1)
        {
            set_key_status(KeyCode.Mouse1, KeyStatus.DOWN);
            multi = try_multi(KeyCode.Mouse1);
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
        }
        else if (e.button == 2)
        {
            set_key_status(KeyCode.Mouse2, KeyStatus.DOWN);
            multi = try_multi(KeyCode.Mouse2);
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(KeyCode.Mouse2, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(KeyCode.Mouse2, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse2, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
        }
    }

    /// <summary>
    /// check for each mouse button up
    /// </summary>
    /// <param name="e"></param>
    /// <param name="multi"></param>
    void TryMouseUp(Event e, ref KeySet multi)
    {
        
        if (e.button == 0)
        {
            if (is_multi(KeyCode.Mouse0))
            {
                multi_key_up(KeyCode.Mouse0);
                remove_multi_key_event(KeyCode.Mouse0);
            }
            //key up gets called for everything (don't want to miss any key up)
            InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            remove_held_down(KeyCode.Mouse0);
            set_key_status(KeyCode.Mouse0, KeyStatus.UP);
        }
        else if (e.button == 1)
        {
            if (is_multi(KeyCode.Mouse1))
            {
                //if a multi key group ended
                multi_key_up(KeyCode.Mouse1);
                remove_multi_key_event(KeyCode.Mouse1);
            }
            //key up gets called for everything (don't want to miss any key up)
            InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            remove_held_down(KeyCode.Mouse1);
            set_key_status(KeyCode.Mouse1, KeyStatus.UP);
        }
        else if (e.button == 2)
        {
            if (is_multi(KeyCode.Mouse2))
            {
                multi_key_up(KeyCode.Mouse2);
                remove_multi_key_event(KeyCode.Mouse2);
            }
            //key up gets called for everything (don't want to miss any key up)
            InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse2, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            remove_held_down(KeyCode.Mouse2);
            set_key_status(KeyCode.Mouse2, KeyStatus.UP);
        }
    }

    void HandleShift(Event e, ref KeySet multi)
    {
        //shift key down. Unity doesn't consider shift to be a normal keydown event. bleh
        if (e.shift)
        {
            if (FramesSinceLastShiftDown == 0)
                return;
            FramesSinceLastShiftDown = 0;
            set_key_status(KeyCode.LeftShift, KeyStatus.DOWN);
            multi = try_multi(KeyCode.LeftShift);
            //If this current key is involved in a multi-key event
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(KeyCode.LeftShift, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(KeyCode.LeftShift, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.LeftShift, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.LeftShift, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
        }
        //shift key up, we have to fake it
        if (FramesSinceLastShiftDown > 4)
        {
            FramesSinceLastShiftDown = -1;
            multi = try_multi(KeyCode.LeftShift);
            //if a multi key group ended
            if (is_multi(KeyCode.LeftShift))
            {
                multi_key_up(KeyCode.LeftShift);
                remove_multi_key_event(KeyCode.LeftShift);
            }
            remove_held_down(KeyCode.LeftShift);
            //key up gets called for everything (don't want to miss any key up). We don't have to remove shift from the beingHeldDown table
            InputHandler.RunInputCheck(new KeySet(KeyCode.LeftShift, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            set_key_status(KeyCode.LeftShift, KeyStatus.UP);
        }
    }
}
