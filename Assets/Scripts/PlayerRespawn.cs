﻿using UnityEngine;
using System.Collections;

public class PlayerRespawn : MonoBehaviour {

	public void RespawnRoutine()
    {
        UIMenus.SetAllNotActive();
        StartCoroutine(Routine());
    }

    IEnumerator Routine()
    {
        DEADUI.DeadObj.SetActive(true);
        SoundFX.Play(SoundFX.GetClip("DeathNoise"));
        yield return new WaitForSeconds(5.0f);
        yield return ScreenToBlack.Fadeout();

        GameSettings.Instance.QuitGameNoSave();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        DEADUI.DeadObj.SetActive(false);
    }
}
