﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class Dialogue : MonoBehaviour
{
    /// <summary>
    /// The Dialgue regarding this instance. The immediate Dialogue that is being spoken.
    /// </summary>
    [SerializeField]
    [Tooltip("A list of things that the NPC (or player) will say before a response will occur (if any)")]
    public List<string> ThisDialogue;
    /// <summary>
    /// Responses for this Dialogue
    /// </summary>
    [SerializeField]
    [Tooltip("A list of possible Responses to the above Dialogue. The player will select, an NPC will randomly choose.")]
    public List<Dialogue> ThisResponses;

    [SerializeField]
    [Tooltip("Event type")]
    public DialogueEventType ThisDialogueEventType;
    [SerializeField]
    [Tooltip("Does a specific faction use this Dialogue?")]
    public Factions FactionDialogue = Factions.none;

    [SerializeField]
    [Tooltip("Does a specific Personality use this Dialogue?")]
    public Personality PersonalityType = Personality.none;

    [SerializeField]
    [Tooltip("This dialogue will only be spoken by this character")]
    public CharacterData SaysThisDialogue;

    private static bool PlayerInConversation = false;
    private static NPCDialogue CurrentDialogue = null;
    private static bool InNarratorConversation = false;

    /// <summary>
    /// Makes a dialogue bubble above the input character
    /// with the specified dialogue. It will be automatically destroyed after a bit.
    /// This is not used for Story/Quest dialogue Chains.
    /// </summary>
    /// <param name="input"></param>
    /// <param name="talking_to"></param>
    public static void MakeDialogueBubble(Dialogue input, CharacterData MyBubble, NPCAI sender)
    {
        if (sender.InConversation || sender.DisabledNormalConversation)
            return;

        GameObject box = Prefabs.GetNewPrefab("Prefabs/DialogueBox");
        sender.InConversation = true;
        sender.CurrentDialogue = box;
        CoroutineHelper.RunCoroutine(DialogueRoutine(input, box, MyBubble, sender));
    }
    /// <summary>
    /// Interactive Player to NPC Dialogue
    /// </summary>
    /// <param name="input"></param>
    /// <param name="d"></param>
    /// <param name="position"></param>
    public static void MakeQuestDialogueBubble(Dialogue input, NPCDialogue d, Transform position)
    {
        //If the narrator is speaking with an interactive dialogue...
        if (InNarratorConversation)
        {
            d.ResetEvent();
            return;
        }
        //If already in a quest conversation, then just reset and break
        if (PlayerInConversation)
        {
            //dont reset dialogue if this is the same one. (Although, this shouldn't happen)
            if (d == CurrentDialogue)
            {
                return;
            }
            else
            {
                d.ResetEvent();
                return;
            }
        }
        //If character is null, return
        if (CharacterMove.CharacterTransform == null)
        {
            d.ResetEvent();
            return;
        }
        //Set in conversation variable
        PlayerInConversation = true;
        CurrentDialogue = d;

        CoroutineHelper.RunCoroutine(QuestDialogueRoutine(input, d, position));
    }
    /// <summary>
    /// Runs a non interactive Dialogue. All dialogues are expected to have only one respones.
    /// No random Dialogue. This is like a conversation. If the character attatched to the dialogue is null,
    /// then a random near character will be chosen to say it. If no characters are near, then the dialogue ends early.
    /// There must be a character to start the dialogue, other wise the location to start the dialogue is undefined.
    /// </summary>
    /// <param name="d"></param>
    public static void SingleDialogue(Dialogue d, FunctionCall f = null)
    {
        CoroutineHelper.RunCoroutine(SingleDialogueRoutine(d, f));
    }
    /// <summary>
    /// Make a Narrator DIalogue Box (single)
    /// </summary>
    /// <param name="d"></param>
    /// <param name="f"></param>
    public static void NarratorSingleDialogue(Dialogue d, FunctionCall f)
    {
        InNarratorConversation = true;
        CoroutineHelper.RunCoroutine(NarratorSinlgeDialogueRoutine(d, f));
    }
    /// <summary>
    /// A sub-Narrator dialogue that does not interact with the player
    /// </summary>
    /// <param name="d"></param>
    /// <param name="f"></param>
    public static void NarratorSingleSmallDialogue(Dialogue d, FunctionCall f)
    {
        CoroutineHelper.RunCoroutine(NarratorSinlgeSmallDialogueRoutine(d, f));
    }
    /// <summary>
    /// Makes a single dialogue above the player.
    /// </summary>
    /// <param name="d"></param>
    /// <param name="f"></param>
    public static void PlayerSingleDialogue(Dialogue d, FunctionCall f)
    {
        CoroutineHelper.RunCoroutine(PlayerSingleDialogueRoutine(d, f));
    }
    /// <summary>
    /// This Randomly chooses one dialogue string from the Dialogue.ThisDialogue string list and creates
    /// a dialogue bubble for it as if it were a standalone dialogue
    /// </summary>
    /// <param name="d"></param>
    /// <param name="OnThis"></param>
    public static void SingleRandomDialogueSelect(Dialogue d, CharacterData OnThis)
    {
        CoroutineHelper.RunCoroutine(SingleRandomDialogueRoutine(d, OnThis));
    }

    /// <summary>
    /// AudioClip used for NPC talking
    /// </summary>
    public static AudioClip NPCVoice
    {
        get
        {
            if (npcvoice == null)
                 npcvoice = SoundFX.GetClip("NPCVoice");
            return npcvoice;
        }
    }
    private static AudioClip npcvoice;

    private static IEnumerator QuestDialogueRoutine(Dialogue d, NPCDialogue npc_d, Transform t_pos)
    {
        yield return 0; yield return 0; yield return 0;
        int i = 0; NPCAI ainpc = null; GameObject d_box = null;
        foreach (string s in d.ThisDialogue)
        {
            //Get the NPC's AI and tell it that its in an important conversation. Yack!
            ainpc = d.SaysThisDialogue.GetComponent<NPCAI>();
            ainpc.InPlayerConversation = true;
            if (ainpc.CurrentDialogue != null) 
                Destroy(ainpc.CurrentDialogue);
            ainpc.CurrentDialogue = null;

            //make the dialogue box
            d_box = Prefabs.GetNewPrefab("Prefabs/DialogueBox");
            d_box.GetComponent<Canvas>().sortingOrder = 32;
            d_box.GetComponentInChildren<Text>().text = ModifyString(s);
            ChatDisplay.DisplayDialogue(d, d.SaysThisDialogue, i);
            d_box.transform.position = new Vector3(ainpc.transform.position.x - 0.5f, 0.01f,
                ainpc.transform.position.z + 0.32f);

            //Show wait for space UI..
            PlayerDialogueUI.SpaceTextObj.SetActive(true);
            PlayerDialogueUI.NPCDialogueBox(d, i);

            //play blip sound
            SoundFX.PlayAtPosition(d.SaysThisDialogue.transform.position, "Blip", 3.0f);

            ///display the character that is speaking
            PlayerDialogueUI.SetCharacterSpeaking(d.SaysThisDialogue);

            //Wait for space...
            while (!Input.GetKeyUp(KeyCode.Space))
            {
                //do speech fx
                DoNPCVoice(d.SaysThisDialogue);
                //if too far away from the conversation...
                if (CharacterMove.CharacterTransform == null ||
                    (t_pos.position - CharacterMove.CharacterTransform.position).magnitude > npc_d.AllowedDistanceFromDialogue)
                {
                    //stop speech fx
                    EndNPCVoice(d.SaysThisDialogue);
                    //reset player state
                    PlayerInConversation = false;
                    CurrentDialogue = null;
                    PlayerDialogueUI.HideNPCDialogueBox();
                    //reset..
                    npc_d.ResetEvent();
                    //Hide wait for space UI
                    PlayerDialogueUI.SpaceTextObj.SetActive(false);
                    //free resources (if we made any)
                    if (ainpc != null)
                        ainpc.InPlayerConversation = false;
                    if (d_box != null)
                        Destroy(d_box);
                    yield break;
                }
                yield return 0;
            } yield return 0;

            //stop speech fx
            EndNPCVoice(d.SaysThisDialogue);

            //Hide wait for space UI
            PlayerDialogueUI.SpaceTextObj.SetActive(false);
            PlayerDialogueUI.HideNPCDialogueBox();

            //increment i
            i++;

            //only destroy this dialogue if its not the last, the final is kept
            //for the player responses
            if (i < d.ThisDialogue.Count)
            {
                //destroy this dialogue gameobject, and wait for the next
                Destroy(d_box);
                d_box = null;
            }

            //box manager needs us to wait a few frames, for safety
            yield return 0; yield return 0; yield return 0; yield return 0;
        }

        //new dialogue var
        Dialogue NewDialogue = null;

        //if the dialogue ended... (the player doesn't have to respond)
        if (d.ThisResponses == null || d.ThisResponses.Count == 0)
        {
            //reset player state
            PlayerInConversation = false;
            CurrentDialogue = null;
            //free resources (if we made any)
            if (ainpc != null)
                ainpc.InPlayerConversation = false;
            if (d_box != null)
                Destroy(d_box);
            if (!npc_d.AutomaticAdvance)
            {
                if (npc_d.CanRun())
                    npc_d.BaseQuest.NextEvent();
            }
            yield break;
        }
        else
        {
            //Set up player's choices
            PlayerDialogueUI.MakeDialogue(d);

            yield return 0; yield return 0;
            //wait for player choice...
            while (!NPCDialogue.PlayerResponded)
            {
                //if too far away from the conversation...
                if (CharacterMove.CharacterTransform == null ||
                    (t_pos.position - CharacterMove.CharacterTransform.position).magnitude > npc_d.AllowedDistanceFromDialogue)
                {
                    //reset player state
                    PlayerInConversation = false;
                    CurrentDialogue = null;
                    //reset..
                    npc_d.ResetEvent();
                    //reset dialogue
                    PlayerDialogueUI.ResetDialogue();
                    //free resources (if we made any)
                    if (ainpc != null)
                        ainpc.InPlayerConversation = false;
                    if (d_box != null)
                        Destroy(d_box);

                    NPCDialogue.PlayerSelectedNewDialogue = null;
                    NPCDialogue.PlayerResponded = false;

                    yield break;
                }
                yield return 0;
            }
            yield return 0;

            //The new dialogue chain is something assumed to have been said by the NPC
            NewDialogue = NPCDialogue.PlayerSelectedNewDialogue;
            NPCDialogue.PlayerSelectedNewDialogue = null;
            NPCDialogue.PlayerResponded = false;

            //If the next dialogue is null, then the NPC doesn't respond from the player's choice,
            //and the dialogue event is over
            if (NewDialogue == null)
            {
                PlayerInConversation = false;
                CurrentDialogue = null;
                if (!npc_d.AutomaticAdvance)
                {
                    npc_d.BaseQuest.NextEvent();
                }
            }
        }

        //free resources (if we made any)
        if (ainpc != null)
            ainpc.InPlayerConversation = false;
        if (d_box != null)
            Destroy(d_box);
        PlayerInConversation = false;
        CurrentDialogue = null;

        //Recursively Make next quest dialogue (If there is one)
        if (NewDialogue != null)
        {
            MakeQuestDialogueBubble(NewDialogue, npc_d, t_pos);
        }

        yield return 0;
    }

    private static IEnumerator SingleDialogueRoutine(Dialogue d, FunctionCall f = null)
    {
        CharacterData c = d.SaysThisDialogue;
        //If there is no one to say the dialogue...
        if (c == null || d.ThisDialogue == null || d.ThisDialogue.Count == 0)
        {
            //Continue quest
            if (f != null && !f.AutomaticAdvance && f.CanRun())
                f.BaseQuest.NextEvent();
            yield break;
        }

        //Get NPCAI
        NPCAI npai = c.GetComponent<NPCAI>();

        //cant interupt player conversation..
        if (npai.InPlayerConversation)
        {
            //Continue quest
            if (f != null && !f.AutomaticAdvance && f.CanRun())
                f.BaseQuest.NextEvent();
            yield break;
        }

        //end previous dialogue
        if (npai.InConversation)
        {
            Destroy(npai.CurrentDialogue); npai.CurrentDialogue = null;
            npai.InConversation = true;
        }

        //play blip sound
        SoundFX.PlayAtPosition(c.transform.position, "Blip", 3.0f);

        //assign new dialogue
        GameObject box = Prefabs.GetNewPrefab("Prefabs/DialogueBox");
        npai.CurrentDialogue = box;

        //make dialogue and wait
        Text t = box.GetComponentInChildren<Text>();
        t.text = ModifyString(d.ThisDialogue[0]);
        box.transform.position = new Vector3(c.transform.position.x - 0.5f, 0.01f,
            c.transform.position.z + 0.32f);
        BoxManager.RepositionDialogue(box.GetComponentInChildren<Image>().rectTransform, c);
        box.GetComponent<BoxManager>().follow_this = c.gameObject;
        ChatDisplay.DisplayDialogue(d, c, 0);
        yield return new WaitForSeconds(t.text.Length * 0.09f < 1.25f ? 1.25f : t.text.Length * 0.09f + 0.1f); yield return 0;

        //Continue quest
        if (f != null &&!f.AutomaticAdvance && f.CanRun())
            f.BaseQuest.NextEvent();

        //free resources && reset state
        npai.InConversation = false;
        if (box != null && !npai.InPlayerConversation)
            Destroy(box);
        npai.CurrentDialogue = null;
    }
    private static IEnumerator NarratorSinlgeDialogueRoutine(Dialogue d, FunctionCall f)
    {
        if (CharacterMove.CharacterTransform == null)
        {
            InNarratorConversation = false;
            yield break;
        }

        //If there is no one to say the dialogue...
        if (d.ThisDialogue == null || d.ThisDialogue.Count == 0)
        {
            InNarratorConversation = false;
            //Continue quest
            if (!f.AutomaticAdvance && f.CanRun())
                f.BaseQuest.NextEvent();
            yield break;
        }

        //play blip sound
        SoundFX.Play(SoundFX.GetClip("Blip"), 2.0f);

        //make dialogue
        NarratorUI.NarratorBigSaySingle(d);
        PlayerDialogueUI.SpaceTextObj.SetActive(true);

        //wait for space
        while (!Input.GetKeyUp(KeyCode.Space)) yield return 0; yield return 0;

        //hide dialogue
        PlayerDialogueUI.SpaceTextObj.SetActive(false);
        NarratorUI.HideAllBig();

        //reset state
        InNarratorConversation = false;
        //cal next event
        if (!f.AutomaticAdvance && f.CanRun())
            f.BaseQuest.NextEvent();
    }
    private static IEnumerator NarratorSinlgeSmallDialogueRoutine(Dialogue d, FunctionCall f)
    {
        if (CharacterMove.CharacterTransform == null)
            yield break;

        //If there is no one to say the dialogue...
        if (d.ThisDialogue == null || d.ThisDialogue.Count == 0)
        {
            //Continue quest
            if (!f.AutomaticAdvance && f.CanRun())
                f.BaseQuest.NextEvent();
            yield break;
        }

        //play blip sound
        SoundFX.Play(SoundFX.GetClip("Blip"), 2.0f);

        //make dialogue
        NarratorUI.NarratorLittleSaySingle(d);

        //wait for space
        yield return new WaitForSeconds(d.ThisDialogue[0].Length * 0.1f < 1.4f ? 1.4f :
            d.ThisDialogue[0].Length * 0.1f + 0.1f); yield return 0;

        //hide dialogue
        NarratorUI.HideAllSmall();

        if (!f.AutomaticAdvance && f.CanRun())
            f.BaseQuest.NextEvent();
    }
    private static IEnumerator PlayerSingleDialogueRoutine(Dialogue d, FunctionCall f)
    {
        if (CharacterMove.CharacterTransform == null)
            yield break;

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        //If there is no one to say the dialogue...
        if (c == null || d.ThisDialogue == null || d.ThisDialogue.Count == 0)
        {
            //Continue quest
            if (!f.AutomaticAdvance && f.CanRun())
                f.BaseQuest.NextEvent();
            yield break;
        }

        //play blip sound
        SoundFX.PlayAtPosition(c.transform.position, "Blip", 3.0f);

        //assign new dialogue
        GameObject box = Prefabs.GetNewPrefab("Prefabs/DialogueBox");

        //make dialogue and wait
        Text t = box.GetComponentInChildren<Text>();
        t.text = ModifyString(d.ThisDialogue[0]);
        box.transform.position = new Vector3(c.transform.position.x - 0.5f, 0.01f,
            c.transform.position.z + 0.32f);
        BoxManager.RepositionDialogue(box.GetComponentInChildren<Image>().rectTransform, c);
        box.GetComponent<BoxManager>().follow_this = c.gameObject;
        ChatDisplay.DisplayDialogue(d, c, 0);
        yield return new WaitForSeconds(t.text.Length * 0.09f < 1.4f ? 1.4f : t.text.Length * 0.09f + 0.1f); yield return 0;

        //Continue quest
        if (!f.AutomaticAdvance && f.CanRun())
            f.BaseQuest.NextEvent();

        if (box != null)
            Destroy(box);
    }
    private static IEnumerator DialogueRoutine(Dialogue d, GameObject box, CharacterData MyBubble, NPCAI sender)
    {
        if (!MyBubble.IsDead)
        {
            int i = 0;
            foreach (string s in d.ThisDialogue)
            {
                if (box != null)
                {
                    if (MyBubble.IsDead)
                        break;
                    if (sender.InPlayerConversation)
                        break;

                    //play blip sound
                    SoundFX.PlayAtPosition(MyBubble.transform.position, "Blip", 3.0f);

                    Text t = box.GetComponentInChildren<Text>();
                    t.text = ModifyString(s);
                    box.transform.position = new Vector3(MyBubble.transform.position.x -0.5f, 0.01f,
                        MyBubble.transform.position.z + 0.32f);
                    BoxManager.RepositionDialogue(box.GetComponentInChildren<Image>().rectTransform, MyBubble);
                    box.GetComponent<BoxManager>().follow_this = MyBubble.gameObject;
                    ChatDisplay.DisplayDialogue(d, MyBubble, i);
                    yield return new WaitForSeconds(t.text.Length * 0.08f < 1.25f ? 1.25f : t.text.Length * 0.08f + 0.1f);
                }
                i++;
            }
        }

        //If this dialogue contains responses, try to have someone respond.
        if (d.ThisResponses != null && d.ThisResponses.Count > 0 && !MyBubble.IsDead)
        {
            CharacterData Responder = GetClosestNPC(MyBubble, 3.5f);
            if (Responder != null)
                MakeDialogueBubble(d.ThisResponses[Random.Range(0, d.ThisResponses.Count)],
                    Responder, Responder.GetComponent<NPCAI>());
        }

        sender.InConversation = false;
        if (box != null && !sender.InPlayerConversation)
            Destroy(box);
        sender.CurrentDialogue = null;
    }

    private static IEnumerator SingleRandomDialogueRoutine(Dialogue d, CharacterData c)
    {
        //If there is no one to say the dialogue...
        if (c == null || d.ThisDialogue == null || d.ThisDialogue.Count == 0)
        {
            yield break;
        }

        //Get NPCAI
        NPCAI npai = c.GetComponent<NPCAI>();

        //cant interupt player conversation..
        if (npai.InPlayerConversation)
        {
            yield break;
        }

        //end previous dialogue
        if (npai.InConversation)
        {
            Destroy(npai.CurrentDialogue); npai.CurrentDialogue = null;
            npai.InConversation = true;
        }

        //play blip sound
        SoundFX.PlayAtPosition(c.transform.position, "Blip", 3.0f);

        //assign new dialogue
        GameObject box = Prefabs.GetNewPrefab("Prefabs/DialogueBox");
        npai.CurrentDialogue = box;

        //make dialogue and wait
        Text t = box.GetComponentInChildren<Text>();
        int num = 0;
        t.text = ModifyString(d.ThisDialogue[num = Random.Range(0, d.ThisDialogue.Count)]);
        box.transform.position = new Vector3(c.transform.position.x - 0.5f, 0.01f,
            c.transform.position.z + 0.32f);
        BoxManager.RepositionDialogue(box.GetComponentInChildren<Image>().rectTransform, c);
        box.GetComponent<BoxManager>().follow_this = c.gameObject;
        ChatDisplay.DisplayDialogue(d, c, num);
        yield return new WaitForSeconds(t.text.Length * 0.09f < 1.25f ? 1.25f : t.text.Length * 0.09f + 0.1f); yield return 0;

        //free resources && reset state
        npai.InConversation = false;
        if (box != null && !npai.InPlayerConversation)
            Destroy(box);
        npai.CurrentDialogue = null;
    }

    /// <summary>
    /// Returns closest NPC within 'radius' units of the input character. Returns null if there is none
    /// </summary>
    /// <param name="nearThis"></param>
    /// <returns></returns>
    private static CharacterData GetClosestNPC(CharacterData nearThis, float radius)
    {
        Collider[] cols = 
            Physics.OverlapSphere(nearThis.transform.position, radius, 1 << LayerMask.NameToLayer("NPCOnlyCollisions"));

        if (cols != null)
        {
            CharacterData foundChar = null;
            float shortest_dist = float.MaxValue;
            foreach (Collider c in cols)
            {
                CharacterData current_char = null;
                current_char = c.transform.parent.GetComponent<CharacterData>();
                if (current_char != nearThis && current_char != null && (current_char.transform.position - nearThis.transform.position).sqrMagnitude
                    < shortest_dist)
                {
                    foundChar = current_char;
                    shortest_dist = (current_char.transform.position - nearThis.transform.position).sqrMagnitude;
                }
            }
            return foundChar;
        }
        return null;
    }

    /// <summary>
    /// Replaces special string sequences
    /// </summary>
    /// <param name="says"></param>
    public static string ModifyString(string says)
    {
        return says.Replace(":name:", CharacterMove.CharacterTransform.GetComponent<CharacterData>().first_name);
    }

    /// <summary>
    /// Ensure the NPC is using speech audio
    /// </summary>
    /// <param name="c"></param>
    static void DoNPCVoice(CharacterData c)
    {
        if (c != null)
        {
            AudioSource s = c.GetComponent<AudioSource>();
            if (s.clip != NPCVoice)
                s.clip = NPCVoice;
            if (!s.isPlaying)
                s.Play();
            if (!s.loop)
                s.loop = true;
            if (s.volume != 1.75f)
                s.volume = 1.75f;
        }
    }
    /// <summary>
    /// Stop the NPC from using speech audio
    /// </summary>
    /// <param name="c"></param>
    static void EndNPCVoice(CharacterData c)
    {
        if (c != null)
        {
            AudioSource s = c.GetComponent<AudioSource>();
            if (s.clip != null)
            {
                s.Stop();
                s.clip = null;
            }
            s.loop = false;
            s.volume = 1.0f;
        }
    }
}
