﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class QuestDat
{
    public int Stage;
    public bool completed;
    public bool started;
    public string QuestName;
}
