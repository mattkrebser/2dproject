﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SphereCollider))]
public class EnvironmentLoop : MonoBehaviour
{
    [Tooltip("This music clip will be played continuously while the player is in this zone")]
    public MusicClip PlayThis;

    public SpriteRenderer MapImage;
    public LightingMode lightMode;

    private float radiusSqrd = 0.0f;
    private Color ColorTarget;

    private Color[] colors = { Color.red, Color.blue, Color.green, Color.magenta, Color.yellow, Color.cyan };

    void Awake()
    {
        radiusSqrd = GetComponent<SphereCollider>().radius;
        radiusSqrd *= radiusSqrd;
    }

    void OnTriggerEnter(Collider c)
    {
        
        if (PlayThis != null && PlayThis.music != null)
            MusicPlayer.PlayTrack(PlayThis.music, 1000000000f, PlayThis.volume);
    }

    void OnTriggerExit(Collider c)
    {
        if (PlayThis != null && PlayThis.music != null)
        MusicPlayer.ResumeNormalPlayer();
    }

    void OnTriggerStay(Collider c)
    {
        DoLighting();
    }

    void DoLighting()
    {
        if (MapImage != null)
        {
            NextColor();
            MapImage.color = Color.Lerp(MapImage.color, ColorTarget, Time.deltaTime * 10);
        }
    }

    void NextColor()
    {
        if (lightMode == LightingMode.Colorful)
        {
            if (MapImage.color == ColorTarget || ColorTarget == new Color(0,0,0,0))
            {
                ColorTarget = colors[Random.Range(0, colors.Length)] * 0.5f + Color.white * 0.5f;
            }
        }
    }
}


public enum LightingMode
{
    Colorful
}