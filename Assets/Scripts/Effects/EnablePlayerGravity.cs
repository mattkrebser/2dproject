﻿using UnityEngine;
using System.Collections;

public class EnablePlayerGravity : MonoBehaviour
{

    void OnTriggerEnter(Collider c)
    {
        CharacterData cD = c.GetComponent<CharacterData>();
        if (cD != null && cD.IsPlayer)
        {
            cD.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        }
    }

    void OnTriggerExit(Collider c)
    {
        CharacterData cD = c.GetComponent<CharacterData>();
        if (cD != null && cD.IsPlayer)
        {
            cD.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation |
                RigidbodyConstraints.FreezePositionY;
        }
    }
}
