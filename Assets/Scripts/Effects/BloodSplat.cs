﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BloodSplat : MonoBehaviour
{
    public float time_until_destroy = 15.0f;

    private static List<Sprite> blood_splats;

    private static List<Sprite> blood_bodies;
    private static List<Sprite> blood_faces;
    private static List<Sprite> blood_feet;
    private static List<Sprite> blood_armL;
    private static List<Sprite> blood_armR;

    private static List<Sprite> large_blood_splats;

    private static Transform this_transform;

    static Queue<BloodHolder> LargeSplats = new Queue<BloodHolder>();
    static Queue<BloodHolder> Splats = new Queue<BloodHolder>();
    static Queue<BloodHolder> BodyParts = new Queue<BloodHolder>();

    void Awake()
    {
        blood_splats = new List<Sprite>(Resources.LoadAll<Sprite>("BloodSprites/Splats"));
        blood_bodies = new List<Sprite>(Resources.LoadAll<Sprite>("BloodSprites/BloodBody/BloodBody"));
        blood_faces = new List<Sprite>(Resources.LoadAll<Sprite>("BloodSprites/BloodBody/BloodFace"));
        blood_feet = new List<Sprite>(Resources.LoadAll<Sprite>("BloodSprites/BloodBody/BloodFeet"));
        blood_armL = new List<Sprite>(Resources.LoadAll<Sprite>("BloodSprites/BloodBody/BloodLeftArm"));
        blood_armR = new List<Sprite>(Resources.LoadAll<Sprite>("BloodSprites/BloodBody/BloodRightArm"));
        large_blood_splats = new List<Sprite>(Resources.LoadAll<Sprite>("BloodSprites/LargeSplats"));

        if (this_transform == null)
        {
            this_transform = transform;
        }
    }

    void Update()
    {
        while (LargeSplats.Count > 25 || (LargeSplats.Count > 0 &&
            Time.realtimeSinceStartup - LargeSplats.Peek().time > time_until_destroy))
        {
            BloodHolder bH = LargeSplats.Dequeue();
            Destroy(bH.blood);
        }
        while (Splats.Count > 25 || (Splats.Count > 0 &&
            Time.realtimeSinceStartup - Splats.Peek().time > time_until_destroy))
        {
            BloodHolder bH = Splats.Dequeue();
            Destroy(bH.blood);
        }
        while (BodyParts.Count > 25 || (BodyParts.Count > 0 &&
            Time.realtimeSinceStartup - BodyParts.Peek().time > time_until_destroy))
        {
            BloodHolder bH = BodyParts.Dequeue();
            Destroy(bH.blood);
        }
    }

    void OnDestroy()
    {
        blood_splats.Clear();
        blood_bodies.Clear();
        blood_faces.Clear();
        blood_feet.Clear();
        blood_armL.Clear();
        blood_armR.Clear();
        large_blood_splats.Clear();
        LargeSplats.Clear();
        Splats.Clear();
        BodyParts.Clear();
        this_transform = null;
    }

	public static void MakeBloodSplat(Vector3 position)
    {
        //if touching the wall, then just return
        if (Physics.CheckSphere(position, 0.08f, 1 << LayerMask.NameToLayer("Walls")))
            return;

        GameObject splat = new GameObject("BloodSplat");
        splat.layer = LayerMask.NameToLayer("FX");
        splat.transform.SetParent(this_transform);
        float dx = Random.Range(-0.2f, 0.2f);
        float dz = Random.Range(-0.2f, 0.2f);
        splat.transform.position = new Vector3(position.x + dx, 0.01f, position.z + dz);
        splat.transform.eulerAngles = new Vector3(90, Random.Range(0, 360), 0);
        splat.transform.localScale = new Vector3(1.75f, 1.75f, 1.75f);
        SpriteRenderer s = splat.AddComponent<SpriteRenderer>();
        s.sortingOrder = 1;
        s.sprite = blood_splats[Random.Range(0,blood_splats.Count)];
        Splats.Enqueue(new BloodHolder(splat, Time.realtimeSinceStartup));
    }

    public static void MakeLargeBloodSplat(Vector3 position)
    {
        //if touching the wall, then just return
        if (Physics.CheckSphere(position, 0.13f, 1 << LayerMask.NameToLayer("Walls")))
            return;

        GameObject splat = new GameObject("LargeBloodSplat");
        splat.layer = LayerMask.NameToLayer("FX");
        splat.transform.SetParent(this_transform);
        float dx = Random.Range(-0.2f, 0.2f);
        float dz = Random.Range(-0.2f, 0.2f);
        splat.transform.position = new Vector3(position.x + dx, 0, position.z + dz);
        splat.transform.eulerAngles = new Vector3(90, Random.Range(0, 360), 0);
        splat.transform.localScale = new Vector3(1.75f, 1.75f, 1.75f);
        SpriteRenderer s = splat.AddComponent<SpriteRenderer>();
        s.sortingOrder = 1;
        s.sprite = large_blood_splats[Random.Range(0, large_blood_splats.Count)];
        LargeSplats.Enqueue(new BloodHolder(splat, Time.realtimeSinceStartup));
    }

    public static void KillCharacterFX(CharacterData c)
    {
        GameObject rupture = Prefabs.GetNewPrefab("FX/Rupture");
        rupture.transform.position = c.transform.position;
        Destroy(rupture, rupture.GetComponent<ParticleSystem>().startLifetime);

        //randomly place body parts on the ground... woop woop!
        if (Random.Range(0, 2) == 1)
            MakeBloodParts(c.transform.position, blood_bodies[Random.Range(0, blood_bodies.Count)]);
        if (Random.Range(0, 2) == 1)
            MakeBloodParts(c.transform.position, blood_faces[Random.Range(0, blood_faces.Count)]);
        if (Random.Range(0, 2) == 1)
            MakeBloodParts(c.transform.position, blood_feet[Random.Range(0, blood_feet.Count)]);
        if (Random.Range(0, 2) == 1)
            MakeBloodParts(c.transform.position, blood_armL[Random.Range(0, blood_armL.Count)]);
        if (Random.Range(0, 2) == 1)
            MakeBloodParts(c.transform.position, blood_armR[Random.Range(0, blood_armR.Count)]);

        MakeLargeBloodSplat(c.transform.position);
        MakeLargeBloodSplat(c.transform.position);
    }

    static void MakeBloodParts(Vector3 position, Sprite sprite)
    {
        //if touching the wall, then just return
        if (Physics.CheckSphere(position, 0.08f, 1 << LayerMask.NameToLayer("Walls")))
            return;

        GameObject splat = new GameObject("BloodPart");
        splat.layer = LayerMask.NameToLayer("FX");
        splat.transform.SetParent(this_transform);
        float dx = Random.Range(-0.3f, 0.2f);
        float dz = Random.Range(-0.3f, 0.2f);
        splat.transform.position = new Vector3(position.x + dx, 0, position.z + dz);
        splat.transform.eulerAngles = new Vector3(90, 0, 0);
        splat.transform.localScale = new Vector3(1.75f, 1.75f, 1.75f);
        SpriteRenderer s = splat.AddComponent<SpriteRenderer>();
        s.sortingOrder = 1;
        s.sprite = sprite;
        BodyParts.Enqueue(new BloodHolder(splat, Time.realtimeSinceStartup));
    }

    public struct BloodHolder
    {
        public GameObject blood;
        public float time;

        public BloodHolder(GameObject obj, float t)
        {
            blood = obj; time = t;
        }
    }
}
