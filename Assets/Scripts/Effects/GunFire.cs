﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Spawns weapon fire in front of weapons that use it
/// </summary>
public class GunFire : MonoBehaviour
{
    private static Dictionary<string, Vector3> gun_barrel_position = new Dictionary<string, Vector3>();

    void Awake()
    {
        //FX Positions Yack!
        gun_barrel_position.Add("SMG_R_East", new Vector3(0.338f, 0, -0.095f));
        gun_barrel_position.Add("SMG_R_South", new Vector3(-0.432f, 0, -0.258f));
        gun_barrel_position.Add("SMG_R_West", new Vector3(-0.338f, 0, -0.095f));
        gun_barrel_position.Add("SMG_R_North", new Vector3(0.395f, 0, 0.18f));

        gun_barrel_position.Add("SMG_L_East", new Vector3(0.338f, 0, -0.095f));
        gun_barrel_position.Add("SMG_L_South", new Vector3(0.432f, 0, -0.258f));
        gun_barrel_position.Add("SMG_L_West", new Vector3(-0.338f, 0, -0.095f));
        gun_barrel_position.Add("SMG_L_North", new Vector3(-0.395f, 0, 0.18f));

        gun_barrel_position.Add("M16_R_East", new Vector3(0.338f, 0, -0.095f));
        gun_barrel_position.Add("M16_R_South", new Vector3(-0.432f, 0, -0.258f));
        gun_barrel_position.Add("M16_R_West", new Vector3(-0.338f, 0, -0.095f));
        gun_barrel_position.Add("M16_R_North", new Vector3(0.395f, 0, 0.18f));

        gun_barrel_position.Add("M16_L_East", new Vector3(0.338f, 0, -0.095f));
        gun_barrel_position.Add("M16_L_South", new Vector3(0.432f, 0, -0.258f));
        gun_barrel_position.Add("M16_L_West", new Vector3(-0.338f, 0, -0.095f));
        gun_barrel_position.Add("M16_L_North", new Vector3(-0.395f, 0, 0.18f));

        gun_barrel_position.Add("Shotgun_R_East", new Vector3(0.338f, 0, -0.095f));
        gun_barrel_position.Add("Shotgun_R_South", new Vector3(-0.432f, 0, -0.258f));
        gun_barrel_position.Add("Shotgun_R_West", new Vector3(-0.338f, 0, -0.095f));
        gun_barrel_position.Add("Shotgun_R_North", new Vector3(0.395f, 0, 0.18f));

        gun_barrel_position.Add("Shotgun_L_East", new Vector3(0.338f, 0, -0.095f));
        gun_barrel_position.Add("Shotgun_L_South", new Vector3(0.432f, 0, -0.258f));
        gun_barrel_position.Add("Shotgun_L_West", new Vector3(-0.338f, 0, -0.095f));
        gun_barrel_position.Add("Shotgun_L_North", new Vector3(-0.395f, 0, 0.18f));

        gun_barrel_position.Add("UZI_R_East", new Vector3(0.29f, 0, -0.06f));
        gun_barrel_position.Add("UZI_R_South", new Vector3(-0.372f, 0, -0.198f));
        gun_barrel_position.Add("UZI_R_West", new Vector3(-0.29f, 0, -0.06f));
        gun_barrel_position.Add("UZI_R_North", new Vector3(0.335f, 0, 0.1f));

        gun_barrel_position.Add("UZI_L_East", new Vector3(0.29f, 0, -0.06f));
        gun_barrel_position.Add("UZI_L_South", new Vector3(0.352f, 0, -0.198f));
        gun_barrel_position.Add("UZI_L_West", new Vector3(-0.29f, 0, -0.06f));
        gun_barrel_position.Add("UZI_L_North", new Vector3(-0.335f, 0, 0.1f));

        gun_barrel_position.Add("UMP_R_East", new Vector3(0.29f, 0, -0.09f));
        gun_barrel_position.Add("UMP_R_South", new Vector3(-0.372f, 0, -0.198f));
        gun_barrel_position.Add("UMP_R_West", new Vector3(-0.29f, 0, -0.09f));
        gun_barrel_position.Add("UMP_R_North", new Vector3(0.335f, 0, 0.1f));

        gun_barrel_position.Add("UMP_L_East", new Vector3(0.29f, 0, -0.09f));
        gun_barrel_position.Add("UMP_L_South", new Vector3(0.352f, 0, -0.198f));
        gun_barrel_position.Add("UMP_L_West", new Vector3(-0.29f, 0, -0.09f));
        gun_barrel_position.Add("UMP_L_North", new Vector3(-0.335f, 0, 0.1f));

        gun_barrel_position.Add("Handgun_R_East", new Vector3(0.29f, 0, -0.09f));
        gun_barrel_position.Add("Handgun_R_South", new Vector3(-0.372f, 0, -0.198f));
        gun_barrel_position.Add("Handgun_R_West", new Vector3(-0.29f, 0, -0.09f));
        gun_barrel_position.Add("Handgun_R_North", new Vector3(0.335f, 0, 0.1f));

        gun_barrel_position.Add("Handgun_L_East", new Vector3(0.29f, 0, -0.09f));
        gun_barrel_position.Add("Handgun_L_South", new Vector3(0.352f, 0, -0.198f));
        gun_barrel_position.Add("Handgun_L_West", new Vector3(-0.29f, 0, -0.09f));
        gun_barrel_position.Add("Handgun_L_North", new Vector3(-0.335f, 0, 0.1f));

        gun_barrel_position.Add("Magnum_R_East", new Vector3(0.29f, 0, -0.09f));
        gun_barrel_position.Add("Magnum_R_South", new Vector3(-0.372f, 0, -0.198f));
        gun_barrel_position.Add("Magnum_R_West", new Vector3(-0.29f, 0, -0.09f));
        gun_barrel_position.Add("Magnum_R_North", new Vector3(0.335f, 0, 0.1f));

        gun_barrel_position.Add("Magnum_L_East", new Vector3(0.29f, 0, -0.09f));
        gun_barrel_position.Add("Magnum_L_South", new Vector3(0.352f, 0, -0.198f));
        gun_barrel_position.Add("Magnum_L_West", new Vector3(-0.29f, 0, -0.09f));
        gun_barrel_position.Add("Magnum_L_North", new Vector3(-0.335f, 0, 0.1f));

        gun_barrel_position.Add("Bazooka_R_East", new Vector3(0.338f, 0, -0.095f));
        gun_barrel_position.Add("Bazooka_R_South", new Vector3(-0.432f, 0, -0.258f));
        gun_barrel_position.Add("Bazooka_R_West", new Vector3(-0.338f, 0, -0.095f));
        gun_barrel_position.Add("Bazooka_R_North", new Vector3(0.395f, 0, 0.18f));

        gun_barrel_position.Add("Bazooka_L_East", new Vector3(0.338f, 0, -0.095f));
        gun_barrel_position.Add("Bazooka_L_South", new Vector3(0.432f, 0, -0.258f));
        gun_barrel_position.Add("Bazooka_L_West", new Vector3(-0.338f, 0, -0.095f));
        gun_barrel_position.Add("Bazooka_L_North", new Vector3(-0.395f, 0, 0.18f));
    }

    void OnDestroy()
    {
        gun_barrel_position.Clear();
    }

    /// <summary>
    /// Plays gunfire animation and hit position, also fires
    /// bazooka and throws grenade
    /// </summary>
    /// <param name="isRight">right/left weapon</param>
    /// <param name="c"></param>
    /// <param name="guntype"></param>
    /// <param name="washit"> Did the weapon projectile hit something? (Should we add bullet hit effect?)</param>
    public static void FiredWeaponAnimation(bool isRight, CharacterData c, Guns guntype,
        RaycastHit hitpos, bool washit = false)
    {
        if (CharacterMove.CharacterTransform == null)
            return;
        if ((CharacterMove.CharacterTransform.position - c.transform.position).magnitude < Constants.RenderFXDistance)
        {
            Vector3 GunFireRotationEuler;
            //Particle system rotations are in radians!!!!
            if (isRight)
            {
                if (c.CharacterFacingDirection == Direction.North)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 1.25f);
                }
                else if (c.CharacterFacingDirection == Direction.South)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 0.25f);
                }
                else if (c.CharacterFacingDirection == Direction.East)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 1.5f);
                }
                else
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 0.5f);
                }
            }
            else
            {
                if (c.CharacterFacingDirection == Direction.North)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 0.75f);
                }
                else if (c.CharacterFacingDirection == Direction.South)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 1.75f);
                }
                else if (c.CharacterFacingDirection == Direction.East)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 1.5f);
                }
                else
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 0.5f);
                }
            }

            if (guntype == Guns.Bazooka || guntype == Guns.Grenade)
            {
                Projectile(isRight, c, guntype, GunFireRotationEuler);
                return;
            }

            GameObject anim = Prefabs.GetNewPrefab("FX/GunFire1");
            anim.transform.eulerAngles = new Vector3(0, 0, 0);
            ParticleSystem ps = anim.GetComponent<ParticleSystem>();
            ps.startRotation3D = GunFireRotationEuler;

            if (guntype != Guns.Bazooka && guntype != Guns.Grenade)
            {
                if (c.IsPlayer)
                    anim.transform.SetParent(c.transform);
                else
                    anim.transform.SetParent(c.sprite_child.transform);
            }

            if (guntype == Guns.SMG)
            {
                anim.transform.localPosition = gun_barrel_position["SMG_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.UMP)
            {
                anim.transform.localPosition = gun_barrel_position["UMP_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.UZI)
            {
                anim.transform.localPosition = gun_barrel_position["UZI_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.Handgun)
            {
                anim.transform.localPosition = gun_barrel_position["Handgun_" + (isRight ? "R_" : "L_")
                 + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.Shotgun)
            {
                anim.transform.localPosition = gun_barrel_position["Shotgun_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.Magnum)
            {
                anim.transform.localPosition = gun_barrel_position["Magnum_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.M16)
            {
                anim.transform.localPosition = gun_barrel_position["M16_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            if (washit && hitpos.collider != null)
                BulletLineFX(c, anim.transform.position, hitpos);

            anim.transform.position = new Vector3(anim.transform.position.x, 0, anim.transform.position.z);

            GunNoise(guntype, c);
            Destroy(anim, ps.startLifetime);
        }
    }

    static void Projectile(bool isRight, CharacterData c, Guns guntype, Vector3 gun_rotation)
    {
        if (guntype == Guns.Bazooka)
        {
            GameObject anim = Prefabs.GetNewPrefab("FX/GunFire2");
            ParticleSystem ps = anim.GetComponent<ParticleSystem>();
            ps.startRotation3D = gun_rotation;
            anim.transform.SetParent(c.transform);
            anim.transform.localPosition = gun_barrel_position["Bazooka_" + (isRight ? "R_" : "L_")
                + c.CharacterFacingDirection.ToString()];
            Destroy(anim, ps.startLifetime);
        }
    }

    /// <summary>
    /// Returns where the gun barallel ends for the specified character and weapon
    /// </summary>
    /// <param name="c"></param>
    /// <param name="guntype"></param>
    /// <param name="isRight">right/left weapon</param>
    /// <returns></returns>
    public static Vector3 GetWeaponFirePoint(CharacterData c, Guns guntype, bool isRight)
    {
        Vector3 offset = Vector3.zero;

        if (guntype == Guns.SMG)
        {
            offset = gun_barrel_position["SMG_" + (isRight ? "R_" : "L_")
                + c.CharacterFacingDirection.ToString()];
        }
        else if (guntype == Guns.UMP)
        {
            offset = gun_barrel_position["UMP_" + (isRight ? "R_" : "L_")
                + c.CharacterFacingDirection.ToString()];
        }
        else if (guntype == Guns.UZI)
        {
            offset = gun_barrel_position["UZI_" + (isRight ? "R_" : "L_")
                + c.CharacterFacingDirection.ToString()];
        }
        else if (guntype == Guns.Handgun)
        {
            offset = gun_barrel_position["Handgun_" + (isRight ? "R_" : "L_")
             + c.CharacterFacingDirection.ToString()];
        }
        else if (guntype == Guns.Shotgun)
        {
            offset = gun_barrel_position["Shotgun_" + (isRight ? "R_" : "L_")
                + c.CharacterFacingDirection.ToString()];
        }
        else if (guntype == Guns.Magnum)
        {
            offset = gun_barrel_position["Magnum_" + (isRight ? "R_" : "L_")
                + c.CharacterFacingDirection.ToString()];
        }
        else if (guntype == Guns.M16)
        {
            offset = gun_barrel_position["M16_" + (isRight ? "R_" : "L_")
                + c.CharacterFacingDirection.ToString()];
        }
        else if (guntype == Guns.Bazooka)
        {
            offset = gun_barrel_position["Bazooka_" + (isRight ? "R_" : "L_")
                + c.CharacterFacingDirection.ToString()];
        }

        return c.transform.position + offset;
    }

    /// <summary>
    /// Make bullet lines fx
    /// </summary>
    /// <param name="c"></param>
    /// <param name="Start"></param>
    /// <param name="r"></param>
    static void BulletLineFX(CharacterData c, Vector3 Start, RaycastHit r)
    {
        if ((CharacterMove.CharacterTransform.position - Start).magnitude < Constants.RenderFXDistance)
        {
            Vector3 pt = RandomizePoint(0.1f, r.point);
            Vector3 upLine = new Vector3(0, 0.3f, 0);
            //bullet line
            GameObject bullet_streak = Prefabs.GetNewPrefab("FX/BulletFX");
            LineRenderer lr = bullet_streak.GetComponent<LineRenderer>();
            lr.SetPosition(0, Start + upLine);
            lr.SetPosition(1, pt + upLine);
            Destroy(bullet_streak, 0.1f);
            //bullet hit
            GameObject bulletHitFX;

            if (r.collider.gameObject.GetComponent<RecognizeWasHit>() != null &&
                r.collider.gameObject.GetComponent<RecognizeWasHit>().attatchedCharacter != null)
            {
                bulletHitFX = Prefabs.GetNewPrefab("FX/BulletHitRed");
            }
            else
            {
                bulletHitFX = Prefabs.GetNewPrefab("FX/BulletHit");
            }

            bulletHitFX.transform.position = pt;

            ParticleSystem p = bulletHitFX.GetComponent<ParticleSystem>();
            p.startRotation = (-c.RotationEuler.y) * Mathf.Deg2Rad;

            Destroy(bulletHitFX, p.startLifetime);
        }
    }

    /// <summary>
    /// Randomize the input point by up to the input amount (only xz coords)
    /// </summary>
    /// <param name="magnitude"></param>
    /// <param name="point"></param>
    /// <returns></returns>
    static Vector3 RandomizePoint(float magnitude, Vector3 point)
    {
        float dz = Random.Range(0, magnitude);
        float dx = Random.Range(0, magnitude);

        dz = Random.Range(0, 2) == 1 ? dz : -dz;
        dx = Random.Range(0, 2) == 1 ? dx : -dx;

        return new Vector3(point.x + dx, point.y, point.z + dz);
    }

    /// <summary>
    /// Fire Weapon Animation without any bullet hit effects
    /// </summary>
    /// <param name="isRight"></param>
    /// <param name="c"></param>
    /// <param name="guntype"></param>
    /// <param name="start"></param>
    /// <param name="end"></param>
    public static void FiredWeaponNoHit(bool isRight, CharacterData c, Guns guntype, Vector3 start, Vector3 end)
    {
        if (CharacterMove.CharacterTransform == null)
            return;
        if ((CharacterMove.CharacterTransform.position - start).magnitude < Constants.RenderFXDistance)
        {
            Vector3 GunFireRotationEuler;
            //Particle system rotations are in radians!!!!
            if (isRight)
            {
                if (c.CharacterFacingDirection == Direction.North)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 1.25f);
                }
                else if (c.CharacterFacingDirection == Direction.South)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 0.25f);
                }
                else if (c.CharacterFacingDirection == Direction.East)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 1.5f);
                }
                else
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 0.5f);
                }
            }
            else
            {
                if (c.CharacterFacingDirection == Direction.North)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 0.75f);
                }
                else if (c.CharacterFacingDirection == Direction.South)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 1.75f);
                }
                else if (c.CharacterFacingDirection == Direction.East)
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 1.5f);
                }
                else
                {
                    GunFireRotationEuler = new Vector3(0, 0, Mathf.PI * 0.5f);
                }
            }

            if (guntype == Guns.Bazooka || guntype == Guns.Grenade)
            {
                Projectile(isRight, c, guntype, GunFireRotationEuler);
                return;
            }

            GameObject anim = Prefabs.GetNewPrefab("FX/GunFire1");
            anim.transform.eulerAngles = new Vector3(0, 0, 0);
            ParticleSystem ps = anim.GetComponent<ParticleSystem>();
            ps.startRotation3D = GunFireRotationEuler;

            if (guntype != Guns.Bazooka && guntype != Guns.Grenade)
            {
                if (c.IsPlayer)
                    anim.transform.SetParent(c.transform);
                else
                    anim.transform.SetParent(c.sprite_child.transform);
            }

            if (guntype == Guns.SMG)
            {
                anim.transform.localPosition = gun_barrel_position["SMG_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.UMP)
            {
                anim.transform.localPosition = gun_barrel_position["UMP_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.UZI)
            {
                anim.transform.localPosition = gun_barrel_position["UZI_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.Handgun)
            {
                anim.transform.localPosition = gun_barrel_position["Handgun_" + (isRight ? "R_" : "L_")
                 + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.Shotgun)
            {
                anim.transform.localPosition = gun_barrel_position["Shotgun_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.Magnum)
            {
                anim.transform.localPosition = gun_barrel_position["Magnum_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }
            else if (guntype == Guns.M16)
            {
                anim.transform.localPosition = gun_barrel_position["M16_" + (isRight ? "R_" : "L_")
                    + c.CharacterFacingDirection.ToString()];
            }

            anim.transform.position = new Vector3(anim.transform.position.x, 0, anim.transform.position.z);

            BulletLineFX(c, start, end);
            GunNoise(guntype, c);
            Destroy(anim, ps.startLifetime);
        }
    }

    /// <summary>
    /// Make bullet lines fx
    /// </summary>
    /// <param name="c"></param>
    /// <param name="Start"></param>
    /// <param name="r"></param>
    static void BulletLineFX(CharacterData c, Vector3 Start, Vector3 end)
    {
        if ((CharacterMove.CharacterTransform.position - Start).magnitude < Constants.RenderFXDistance)
        {
            Vector3 pt = RandomizePoint(0.1f, end);
            //bullet line
            GameObject bullet_streak = Prefabs.GetNewPrefab("FX/BulletFX");
            LineRenderer lr = bullet_streak.GetComponent<LineRenderer>();
            lr.SetPosition(0, Start);
            lr.SetPosition(1, pt);
            Destroy(bullet_streak, .1f);
        }
    }

    /// <summary>
    /// Make a gun noise on the input character with the input gun
    /// </summary>
    /// <param name="guntype"></param>
    /// <param name="c"></param>
    static void GunNoise(Guns guntype, CharacterData c)
    {
        AudioSource a = c.GetComponent<AudioSource>();
        if (a != null)
        {
            if (guntype == Guns.SMG)
                a.PlayOneShot(SoundFX.GetClip("AntiMaterialRifle_3p_01"), 1);
            else if (guntype == Guns.M16)
                a.PlayOneShot(SoundFX.GetClip("AutoGun_3p_01"), 1);
            else if (guntype == Guns.Shotgun)
                a.PlayOneShot(SoundFX.GetClip("SingleHeavyRoundGunfire"), 1);
            else if (guntype == Guns.Handgun)
                a.PlayOneShot(SoundFX.GetClip("SingleSmallRoundGunfire"), 0.55f);
            else if (guntype == Guns.Magnum)
                a.PlayOneShot(SoundFX.GetClip("SingleHeavyRoundGunfire"), 1);
            else if (guntype == Guns.UMP)
                a.PlayOneShot(SoundFX.GetClip("SingleMediumRoundGunfire"), 1);
            else if (guntype == Guns.UZI)
                a.PlayOneShot(SoundFX.GetClip("SingleSmallRoundGunfire"), 0.55f);
        }
    }
}
