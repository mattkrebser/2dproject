﻿using UnityEngine;
using System.Collections;

public class CarSFX : MonoBehaviour {

    public VehicleScript MyVehicle;
    public AudioSource MyAudio;

    Rigidbody r;
    NavMeshAgent n;

    float lo = -20f;
    float hi = -20f;

	void Update ()
    {
	    if (MyVehicle != null && MyAudio != null && GameSettings.Running)
        {
            //get pitch range
            if (lo == -20f || hi == -20f)
            {
                lo = GetPitchLow(MyVehicle.VehicleSkin);
                hi = MyAudio.pitch = GetPitchHigh(MyVehicle.VehicleSkin);
            }

            //get clip
            if (MyAudio.clip == null)
                MyAudio.clip = SoundFX.GetClip("EngineNoise");

            //only play if within distance of character
            if (CharacterMove.NearPlayer(MyVehicle.transform, 64))
            {
                //only play sound if someone is in the vehicle
                if (!MyVehicle.isEmpty)
                {
                    if (!MyAudio.isPlaying)
                        MyAudio.Play();

                    //no driver
                    if (MyVehicle.driver == null)
                    {
                        MyAudio.pitch = lo;
                    }
                    //player driver
                    else if (MyVehicle.driver.IsPlayer)
                    {
                        n = null;
                        if (r == null)
                            r = MyVehicle.GetComponent<Rigidbody>();
                        //assign pitch
                        MyAudio.pitch = GetPitch(r.velocity.magnitude / MyVehicle.speed);
                    }
                    //npc driver
                    else
                    {
                        r = null;
                        if (n == null)
                            n = MyVehicle.GetComponent<NavMeshAgent>();
                        MyAudio.pitch = GetPitch(n.velocity.magnitude / MyVehicle.speed);
                    }
                }
                else
                {
                    MyAudio.Stop();
                }
            }
            else
            {
                MyAudio.Stop();
            }
        }
	}

    /// <summary>
    /// get low pitch for the input vehicle
    /// </summary>
    /// <param name="skin"></param>
    /// <returns></returns>
    float GetPitchLow(string skin)
    {
        return 0;
    }
    /// <summary>
    /// get high pitch for the input vehicle
    /// </summary>
    /// <param name="skin"></param>
    /// <returns></returns>
    float GetPitchHigh(string skin)
    {
        return 3;
    }

    /// <summary>
    /// Returns the pitch at the specified ratio from lo to hi.
    /// Ratio being a number between 0 and 1
    /// </summary>
    /// <param name="ratio"></param>
    /// <returns></returns>
    float GetPitch(float ratio)
    {
        if (lo < 0)
        {
            return (Mathf.Abs(lo) + hi) * ratio + lo;
        }
        else
        {
            return (hi - lo) * ratio + lo;
        }
    }
}
