﻿using UnityEngine;
using System.Collections;

public class SlowOnCollide : MonoBehaviour {

    public float slow_speed_multiplier = 0.3f;

	void OnTriggerStay(Collider c)
    {
        if (c.GetComponent<CharacterMove>() == null)
        {
            if (c.GetComponent<VehicleScript>() != null)
            {
                VehicleScript v = c.GetComponent<VehicleScript>();
                if (v.driver == null)
                    return;
                if (v.driver.faction == Factions.Character)
                {
                    v.driver.GetComponent<CharacterMove>().slow_speed = 0.9f;
                }
                else
                {
                    v.driver.GetComponent<AIMove>().slow_speed = 0.9f;
                }
            }
            else if (c.GetComponent<AIMove>() != null)
            {
                c.GetComponent<AIMove>().slow_speed = slow_speed_multiplier;
            }
        }
        else
            c.transform.GetComponent<CharacterMove>().slow_speed = slow_speed_multiplier;
    }

    void OnTriggerExit(Collider c)
    {
        if (c.GetComponent<CharacterMove>() == null)
        {
            if (c.GetComponent<VehicleScript>() != null)
            {
                VehicleScript v = c.GetComponent<VehicleScript>();
                if (v.driver == null)
                    return;
                if (v.driver.faction == Factions.Character)
                {
                    v.driver.GetComponent<CharacterMove>().slow_speed = 1.0f;
                }
                else
                {
                    v.driver.GetComponent<AIMove>().slow_speed = 1.0f;
                }
            }
            else if (c.GetComponent<AIMove>() != null)
            {
                c.GetComponent<AIMove>().slow_speed = 1.0f;
            }
        }
        else
            c.transform.GetComponent<CharacterMove>().slow_speed = 1.0f;
    }
}
