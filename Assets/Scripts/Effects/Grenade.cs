﻿using UnityEngine;
using System.Collections;

public class Grenade : MonoBehaviour
{
    CharacterData originated_from;
    float damage_on_hit;
    float damage_radius;

    public void ThrowGrenade(CharacterData thrower, Vector3 direction, float damage, float radius, float explode_after, Vector3 throw_pos)
    {
        damage_on_hit = damage;
        damage_radius = radius;
        originated_from = thrower;
        transform.position = throw_pos + direction * 0.18f + new Vector3(0, 0.25f,0);
        transform.forward = direction;
        GetComponent<Rigidbody>().AddForce(direction.normalized * 177, ForceMode.Acceleration);
        StartCoroutine(GrenadeGoBoom(explode_after));
    }

    public IEnumerator GrenadeGoBoom(float t)
    {
        //after x seconds, blow up
        yield return new WaitForSeconds(t);

        SphereCollider my_collider = GetComponent<SphereCollider>();
        //do sphere overlap
        Collider[] colls = Physics.OverlapSphere(my_collider.center + transform.position, damage_radius,
            1 << LayerMask.NameToLayer("ProjectileHit") | 1 << LayerMask.NameToLayer("VehicleProjectileHit"));

        //if there was an overlap, then apply damage to effected objects
        if (colls != null && colls.Length > 0)
        {
            foreach (Collider c in colls)
            {
                RecognizeWasHit r = c.GetComponent<RecognizeWasHit>();
                if (r != null)
                {
                    //if this collider is a character in a different faction...
                    if (r.attatchedCharacter != null && originated_from.faction != r.attatchedCharacter.faction)
                    {
                        float dist = (r.attatchedCharacter.transform.position - (my_collider.center + transform.position)).magnitude;
                        if (dist < damage_radius && damage_radius > 0)
                        {
                            float mult = 1 - (dist / damage_radius);
                            r.attatchedCharacter.SetCharHealth(originated_from, 
                                r.attatchedCharacter.Health - (int)(damage_on_hit * mult));

                            //bloody it up
                            BloodSplat.MakeBloodSplat(c.transform.position);
                            BloodSplat.MakeBloodSplat(c.transform.position);
                            BloodSplat.MakeBloodSplat(c.transform.position);
                            BloodSplat.MakeBloodSplat(c.transform.position);
                        }
                    }
                    //else if this is a vehicle and the driver is not in the same faction
                    else if (r.attatchedVehicle != null && (r.attatchedVehicle.driver == null ||
                        r.attatchedVehicle.driver.faction != originated_from.faction))
                    {
                        r.attatchedVehicle.SetHealth(originated_from, r.attatchedVehicle.Health - (int)damage_on_hit);
                    }
                }
            }
        }

        //Make FX
        MakeExplosion(my_collider.center + transform.position);
        //Destroy
        Destroy(gameObject);
    }


    public static Grenade GetNewGrenade()
    {
        return Prefabs.GetNewPrefab("FX/Grenade").GetComponent<Grenade>();
    }


    /// <summary>
    /// Spawns an explosion FX at the input point that deletes itself when done exploding
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    private void MakeExplosion(Vector3 position)
    {
        if (CharacterMove.CharacterTransform == null)
            return;
        if ((CharacterMove.CharacterTransform.position - position).magnitude < Constants.RenderFXDistance)
        {
            GameObject exp = Prefabs.GetNewPrefab("FX/Explosion1");
            exp.transform.position = position;
            SoundFX.PlayAtPosition(position, "explosion", 1.5f);
            Destroy(exp, exp.GetComponent<ParticleSystem>().startLifetime);
        }
    }
}
