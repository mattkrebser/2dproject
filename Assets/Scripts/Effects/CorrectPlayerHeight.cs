﻿using UnityEngine;
using System.Collections;

public class CorrectPlayerHeight : MonoBehaviour {

    void OnTriggerStay(Collider c)
    {
        CharacterData cD = c.GetComponent<CharacterData>();
        if (cD != null && cD.IsPlayer)
        {
            if (cD.transform.position.y > 0.2305f || cD.transform.position.y < 0.2295f)
                cD.transform.position = new Vector3(transform.position.x, 0.23f, transform.position.z);
        }
    }
}
