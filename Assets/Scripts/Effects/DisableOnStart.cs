﻿using UnityEngine;
using System.Collections;

public class DisableOnStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (GetComponent<Collider>() != null)
            GetComponent<Collider>().enabled = false;
	}
}
