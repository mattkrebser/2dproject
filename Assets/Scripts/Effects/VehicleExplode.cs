﻿using UnityEngine;
using System.Collections;

public class VehicleExplode : MonoBehaviour
{
    static Sprite DeadCar;
    private static float DestroyAfter = 15.0f;
    public float DestroyVehicleAfter = 15.0f;

    void Awake()
    {
        DeadCar = Resources.Load<Sprite>("VehicleSprites/carDead");
        DestroyAfter = DestroyVehicleAfter;
    }

    public static void MakeCarExplode(VehicleScript v)
    {
        MakeBigExplosion(v.transform.position, 1.5f);

        GameObject dead_car = new GameObject("DeadCar");
        SpriteRenderer sr = dead_car.AddComponent<SpriteRenderer>();
        sr.sortingOrder = 1;
        sr.sprite = DeadCar;
        dead_car.transform.eulerAngles = new Vector3(90, 0, 0);
        dead_car.transform.position = new Vector3(v.transform.position.x, 0.01f, v.transform.position.z);

        RandomPlaceFire(new Vector3(v.transform.position.x, 0, v.transform.position.z));

        Destroy(dead_car, DestroyAfter);
    }

    static void RandomPlaceFire(Vector3 position)
    {
        for (int i = 0; i < 10; i++)
        {
            Vector3 new_pos = RandomizePoint(1, position);
            if (Physics.CheckSphere(new_pos, 0.35f, 1 << LayerMask.NameToLayer("Walls")))
                continue;
            GameObject f1 = Prefabs.GetNewPrefab("FX/Fire1");
            f1.transform.position = new_pos;
            float scale;
            f1.GetComponent<ParticleSystem>().startSize = scale = Random.Range(0.4f, 0.9f);
            f1.GetComponent<ParticleSystem>().startDelay = Random.Range(0, 0.56f);
            MakeCarRubble(f1.transform.position, scale);
            Destroy(f1, DestroyAfter);
        }
    }

    static void MakeCarRubble(Vector3 position, float scale)
    {
        GameObject dead_car = new GameObject("DeadCarBurning");
        SpriteRenderer sr = dead_car.AddComponent<SpriteRenderer>();
        sr.sortingOrder = 1;
        sr.transform.localScale = scale * new Vector3(1, 1, 1);
        sr.sprite = DeadCar;
        dead_car.transform.eulerAngles = new Vector3(90, Random.Range(0, 360), 0);
        dead_car.transform.position = new Vector3(position.x, 0.0f, position.z - 0.33f);
        Destroy(dead_car, DestroyAfter);
    }

    /// <summary>
    /// Randomize the input point by up to the input amount (only xz coords)
    /// </summary>
    /// <param name="magnitude"></param>
    /// <param name="point"></param>
    /// <returns></returns>
    static Vector3 RandomizePoint(float magnitude, Vector3 point)
    {
        float dz = Random.Range(0, magnitude);
        float dx = Random.Range(0, magnitude);

        dz = Random.Range(0, 2) == 1 ? dz : -dz;
        dx = Random.Range(0, 2) == 1 ? dx : -dx;

        return new Vector3(point.x + dx, point.y, point.z + dz);
    }

    static void ExplodeFX(Vector3 Position)
    {
        GameObject exp = Prefabs.GetNewPrefab("FX/Explosion1");
        exp.transform.position = Position;
        ParticleSystem p = exp.GetComponent<ParticleSystem>();
        p.startSize = 3;
        SoundFX.PlayAtPosition(Position, "explosion", 1.75f);
        Destroy(exp, exp.GetComponent<ParticleSystem>().startLifetime);
    }

    static void MakeBigExplosion(Vector3 Position, float kill_radius)
    {
        ExplodeFX(Position);
        ExplodeFX(RandomizePoint(0.5f, Position));
        ExplodeFX(RandomizePoint(0.5f, Position));
        //do an overlap and damage nearby characters/vehicles
        Collider[] colls = Physics.OverlapSphere(Position, kill_radius,
           1 << LayerMask.NameToLayer("ProjectileHit") | 1 << LayerMask.NameToLayer("VehicleProjectileHit"));

        //if there was an overlap, then apply damage to effected objects
        if (colls != null && colls.Length > 0)
        {
            foreach (Collider c in colls)
            {
                RecognizeWasHit r = c.GetComponent<RecognizeWasHit>();
                if (r != null)
                {
                    if (r.attatchedCharacter != null)
                    {
                        float dist = (r.attatchedCharacter.transform.position -
                        Position).magnitude;

                        if (dist < kill_radius && kill_radius > 0)
                        {
                            float mult = 1 - (dist / kill_radius);
                            r.attatchedCharacter.SetCharHealth(null, r.attatchedCharacter.Health -
                                (int)(Constants.Grenade_Damage * mult));

                            //bloody it up
                            BloodSplat.MakeBloodSplat(c.transform.position);
                            BloodSplat.MakeBloodSplat(c.transform.position);
                            BloodSplat.MakeBloodSplat(c.transform.position);
                            BloodSplat.MakeBloodSplat(c.transform.position);
                        }
                    }
                    //else if this is a vehicle and the driver is not in the same faction
                    else if (r.attatchedVehicle != null)
                    {
                        r.attatchedVehicle.SetHealth(null, r.attatchedVehicle.Health - (int)Constants.Grenade_Damage);
                    }
                }
            }
        }
    }
}
