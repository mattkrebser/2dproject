﻿using UnityEngine;
using System.Collections.Generic;

public class DoorEnter : MonoBehaviour
{
    [Tooltip("Leave 'Goto' and 'MyExitsMap' null to make this an exit-only door")]
    public DoorEnter Goto;
    public MapData MyExitsMap;

    [System.NonSerialized]
    public MapData MyMap;

    private GameObject myParticle;

    void Start()
    {
        if (Goto != null && MyExitsMap != null)
        {
            DoorGraph.AddDoor(this);
        }
    }

    void OnDestroy()
    {
        DoorGraph.RemoveDoor(this);
    }

    public override int GetHashCode()
    {
        return GetInstanceID();
    }

    public override bool Equals(object o)
    {
        return o is UnityEngine.Object ? ((UnityEngine.Object)o).GetInstanceID() == GetInstanceID() : false;
    }

    void OnTriggerEnter(Collider c)
    {
        GameObject doorFX = Prefabs.GetNewPrefab("FX/CircleFX");
        doorFX.transform.SetParent(transform);
        doorFX.transform.localPosition = new Vector3(0, 0, 0);
        myParticle = doorFX;
        ZoneLabel.PlayerCanInteract = true;
    }

    void OnTriggerExit(Collider c)
    {
        Destroy(myParticle);
        ZoneLabel.PlayerCanInteract = false;
    }

    public static void EnterDoor(DoorEnter d, CharacterData c)
    {
       GameSettings.Instance.HibernateMap(MapData.ActiveMap);
        d.EnterDoor(c);
    }

    void EnterDoor(CharacterData c)
    {
        if (Goto != null && MyExitsMap != null)
        {
            if (c.IsPlayer)
            {
                ScreenToBlack.ToBlackThenFadeIn(0.5f);

                CameraFollow.SetMap(MyExitsMap);
                c.GetComponent<PlayerDat>().playerInfo2.ActiveMap = MyExitsMap.ZoneName;
                MapData.ActiveMap = MyExitsMap;

                if (MapData.ActiveMap.isAsleep)
                    GameSettings.Instance.WakeUpMap(MapData.ActiveMap);

                c.transform.position = Goto.transform.position;

                if (c.IsPlayer)
                    CameraFollow.cam.transform.position = new Vector3(c.transform.position.x, 2, c.transform.position.z);
            }
            else
            {

            }
        }
    }
}
