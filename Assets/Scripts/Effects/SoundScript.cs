﻿using UnityEngine;
using System.Collections;

public class SoundScript : MonoBehaviour {

    AudioSource mySource;
    void Awake()
    {
        mySource = GetComponent<AudioSource>();
    }

	public void PlaySound(AudioClip a, float soundMultiplier)
    {
        mySource.PlayOneShot(a, soundMultiplier);
        StartCoroutine(WaitTillDoneRoutine());
    }

    IEnumerator WaitTillDoneRoutine()
    {
        while (mySource.isPlaying)
            yield return new WaitForSeconds(0.25f);
        SoundFX.FreeSoundScript(this);
    }
}
