﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Holds all of the soundFX used. Please use unique names for all
/// audio files
/// </summary>
public static class SoundFX
{
    //dictionary of audio clips
    static Dictionary<string, AudioClip> clips;

    //list of sound objects that are not in use
    static Queue<SoundScript> Unused = new Queue<SoundScript>();

    public static void Reset()
    {
        Unused.Clear();
    }

    /// <summary>
    /// Trys to load in the sound library if it doesn't exist
    /// </summary>
    static void CheckSound()
    {
        if (clips == null)
        {
            clips = new Dictionary<string, AudioClip>();
            AudioClip[] acs = Resources.LoadAll<AudioClip>("Audio");
            foreach (AudioClip c in acs)
            {
                clips.Add(c.name, c);
            }
        }
    }

    /// <summary>
    /// Returns the audio clip with the input name (not path name, just file name), returns null if not found
    /// </summary>
    /// <param name="clip_name"></param>
    /// <returns></returns>
    public static AudioClip GetClip(string clip_name)
    {
        CheckSound();
        if (clips.ContainsKey(clip_name))
            return clips[clip_name];
        return null;
    }

    /// <summary>
    /// Play a sound at the input position
    /// </summary>
    /// <param name="playHere"></param>
    /// <param name="ClipName"></param>
    public static void PlayAtPosition(Vector3 playHere, string ClipName, float soundMultiplier)
    {
        CheckSound();
        SoundScript s = getFreeScript();
        s.transform.position = playHere;
        s.PlaySound(GetClip(ClipName), soundMultiplier);
    }

    public static void Play(AudioClip a, float soundMultiplier = 1.0f)
    {
        CheckSound();
        SoundScript s = getFreeScript();
        s.GetComponent<AudioSource>().spatialBlend = 0.0f;
        s.PlaySound(a, soundMultiplier);
    }

    static SoundScript getFreeScript()
    {
        CheckSound();
        if (Unused.Count > 0)
        {
            return Unused.Dequeue();
        }
        else
        {
            GameObject newObj = Prefabs.GetNewPrefab("Prefabs/AudioObject");
            newObj.transform.SetParent(GameSettings.Instance.transform);
            return newObj.GetComponent<SoundScript>();
        }
    }

    /// <summary>
    /// Marks the input sound script as unused, and adds it to the free queue
    /// </summary>
    /// <param name="s"></param>
    public static void FreeSoundScript(SoundScript s)
    {
        CheckSound();
        s.GetComponent<AudioSource>().spatialBlend = 1.0f;
        Unused.Enqueue(s);
    }
}
