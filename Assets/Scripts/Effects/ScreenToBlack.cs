﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenToBlack : MonoBehaviour {

    public Image image;
    static Image statImage;

    void Awake()
    {
        statImage = image;
    }

    public static void FadeOutFadeIn(float time)
    {
        CoroutineHelper.RunCoroutine(FadeRoutine(time));
    }

    public static void ToBlackThenFadeIn(float time)
    {
        statImage.enabled = true;
        Color color = statImage.color;
        statImage.color = new Color(color.r, color.g, color.b, 1);
        CoroutineHelper.RunCoroutine(FadeInRoutine(time));
    }

    public static void Fadout(FunctionCall f = null)
    {
        CoroutineHelper.RunCoroutine(FadeoutRoutine(f));
    }
    public static void FadeIn(FunctionCall f = null)
    {
        CoroutineHelper.RunCoroutine(FadeinRoutine(f));
    }

    public static IEnumerator Fadeout()
    {
        statImage.enabled = true;
        for (int i = 0; i < 256; i += 2)
        {
            Color c = statImage.color;
            statImage.color = new Color(c.r, c.g, c.b, i / 256f);
            yield return new WaitForSeconds(2.0f / 256f);
        }
    }
    static IEnumerator FadeoutRoutine(FunctionCall f)
    {
        statImage.enabled = true;
        for (int i = 0; i < 256; i += 2)
        {
            Color c = statImage.color;
            statImage.color = new Color(c.r, c.g, c.b, i / 256f);
            yield return new WaitForSeconds(2.0f / 256f);
        }

        if (f != null)
        {
            if (!f.AutomaticAdvance && f.CanRun() && f.BaseQuest != null)
                f.BaseQuest.NextEvent();
        }
    }
    static IEnumerator FadeinRoutine(FunctionCall f)
    {
        statImage.enabled = true;
        for (int i = 255; i >= 0; i -= 2)
        {
            Color c = statImage.color;
            statImage.color = new Color(c.r, c.g, c.b, i / 256f);
            yield return new WaitForSeconds(2.0f / 256f);
        }

        if (f != null)
        {
            if (!f.AutomaticAdvance && f.CanRun() && f.BaseQuest != null)
                f.BaseQuest.NextEvent();
        }
    }

    static IEnumerator FadeRoutine(float time)
    {
        float WaitTime = time / 3;
        statImage.enabled = true;

        for (int i = 0; i < 256; i += 2)
        {
            Color c = statImage.color;
            statImage.color = new Color(c.r, c.g, c.b, i / 256f);
            yield return new WaitForSeconds(WaitTime / 256f);
        }

        yield return new WaitForSeconds(WaitTime);

        for (int i = 255; i >= 0; i -= 2)
        {
            Color c = statImage.color;
            statImage.color = new Color(c.r, c.g, c.b, i / 256f);
            yield return new WaitForSeconds(WaitTime / 256f);
        }


        statImage.enabled = false;
    }

    static IEnumerator FadeInRoutine(float time)
    {
        float WaitTime = time / 3;

        yield return new WaitForSeconds(WaitTime);

        for (int i = 255; i >= 0; i -= 2)
        {
            Color c = statImage.color;
            statImage.color = new Color(c.r, c.g, c.b, i / 256f);
            yield return new WaitForSeconds(WaitTime / 256f);
        }


        statImage.enabled = false;
    }
}
