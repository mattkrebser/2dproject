﻿using UnityEngine;
using System.Collections;

public class ForceNoXZRotation : MonoBehaviour {
	
    private float init_x;
    private float init_z;

    void Awake()
    {
        init_x = transform.eulerAngles.x;
        init_z = transform.eulerAngles.z;
    }
	// Update is called once per frame
	void Update ()
    {
        transform.eulerAngles = new Vector3(init_x, transform.eulerAngles.y, init_z);
	}
}
