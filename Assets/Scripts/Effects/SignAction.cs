﻿using UnityEngine;
using System.Collections;

public class SignAction : MonoBehaviour {
    /// <summary>
    /// A string flag used to show what the current sign says that the player is hovering over (with the mouse)
    /// </summary>
    public static string CurrentSign = "";
}
