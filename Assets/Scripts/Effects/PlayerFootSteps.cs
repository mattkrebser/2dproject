﻿using UnityEngine;
using System.Collections;

public class PlayerFootSteps : MonoBehaviour {

    float TimeOfLastFootStep = 0.0f;
    CharacterMove instance;

    public float FootStepSpeed = 1.0f;
    public float Volume = 1.0f;

    public AudioSource mySource;
    public AudioClip[] footSteps;

	void Update()
    {
        if (CharacterMove.CharacterTransform != null)
        {
            if (instance == null)
                instance = CharacterMove.CharacterTransform.GetComponent<CharacterMove>();
            if (instance != null && instance.IsMoving && instance.Car == null)
            {
                if (Time.realtimeSinceStartup - TimeOfLastFootStep > FootStepSpeed)
                {
                    mySource.PlayOneShot(footSteps[Random.Range(0, footSteps.Length)], Volume);
                    TimeOfLastFootStep = Time.realtimeSinceStartup;
                }
            }
        }
        else
        {
            instance = null;
        }
    }
}
