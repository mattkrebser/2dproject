﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour
{
    private float damage_on_hit;
    float damage_radius;
    CharacterData originated_from;

    /// <summary>
    /// Launch the rocket!
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="radius"></param>
    /// <param name="launched_from"></param>
    /// <param name="direction"></param>
    /// <param name="launch_position"></param>
	public void Launch(float damage, float radius, CharacterData launched_from, Vector3 direction, Vector3 launch_position)
    {
        damage_on_hit = damage;
        damage_radius = radius;
        originated_from = launched_from;
        transform.position = launch_position;
        transform.eulerAngles = Quaternion.LookRotation(direction, Vector3.up).eulerAngles;
        SoundFX.PlayAtPosition(launch_position, "RocketLaunch", 1f);
        GetComponent<ParticleSystem>().startRotation = (-launched_from.RotationEuler.y) * Mathf.Deg2Rad;
        GetComponent<Rigidbody>().AddForce(direction.normalized * 250, ForceMode.Acceleration);
        GetComponentInChildren<SpriteRenderer>().transform.localEulerAngles = new Vector3(90, 315, 0);
        GetComponentInChildren<SpriteRenderer>().transform.localPosition = new Vector3(0.005f, 0, 0.09f);
    }

    bool is_destroyed = false;
    void OnDestroy()
    {
        is_destroyed = true;
    }

    //If the character manages to get a rocket off the map somehow...
    //this will delete it
    IEnumerator LongRunningDelete()
    {
        yield return new WaitForSeconds(15);
        if (!is_destroyed)
            Destroy(gameObject);
    }

    /// <summary>
    /// Get a new unlaunched rocket
    /// </summary>
    /// <returns></returns>
    public static Rocket GetNewRocket()
    {
        return Prefabs.GetNewPrefab("FX/Rocket").GetComponent<Rocket>();
    }

    /// <summary>
    /// Spawns an explosion FX at the input point that deletes itself when done exploding
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    private void MakeExplosion(Vector3 position)
    {
        if (CharacterMove.CharacterTransform == null)
            return;
        if ((CharacterMove.CharacterTransform.position - position).magnitude < Constants.RenderFXDistance)
        {
            GameObject exp = Prefabs.GetNewPrefab("FX/Explosion1");
            exp.transform.position = position;
            SoundFX.PlayAtPosition(position, "explosion", 1.5f);
            Destroy(exp, exp.GetComponent<ParticleSystem>().startLifetime);
        }
    }

    //Take damage to nearby characters, and call the FX function
    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider == originated_from.CharacterCollider || (originated_from.InVehicle != null &&
            collision.collider == originated_from.InVehicle.ProjectileCollider))
            return;

        SphereCollider my_collider = GetComponent<SphereCollider>();
        //do sphere overlap
        Collider[] colls = Physics.OverlapSphere(my_collider.center + transform.position, damage_radius,
            1 << LayerMask.NameToLayer("ProjectileHit") | 1 << LayerMask.NameToLayer("VehicleProjectileHit"));

        //if there was an overlap, then apply damage to effected objects
        if (colls != null && colls.Length > 0)
        {
            foreach (Collider c in colls)
            {
                RecognizeWasHit r = c.GetComponent<RecognizeWasHit>();
                if (r != null)
                {
                    //if this collider is a character in a different faction...
                    if (r.attatchedCharacter != null && originated_from.faction != r.attatchedCharacter.faction)
                    {
                        float dist = (r.attatchedCharacter.transform.position - (my_collider.center + transform.position)).magnitude;
                        if (dist < damage_radius && damage_radius > 0)
                        {
                            float mult = 1 - (dist / damage_radius);
                            r.attatchedCharacter.SetCharHealth(originated_from, 
                                r.attatchedCharacter.Health - (int)(damage_on_hit * mult));

                            //bloody it up
                            BloodSplat.MakeBloodSplat(c.transform.position);
                            BloodSplat.MakeBloodSplat(c.transform.position);
                            BloodSplat.MakeBloodSplat(c.transform.position);
                            BloodSplat.MakeBloodSplat(c.transform.position);
                        }
                    }
                    //else if this is a vehicle and the driver is not in the same faction
                    else if (r.attatchedVehicle != null && (r.attatchedVehicle.driver == null ||
                        r.attatchedVehicle.driver.faction != originated_from.faction))
                    {
                        r.attatchedVehicle.SetHealth(originated_from, r.attatchedVehicle.Health - (int)damage_on_hit);
                    }
                }
            }
        }

        //Make FX
        MakeExplosion(my_collider.center + transform.position);
        //Destroy
        Destroy(gameObject);
    }
}
