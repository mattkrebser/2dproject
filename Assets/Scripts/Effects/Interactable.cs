﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

/// <summary>
/// A non Quest-related player interaction object.
/// </summary>
public class Interactable : MonoBehaviour {

    private GameObject myParticle;

    [Tooltip("Show interact FX")]
    public bool HasParticle = true;

    [Tooltip("Function to invoke")]
    public UnityEvent function;

    [Tooltip("Input key to use, changing after the object has been initialized has no effect. Key=None means this is triggered on entry")]
    public KeyCode Key;
    private KeyCode key;

    private bool inside = false;

    // Use this for initialization
    void Start () {

        key = Key;

        if (key == KeyCode.None)
            return;

        InputHandler.AddGlobalInput(new KeySet(key, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), ekey);
    }
	
	// Update is called once per frame
	void OnDestroy () {

        if (key == KeyCode.None)
            return;

        InputHandler.RemoveGlobalInput(new KeySet(key, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), ekey);
    }

    void ekey()
    {
        if (key == KeyCode.None)
            return;

        if (CharacterMove.CharacterTransform != null && inside)
            function.Invoke();
    }

    void OnTriggerEnter(Collider c)
    {
        if (HasParticle)
        {
            GameObject doorFX = Prefabs.GetNewPrefab("FX/CircleFX");
            doorFX.transform.SetParent(transform);
            doorFX.transform.localPosition = new Vector3(0, 0, 0);
            myParticle = doorFX;
        }
        inside = true;

        if (key == KeyCode.None)
        {
            if (CharacterMove.CharacterTransform != null && inside)
                function.Invoke();
            return;
        }

        ZoneLabel.PlayerCanInteract = true;
    }

    void OnTriggerExit(Collider c)
    {
        if (HasParticle)
        {
            Destroy(myParticle);
        }
        inside = false;

        if (key == KeyCode.None)
            return;

        ZoneLabel.PlayerCanInteract = false;
    }
}
