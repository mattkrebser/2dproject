﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HealFX : MonoBehaviour
{
    public SpriteRenderer sprite;

    static List<Sprite> HealSprites;
    static Dictionary<CharacterData, float> fx = new Dictionary<CharacterData, float>();

	public static void MakeHealFX(CharacterData c)
    {
        if (CharacterMove.CharacterTransform == null || c == null)
            return;

        if (fx.ContainsKey(c))
            fx[c] = 0.0f;
        else
        {
            GameObject SpriteParent = c.sprite_child;
            GameObject heal_fx = Prefabs.GetNewPrefab("FX/HealFX");
            heal_fx.transform.SetParent(SpriteParent.transform);
            heal_fx.transform.localPosition = new Vector3(0, 0, 0);
            heal_fx.transform.position = new Vector3(heal_fx.transform.position.x, 0, heal_fx.transform.position.z);
            fx.Add(c, 0.0f);
            CoroutineHelper.RunCoroutine(HealFXRoutine(heal_fx.GetComponent<HealFX>().sprite, c));
        }
    }

    static IEnumerator HealFXRoutine(SpriteRenderer r, CharacterData c)
    {
        //inititalize if not initialized..
        if (HealSprites == null)
        {
            HealSprites = new List<Sprite>(Resources.LoadAll<Sprite>("FX/Textures/HealTextures"));
        }

        int CurrentSprite = 0;

        //change the sprite every .17 seconds, repeat for 3.5 seconds
        while (fx[c] < 3.5f && c != null && !c.IsDead)
        {
            r.sprite = HealSprites[CurrentSprite];
            //clamp value to stay inside array
            CurrentSprite = CurrentSprite + 1 >= HealSprites.Count ? 0 : CurrentSprite + 1;
            yield return new WaitForSeconds(0.17f);
            fx[c] += 0.17f;
        }

        fx.Remove(c);

        //destroy
        if (c != null && !c.IsDead)
        {
            Destroy(r.transform.parent.gameObject);
        }
    }
}
