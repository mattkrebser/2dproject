﻿using UnityEngine;
using System.Collections;

public class DisableAllChildrenMeshRenderers : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        MeshRenderer[] arr = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer r in arr)
        {
            r.enabled = false;
        }
	}
	
}
