﻿using UnityEngine;
using System.Collections;

public class MapFlash : MonoBehaviour {

    static SpriteRenderer s;
    void Awake()
    {
        s = GetComponent<SpriteRenderer>();
        s.color = new Color(0, 0, 0, 0);
    }

    public static void DoMapFlash(int numFlashes, Color color)
    {
        CoroutineHelper.RunCoroutine(flashRoutine(numFlashes, color));
    }

    static IEnumerator flashRoutine(int numFlashes, Color color)
    {
        s.color = new Color(0, 0, 0, 0);
        for (int i = 0; i < numFlashes; i++)
        {
            yield return LerpUp(color);
            yield return LerpDown(color);
        }
        s.color = new Color(0, 0, 0, 0);
    }

    static IEnumerator LerpUp(Color color)
    {
        float num = 0;
        for (int i = 0; i < 20; i++)
        {
            num += 0.05f;
            s.color = color * num;
            yield return new WaitForSeconds(0.05f);
        }
    }

    static IEnumerator LerpDown(Color color)
    {
        float num = 1.0f;
        for (int i = 0; i < 20; i++)
        {
            num += -0.05f;
            s.color = color * num;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
