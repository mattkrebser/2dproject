﻿using UnityEngine;
using System.Collections;

public class SignHighlight : MonoBehaviour {

    public Sprite signleft;
    public Sprite signright;
    public Sprite signstraight;
    public Sprite signleft_h;
    public Sprite signright_h;
    public Sprite signstraight_h;

	public void Highlight()
    {
        SpriteRenderer r = GetComponent<SpriteRenderer>();
        if (r.sprite.name == "SignLeft")
        {
            r.sprite = signleft_h;
        }
        else if (r.sprite.name == "SignRight")
        {
            r.sprite = signright_h;
        }
        else if (r.sprite.name == "SignStraight")
        {
            r.sprite = signstraight_h;
        }
    }

    public void UnHighlight()
    {
        SpriteRenderer r = GetComponent<SpriteRenderer>();
        if (r.sprite.name == "SignLeftHighlighted")
        {
            r.sprite = signleft;
        }
        else if (r.sprite.name == "SignRightHighlighted")
        {
            r.sprite = signright;
        }
        else if (r.sprite.name == "SignStraightHighlighted")
        {
            r.sprite = signstraight;
        }
    }
}
