﻿using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// Interaction functions for weapons, health packs
/// </summary>
public static class ItemInteract
{
    /// <summary>
    /// Shoots the weapon.
    /// </summary>
    /// <param name="character"> The shooter</param>
    /// <param name="isRightWeapon"> Did we shoot from the right or left weapon?</param>
    /// <param name="shootHere"> shoot at this point, used for NPC's. </param>
    /// <param name="InPosition"> Use the input 'shootHere' variable?</param>
    /// <param name="ForceMiss"> Only works with guns. The Npc will look like they are firing, but no damage
    /// will be applied (miss)</param>
	public static void Shoot(CharacterData character, bool isRightWeapon,
        Vector3 shootHere = default(Vector3), bool InPosition = false, bool ForceMiss = false)
    {

        if (!character.GunsOut)
            return;

        if (isRightWeapon && !PeopleData.WeaponExists(character.R_weapon))
            return;
        else if (!isRightWeapon && !PeopleData.WeaponExists(character.L_weapon))
            return;

        //enforce cooldown
        if (isRightWeapon && character.RemainingCooldownRight > 0.001f)
            return;
        else if (!isRightWeapon && character.RemainingCooldownLeft > 0.001f)
            return;

        //get gun
        string weapon = isRightWeapon ? character.R_weapon : character.L_weapon;

        //weapon modifiers
        float damage = 0.0f;
        float wep_range = 0.0f;

        //rocket/grenade
        bool isProjectile = false, isThrown = false;

        //variable for this gun type
        Guns weapontype = Guns.M16;

        //add in shoot speed modifers
        float shootSpeedModifer = character.IsPlayer ? 1 : Constants.NPCShootSpeed;

        //get gun information
        if (weapon.Contains("SMG"))
        {
            damage = Constants.SMG_Damage;
            wep_range = Constants.SMG_Range;
            if (isRightWeapon) character.RemainingCooldownRight = Constants.SMG_fireRate * shootSpeedModifer;
            else character.RemainingCooldownLeft = Constants.SMG_fireRate * shootSpeedModifer;
            weapontype = Guns.SMG;
        }
        else if (weapon.Contains("Handgun"))
        {
            damage = Constants.Handgun_Damage;
            wep_range = Constants.Handgun_Range;
            if (isRightWeapon) character.RemainingCooldownRight = Constants.Handgun_fireRate * shootSpeedModifer;
            else character.RemainingCooldownLeft = Constants.Handgun_fireRate * shootSpeedModifer;
            weapontype = Guns.Handgun;
        }
        else if (weapon.Contains("UMP"))
        {
            damage = Constants.UMP_Damage;
            wep_range = Constants.UMP_Range;
            if (isRightWeapon) character.RemainingCooldownRight = Constants.UMP_fireRate * shootSpeedModifer;
            else character.RemainingCooldownLeft = Constants.UMP_fireRate * shootSpeedModifer;
            weapontype = Guns.UMP;
        }
        else if (weapon.Contains("UZI"))
        {
            damage = Constants.UZI_Damage;
            wep_range = Constants.UZI_Range;
            if (isRightWeapon) character.RemainingCooldownRight = Constants.UZI_fireRate * shootSpeedModifer;
            else character.RemainingCooldownLeft = Constants.UZI_fireRate * shootSpeedModifer;
            weapontype = Guns.UZI;
        }
        else if (weapon.Contains("Magnum"))
        {
            damage = Constants.Magnum_Damage;
            wep_range = Constants.Magnum_Range;
            if (isRightWeapon) character.RemainingCooldownRight = Constants.Magnum_fireRate * shootSpeedModifer;
            else character.RemainingCooldownLeft = Constants.Magnum_fireRate * shootSpeedModifer;
            weapontype = Guns.Magnum;
        }
        else if (weapon.Contains("Shotgun"))
        {
            damage = Constants.Shotgun_Damage;
            wep_range = Constants.Shotgun_Range;
            if (isRightWeapon) character.RemainingCooldownRight = Constants.Shotgun_fireRate * shootSpeedModifer;
            else character.RemainingCooldownLeft = Constants.Shotgun_fireRate * shootSpeedModifer;
            weapontype = Guns.Shotgun;
        }
        else if (weapon.Contains("M16"))
        {
            damage = Constants.M16_Damage;
            wep_range = Constants.M16_Range;
            if (isRightWeapon) character.RemainingCooldownRight = Constants.M16_fireRate * shootSpeedModifer;
            else character.RemainingCooldownLeft = Constants.M16_fireRate * shootSpeedModifer;
            weapontype = Guns.M16;
        }
        else if (weapon.Contains("Bazooka"))
        {
            damage = Constants.Bazooka_Damage;
            wep_range = Constants.Bazooka_Radius;
            if (isRightWeapon) character.RemainingCooldownRight = Constants.Bazooka_fireRate * shootSpeedModifer;
            else character.RemainingCooldownLeft = Constants.Bazooka_fireRate * shootSpeedModifer;
            isProjectile = true;
            weapontype = Guns.Bazooka;
        }
        else if (weapon.Contains("Grenade"))
        {
            damage = Constants.Grenade_Damage;
            wep_range = Constants.Grenade_Radius;
            if (isRightWeapon) character.RemainingCooldownRight = Constants.Grenade_fireRate * shootSpeedModifer;
            else character.RemainingCooldownLeft = Constants.Grenade_fireRate * shootSpeedModifer;
            isThrown = true;
            weapontype = Guns.Grenade;
        }

        //Modify ammo value
        if (isRightWeapon) character.RWeaponAmmo--; else character.LWeaponAmmo--;

        //Set shooting position
        Vector3 shoot_pos = GunFire.GetWeaponFirePoint(character, weapontype, isRightWeapon);

        //get shooting direction
        Vector3 shoot_dir; RaycastHit hit = default(RaycastHit);
        if (InPosition)
            shoot_dir = (shootHere - shoot_pos).normalized;
        else
        {
            //ray cast to the ground
            if (Physics.Raycast(CameraFollow.cam.ScreenPointToRay(Input.mousePosition),
                out hit, 25, 1 << LayerMask.NameToLayer("Ground")))
            {
                //assign shoot direction based on hit
                shoot_dir = (hit.point - shoot_pos);
                shoot_dir.y = weapontype == Guns.Bazooka ? shoot_dir.y : 0; shoot_dir = shoot_dir.normalized;
            }
            //If raycast didn't hit, then use the characters rotation. (this should never happen)
            else
                shoot_dir = Quaternion.Euler(character.RotationEuler) * Vector3.forward;
        }

        //bazooka/rockets
        if (isProjectile)
        {
            if (character.IsPlayer)
            {
                Rocket r = Rocket.GetNewRocket();
                r.Launch(Constants.Bazooka_Damage, Constants.Bazooka_Radius, character,
                    shoot_dir, GunFire.GetWeaponFirePoint(character, weapontype, isRightWeapon));

                GunFire.FiredWeaponAnimation(isRightWeapon, character, weapontype, hit);
                Aimer.AimerPropogate(Constants.Bazooka_fireRate);
            }
            else
            {
                Vector3 launch_pos = GunFire.GetWeaponFirePoint(character, weapontype, isRightWeapon) + new Vector3(0, 0.15f, 0);
                Vector3 launch_dir = (new Vector3(shootHere.x, 0.03f, shootHere.z) - launch_pos).normalized;
                Rocket r = Rocket.GetNewRocket();
                r.Launch(Constants.Bazooka_Damage, Constants.Bazooka_Radius, character, launch_dir, launch_pos);
                GunFire.FiredWeaponAnimation(isRightWeapon, character, weapontype, default(RaycastHit));
                character.GetComponent<NPCAI>().DealtDamage();
            }
        }
        //thrown weapons
        else if (isThrown)
        {
            Grenade g = Grenade.GetNewGrenade();
            if (character.IsPlayer)
            {
                g.ThrowGrenade(character, shoot_dir, Constants.Grenade_Damage,
                    Constants.Grenade_Radius, 1.3f, shoot_pos);
                Aimer.AimerPropogate(Constants.Grenade_fireRate);
            }
            else
            {
                Vector3 throw_pos = GunFire.GetWeaponFirePoint(character, weapontype, isRightWeapon) + new Vector3(0, 0.15f, 0);
                Vector3 throw_dir = (new Vector3(shootHere.x, 0.03f, shootHere.z) - throw_pos).normalized;
                g.ThrowGrenade(character, throw_dir, Constants.Grenade_Damage,
                       Constants.Grenade_Radius, 1.3f, throw_pos);
                character.GetComponent<NPCAI>().DealtDamage();
            }
        }
        //normal guns
        else
        {
            RaycastHit hit1, hit2, hit3;
            bool h1 = false, h2 = false, h3 = false;

            //raycast in direction
            h1 = Physics.Raycast(shoot_pos, shoot_dir, out hit1, Constants.MaxGunRange, 
                1 << LayerMask.NameToLayer("Ground") | 1 << LayerMask.NameToLayer("Walls"));
            h2 = ClosestRayCastHit(character, shoot_pos, shoot_dir, out hit2, Constants.MaxGunRange,
                1 << LayerMask.NameToLayer("VehicleProjectileHit"));
            h3 = ClosestRayCastHit(character, shoot_pos, shoot_dir, out hit3, Constants.MaxGunRange,
                1 << LayerMask.NameToLayer("ProjectileHit"));

            //if this is the player, then propogate the aimer
            if (character.IsPlayer)
                Aimer.AimerPropogate(Constants.FireRate(weapontype));

            //if no hits.
            if (!h1 && !h2 && !h3)
            {
                GunFire.FiredWeaponNoHit(isRightWeapon, character, weapontype, shoot_pos,
                    shoot_pos + shoot_dir.normalized * Constants.MaxGunRange);
                return;
            }

            //determine distance foreach hit
            float d1 = float.MaxValue, d2 = float.MaxValue, d3 = float.MaxValue;
            if (h1)
                d1 = hit1.distance;
            if (h2)
                d2 = hit2.distance;
            if (h3)
                d3 = hit3.distance;

            //Determine which hit to use for gunfire effects
            RaycastHit using_hit;
            if (d1 < d2)
            {
                if (d1 < d3)
                    using_hit = hit1;
                else
                    using_hit = hit3;
            }
            else
            {
                if (d2 < d3)
                    using_hit = hit2;
                else
                    using_hit = hit3;
            }

            //determine if a character or vehicle was hit
            bool v_hit = false, c_hit = false;
            if (d2 < d1)
            {
                v_hit = true;
            }
            if (d3 < d1)
            {
                c_hit = true;
            }

            //choose animation
            if (v_hit && c_hit && d3 > d2)
                GunFire.FiredWeaponAnimation(isRightWeapon, character, weapontype, hit3, true);
            else if (v_hit && c_hit && d2 > d3)
                GunFire.FiredWeaponAnimation(isRightWeapon, character, weapontype, hit2, true);
            else
                GunFire.FiredWeaponAnimation(isRightWeapon, character, weapontype, using_hit, true);

            //Deal Damage AI
            if (!character.IsPlayer)
                character.GetComponent<NPCAI>().DealtDamage();

            //If a vehicle or character was hit before a wall..
            if (v_hit || c_hit)
            {
                float new_dmg = 0.0f;
                damage = using_hit.distance > wep_range ?
                    (new_dmg = (damage - (using_hit.distance - wep_range) * 4.5f)) < 0 ? 0 : new_dmg //hit past max range
                    : damage * 2;  //hit within max range

                if (c_hit)
                {
                    RecognizeWasHit r = hit3.collider.GetComponent<RecognizeWasHit>();
                    CharacterData d = r.attatchedCharacter;
                    //was character
                    if (d != null && d.faction != character.faction)
                    {
                        //only apply damage if it is allowed
                        if (!ForceMiss)
                        {
                            d.SetCharHealth(character, d.Health - (int)damage);
                            BloodSplat.MakeBloodSplat(new Vector3(using_hit.point.x, 0.01f, using_hit.point.z));
                        }
                    }
                }
                if (v_hit)
                {
                    RecognizeWasHit r = using_hit.collider.GetComponent<RecognizeWasHit>();
                    VehicleScript v = r.attatchedVehicle;

                    if (v != null && (v.driver == null || v.driver.faction != character.faction))
                    {
                        //only apply damage if it is allowed
                        if (!ForceMiss)
                        {
                            v.SetHealth(character, v.Health - (int)damage);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Returns the raycast hit of the closest Object hit that is not the input character, or the input
    /// character's vehicle
    /// </summary>
    /// <param name="c"></param>
    /// <param name="start"></param>
    /// <param name="direction"></param>
    /// <param name="MaxRange"></param>
    /// <param name="layerMask"></param>
    /// <param name="hit"></param>
    /// <returns></returns>
    private static bool ClosestRayCastHit(CharacterData c, Vector3 start, Vector3 direction, out RaycastHit hit,
        float MaxRange, int layerMask)
    {
        hit = default(RaycastHit);

        if (c == null)
        {
            Debug.Log("Input null character!");
            return false;
        }

        RaycastHit[] hits = Physics.RaycastAll(start, direction, MaxRange, layerMask);

        if (hits == null)
            return false;

        float MinDist = float.MaxValue;
        bool FoundHit = false;
        foreach (RaycastHit r in hits)
        {
            if (r.collider != null)
            {
                RecognizeWasHit rH = r.collider.GetComponent<RecognizeWasHit>();
                if (rH != null)
                {
                    VehicleScript v = rH.attatchedVehicle;
                    CharacterData current_char = rH.attatchedCharacter;
                    if (current_char != null && current_char != c)
                    {
                        if (r.distance < MinDist)
                        {
                            MinDist = r.distance;
                            hit = r;
                            FoundHit = true;
                        }
                    }
                    else if (v != null && v.driver != c && v.passenger != c)
                    {
                        if (r.distance < MinDist)
                        {
                            MinDist = r.distance;
                            hit = r;
                            FoundHit = true;
                        }
                    }
                }
            }
        }

        return FoundHit;
    }
}