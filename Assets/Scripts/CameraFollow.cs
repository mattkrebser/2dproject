﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This class is only meant to be used once, for the player
/// </summary>
[System.Serializable]
public class CameraFollow : MonoBehaviour
{
    /// <summary>
    /// Player's Camera
    /// </summary>
    public static Camera cam;

    [SerializeField]
    public GameObject follow_this;

    /// <summary>
    /// Reference to whatevr map is currently being used.
    /// </summary>
    public MapData map_data;

    /// <summary>
    /// Offset to add to positon compuations.
    /// </summary>
    Transform offset_transform;

    public float follow_height = 2.5f;

    /// <summary>
    /// Set the camera to follow on a specific map
    /// </summary>
    /// <param name="md"></param>
    public static void SetMap(MapData md)
    {
        Instance.map_data = md;
    }

    static CameraFollow inst;
    static CameraFollow Instance
    {
        get
        {
            if (inst == null)
            {
                inst = GameObject.Find("Camera").GetComponent<CameraFollow>();
            }
            return inst;
        }
    }

    void Awake()
    {
        if (cam == null)
            cam = GetComponent<Camera>();
    }

    public void GoBack()
    {
        transform.position = new Vector3(-256, 36, -256);
        follow_this = null;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //If the camera is in the lobby, then it follows cars
        if (GameSettings.LobbyRunning || GameSettings.InLobby)
        {
            FollowRandomCar();
            return;
        }
        //otherwise it follows the player
        else if (GameSettings.IsInGame && CharacterMove.CharacterTransform != null)
        {
            if (follow_this == null || CharacterMove.CharacterTransform != follow_this.transform)
                follow_this= CharacterMove.CharacterTransform.gameObject;
        }

        //set map
        if (MapData.ActiveMap != null)
            map_data = MapData.ActiveMap;

        //only follow if we can
        if (follow_this != null)
        {
            Vector3 newPos = new Vector3(
                follow_this.transform.position.x,
                follow_this.transform.position.y + follow_height,
                follow_this.transform.position.z);
            transform.position = Vector3.Lerp(transform.position, newPos, 5 * Time.deltaTime);

            Vector3 p1, p2, p3, p4;

            //Keep the camera inside the bounds of the map
            if (map_data != null)
            {
                if (map_data.IsPositionAtGlobalZero)
                {
                    Plane plane = new Plane(Vector3.up, new Vector3(0, transform.position.y - follow_height, 0));
                    Ray ray;
                    float distance;
                    ray = cam.ViewportPointToRay(Vector3.zero); // bottom left ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p1 = ray.GetPoint(distance);
                        if (p1.z < (map_data.d_y / 2) * -1)
                            transform.position = new Vector3(transform.position.x, transform.position.y,
                                transform.position.z + Mathf.Abs(p1.z - (map_data.d_y) / 2 * -1));
                    }
                    ray = cam.ViewportPointToRay(new Vector3(0, 1, 0)); // top left ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p2 = ray.GetPoint(distance);
                        if (p2.x < (map_data.d_x / 2) * -1)
                            transform.position = new Vector3(transform.position.x + Mathf.Abs(p2.x - (map_data.d_x / 2) * -1),
                                transform.position.y, transform.position.z);
                    }
                    ray = cam.ViewportPointToRay(new Vector3(1, 0, 0)); // bottom right ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p3 = ray.GetPoint(distance);
                        if (p3.x > (map_data.d_x / 2))
                            transform.position = new Vector3(transform.position.x - (p3.x - (map_data.d_x / 2)),
                                transform.position.y, transform.position.z);
                    }
                    ray = cam.ViewportPointToRay(new Vector3(1, 1, 0)); // top right ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p4 = ray.GetPoint(distance);
                        if (p4.z > (map_data.d_y / 2))
                            transform.position = new Vector3(transform.position.x, transform.position.y,
                                transform.position.z - (p4.z - map_data.d_y / 2));
                    }
                }
                else
                {
                    transform.position = new Vector3(
                        follow_this.transform.position.x,
                        follow_this.transform.position.y + follow_height - map_data.CamHeightModifer,
                        follow_this.transform.position.z);

                    Plane plane = new Plane(Vector3.up, new Vector3(0, 0, 0));
                    Ray ray;
                    float distance;
                    float bottom = map_data.bottomright.position.z; float left = map_data.topleft.position.x;
                    float top = map_data.topleft.position.z; float right = map_data.bottomright.position.x; ;

                    ray = cam.ViewportPointToRay(Vector3.zero); // bottom left ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p1 = ray.GetPoint(distance);
                        if (p1.z < bottom)
                            transform.position = new Vector3(transform.position.x, transform.position.y,
                                transform.position.z + bottom - p1.z);
                    }
                    ray = cam.ViewportPointToRay(new Vector3(0, 1, 0)); // top left ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p2 = ray.GetPoint(distance);
                        if (p2.x < left)
                            transform.position = new Vector3(transform.position.x + left - p2.x,
                                transform.position.y, transform.position.z);
                    }
                    ray = cam.ViewportPointToRay(new Vector3(1, 0, 0)); // bottom right ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p3 = ray.GetPoint(distance);
                        if (p3.x > right)
                            transform.position = new Vector3(transform.position.x - (p3.x - right),
                                transform.position.y, transform.position.z);
                    }
                    ray = cam.ViewportPointToRay(new Vector3(1, 1, 0)); // top right ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p4 = ray.GetPoint(distance);
                        if (p4.z > top)
                            transform.position = new Vector3(transform.position.x, transform.position.y,
                                transform.position.z - (p4.z - top));
                    }
                }
            }
            else
            {
                
            }
        }
    }

    float timeOfLastChange = 0.0f;
    void FollowRandomCar()
    {
        float follow_height = this.follow_height - 0.2f;
        Vector3 fP = follow_this == null ? Vector3.zero : follow_this.transform.position;
        //follow new item every 10 seconds.. or a bunch of other conditions.
        //The goal of the camera is to follow a vehicle
        if (   (Time.realtimeSinceStartup - timeOfLastChange > 10.0f || timeOfLastChange == 0.0f) //if any of these 4 conditions are true..
            || follow_this == null
            || (follow_this.GetComponent<VehicleScript>() == null && Time.realtimeSinceStartup - timeOfLastChange > 10.0f)
            || (CharacterMove.CharacterTransform != null && CharacterMove.CharacterTransform == follow_this.transform)
            || !(fP.x >= -32 && fP.x <= 32 && fP.z <= 32 && fP.z >= -32))
        {
            if (VehicleScript.All_Vehicles != null && VehicleScript.All_Vehicles.Count > 0)
            {
                //attempt to find an in-bounds non-empty vehicle to follow
                List<VehicleScript> vl = new List<VehicleScript>();
                foreach (VehicleScript v in VehicleScript.All_Vehicles.Values)
                {
                    if (v.transform.position.x >= -32 && v.transform.position.x <= 32 &&
                        v.transform.position.z <= 32 && v.transform.position.z >= -32 && !v.isEmpty
                        && v.gameObject != follow_this)
                        vl.Add(v);
                }
                //if a vehicle was found
                if (vl.Count > 0)
                {
                    //fandom select
                    follow_this = vl[Random.Range(0, vl.Count)].gameObject;

                    //set new position
                    Vector3 newPos = new Vector3(
                        follow_this.transform.position.x,
                        follow_this.transform.position.y + follow_height,
                        follow_this.transform.position.z);
                    transform.position = newPos;

                    timeOfLastChange = Time.realtimeSinceStartup;
                }
                else if (CharacterData.AllCharacters != null && CharacterData.AllCharacters.Count > 0)
                {
                    List<CharacterData> chars = new List<CharacterData>();
                    foreach (CharacterData c in CharacterData.AllCharacters)
                    {
                        //only follow dyanmic characters
                        if (!c.IsPlayer && !c.GetComponent<AIMove>().IsStatic && c.gameObject != follow_this)
                            chars.Add(c);
                    }

                    //only set position if we found a suitable character
                    if (chars.Count > 0)
                    {
                        follow_this = chars[Random.Range(0, chars.Count)].gameObject;
                        //set new position
                        Vector3 newPos = new Vector3(
                            follow_this.transform.position.x,
                            follow_this.transform.position.y + follow_height,
                            follow_this.transform.position.z);
                        transform.position = newPos;
                        timeOfLastChange = Time.realtimeSinceStartup;
                    }
                    else
                    {
                        GoBack();
                        return;
                    }
                }
                else
                {
                    GoBack();
                    return;
                }
            }
            else if (CharacterData.AllCharacters != null && CharacterData.AllCharacters.Count > 0)
            {
                GoBack();
                return;
            }
            else
            {
                GoBack();
                return;
            }
        }

        //follow targer
        if (follow_this != null)
        {
            //lerp to new position
            Vector3 newPos = new Vector3(
                follow_this.transform.position.x,
                follow_this.transform.position.y + follow_height,
                follow_this.transform.position.z);
            transform.position = Vector3.Lerp(transform.position, newPos, 5 * Time.deltaTime);

            //modify the player so they are invisible, at the same position as the follow object,
            //and do not collide with anything
            if (CharacterMove.CharacterTransform != null)
            {
                CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
                c.DeleteClothes();
                Rigidbody r = c.GetComponent<Rigidbody>();
                r.useGravity = false;
                c.GetComponent<CapsuleCollider>().enabled = false;
                foreach (Collider col in c.GetComponentsInChildren<Collider>(true))
                {
                    col.enabled = false;
                }
                r.transform.position = new Vector3(newPos.x, 0.0f, newPos.z);
                c.GetComponent<NavMeshObstacle>().enabled = false;
            }

            //clamp camera
            Vector3 p1, p2, p3, p4;
            //Keep the camera inside the bounds of the map
            if (map_data != null)
            {
                if (map_data.IsPositionAtGlobalZero)
                {
                    Plane plane = new Plane(Vector3.up, new Vector3(0, transform.position.y - follow_height, 0));
                    Ray ray;
                    float distance;
                    ray = cam.ViewportPointToRay(Vector3.zero); // bottom left ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p1 = ray.GetPoint(distance);
                        if (p1.z < (map_data.d_y / 2) * -1)
                            transform.position = new Vector3(transform.position.x, transform.position.y,
                                transform.position.z + Mathf.Abs(p1.z - (map_data.d_y) / 2 * -1));
                    }
                    ray = cam.ViewportPointToRay(new Vector3(0, 1, 0)); // top left ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p2 = ray.GetPoint(distance);
                        if (p2.x < (map_data.d_x / 2) * -1)
                            transform.position = new Vector3(transform.position.x + Mathf.Abs(p2.x - (map_data.d_x / 2) * -1),
                                transform.position.y, transform.position.z);
                    }
                    ray = cam.ViewportPointToRay(new Vector3(1, 0, 0)); // bottom right ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p3 = ray.GetPoint(distance);
                        if (p3.x > (map_data.d_x / 2))
                            transform.position = new Vector3(transform.position.x - (p3.x - (map_data.d_x / 2)),
                                transform.position.y, transform.position.z);
                    }
                    ray = cam.ViewportPointToRay(new Vector3(1, 1, 0)); // top right ray
                    if (plane.Raycast(ray, out distance))
                    {
                        p4 = ray.GetPoint(distance);
                        if (p4.z > (map_data.d_y / 2))
                            transform.position = new Vector3(transform.position.x, transform.position.y,
                                transform.position.z - (p4.z - map_data.d_y / 2));
                    }
                }
            }
            else
            {
                GoBack();
            }
        }
    }
}
           