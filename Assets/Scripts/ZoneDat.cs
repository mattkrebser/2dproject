﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ZoneDat
{
    public string ZoneName;
    public int tot_spawn;
    public int tot_dead;
    public bool IsSpawning = true;
    public bool AttackPlayer;
}
