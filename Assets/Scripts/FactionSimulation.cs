﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Holds information about the factions in the current game instance, and modifies them
/// </summary>
public class FactionSimulation : MonoBehaviour {

    /// <summary>
    /// List of Factions
    /// </summary>
    private static List<Faction> AllFactions;
    /// <summary>
    /// All factions..
    /// </summary>
    public static List<Faction> All_Factions
    {
        get
        {
            return AllFactions;
        }
    }

	// Use this for initialization
	void Awake ()
    {
        StartCoroutine(FactionRoutine());
	}

    public static void SetFactions(List<Faction> newList)
    {
        AllFactions = newList;
        if (AllFactions == null)
        {
            AllFactions = newFacList;
        }
    }

    /// <summary>
    /// Simulates faction relationships
    /// </summary>
    /// <returns></returns>
    private IEnumerator FactionRoutine()
    {
        while (true)
        {
            if (GameSettings.Running || GameSettings.LobbyRunning)
            {
                //Change relations by random amount, for a random faction. Besides the Player's faction.
                Faction f = AllFactions[Random.Range(0, AllFactions.Count - 1)];
                foreach (FactionRelation r in f.Relations)
                {
                    if (Random.Range(0, 2) == 1)
                        ModifyRelations(f.faction, r.faction, Random.Range(-30, 30));
                }
            }

            yield return new WaitForSeconds(Constants.FactionSimSpeed);
        }  
    }

    public static bool FactionsAreEnemies(Factions f1, Factions f2)
    {
        if (f1 == Factions.enemy || f2 == Factions.enemy)
        {
            return f1 != f2;
        }
        return AllFactions[(int)f1].Relations[(int)f2].RelationShip < 20;
    }
    public static bool FactionsAreHostile(Factions f1, Factions f2)
    {
        if (f1 == Factions.enemy || f2 == Factions.enemy)
        {
            return false;
        }
        return AllFactions[(int)f1].Relations[(int)f2].RelationShip < 40 &&
            AllFactions[(int)f1].Relations[(int)f2].RelationShip >= 20;
    }
    public static bool FactionsAreNeutral(Factions f1, Factions f2)
    {
        if (f1 == Factions.enemy || f2 == Factions.enemy)
        {
            return false;
        }
        return AllFactions[(int)f1].Relations[(int)f2].RelationShip < 60 &&
            AllFactions[(int)f1].Relations[(int)f2].RelationShip >= 40;
    }
    public static bool FactionsAreFriendly(Factions f1, Factions f2)
    {
        if (f1 == Factions.enemy || f2 == Factions.enemy)
        {
            return false;
        }
        return AllFactions[(int)f1].Relations[(int)f2].RelationShip < 80 &&
            AllFactions[(int)f1].Relations[(int)f2].RelationShip >= 60;
    }
    public static bool FactionsAreAllies(Factions f1, Factions f2)
    {
        if (f1 == Factions.enemy || f2 == Factions.enemy)
        {
            return false;
        }
        return AllFactions[(int)f1].Relations[(int)f2].RelationShip >= 80;
    }

    /// <summary>
    /// Change the relationship for the input factions by x amount
    /// </summary>
    /// <param name="f1"></param>
    /// <param name="f2"></param>
    /// <param name="changeByThisAmount"></param>
    public static void ModifyRelations(Factions f1, Factions f2, int changeByThisAmount)
    {
        AllFactions[(int)f1].Relations[(int)f2].RelationShip += changeByThisAmount;
        AllFactions[(int)f2].Relations[(int)f1].RelationShip += changeByThisAmount;
    }

    /// <summary>
    /// Returns a new List of Factions with default values
    /// </summary>
    private static List<Faction> newFacList
    {
        get
        {
            List<Faction> FactionRelations = new List<Faction>();
            for (int i = 0; i < 15; i++)
            {
                FactionRelations.Add(new Faction());
                FactionRelations[i].faction = (Factions)i;
                FactionRelations[i].InitNewList();
            }
            return FactionRelations;
        }
    }
}

/// <summary>
/// Class representing a faction
/// </summary>
[System.Serializable]
public class Faction
{
    public Factions faction;
    /// <summary>
    /// List of relations between factions
    /// </summary>
    public List<FactionRelation> Relations;

    internal void InitNewList()
    {
        //there is a redundant faction relation for each faction with itself
        Relations = new List<FactionRelation>();
        for (int i = 0; i < 15; i++)
        {
            Relations.Add(new FactionRelation());
            Relations[i].faction = (Factions)i;
            Relations[i].RelationShip = 50;
        }
    }
}

[System.Serializable]
public class FactionRelation
{
    [Tooltip("RelationShip with this faction")]
    public Factions faction;
    private int _relationShip;
    /// <summary>
    /// 0 being aweful and 100 being great
    /// </summary>
    public int RelationShip
    {
        get
        {
            return _relationShip;
        }
        set
        {
            _relationShip = value < 0 ? 0 : value > 100 ? 100 : value;
        }
    }
}