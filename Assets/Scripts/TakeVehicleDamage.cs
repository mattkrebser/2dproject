﻿using UnityEngine;
using System.Collections;

public class TakeVehicleDamage : MonoBehaviour
{

    public CharacterData my_character;

    /// <summary>
    /// Take vehicle Damage! (Player)
    /// </summary>
    /// <param name="col"></param>
    void OnCollisionEnter(Collision col)
    {
        if (my_character.InVehicle != null)
            return;
        if (col.gameObject.layer == LayerMask.NameToLayer("CharVehicleHit"))
        {
            VehicleScript v;
            if ((v = col.transform.parent.GetComponent<VehicleScript>()) != null)
            {
                //rigidbody for player is driving/no driver
                Rigidbody r; NavMeshAgent n;
                if ((r = col.gameObject.GetComponent<Rigidbody>()) != null)
                {
                    //faster than a move threshold
                    if (r.velocity.magnitude > 1.3f)
                    {
                        my_character.SetCharHealth(v.driver, my_character.Health - (int)(r.velocity.magnitude * 30));
                    }
                }
                //navmeshagent for npc drivers
                else if ((n = col.transform.parent.GetComponent<NavMeshAgent>()) != null)
                {
                    //faster than a move threshold
                    if (n.velocity.magnitude > 1.3f)
                    {
                        my_character.SetCharHealth(v.driver, my_character.Health - (int)(n.velocity.magnitude * 30));
                        //np did damage
                        v.driver.GetComponent<NPCAI>().NPCEvent(DialogueEventType.RoadKill, Personality.none, Factions.none);
                    }
                }
            }
        }
    }
    /// <summary>
    /// Take Vehicle Damage (NPC)
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerEnter(Collider col)
    {
        if (my_character.InVehicle != null)
            return;
        if (col.gameObject.layer == LayerMask.NameToLayer("VehicleMove"))
        {
            VehicleScript v;
            if ((v = col.gameObject.GetComponent<VehicleScript>()) != null)
            {
                //rigidbody for player is driving/no driver
                Rigidbody r;
                if ((r = col.gameObject.GetComponent<Rigidbody>()) != null)
                {
                    //faster than a move threshold
                    if (r.velocity.magnitude > 1.1f)
                    {
                        my_character.SetCharHealth(v.driver, my_character.Health - (int)(r.velocity.magnitude * 40));
                    }
                }
            }
        }

        if (col.gameObject.layer == LayerMask.NameToLayer("NPCOnlyCollisions"))
        {
            VehicleScript v;
            if ((v = col.transform.parent.GetComponent<VehicleScript>()) != null)
            {
                NavMeshAgent a = col.transform.parent.GetComponent<NavMeshAgent>();
                //faster than a move threshold
                if (a != null && a.velocity.magnitude > 1.1f)
                {
                    my_character.SetCharHealth(v.driver, my_character.Health - (int)(a.velocity.magnitude * 40));
                    v.driver.GetComponent<NPCAI>().NPCEvent(DialogueEventType.RoadKill, Personality.none, Factions.none);
                }
            }
        }
    }
}
