﻿using UnityEngine;
using System.Collections;

public class CoroutineHelper : MonoBehaviour
{
    static CoroutineHelper instance;
    public void Init()
    {
        if (instance == null)
            instance = this;
    }
	public static void RunCoroutine(IEnumerator routine)
    {
        instance.StartCoroutine(routine);
    }
}
