﻿using UnityEngine;
using System.Collections;

public class QuestEvent : MonoBehaviour
{
    [Tooltip("What should the player do for this event?")]
    public string EventName;
    /// <summary>
    /// The Quest Parent, all Quest events must be parented to their quest parent
    /// </summary>
    public Quest BaseQuest
    {
        get
        {
            return transform.parent.GetComponent<Quest>();
        }
    }

    [Tooltip("If this event is a checkpoint, then it is safe to save this event to the disk as the current event. " +
        "Good checkpoints are usually Interact type events and goto location events")]
    public bool isCheckPoint = false;

    [Tooltip("If set to true, then this event will be called, and the quest chain will continue to the next event immediatly")]
    public bool AutomaticAdvance = false;

    [Tooltip("After completing this event, this description will be added(concatenated) to the Player's Quest Journal "
        + "for this quest. It will appear in order from the other completed events.")]
    public string JournalDescriptionAdd;

    [Tooltip("This is used for the mini-map, so that ic can determine win which direction the player needs to head " +
        "for their quest. If left null, there will be no mini-map Icon (which is fine for many quest events)")]
    public MapData EventLocation;

    /// <summary>
    /// Start the Quest Event
    /// </summary>
    public virtual void StartEvent()
    {

    }

    public virtual void ResetEvent()
    {

    }

    public virtual bool IsCurrentEvent()
    {
        return BaseQuest.Events[BaseQuest.CurrentEvent] == this;
    }

    /// <summary>
    /// In order for an event to be run, it must be the currently active event in the quest, the parent quest must
    /// be started and the parent quest must not be finished
    /// </summary>
    /// <returns></returns>
    public virtual bool CanRun()
    {
        return BaseQuest.Started && !BaseQuest.IsCompleted && IsCurrentEvent();
    }
}
