﻿using UnityEngine;
using System.Collections;

public class AIPathAction : MonoBehaviour
{
    [Tooltip("Stand for x seconds at this node before continuing, Path actions do not work on vehicles")]
    public float WaitOnArrive = 0.0f;
    [Tooltip("Move to a random node next, upon arriving to this node, Path actions do not work on vehicles")]
    public bool MoveToRandNodeOnLeave = false;
    [Tooltip("If set true, upon reached this node, the NPC will match the rotation of this transform. " +
        " This is only useful if the NPC stops if after reaching this node.")]
    public bool EnforceThisTransformRotation = false;
}
