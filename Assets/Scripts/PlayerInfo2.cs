﻿using UnityEngine;
using System.Collections.Generic;
using System;

[System.Serializable]
public class PlayerInfo2
{

    /// <summary>
    /// WeaponLoadout left
    /// </summary>
    public string[] LWeaponsLoadOut = new string[10];
    /// <summary>
    /// weapon loadout right
    /// </summary>
    public string[] RWeaponsLoadOut = new string[10];

    /// <summary>
    /// List of this player'squests. This value is null except for the moment before and after saving
    /// </summary>
    public List<QuestDat> MyQuests;

    public int ActiveLWeapon;
    public int ActiveRWeapon;

    /// <summary>
    /// List of weapons owned
    /// </summary>
    public List<string> LWeapons = new List<string>();
    /// <summary>
    /// List of weapons owned
    /// </summary>
    public List<string> RWeapons = new List<string>();
    public int Ammo;
    public int Rockets;
    public int Grenades;

    public static PlayerInfo2 Default
    {
        get
        {
            PlayerInfo2 p = new PlayerInfo2();

            p.ActiveMap = "Harmony";

            return p;
        }
    }

    public string ActiveMap;

    public bool isFrozen;

    public bool GunsAllowed = true;
}
