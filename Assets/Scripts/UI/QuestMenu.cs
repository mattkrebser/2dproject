﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;

public class QuestMenu : MonoBehaviour {

    public GameObject StartedContent;
    public GameObject CompletedContent;

    public GameObject DescriptionContent;
    public Text Objective;

    public RectTransform ObjectiveRect;
    public RectTransform BarRect;

    private static QuestMenu Instance;
    void Start(){Instance = this;}

    /// <summary>
    /// Called when a quest button is click, this re-displays the description
    /// </summary>
    /// <param name="QuestName"></param>
    public static void SetDescription(string QuestName)
    {
        //reset content
        foreach(RectTransform rT in Instance.DescriptionContent.GetComponentInChildren<RectTransform>(true))
        {
            if (rT != Instance.DescriptionContent.transform && rT != Instance.ObjectiveRect && rT != Instance.BarRect)
            {
                Destroy(rT.gameObject);
            }
        }

        //Make new description tabs and Objective text
        Quest ThisQuest = Quest.AllQuests[QuestName];
        string objective = ThisQuest.IsCompleted ? "Completed" : ThisQuest.Events[ThisQuest.CurrentEvent].EventName;
        Instance.Objective.text = string.IsNullOrEmpty(objective) ? "Do Something!" : objective;
        for (int i = 0; i <= 
            //Clamping Current event to a maximum of Event.Count - 1
            (ThisQuest.CurrentEvent >= ThisQuest.Events.Count ? ThisQuest.Events.Count -1 : ThisQuest.CurrentEvent)
            ; i++)
        {
            //only display current event if the quest is completed
            if (i == ThisQuest.CurrentEvent && !ThisQuest.IsCompleted)
                break;

            //Add a description entry, if the event contains one
            if (ThisQuest.Events[i].JournalDescriptionAdd != null &&
                !String.IsNullOrEmpty(ThisQuest.Events[i].JournalDescriptionAdd.Trim()))
            {
                //Add description Object
                GameObject Desc = Prefabs.GetNewPrefab("UIPrefabs/DescriptionText");
                Desc.GetComponentInChildren<Text>().text = ThisQuest.Events[i].JournalDescriptionAdd;
                Desc.transform.SetParent(Instance.DescriptionContent.transform);
            }
        }

        //rescale
        Instance.DescriptionContent.GetComponent<EnforeUIScale>().ScaleEnfore();
    }

    void OnEnable()
    {
        BuildQuestMenu();
    }
    void OnDisable()
    {
        DestroyQuestMenu();
    }

    /// <summary>
    /// Build the Quest Menu. Fill it with quests.
    /// </summary>
    void BuildQuestMenu()
    {
        bool default_descr = false;
        foreach (Quest q in Quest.AllQuests.Values)
        {
            //if the quest has been started and appears in the players journal
            if (q.Started && q.QuestAppearsInPlayerJournal && !q.IsCompleted)
            {
                GameObject newQ = Prefabs.GetNewPrefab("UIPrefabs/QuestButton");
                newQ.GetComponentInChildren<Text>().text = q.QuestName;
                newQ.transform.SetParent(StartedContent.transform);
                //set the first quest in the description
                if (!default_descr)
                {
                    StartCoroutine(LateDescr(q.QuestName));
                    default_descr = true;
                }
            }
            //if the quest is finished and appears in the player's journal
            else if (q.IsCompleted && q.QuestAppearsInPlayerJournal)
            {
                GameObject newQ = Prefabs.GetNewPrefab("UIPrefabs/QuestButton");
                newQ.GetComponentInChildren<Text>().text = q.QuestName;
                newQ.transform.SetParent(CompletedContent.transform);
            }
        }

        //rescale
        StartedContent.GetComponent<EnforeUIScale>().ScaleEnfore();
        CompletedContent.GetComponent<EnforeUIScale>().ScaleEnfore();
    }

    IEnumerator LateDescr(String QuestName)
    {
        yield return 0;
        SetDescription(QuestName);
    }
    /// <summary>
    /// Wipes the content of the quest menu
    /// </summary>
    void DestroyQuestMenu()
    {
        //reset content
        foreach (RectTransform rT in StartedContent.GetComponentInChildren<RectTransform>())
        {
            if (rT != StartedContent.transform)
            {
                Destroy(rT.gameObject);
            }
        }
        //reset content
        foreach (RectTransform rT in CompletedContent.GetComponentInChildren<RectTransform>())
        {
            if (rT != CompletedContent.transform)
            {
                Destroy(rT.gameObject);
            }
        }
    }
}
