﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerDialogueUI : MonoBehaviour {

    public Text t1;
    public Text t2;
    public Text t3;

    public GameObject b1;
    public GameObject b2;
    public GameObject b3;

    private static PlayerDialogueUI Instance;
    private Dialogue CurrentDialogue;

    public GameObject SpaceText;

    public GameObject NPCDialogueObj;
    public Text NPCDialogueText;

    public Image Background;
    public Image Border1;
    public Image border2;

    public GameObject DialogueImage;

    public Text SpeakerName;
    public Image face;
    public Image eyes;
    public Image outerEyes;
    public Image lipstick;
    public Image earrings;
    public Image hair;
    public Image hat;

    public static bool PlayerInChoiceMenu = false;

    public static GameObject SpaceTextObj
    {
        get
        {
            return Instance.SpaceText;
        }
    }

    void Start()
    {
        Instance = this;
    }

    public void SetDialogue(int choiceNum)
    {
        //We always choose the first element for the NPC to say (because the NPC can't choose, so there
        //should always just be one response to the player)
        //Also remember, that the CurrentDIalogue Variable is the dialogue of the NPC,
        //CurrentDialogue.ThisResponses[choiceNum] is the player's response
        //CurrentDialogue.ThisRespones[choiceNum].ThisRespones[0] is what the NPC will say next (its Dialogue)
        NPCDialogue.PlayerSelectedNewDialogue = CurrentDialogue.ThisResponses[choiceNum].ThisResponses == null ||
            CurrentDialogue.ThisResponses[choiceNum].ThisResponses.Count == 0 ? null :
            CurrentDialogue.ThisResponses[choiceNum].ThisResponses[0];
        NPCDialogue.PlayerResponded = true;

        b1.SetActive(false);
        b2.SetActive(false);
        b3.SetActive(false);
        Instance.NPCDialogueObj.SetActive(false);
        SetName(null);
        SetFace(null);
        EnableBackGround(false);
        CurrentDialogue = null;
        PlayerInChoiceMenu = false;
    }
	
    public static void ResetDialogue()
    {
        Instance.b1.SetActive(false);
        Instance.b2.SetActive(false);
        Instance.b3.SetActive(false);
        Instance.NPCDialogueObj.SetActive(false);
        SetName(null);
        SetFace(null);
        EnableBackGround(false);
        Instance.CurrentDialogue = null;
    }

    public static void NPCDialogueBox(Dialogue d, int i)
    {
        EnableBackGround(true);
        //Make NPC dialogue visual
        Instance.NPCDialogueObj.SetActive(true);
        Instance.NPCDialogueText.text = Dialogue.ModifyString(d.ThisDialogue[i]);
    }
    public static void HideNPCDialogueBox()
    {
        Instance.NPCDialogueObj.SetActive(false);
        EnableBackGround(false);
        SetName(null);
        SetFace(null);
    }

    public static void MakeDialogue(Dialogue d)
    {

        if (d == null || d.ThisResponses.Count == 0)
            return;

        Instance.CurrentDialogue = d;

        PlayerInChoiceMenu = true;

        //Make NPC dialogue visual
        Instance.NPCDialogueObj.SetActive(true);
        Instance.NPCDialogueText.text = Dialogue.ModifyString( d.ThisDialogue[d.ThisDialogue.Count - 1]);

        //display background for the UI
        EnableBackGround(true);

        //set face/name of speaker
        SetName(d.SaysThisDialogue);
        SetFace(d.SaysThisDialogue);

        //Note**, since the player only has one string per choice,
        //all other parts of the dialogue (ie, ThisDialogue[1], 2, 3.. etc)
        //will be ignore. Player Dialogues should only be one string.
        //On the other hand, NPCs can have many strings, will will be said
        //consecutivly
        if (d.ThisResponses.Count >= 1)
        {
            Instance.b1.SetActive(true);
            Instance.t1.text = d.ThisResponses[0].ThisDialogue[0];
        }
        if (d.ThisResponses.Count >= 2)
        {
            Instance.b2.SetActive(true);
            Instance.t2.text = d.ThisResponses[1].ThisDialogue[0];
        }
        if (d.ThisResponses.Count >= 3)
        {
            Instance.b3.SetActive(true);
            Instance.t3.text = d.ThisResponses[2].ThisDialogue[0];
        }
    }

    static void EnableBackGround(bool value)
    {
        Instance.Background.enabled = value;
        Instance.Border1.enabled = value;
        Instance.border2.enabled = value;
    }
    public static void SetCharacterSpeaking(CharacterData c)
    {
        SetName(c);
        SetFace(c);
    }
    static void SetFace(CharacterData c)
    {
        if (c == null)
        {
            Instance.face.sprite = null;
            Instance.face.enabled = false;

            Instance.eyes.sprite = null;
            Instance.eyes.enabled = false;

            Instance.outerEyes.sprite = null;
            Instance.outerEyes.enabled = false;

            Instance.hair.sprite = null;
            Instance.hair.enabled = false;

            Instance.hat.sprite = null;
            Instance.hat.enabled = false;

            Instance.lipstick.sprite = null;
            Instance.lipstick.enabled = false;

            Instance.earrings.sprite = null;
            Instance.earrings.enabled = false;

            Instance.DialogueImage.SetActive(false);
        }
        else
        {
            Instance.DialogueImage.SetActive(true);
            Instance.face.sprite = c.GetSprite(c.Facecolor);
            if (Instance.face.sprite != null)
                Instance.face.enabled = true;
            else
                Instance.face.enabled = false;

            Instance.eyes.sprite = c.GetSprite(c.Eyecolor);
            if (Instance.eyes.sprite != null)
                Instance.eyes.enabled = true;
            else
                Instance.eyes.enabled = false;

            Instance.outerEyes.sprite = c.GetSprite(c.Outer_eye);
            if (Instance.outerEyes.sprite != null)
                Instance.outerEyes.enabled = true;
            else
                Instance.outerEyes.enabled = false;

            Instance.hair.sprite = c.GetSprite(c.Hair);
            if (Instance.hair.sprite != null)
                Instance.hair.enabled = true;
            else
                Instance.hair.enabled = false;

            Instance.hat.sprite = c.GetSprite(c.Hat);
            if (Instance.hat.sprite != null)
                Instance.hat.enabled = true;
            else
                Instance.hat.enabled = false;

            Instance.lipstick.sprite = c.GetSprite(c.Lipstick);
            if (Instance.lipstick.sprite != null)
                Instance.lipstick.enabled = true;
            else
                Instance.lipstick.enabled = false;

            Instance.earrings.sprite = c.GetSprite(c.Earings);
            if (Instance.earrings.sprite != null)
                Instance.earrings.enabled = true;
            else
                Instance.earrings.enabled = false;
        }
    }

    static void SetName(CharacterData c)
    {
        if (c == null)
        {
            Instance.SpeakerName.text = "";
        }
        else
        {
            string first = c.first_name == null ? "" : c.first_name;
            string last = c.last_name == null ? "" : c.last_name;
            Instance.SpeakerName.text = first + " " + last;
        }
    }
}
