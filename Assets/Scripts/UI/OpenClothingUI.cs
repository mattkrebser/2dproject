﻿using UnityEngine;
using System.Collections;

public class OpenClothingUI : MonoBehaviour {

    /// <summary>
    /// Opens or closes the menu depending on its current state
    /// </summary>
	public void OpenClose()
    {
        if (UIMenus.ClothingWindow.activeSelf)
        {
            UIMenus.ClothingWindow.SetActive(false);
            return;
        }

        UIMenus.SetAllNotActive();
        UIMenus.ClothingWindow.SetActive(true);
    }

    public void Close()
    {
        UIMenus.ClothingWindow.SetActive(false);
    }
}
