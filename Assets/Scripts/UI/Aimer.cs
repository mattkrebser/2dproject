﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;

public class Aimer : MonoBehaviour {

    public Image aimer;
    public RectTransform parentRect;

    public Sprite armedSprite;
    public Sprite unarmedSprite;

    public Vector3 ArmedScale;
    public Vector3 UnarmedScale;

    public GameObject NPCHP;
    public GameObject NPCNAME;

    public Text NPCName;
    public Text NPCHealth;
    public RectTransform NPCHealthBar;

    public float OriginalHPWidth;

    private static Aimer Instance;

    void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        if (!GameSettings.IsInGame || CharacterMove.CharacterTransform == null)
        {
            if (aimer.enabled)
                aimer.enabled = false;
        }
        else
        {
            //get player data
            CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
            //make the cursor visible if any UI menus are open
            Cursor.visible = UIMenus.AnyActive || PlayerDialogueUI.PlayerInChoiceMenu;
            //the aimer visible state is always opposite the cursor visible state
            aimer.enabled = !Cursor.visible;
            //if the aimer is enabled, the cursor is locked to the game screen
            Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Confined;

            bool ArmedAimer = false, UnderTarget = false;

            //set color/image/scale based on if the character is currently wielding weapons
            if ((String.IsNullOrEmpty(c.L_weapon) && String.IsNullOrEmpty(c.R_weapon)) || c.IsDead)
            {
                if (aimer.sprite != unarmedSprite)
                {
                    aimer.sprite = unarmedSprite;
                    aimer.color = new Color(1, 1, 1, 0.75f);
                }
                aimer.rectTransform.localScale = UnarmedScale;
            }
            //is wielding weapons
            else
            {
                if (aimer.sprite != armedSprite)
                {
                    aimer.sprite = armedSprite;
                    aimer.color = new Color(1, 1, 1, 0.88f);
                }
                aimer.rectTransform.localScale = ArmedScale;
                ArmedAimer = true;
            }

            //if the player is not in any menus...
            if (!Cursor.visible)
            {
                //raycast under the pointer
                Vector3 playerPos = CharacterMove.CharacterTransform.position;
                RaycastHit[] hits;
                if ((hits = Physics.RaycastAll(CameraFollow.cam.ScreenPointToRay(Input.mousePosition), 25,
                    1 << LayerMask.NameToLayer("ProjectileHit") | 1 << LayerMask.NameToLayer("VehicleProjectileHit"))) != null)
                {
                    //retreiving the object under the pointer that is closest to the player
                    RecognizeWasHit closest = null; float smallest_dist = float.MaxValue;
                    foreach (RaycastHit r in hits)
                    {
                        RecognizeWasHit rH = r.collider.GetComponent<RecognizeWasHit>();
                        //if the collider has a recognized hit component and is connected to a live chaaracter/vehicle
                        if (rH != null && (rH.attatchedVehicle != null || rH.attatchedCharacter != null))
                        {
                            //if the character/vehicle is not the player
                            if ((rH.attatchedCharacter == null || (rH.attatchedCharacter != null && rH.attatchedCharacter != c)) &&
                                (rH.attatchedVehicle == null || (rH.attatchedVehicle != null && rH.attatchedVehicle.driver != c &&
                                !rH.attatchedVehicle.isEmpty)))
                            {
                                //choosing the closest under the cursor as the target
                                if ((r.point - playerPos).magnitude < smallest_dist)
                                {
                                    closest = rH; smallest_dist = (r.point - playerPos).magnitude;
                                }
                            }
                        }
                    }
                    //if there is an object under the pointer
                    if (closest != null)
                    {
                        UnderTarget = true;

                        if (!NPCNAME.activeSelf)
                            NPCNAME.SetActive(true);
                        if (!NPCHP.activeSelf)
                            NPCHP.SetActive(true);

                        //get name
                        string name = "";
                        if (closest.attatchedCharacter != null)
                            name = closest.attatchedCharacter.first_name + " " + closest.attatchedCharacter.last_name +
                                " (" + closest.attatchedCharacter.faction.ToString() + ")";
                        else
                        {
                            if (closest.attatchedVehicle.driver != null)
                                name = closest.attatchedVehicle.driver.first_name + " " +
                                    closest.attatchedVehicle.driver.last_name +
                                " (" + closest.attatchedVehicle.driver.faction.ToString() + ")";
                            else
                                name = closest.attatchedVehicle.passenger.first_name + " " +
                                    closest.attatchedVehicle.passenger.last_name +
                                " (" + closest.attatchedVehicle.passenger.faction.ToString() + ")";
                        }

                        //get hp bar width
                        float newWidth = 0.0f;
                        if (closest.attatchedCharacter != null)
                            newWidth = (float)closest.attatchedCharacter.Health / 
                                (float)closest.attatchedCharacter.MaxHealth * OriginalHPWidth;
                        else
                        {
                            if (closest.attatchedVehicle.driver != null)
                                newWidth = (float)closest.attatchedVehicle.driver.Health /
                                 (float)closest.attatchedVehicle.driver.MaxHealth * OriginalHPWidth;
                            else
                                newWidth = (float)closest.attatchedVehicle.passenger.Health /
                                 (float)closest.attatchedVehicle.passenger.MaxHealth * OriginalHPWidth;
                        }

                        //get hp text
                        string health = "";
                        if (closest.attatchedCharacter != null)
                            health = closest.attatchedCharacter.Health.ToString();
                        else
                        {
                            if (closest.attatchedVehicle.driver != null)
                                health = closest.attatchedVehicle.driver.Health.ToString();
                            else
                                health = closest.attatchedVehicle.passenger.Health.ToString();
                        }

                        //assign name, hp width, and hp amount
                        NPCName.text = name;
                        NPCHealthBar.sizeDelta = new Vector2(newWidth, NPCHealthBar.sizeDelta.y);
                        NPCHealth.text = health;
                    }
                }
            }

            //if no NPC is under the mouse..
            if (!UnderTarget)
            {
                if (NPCNAME.activeSelf)
                    NPCNAME.SetActive(false);
                if (NPCHP.activeSelf)
                    NPCHP.SetActive(false);
            }

            //if armed...
            if (ArmedAimer)
            {
                if (UnderTarget)
                    aimer.color = Color.red;
                else
                    aimer.color = Color.white;
            }

            //assign aimer object to the same position as the pointer
            float scalex = parentRect.sizeDelta.x / Screen.width;
            float scaley = parentRect.sizeDelta.y / Screen.height;
            aimer.rectTransform.anchoredPosition3D = 
                new Vector3(scalex * Input.mousePosition.x, scaley * Input.mousePosition.y, 0);
        }
    }

    /// <summary>
    /// bleh
    /// </summary>
    /// <param name="duration"></param>
    public static void AimerPropogate(float duration)
    {
        Instance.StartCoroutine(Instance.propAimer(duration));
    }

    float timeOfLastCall = 0.0f;
    IEnumerator propAimer(float duration)
    {
        if (duration < 0.5f)
            duration = 0.1f;

        if (Time.realtimeSinceStartup - timeOfLastCall < duration)
            yield break;

        float uptime = duration * 0.5f;

        timeOfLastCall = Time.realtimeSinceStartup;

        for (int i = 0; i < 10; i++)
        {
            aimer.rectTransform.localScale *= 1.08f;
            yield return new WaitForSeconds(uptime / 10);
        }

        aimer.rectTransform.localScale = ArmedScale;
    }
}
