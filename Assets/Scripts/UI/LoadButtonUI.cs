﻿using UnityEngine;
using System.Collections;

public class LoadButtonUI : MonoBehaviour {

    [System.NonSerialized]
    public LoadGameUI parentRef;

	// Use this for initialization
	void Start ()
    {
        parentRef = transform.parent.parent.parent.GetComponent<LoadGameUI>();
	}

    public void Action()
    {
        parentRef.CurrentlySelected = gameObject;
    }
}
