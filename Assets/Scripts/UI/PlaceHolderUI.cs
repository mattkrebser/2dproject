﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PlaceHolderUI : MonoBehaviour {

	public void PlaceHolderVisible(GameObject placeHolder)
    {
        if (String.IsNullOrEmpty(GetComponent<InputField>().text))
        {
            placeHolder.GetComponent<Text>().enabled = true;
        }
    }
}
