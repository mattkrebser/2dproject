﻿using UnityEngine;
using System.Collections;

public class UIMenus : MonoBehaviour {

    public static GameObject escapeMenu;
    public static GameObject LoadoutWindow;
    public static GameObject GunWindow;
    public static GameObject NewSaveWindow;
    public static GameObject ConfirmSaveWindow;
    public static GameObject SaveGameWindow;
    public static GameObject QuestWindow;
    public static GameObject ClothingWindow;
    public static GameObject ControlsWindow;
    public static GameObject MedicWindow;

    public GameObject EscapeMenu;
    public GameObject LoadoutMenu;
    public GameObject GunMenu;
    public GameObject NewSaveMenu;
    public GameObject ConfirmSaveMenu;
    public GameObject SaveGameMenu;
    public GameObject JournalMenu;
    public GameObject ClothingMenu;
    public GameObject ControlsMenu;
    public GameObject MedicMenu;

    public GameObject MainMenu;
    public GameObject LoadMenu;
    public GameObject NewGameMenu;

    public GameObject PlayerDialogue;

    /// <summary>
    /// Returns true if any windows are active
    /// </summary>
    public static bool AnyActive
    {
        get
        {
            return escapeMenu.activeSelf || LoadoutWindow.activeSelf || GunWindow.activeSelf ||
                NewSaveWindow.activeSelf || ConfirmSaveWindow.activeSelf || SaveGameWindow.activeSelf ||
                QuestWindow.activeSelf || ClothingWindow.activeSelf || ControlsWindow.activeSelf || MedicWindow.activeSelf;
        }
    }

    // Use this for initialization
    void Awake()
    {
        escapeMenu = EscapeMenu;
        LoadoutWindow = LoadoutMenu;
        GunWindow = GunMenu;
        NewSaveWindow = NewSaveMenu;
        ConfirmSaveWindow = ConfirmSaveMenu;
        SaveGameWindow = SaveGameMenu;
        QuestWindow = JournalMenu;
        ClothingWindow = ClothingMenu;
        ControlsWindow = ControlsMenu;
        MedicWindow = MedicMenu;

        InputHandler.AddGlobalInput(KeySet.MoveMouse, EnableMainMenu);
    }

    void OnDestroy()
    {
        InputHandler.RemoveGlobalInput(KeySet.MoveMouse, EnableMainMenu);
    }

    void EnableMainMenu()
    {
        timeOfLastMouseMove = Time.realtimeSinceStartup;
        if (GameSettings.LobbyRunning && !LoadMenu.activeSelf && !NewGameMenu.activeSelf)
        {
            if (!MainMenu.activeSelf)
                MainMenu.SetActive(true);
        }
    }

    /// <summary>
    /// Set all UIMenus to not active
    /// </summary>
    public static void SetAllNotActive()
    {
        escapeMenu.gameObject.SetActive(false);
        LoadoutWindow.gameObject.SetActive(false);
        GunWindow.gameObject.SetActive(false);
        NewSaveWindow.gameObject.SetActive(false);
        ConfirmSaveWindow.gameObject.SetActive(false);
        SaveGameWindow.gameObject.SetActive(false);
        QuestWindow.gameObject.SetActive(false);
        ClothingWindow.gameObject.SetActive(false);
        ControlsWindow.gameObject.SetActive(false);
        MedicWindow.gameObject.SetActive(false);
    }

    float timeOfLastMouseMove = 0.0f;
    void Update()
    {
        if (GameSettings.LobbyRunning)
        {
            if (Time.realtimeSinceStartup - timeOfLastMouseMove > 15.0f)
            {
                MainMenu.SetActive(false);
            }
            else
            {
                if (!LoadMenu.activeSelf && !NewGameMenu.activeSelf)
                    MainMenu.SetActive(true);
            }
        }
        else
        {
            if (MainMenu.activeSelf)
            {
                MainMenu.SetActive(false);
            }
            if (!PlayerDialogue.activeSelf)
            {
                PlayerDialogue.SetActive(true);
            }
        }
    }
}
