﻿using UnityEngine;
using System.Collections;

public class BuyMedPacksUI : MonoBehaviour {

    public void BuyMeds(int amount)
    {
        //ifthe player object is not null
        if (CharacterMove.CharacterTransform != null)
        {
            CharacterData player = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
            //buy a medpack a bunch of times
            for (int i = 0; i < amount; i++)
            {
                if (player.Money >= 10)
                {
                    player.NumHealthPacks++;
                    player.Money -= 10;
                }
            }
        }
    }
}
