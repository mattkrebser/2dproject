﻿using UnityEngine;
using System.Collections;

public class EnforeUIScale : MonoBehaviour {

    public Vector3 ScaleToEnforce = new Vector3(1, 1, 1);
    public int WaitFramesForEnforce = 5;

	void OnEnable()
    {
        StartCoroutine(ScaleEnforce());
    }

    public void ScaleEnfore()
    {
        StartCoroutine(ScaleEnforce());
    }

    IEnumerator ScaleEnforce()
    {
        for (int i = 0; i < WaitFramesForEnforce; i++)
            yield return 0;
        foreach(RectTransform t in GetComponentsInChildren<RectTransform>(true))
        {
            if (t.parent == transform)
                t.localScale = ScaleToEnforce;
        }
    }
}
