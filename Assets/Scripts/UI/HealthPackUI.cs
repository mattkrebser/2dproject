﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthPackUI : MonoBehaviour {


    public Text NumPacks;
    public RectTransform sub1, sub2, sub3;
    public Image MyImage;

    private CharacterData Player;

    // Update is called once per frame
    void Update()
    {
        //if not in a game...
        if (!GameSettings.IsInGame)
        {
            //disbale health UI
            if (sub1.gameObject.activeSelf)
            {
                sub1.gameObject.SetActive(false);
                sub2.gameObject.SetActive(false);
                sub3.gameObject.SetActive(false);
                MyImage.enabled = false;
            }

            Player = null;
        }
        else
        {
            //enable health UI
            if (!sub1.gameObject.activeSelf)
            {
                sub1.gameObject.SetActive(true);
                sub2.gameObject.SetActive(true);
                sub3.gameObject.SetActive(true);
                MyImage.enabled = true;
            }

            //find player
            if (Player == null && CharacterMove.CharacterTransform != null)
                Player = CharacterMove.CharacterTransform.GetComponent<CharacterData>();

            //Set health pack visual
            if(Player != null)
            {
                NumPacks.text = Player.NumHealthPacks.ToString();
            }
        }
    }
}
