﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VehicleHealthBarUI : MonoBehaviour
{

    public Text HealthText;
    public RectTransform HealthRect;
    public RectTransform HP_Icon;
    public Image MyImage;
    /// <summary>
    /// The size of the Health bar at 100% health
    /// </summary>
    public float HealthNormalSize;

    private CharacterData Player;

    // Use this for initialization
    void Start()
    {
        HealthNormalSize = HealthRect.sizeDelta.y;
    }

    // Update is called once per frame
    void Update()
    {
        //if not in a game...
        if (!GameSettings.IsInGame)
        {
            //disbale health UI
            if (HealthRect.gameObject.activeSelf)
            {
                HP_Icon.gameObject.SetActive(false);
                HealthRect.gameObject.SetActive(false);
                HealthText.gameObject.SetActive(false);
                MyImage.enabled = false;
            }

            Player = null;
        }
        else
        {
            //find player
            if (Player == null && CharacterMove.CharacterTransform != null)
                Player = CharacterMove.CharacterTransform.GetComponent<CharacterData>();

            //Set health visuals
            if (Player != null)
            {
                //if not in a vehicle...
                if (Player.InVehicle == null)
                {
                    //disbale health UI
                    if (HealthRect.gameObject.activeSelf)
                    {
                        HP_Icon.gameObject.SetActive(false);
                        HealthRect.gameObject.SetActive(false);
                        HealthText.gameObject.SetActive(false);
                        MyImage.enabled = false;
                    }
                    return;
                }
                else
                {
                    //enable health UI
                    if (!HealthRect.gameObject.activeSelf)
                    {
                        HP_Icon.gameObject.SetActive(true);
                        HealthRect.gameObject.SetActive(true);
                        HealthText.gameObject.SetActive(true);
                        MyImage.enabled = true;
                    }
                }

                float PlayerHealthPercentage = Player.InVehicle.Health / (float)Player.InVehicle.MaxHealth;
                HealthRect.sizeDelta = new Vector2(HealthRect.sizeDelta.x, HealthNormalSize * PlayerHealthPercentage);
                HealthText.text = Player.InVehicle.Health < 0 ? 0.ToString() : Player.InVehicle.Health.ToString();
            }
        }
    }
}
