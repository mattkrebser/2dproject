﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class LoadOutSelector : MonoBehaviour {

    public bool EditingRightWeapon = false;

    public string EditingWeapon = "";
    public int EditingSlot = -1;

    public Sprite Bazooka;
    public Sprite Grenade;
    public Sprite M16;
    public Sprite SMG;
    public Sprite Shotgun;
    public Sprite Magnum;
    public Sprite Handgun;
    public Sprite UZI;
    public Sprite UMP;

    public List<Image> Left = new List<Image>();
    public List<Image> Right = new List<Image>();

    void OnEnable()
    {
        if (EditingSlot != -1)
        {
            PlayerDat p = CharacterMove.CharacterTransform.GetComponent<PlayerDat>();
            string WeaponName = null;
            if (EditingRightWeapon)
            {
                if (EditingWeapon == "")
                {
                    p.playerInfo2.RWeaponsLoadOut[EditingSlot] = null;
                }
                else if (EditingWeapon.Contains("Grenade"))
                {
                    WeaponName = "Weapons-" + EditingWeapon;
                    //un-comment these to disable allowing all weapons.
                    //if (p.OwnsWeapon(WeaponName, EditingRightWeapon))
                        p.playerInfo2.RWeaponsLoadOut[EditingSlot] = WeaponName;
                }
                else
                {
                    WeaponName = "Weapons-" + EditingWeapon + "-R";
                    //if (p.OwnsWeapon(WeaponName, EditingRightWeapon))
                        p.playerInfo2.RWeaponsLoadOut[EditingSlot] = WeaponName;
                }
            }
            else
            {
                if (EditingWeapon == "")
                {
                    p.playerInfo2.LWeaponsLoadOut[EditingSlot] = null;
                }
                else if (EditingWeapon.Contains("Grenade"))
                {
                    WeaponName = "Weapons-" + EditingWeapon;
                    //if (p.OwnsWeapon(WeaponName, EditingRightWeapon))
                        p.playerInfo2.LWeaponsLoadOut[EditingSlot] = "Weapons-" + EditingWeapon;
                }
                else
                {
                    WeaponName = "Weapons-" + EditingWeapon + "-L";
                    //if (p.OwnsWeapon(WeaponName, EditingRightWeapon))
                        p.playerInfo2.LWeaponsLoadOut[EditingSlot] = WeaponName;
                }
            }
        }

        RePopulateLoadout();
        EditingWeapon = "";
        EditingRightWeapon = false;
        EditingSlot = -1;
    }

    void RePopulateLoadout()
    {
        PlayerDat p = CharacterMove.CharacterTransform.GetComponent<PlayerDat>();

        for(int i = 0; i < Left.Count; i++)
        {
            Left[i].sprite = GetTileSprite(p.playerInfo2.LWeaponsLoadOut[i]);
            if (Left[i].sprite == null)
            {
                Left[i].color = new Color(0,0,0,0);
            }
            else
            {
                Left[i].color = Color.white;
            }
        }
        for (int i = 0; i < Right.Count; i++)
        {
            Right[i].sprite = GetTileSprite(p.playerInfo2.RWeaponsLoadOut[i]);
            if (Right[i].sprite == null)
            {
                Right[i].color = new Color(0, 0, 0, 0);
            }
            else
            {
                Right[i].color = Color.white;
            }
        }
    }

    Sprite GetTileSprite(string WeaponName)
    {
        if (string.IsNullOrEmpty(WeaponName))
            return null;

        if (WeaponName.Contains("Bazooka"))
        {
            return Bazooka;
        }
        else if (WeaponName.Contains("Grenade"))
        {
            return Grenade;
        }
        else if (WeaponName.Contains("M16"))
        {
            return M16;
        }
        else if (WeaponName.Contains("SMG"))
        {
            return SMG;
        }
        else if (WeaponName.Contains("Shotgun"))
        {
            return Shotgun;
        }
        else if (WeaponName.Contains("Handgun"))
        {
            return Handgun;
        }
        else if (WeaponName.Contains("Magnum"))
        {
            return Magnum;
        }
        else if (WeaponName.Contains("UMP"))
        {
            return UMP;
        }
        else if (WeaponName.Contains("UZI"))
        {
            return UZI;
        }

        return null;
    }

    public void SetSlot(int num)
    {
        EditingSlot = num;
    }

    void OnDisable()
    {
        EditingWeapon = "";
    }

	public void SetRL(bool isRight)
    {
        EditingRightWeapon = isRight;
    }
}
