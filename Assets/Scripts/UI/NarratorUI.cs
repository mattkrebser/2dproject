﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NarratorUI : MonoBehaviour {

    public Text BigT;
    public GameObject big1;
    public GameObject bigTri;
    public GameObject littleTri;
    public GameObject little1;
    public Text littleT;
    public Image Satan;
    public GameObject SaucyText;

    private static NarratorUI Instance;

    void Start()
    {
        Instance = this;
    }

    void Update()
    {
        if (!GameSettings.IsInGame || CharacterMove.CharacterTransform == null)
        {
            if (Satan.enabled)
            {
                Satan.enabled = false;
                big1.SetActive(false);
                bigTri.SetActive(false);
                little1.SetActive(false);
                littleTri.SetActive(false);
                SaucyText.SetActive(false);
            }
        }
        else
        {
            if (!Satan.enabled)
                Satan.enabled = true;
        }
    }

    public static void NarratorBigSaySingle(Dialogue d)
    {
        Instance.big1.SetActive(true);
        Instance.bigTri.SetActive(true);
        Instance.SaucyText.SetActive(true);
        Instance.BigT.text = Dialogue.ModifyString(d.ThisDialogue[0]);
    }

    public static void HideAllBig()
    {
        Instance.big1.SetActive(false);
        Instance.bigTri.SetActive(false);
        Instance.SaucyText.SetActive(false);
    }

    public static void HideAllSmall()
    {
        Instance.littleTri.SetActive(false);
        Instance.little1.SetActive(false);
    }

    public static void NarratorLittleSaySingle(Dialogue d)
    {
        Instance.little1.SetActive(true);
        Instance.littleTri.SetActive(true);
        Instance.littleT.text = Dialogue.ModifyString(d.ThisDialogue[0]);
    }
}
