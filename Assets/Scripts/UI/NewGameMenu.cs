﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NewGameMenu : MonoBehaviour {

    public InputField FirstName;
    public InputField LastName;

    public bool IsMale = false;

    public void SetMale(bool isMale)
    {
        IsMale = isMale;
    }

    public void Reset()
    {
        FirstName.text = "";
        LastName.text = "";
    }
}
