﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ClothesUI : MonoBehaviour {

    Dictionary<Clothes, List<string>> clothes;

    //image icons to display clothing items in UI
    public Image Hair;
    int hair_i;

    public Image Hat;
    int hat_i;

    public Image Glasses;
    int glasses_i;

    public Image Shoes;
    int shoes_i;

    public Image Lipstick;
    int lipstick_i;

    public Image Earrings;
    int earrings_i;

    public Image EyeColor;
    int eyecolor_i;

    public Image Nose;
    int nose_i;

    public Image Face;
    int face_i;

    public Image Arms;
    int arms_i;

    public Image Shirt;
    int shirt_i;

    public Image Legs;
    int legs_i;

    void OnEnable()
    {
        Draw();
    }

    void Draw()
    {
        if (clothes == null)
            clothes = PeopleData.GetAllClothes();

        if (CharacterMove.CharacterTransform != null)
        {
            CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
            //initialize clothing thumbnails
            Hair.sprite = CharacterData.GetClothingSprite(c.Hair);
            setPostion(Clothes.hair, c.Hair, ref hair_i);
            Hair.color = Hair.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Hat.sprite = CharacterData.GetClothingSprite(c.Hat);
            setPostion(Clothes.hat, c.Hat, ref hat_i);
            Hat.color = Hat.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Glasses.sprite = CharacterData.GetClothingSprite(c.Glasses);
            setPostion(Clothes.glasses, c.Glasses, ref glasses_i);
            Glasses.color = Glasses.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Shoes.sprite = CharacterData.GetClothingSprite(c.Shoes);
            setPostion(Clothes.shoes, c.Shoes, ref shoes_i);
            Shoes.color = Shoes.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Lipstick.sprite = CharacterData.GetClothingSprite(c.Lipstick);
            setPostion(Clothes.lipstick, c.Lipstick, ref lipstick_i);
            Lipstick.color = Lipstick.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Earrings.sprite = CharacterData.GetClothingSprite(c.Earings);
            setPostion(Clothes.earrings, c.Earings, ref earrings_i);
            Earrings.color = Earrings.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            EyeColor.sprite = CharacterData.GetClothingSprite(c.Eyecolor);
            setPostion(Clothes.eye_color, c.Eyecolor, ref eyecolor_i);
            EyeColor.color = EyeColor.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Nose.sprite = CharacterData.GetClothingSprite(c.Nose);
            setPostion(Clothes.nose, c.Nose, ref nose_i);
            Nose.color = Nose.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Face.sprite = CharacterData.GetClothingSprite(c.Facecolor);
            setPostion(Clothes.face, c.Facecolor, ref face_i);
            Face.color = Face.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Arms.sprite = CharacterData.GetClothingSprite(c.Arms);
            setPostion(Clothes.arms, c.Arms, ref arms_i);
            Arms.color = Arms.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Shirt.sprite = CharacterData.GetClothingSprite(c.Shirt);
            setPostion(Clothes.shirt, c.Shirt, ref shirt_i);
            Shirt.color = Shirt.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);

            Legs.sprite = CharacterData.GetClothingSprite(c.Pants);
            setPostion(Clothes.legs, c.Pants, ref legs_i);
            Legs.color = Legs.sprite == null ? new Color(0, 0, 0, 0) : new Color(1, 1, 1, 1);
        }
    }

    /// <summary>
    /// sets the position of the input 'i' to the position of a matched string 'item'
    /// If the input string 'item' is not found, then the input intiger value 'i' is not modified
    /// </summary>
    /// <param name="type"></param>
    /// <param name="item"></param>
    /// <param name="i"></param>
    void setPostion(Clothes type, string item, ref int i)
    {
        if (item == null)
            item = "";
        if (clothes == null) return;
        if (!clothes.ContainsKey(type)) return;

        List<string> items = clothes[type];

        if (items == null) return;

        for (int n = 0; n < items.Count; n++)
        {
            if (items[n] == item)
                i = n;
        }
    }

    /// <summary>
    /// Add the delta to the value. If less than min, set to max. If greater than max, set to min.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="delta"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    void ClamedIncrement(ref int value, int delta, int min, int max)
    {
        value += delta;

        if (value < min)
            value = max;
        if (value > max)
            value = min;
    }

    public void ChangeHat(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref hat_i, 1, 0, clothes[Clothes.hat].Count - 1);
        else ClamedIncrement(ref hat_i, -1, 0, clothes[Clothes.hat].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Hat = clothes[Clothes.hat][hat_i];

        Draw();
    }
    public void ChangeHair(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref hair_i, 1, 0, clothes[Clothes.hair].Count - 1);
        else ClamedIncrement(ref hair_i, -1, 0, clothes[Clothes.hair].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Hair = clothes[Clothes.hair][hair_i];
        Draw();
    }
    public void ChangeGlasses(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref glasses_i, 1, 0, clothes[Clothes.glasses].Count - 1);
        else ClamedIncrement(ref glasses_i, -1, 0, clothes[Clothes.glasses].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Glasses = clothes[Clothes.glasses][glasses_i];
        Draw();
    }
    public void ChangeShoes(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref shoes_i, 1, 0, clothes[Clothes.shoes].Count - 1);
        else ClamedIncrement(ref shoes_i, -1, 0, clothes[Clothes.shoes].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Shoes = clothes[Clothes.shoes][shoes_i];
        Draw();
    }
    public void ChangeLipstick(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref lipstick_i, 1, 0, clothes[Clothes.lipstick].Count - 1);
        else ClamedIncrement(ref lipstick_i, -1, 0, clothes[Clothes.lipstick].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Lipstick = clothes[Clothes.lipstick][lipstick_i];
        Draw();
    }
    public void ChangeEarrings(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref earrings_i, 1, 0, clothes[Clothes.earrings].Count - 1);
        else ClamedIncrement(ref earrings_i, -1, 0, clothes[Clothes.earrings].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Earings = clothes[Clothes.earrings][earrings_i];
        Draw();
    }
    public void ChangeEyecolor(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref eyecolor_i, 1, 0, clothes[Clothes.eye_color].Count - 1);
        else ClamedIncrement(ref eyecolor_i, -1, 0, clothes[Clothes.eye_color].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Eyecolor = clothes[Clothes.eye_color][eyecolor_i];
        Draw();
    }
    public void ChangeNose(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref nose_i, 1, 0, clothes[Clothes.nose].Count - 1);
        else ClamedIncrement(ref nose_i, -1, 0, clothes[Clothes.nose].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Nose = clothes[Clothes.nose][nose_i];
        Draw();
    }
    public void ChangeFace(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref face_i, 1, 0, clothes[Clothes.face].Count - 1);
        else ClamedIncrement(ref face_i, -1, 0, clothes[Clothes.face].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Facecolor = clothes[Clothes.face][face_i];
        Draw();
    }
    public void ChangeArms(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref arms_i, 1, 0, clothes[Clothes.arms].Count - 1);
        else ClamedIncrement(ref arms_i, -1, 0, clothes[Clothes.arms].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Arms = clothes[Clothes.arms][arms_i];
        Draw();
    }
    public void ChangeShirt(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref shirt_i, 1, 0, clothes[Clothes.shirt].Count - 1);
        else ClamedIncrement(ref shirt_i, -1, 0, clothes[Clothes.shirt].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Shirt = clothes[Clothes.shirt][shirt_i];
        Draw();
    }
    public void ChangeLegs(bool right)
    {
        //if no player
        if (CharacterMove.CharacterTransform == null)
            return;

        //increment
        if (right) ClamedIncrement(ref legs_i, 1, 0, clothes[Clothes.legs].Count - 1);
        else ClamedIncrement(ref legs_i, -1, 0, clothes[Clothes.legs].Count - 1);

        CharacterData c = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        c.Pants = clothes[Clothes.legs][legs_i];
        Draw();
    }
}
