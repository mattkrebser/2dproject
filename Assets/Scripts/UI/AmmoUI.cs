﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class AmmoUI : MonoBehaviour {

    public Text Ammo;
    /// <summary>
    /// Ammo for right weapon?
    /// </summary>
    public bool isRight = false;

    public RectTransform sub1, sub2, sub3, sub4;
    public Image ParentImage;

    private CharacterData Player;
    private PlayerInfo2 pInfo;
    private PlayerDat pdat;
	
	// Update is called once per frame
	void Update () {
        //if not in a game...
        if (!GameSettings.IsInGame)
        {
            //disbale health UI
            if (sub1.gameObject.activeSelf)
            {
                sub1.gameObject.SetActive(false);
                sub2.gameObject.SetActive(false);
                sub3.gameObject.SetActive(false);
                sub4.gameObject.SetActive(false);
                ParentImage.enabled = false;
            }

            Player = null;
            pInfo = null;
            pdat = null;
        }
        else
        {
            //disbale health UI
            if (!sub1.gameObject.activeSelf)
            {
                sub1.gameObject.SetActive(true);
                sub2.gameObject.SetActive(true);
                sub3.gameObject.SetActive(true);
                sub4.gameObject.SetActive(true);
                ParentImage.enabled = true;
            }

            //find player
            if ((Player == null || pInfo == null) && CharacterMove.CharacterTransform != null)
            {
                Player = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
                if (Player != null)
                {
                    pdat = Player.GetComponent<PlayerDat>();
                    pInfo = pdat.playerInfo2;
                }
            }

            //non null pInfo
            if (pInfo != null && Player != null)
            {
                if (!isRight)
                {
                    if (!System.String.IsNullOrEmpty(Player.L_weapon))
                        Ammo.text = AmmoText(Player.L_weapon, false, pdat);
                    else
                        Ammo.text = "0";
                }
                else
                {
                    if (!System.String.IsNullOrEmpty(Player.R_weapon))
                        Ammo.text = AmmoText(Player.R_weapon, false, pdat);
                    else
                        Ammo.text = "0";
                }
            }
        }
    }

    private string AmmoText(string weapon, bool isRight, PlayerDat pd)
    {
        int ammo = pd.GetWeaponAmmo(weapon);
       ammo = ammo > 9999 ? 9999 : ammo;
        return ammo.ToString();
    }
}
