﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GunsMenu : MonoBehaviour {

    public Text Left;
    public Text Right;
    public Text NotOwned;

    public Image DisplaySprite;
    public Text GunName;

    public RectTransform RateOfFire;
    public RectTransform Damage;
    public RectTransform Range;

    public float OriginalPositionX;

    [System.NonSerialized]
    public string Weapon;

    public LoadOutSelector LoadOutEditor;

    void OnEnable()
    {
        Weapon = "";
        DisplaySprite.sprite = null;
        DisplaySprite.color = new Color(0, 0, 0, 0);
        Left.enabled = false;
        Right.enabled = false;
        NotOwned.enabled = false;
    }

    public void SetLoadOutWeapon()
    {
        LoadOutEditor.EditingWeapon = Weapon;
    }

    public void SetDisplay(Sprite setWithThis)
    {
        DisplaySprite.sprite = setWithThis;
        DisplaySprite.color = Color.white;

        if (LoadOutEditor.EditingRightWeapon)
            Right.enabled = true;
        else
            Left.enabled = true;

        if (setWithThis.name.Contains("Bazooka"))
        {
            Weapon = setWithThis.name;
            GunName.text = Weapon;
            float rof = (1 - (Constants.Bazooka_fireRate / 2)) * 320;
            float dam = (Constants.Bazooka_Damage / 300) * 320;
            RateOfFire.anchoredPosition3D = new Vector3(OriginalPositionX + rof, RateOfFire.anchoredPosition3D.y);
            Damage.anchoredPosition3D = new Vector3(OriginalPositionX + dam, Damage.anchoredPosition3D.y);
            Range.anchoredPosition3D = new Vector3(OriginalPositionX - 300, Range.anchoredPosition3D.y);
        }
        else if (setWithThis.name.Contains("Grenade"))
        {
            float rof = (1 - (Constants.Grenade_fireRate / 2)) * 320;
            float dam = (Constants.Grenade_Damage / 300) * 320;
            Weapon = setWithThis.name;
            GunName.text = Weapon;
            RateOfFire.anchoredPosition3D = new Vector3(OriginalPositionX + rof, RateOfFire.anchoredPosition3D.y);
            Damage.anchoredPosition3D = new Vector3(OriginalPositionX + dam, Damage.anchoredPosition3D.y);
            Range.anchoredPosition3D = new Vector3(OriginalPositionX - 300, Range.anchoredPosition3D.y);
        }
        else if (setWithThis.name.Contains("M16"))
        {
            float rof = (1 - (Constants.M16_fireRate / 2)) * 320;
            float dam = (Constants.M16_Damage / 300) * 320;
            float range = (Constants.M16_Range / 4) * 320;
            Weapon = setWithThis.name;
            GunName.text = Weapon;
            RateOfFire.anchoredPosition3D = new Vector3(OriginalPositionX + rof, RateOfFire.anchoredPosition3D.y);
            Damage.anchoredPosition3D = new Vector3(OriginalPositionX + dam, Damage.anchoredPosition3D.y);
            Range.anchoredPosition3D = new Vector3(OriginalPositionX + range, Range.anchoredPosition3D.y);
        }
        else if (setWithThis.name.Contains("SMG"))
        {
            float rof = (1 - (Constants.SMG_fireRate / 2)) * 320;
            float dam = (Constants.SMG_Damage / 300) * 320;
            float range = (Constants.SMG_Range / 4) * 320;
            Weapon = setWithThis.name;
            GunName.text = Weapon;
            RateOfFire.anchoredPosition3D = new Vector3(OriginalPositionX + rof, RateOfFire.anchoredPosition3D.y);
            Damage.anchoredPosition3D = new Vector3(OriginalPositionX + dam, Damage.anchoredPosition3D.y);
            Range.anchoredPosition3D = new Vector3(OriginalPositionX + range, Range.anchoredPosition3D.y);
        }
        else if (setWithThis.name.Contains("Shotgun"))
        {
            float rof = (1 - (Constants.Shotgun_fireRate / 2)) * 320;
            float dam = (Constants.Shotgun_Damage / 300) * 320;
            float range = (Constants.Shotgun_Range / 4) * 320;
            Weapon = setWithThis.name;
            GunName.text = Weapon;
            RateOfFire.anchoredPosition3D = new Vector3(OriginalPositionX + rof, RateOfFire.anchoredPosition3D.y);
            Damage.anchoredPosition3D = new Vector3(OriginalPositionX + dam, Damage.anchoredPosition3D.y);
            Range.anchoredPosition3D = new Vector3(OriginalPositionX + range, Range.anchoredPosition3D.y);
        }
        else if (setWithThis.name.Contains("Handgun"))
        {
            float rof = (1 - (Constants.Handgun_fireRate / 2)) * 320;
            float dam = (Constants.Handgun_Damage / 300) * 320;
            float range = (Constants.Handgun_Range / 4) * 320;
            Weapon = setWithThis.name;
            GunName.text = Weapon;
            RateOfFire.anchoredPosition3D = new Vector3(OriginalPositionX + rof, RateOfFire.anchoredPosition3D.y);
            Damage.anchoredPosition3D = new Vector3(OriginalPositionX + dam, Damage.anchoredPosition3D.y);
            Range.anchoredPosition3D = new Vector3(OriginalPositionX + range, Range.anchoredPosition3D.y);
        }
        else if (setWithThis.name.Contains("Magnum"))
        {
            float rof = (1 - (Constants.Magnum_fireRate / 2)) * 320;
            float dam = (Constants.Magnum_Damage / 300) * 320;
            float range = (Constants.Magnum_Range / 4) * 320;
            Weapon = setWithThis.name;
            GunName.text = Weapon;
            RateOfFire.anchoredPosition3D = new Vector3(OriginalPositionX + rof, RateOfFire.anchoredPosition3D.y);
            Damage.anchoredPosition3D = new Vector3(OriginalPositionX + dam, Damage.anchoredPosition3D.y);
            Range.anchoredPosition3D = new Vector3(OriginalPositionX + range, Range.anchoredPosition3D.y);
        }
        else if (setWithThis.name.Contains("UMP"))
        {
            float rof = (1 - (Constants.UMP_fireRate / 2)) * 320;
            float dam = (Constants.UMP_Damage / 300) * 320;
            float range = (Constants.UMP_Range / 4) * 320;
            Weapon = setWithThis.name;
            GunName.text = Weapon;
            RateOfFire.anchoredPosition3D = new Vector3(OriginalPositionX + rof, RateOfFire.anchoredPosition3D.y);
            Damage.anchoredPosition3D = new Vector3(OriginalPositionX + dam, Damage.anchoredPosition3D.y);
            Range.anchoredPosition3D = new Vector3(OriginalPositionX + range, Range.anchoredPosition3D.y);
        }
        else if (setWithThis.name.Contains("UZI"))
        {
            float rof = (1 - (Constants.UZI_fireRate / 2)) * 320;
            float dam = (Constants.UZI_Damage / 300) * 320;
            float range = (Constants.UZI_Range / 4) * 320;
            Weapon = setWithThis.name;
            GunName.text = Weapon;
            RateOfFire.anchoredPosition3D = new Vector3(OriginalPositionX + rof, RateOfFire.anchoredPosition3D.y);
            Damage.anchoredPosition3D = new Vector3(OriginalPositionX + dam, Damage.anchoredPosition3D.y);
            Range.anchoredPosition3D = new Vector3(OriginalPositionX + range, Range.anchoredPosition3D.y);
        }

        if (CharacterMove.CharacterTransform.GetComponent<PlayerDat>().OwnsWeapon(Weapon, LoadOutEditor.EditingRightWeapon))
            NotOwned.enabled = false;
        else
            NotOwned.enabled = true;
    }
}
