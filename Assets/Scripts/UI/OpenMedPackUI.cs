﻿using UnityEngine;
using System.Collections;

public class OpenMedPackUI : MonoBehaviour {

    /// <summary>
    /// Opens or closes the menu depending on its current state
    /// </summary>
    public void OpenClose()
    {
        if (UIMenus.MedicWindow.activeSelf)
        {
            UIMenus.MedicWindow.SetActive(false);
            return;
        }

        UIMenus.SetAllNotActive();
        UIMenus.MedicWindow.SetActive(true);
    }

    public void Close()
    {
        UIMenus.MedicWindow.SetActive(false);
    }
}
