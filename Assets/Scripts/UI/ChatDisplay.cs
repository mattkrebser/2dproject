﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ChatDisplay : MonoBehaviour
{
    public Text t1;
    public Text t2;
    public Text t3;
    public Text t4;

    public GameObject ul1, ul2, ul3;

    float[] times = new float[4];
    string[] text = new string[4];

    static ChatDisplay instance;

    void Start() { instance = this; CoroutineHelper.RunCoroutine(chat_update()); }

    /// <summary>
    /// Updates the chat display UI
    /// </summary>
    /// <returns></returns>
    IEnumerator chat_update()
    {
        yield return 0;

        while (true)
        {
            if (Time.realtimeSinceStartup - times[3] > 4f)
            {
                t4.text = null;
                times[3] = 0; text[3] = null;
            }
            else
                t4.text = text[3];

            if (Time.realtimeSinceStartup - times[2] > 4f)
            {
                t3.text = null;
                times[2] = 0; text[2] = null;
            }
            else
                t3.text = text[2];

            if (Time.realtimeSinceStartup - times[1] > 4f)
            {
                t2.text = null;
                times[1] = 0; text[1] = null;
            }
            else
                t2.text = text[1];

            if (Time.realtimeSinceStartup - times[0] > 4f)
            {
                t1.text = null;
                times[0] = 0; text[0] = null;
            }
            else
                t1.text = text[0];

            if (text[1] != null)
                ul1.SetActive(true);
            else
                ul1.SetActive(false);
            if (text[2] != null)
                ul2.SetActive(true);
            else
                ul2.SetActive(false);
            if (text[3] != null)
                ul3.SetActive(true);
            else
                ul3.SetActive(false);

            yield return new WaitForSeconds(0.25f);
        }
    }

    /// <summary>
    /// Display the input dialogue in the cfhat display
    /// </summary>
    /// <param name="d"></param>
    /// <param name="c"></param>
    public static void DisplayDialogue(Dialogue d, CharacterData c, int dialogue)
    {
        if (instance.text[0] == null)
        {
            string color = PeopleData.FColor(c.faction);
            string cs = "<color=" + color + ">";
            string fac = " (" + c.faction.ToString() + ")";
            instance.text[0] = cs + c.first_name + " " + (c.last_name == null ? "" : c.last_name)
                + fac + ":</color> " + d.ThisDialogue[dialogue];
            instance.times[0] = Time.realtimeSinceStartup;
        }
        else
        {
            string color = PeopleData.FColor(c.faction);
            string cs = "<color=" + color + ">";
            string fac = " (" + c.faction.ToString() + ")";

            //Move elements in both array down
            List<string> s;
            MoveAllOneDown<string>(s = new List<string>(instance.text));
            instance.text = s.ToArray();

            List<float> f;
            MoveAllOneDown<float>(f = new List<float>(instance.times));
            instance.times = f.ToArray();

            instance.text[0] = cs + c.first_name + " " + (c.last_name == null ? "" : c.last_name)
                + fac + ":</color> " + d.ThisDialogue[dialogue];
            instance.times[0] = Time.realtimeSinceStartup;
        }
    }

    /// <summary>
    /// Return the entire dialogue sequence concatenated into one string
    /// </summary>
    /// <param name="d"></param>
    /// <returns></returns>
    static string GetConcatDialogue(Dialogue d)
    {
        string rstring = "";
        if (d.ThisDialogue != null)
        {
            for (int i = 0; i < d.ThisDialogue.Count; i++)
            {
                rstring += d.ThisDialogue[i] + " ";
            }
        }
        return rstring;
    }

    /// <summary>
    /// Moves all elements down, if full the last element is dropped
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    static void MoveAllOneDown<T>(List<T> list)
    {
        if (list != null)
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (i == 0)
                    list[i] = default(T);
                else
                    list[i] = list[i - 1];
            }
    }
}
