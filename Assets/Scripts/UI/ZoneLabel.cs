﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ZoneLabel : MonoBehaviour
{
    public Text text;
    public Image line;
    public Text targetZone;
    public Text pressE;

    /// <summary>
    /// A flag used to show that the player is currently able to interact with some object in the game
    /// </summary>
    public static bool PlayerCanInteract = false;

	// Update is called once per frame
	void Update ()
    {
        if (!GameSettings.IsInGame)
        {
            text.text = null;
            if (line.enabled)
                line.enabled = false;
            if (targetZone.gameObject.activeSelf)
            {
                targetZone.gameObject.SetActive(false);
            }
            if (pressE.gameObject.activeSelf)
                pressE.gameObject.SetActive(false);
        }
        else
        {
            if (!line.enabled)
                line.enabled = true;

            if (SignAction.CurrentSign != "" && SignAction.CurrentSign != null)
            {
                if (!targetZone.gameObject.activeSelf)
                {
                    targetZone.gameObject.SetActive(true);
                }
                targetZone.text = SignAction.CurrentSign;
            }
            else if (targetZone.gameObject.activeSelf)
            {
                targetZone.gameObject.SetActive(false);
            }

            if (PlayerCanInteract)
                pressE.gameObject.SetActive(true);
            else
                pressE.gameObject.SetActive(false);

            if (MapData.ActiveMap != null)
                text.text = MapData.ActiveMap.ZoneName;
        }
	}
}
