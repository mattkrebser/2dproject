﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class MoneyUI : MonoBehaviour {

    public Text money;
    public Image BaseImage;

    CharacterData player;

    int prevMoney = 0;

    // Update is called once per frame
    void Update()
    {
        //if not in a game...
        if (!GameSettings.IsInGame || CharacterMove.CharacterTransform == null)
        {
            if (BaseImage.enabled)
            {
                BaseImage.enabled = false;
                money.enabled = false;
            }
        }
        else
        {
            if (!BaseImage.enabled)
            {
                BaseImage.enabled = true;
                money.enabled = true;
            }

            if (player == null)
                player = CharacterMove.CharacterTransform.GetComponent<CharacterData>();

            if (player.Money != prevMoney || String.IsNullOrEmpty(money.text))
            {
                prevMoney = player.Money;
                money.text = "$" + player.Money.ToString();
            }
        }
    }
}
