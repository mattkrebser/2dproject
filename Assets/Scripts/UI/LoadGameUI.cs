﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class LoadGameUI : MonoBehaviour {

    public GameObject Content;
    private GameObject _CurrentlySelected;
    public GameObject CurrentlySelected
    {
        get
        {
            return _CurrentlySelected;
        }
        set
        {
            Button[] buttons = Content.GetComponentsInChildren<Button>();
            if (buttons != null)
                foreach (Button b in buttons)
                {
                    if (b.gameObject == value)
                    {
                        ColorBlock colors = b.colors;
                        colors.normalColor = new Color(0.8f, 0.8f, 0.8f, 1);
                        colors.highlightedColor = new Color(0.8f, 0.8f, 0.8f, 1);
                        colors.pressedColor = new Color(0.8f, 0.8f, 0.8f, 1);
                        b.colors = colors;
                    }
                    else
                    {
                        ColorBlock colors = b.colors;
                        colors.normalColor = new Color(0.96f, 0.96f, 0.96f, 1);
                        colors.highlightedColor = new Color(0.8f, 0.8f, 0.8f, 1);
                        colors.pressedColor = new Color(0.8f, 0.8f, 0.8f, 1);
                        b.colors = colors;
                    }
                }
            _CurrentlySelected = value;
        }
    }

    public GameObject ReturnMenu;

    public bool SaveMode = false;

	// Use this for initialization
	void OnEnable()
    {
        CoroutineHelper.RunCoroutine(AddRoutine());
    }

    IEnumerator AddRoutine()
    {
        //Needs a coroutine because, currently the Content Panel UI can't handle the prefabs
        //being instantaited and parent on to it in the same frame as OnEnable. (A weird Unity bug occurs)
        Content.SetActive(false);
        yield return 0; yield return 0; yield return 0;
        List<string> GameSaves = DiskOp.GetAllSaveNames();
        foreach (string s in GameSaves)
        {
            GameObject newButton = Prefabs.GetNewPrefab("UIPrefabs/LoadButton");
            newButton.transform.SetParent(Content.transform);
            newButton.GetComponentInChildren<Text>(true).text =
                s.Remove(s.IndexOf(DiskOp.SaveDirectory), DiskOp.SaveDirectory.Length + 1);
            ColorBlock colors = newButton.GetComponent<Button>().colors;
            colors.normalColor = new Color(0.96f, 0.96f, 0.96f, 1);
            colors.highlightedColor = new Color(0.96f, 0.96f, 0.96f, 1);
            colors.pressedColor = new Color(0.96f, 0.96f, 0.96f, 1);
            newButton.GetComponent<Button>().colors = colors;
            newButton.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
        yield return 0; yield return 0; yield return 0;
        Content.SetActive(true);
    }
	
	public void LoadGame()
    {
        if (CurrentlySelected != null)
        {
            GameObject.Find("Base").GetComponent<GameSettings>()
                .LoadGame(CurrentlySelected.GetComponentInChildren<Text>().text);
            gameObject.SetActive(false);
        }
    }

    public void SaveGame()
    {
        if (GameSettings.IsInGame)
        {
            if (CurrentlySelected != null)
            {
                GameObject.Find("Base").GetComponent<GameSettings>()
                    .SaveGame(CurrentlySelected.GetComponentInChildren<Text>().text);
                ReturnMenu.SetActive(true);
                gameObject.SetActive(false);
            }
        }
    }

    void OnDisable()
    {
        CoroutineHelper.RunCoroutine(DisableRoutine());
    }

    IEnumerator DisableRoutine()
    {
        Content.SetActive(false);
        yield return 0;
        foreach (LoadButtonUI b in GetComponentsInChildren<LoadButtonUI>(true))
        {
            Destroy(b.gameObject);
        }
        yield return 0;
        Content.SetActive(true);
    }
}
