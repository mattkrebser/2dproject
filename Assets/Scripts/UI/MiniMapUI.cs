﻿using UnityEngine;
using System.Collections;

public class MiniMapUI : MonoBehaviour
{

    public GameObject MapParent;
    public Camera MapCam;

    [Tooltip("X,Y = Camera xy scale, z = CamSize")]
    public Vector3 ZoomSettings1;
    [Tooltip("X,Y = Camera xy scale, z = CamSize")]
    public Vector3 ZoomSettings2;
    [Tooltip("X,Y = Camera xy scale, z = CamSize")]
    public Vector3 ZoomSettings3;

    private readonly float CameraHeight = 36;

    // Update is called once per frame
    void Update ()
    {
        if (!GameSettings.IsInGame || CharacterMove.CharacterTransform == null)
        {
            if (MapCam.enabled)
                MapCam.enabled = false;
        }
        else
        {
            if (!MapCam.enabled)
                MapCam.enabled = true;

            //set the camera position
            SetCamPosition();

            //Set the quest marker positions
            foreach (Quest q in Quest.AllQuests.Values)
            {
                //if the quest has been started and is not completed...
                if (q.Started && !q.IsCompleted && q.QuestAppearsInPlayerJournal)
                {
                    //if there is no pointer, then get one
                    if (q.MiniMapPointer == null)
                    {
                        q.MiniMapPointer = Prefabs.GetNewPrefab("Prefabs/MiniMapPointer");
                        q.MiniMapPointer.transform.SetParent(MapParent.transform);
                        q.MiniMapPointer.transform.localPosition = new Vector3(0, 0, 0);
                        q.MiniMapPointer.transform.localScale = new Vector3(0.1f, 1, 1);
                    }
                    //set rotation
                    SetRotation(q);
                }
                else if (q.MiniMapPointer != null)
                    Destroy(q.MiniMapPointer);
            }
        }
	}

    void SetCamPosition()
    {
        //get variables
        MapData CurrentMap = MapData.ActiveMap;
        Transform follow = CharacterMove.CharacterTransform;

        //resize camera to map
        MapCam.orthographicSize = CamSize(CurrentMap);
        transform.localScale = CamScale(CurrentMap);

        //Assign to same position as the character
        transform.position = Vector3.Lerp(transform.position, new Vector3(follow.position.x, CameraHeight, follow.position.z),
            Time.deltaTime * 10);

        //get camera corners
        Vector3 topleft = MapCam.ViewportToWorldPoint(new Vector3(0, 1, CameraHeight));
        Vector3 botright = MapCam.ViewportToWorldPoint(new Vector3(1, 0, CameraHeight));

        float padding = CurrentMap.MiniMapPadding;
        topleft.x -= padding;
        topleft.z += padding;
        botright.x += padding;
        botright.z -= padding;

        //clamp camera to MapData bounds
        if (topleft.x < CurrentMap.topleft.position.x)
            transform.position = new Vector3(transform.position.x +
                Mathf.Abs(CurrentMap.topleft.position.x - topleft.x),
                CameraHeight, transform.position.z);
        if (topleft.z > CurrentMap.topleft.position.z)
            transform.position = new Vector3(transform.position.x, CameraHeight,
                transform.position.z - Mathf.Abs(topleft.z - CurrentMap.topleft.position.z));
        if (botright.x > CurrentMap.bottomright.position.x)
            transform.position = new Vector3(transform.position.x - 
                Mathf.Abs(botright.x - CurrentMap.bottomright.position.x),
                CameraHeight, transform.position.z);
        if (botright.z < CurrentMap.bottomright.position.z)
            transform.position = new Vector3(transform.position.x, CameraHeight,
                transform.position.z + Mathf.Abs(CurrentMap.bottomright.position.z - botright.z));
    }

    /// <summary>
    /// Set the rotation of the minimap pointer
    /// </summary>
    /// <param name="q"></param>
    void SetRotation(Quest q)
    {
        if (q.Events == null || q.Events.Count == 0)
            return;

        //retreive map locations
        MapData curr_map = MapData.ActiveMap;
        MapData WantedMap = q.Events[q.CurrentEvent].EventLocation;

        //If there is no destination, then hide the map pointer, and return
        if (WantedMap == null)
        {
            if (q.MiniMapPointer.activeSelf)
                q.MiniMapPointer.SetActive(false);
            return;
        }

        //Get the world position for the icon to point towards
        DoorEnter found_door = null;
        Transform point_towards = WantedMap == curr_map ? q.Events[q.CurrentEvent].transform :
            (found_door = DoorGraph.GetNextDoor(curr_map, WantedMap)) == null ? null : found_door.transform;

        //If we have something to point at...
        if (point_towards != null)
        {
            //if the pointer was null already...
            if (!q.MiniMapPointer.activeSelf)
                q.MiniMapPointer.SetActive(true);

            //assign rotation of pointer
            Vector3 direction = (point_towards.position - CharacterMove.CharacterTransform.position).normalized;
            q.MiniMapPointer.transform.rotation = Quaternion.LookRotation(direction == Vector3.zero ?
                Vector3.forward : direction, Vector3.up);
        }

    }

    float CamSize(MapData m)
    {
        if (m.ZoomLevel == OrthoGraphicCameraZoomLevel.far)
        {
            return ZoomSettings1.z;
        }
        else if (m.ZoomLevel == OrthoGraphicCameraZoomLevel.medium)
        {
            return ZoomSettings2.z;
        }
        else
        {
            return ZoomSettings3.z;
        }
    }
    Vector3 CamScale(MapData m)
    {
        if (m.ZoomLevel == OrthoGraphicCameraZoomLevel.far)
        {
            return new Vector3(ZoomSettings1.x, ZoomSettings1.y, 1);
        }
        else if (m.ZoomLevel == OrthoGraphicCameraZoomLevel.medium)
        {
            return new Vector3(ZoomSettings2.x, ZoomSettings2.y, 1);
        }
        else
        {
            return new Vector3(ZoomSettings3.x, ZoomSettings3.y, 1);
        }
    }
}

public enum OrthoGraphicCameraZoomLevel
{
    far,
    medium,
    close
}