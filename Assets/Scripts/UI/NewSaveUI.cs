﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class NewSaveUI : MonoBehaviour {

    public InputField input;
    public GameObject ConfirmSave;
    public GameObject SaveMenu;

    void OnEnable()
    {
        input.text = "";
    }

    public void NewSave()
    {
        if (GameSettings.IsInGame && !ConfirmSave.activeSelf)
        {
            if (!String.IsNullOrEmpty(input.text) && !String.IsNullOrEmpty(input.text.Trim()))
                if (!DiskOp.SaveExists(input.text))
                {
                    GameObject.Find("Base").GetComponent<GameSettings>()
                        .SaveGame(input.text);
                    SaveMenu.SetActive(true);
                    gameObject.SetActive(false);
                }
                else
                {
                    ConfirmSave.SetActive(true);
                    gameObject.SetActive(false);
                }
        }
    }

    public void NewSave(bool NeedsConfirm)
    {
        if (GameSettings.IsInGame && ConfirmSave.activeSelf)
        {
            if (!String.IsNullOrEmpty(input.text) && !String.IsNullOrEmpty(input.text.Trim()))
            {
                GameObject.Find("Base").GetComponent<GameSettings>()
                    .SaveGame(input.text);
                SaveMenu.SetActive(true);
                ConfirmSave.SetActive(false);
            }
        }
    }

    public void ResetText()
    {
        input.text = "";
    }

    public void ResetPlaceHolder(Text PlaceHolder)
    {
        if (string.IsNullOrEmpty(input.text))
            PlaceHolder.enabled = true;
    }
}
