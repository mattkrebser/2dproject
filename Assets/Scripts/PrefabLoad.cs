﻿using UnityEngine;
using System.Collections.Generic;

public static class Prefabs
{

    private static Dictionary<string, GameObject> prefabs = new Dictionary<string, GameObject>();

    /// <summary>
    /// Instantiate a new prefab, whatever it may be. Input a full path.
    /// </summary>
    /// <param name="prefabname"></param>
    /// <returns></returns>
    public static GameObject GetNewPrefab(string prefabname)
    {
        if (prefabs.ContainsKey(prefabname))
        {
            GameObject prefab;
            prefabs.TryGetValue(prefabname, out prefab);
            return MonoBehaviour.Instantiate(prefab);
        }
        else
        {
            GameObject load_obj = Resources.Load(prefabname) as GameObject;
            prefabs.Add(prefabname, load_obj);
            return MonoBehaviour.Instantiate(load_obj);
        }
    }
}
