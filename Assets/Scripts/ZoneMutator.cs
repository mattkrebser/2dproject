﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Used to change attack patterns of a zone, typically by 
/// the player walking trhough some point
/// </summary>
public class ZoneMutator : MonoBehaviour {

    [Tooltip("Which zones will take part in the event")]
    public List<ZoneSettings> AffectedZones = new List<ZoneSettings>();

    [Tooltip("Upon (the player)entering the attatched collider, all current NPCs and future NPCs will attack the player")]
    public bool AttackPlayerOnEnter = true;
    [Tooltip("Enable zone spawning on enter")]
    public bool EnableSpawningOnEnter = true;
    [Tooltip("Map flash on enter?")]
    public bool DoesMapFlash = true;

    void OnTriggerEnter()
    {
        CharacterData player = CharacterMove.CharacterTransform.GetComponent<CharacterData>();
        if (AffectedZones != null && AffectedZones.Count > 0)
        {
            if (AttackPlayerOnEnter)
            {
                if (DoesMapFlash)
                    MapFlash.DoMapFlash(5, Color.red);
            }

            foreach (ZoneSettings z in AffectedZones)
            {
                if (AttackPlayerOnEnter)
                    foreach (CharacterData c in CharacterData.AllCharacters)
                    {
                        if (c.SpawnedFrom == z)
                            c.GetComponent<NPCAI>().Attack(player);
                    }

                if (AttackPlayerOnEnter)
                    z.AttackPlayer = true;

                if (EnableSpawningOnEnter)
                    z.IsSpawning = true;
            }
        }
    }
}
