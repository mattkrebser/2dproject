﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DefeatStaticNPC : QuestEvent {

    [Tooltip("Wait for this NPC to die")]
    public CharacterData DefeatMe;
    [Tooltip("Wait for this list of NPCs to die")]
    public List<CharacterData> DefeatGroup;

    [Tooltip("This list of NPCs will attack the NPCs in the 'DefeatGroup' list and the 'DefeatMe' NPC")]
    public List<CharacterData> Attackers;

    [Tooltip("All Npc's will auto-die if the timeout is >= zero. Leave on the default 1e08, so that the NPC doesn't timeout.")]
    public float Timeout = 100000000.0f;

    public override void StartEvent()
    {
        if (CanRun())
        {
            if (DefeatMe != null)
                DefeatMe.IsImmortal = false;
            StartCoroutine(AttackRoutine());
            StartCoroutine(CheckIsDead());
        }
    }

    CharacterData GetRandomCharToAttack()
    {
        List<CharacterData> liveChars = new List<CharacterData>();
        if (DefeatMe != null)
            if (DefeatMe.Health > 0 && DefeatMe.gameObject.activeSelf)
                liveChars.Add(DefeatMe);
        if (DefeatGroup != null && DefeatGroup.Count > 0)
            foreach (CharacterData c in DefeatGroup)
            {
                if (c.Health > 0 && c.gameObject.activeSelf)
                    liveChars.Add(c);
            }
        return liveChars.Count == 0 ? null : liveChars[Random.Range(0, liveChars.Count)];
    }
    IEnumerator AttackRoutine()
    {
        if (Attackers != null && Attackers.Count > 0)
        {
            while (!AllAreDead())
            {
                foreach (CharacterData c in Attackers)
                {
                    NPCAI ai = c.GetComponent<NPCAI>();
                    if (ai.CurrentlyAttacking == null || ai.CurrentlyAttacking.IsDead ||
                        !ai.CurrentlyAttacking.gameObject.activeSelf)
                    {
                        CharacterData a = GetRandomCharToAttack();
                        if (a != null)
                        {
                            a.IsImmortal = false;
                            ai.Attack(a);
                        }
                    }
                }

                yield return new WaitForSeconds(0.5f);
            }
        }
    }

    IEnumerator CheckIsDead()
    {
        if (DefeatMe != null)
            while (DefeatMe.Health > 0 && DefeatMe.gameObject.activeSelf && Timeout > 0)
            {
                yield return new WaitForSeconds(0.5f);
                Timeout -= 0.5f;
            }

        if (DefeatGroup != null && DefeatGroup.Count > 0)
            while (!GroupIsDead() && Timeout > 0)
            {
                yield return new WaitForSeconds(0.5f);
                Timeout -= 0.5f;
            }

        if (Timeout <= 0)
            KillAll();

        if (!AutomaticAdvance && CanRun())
            BaseQuest.NextEvent();
    }

    bool GroupIsDead()
    {
        foreach (CharacterData c in DefeatGroup)
        {
            if (c.Health > 0 && c.gameObject.activeSelf)
                return false;
        }
        return true;
    }

    bool AllAreDead()
    {
        foreach (CharacterData c in DefeatGroup)
        {
            if (c.Health > 0 && c.gameObject.activeSelf)
                return false;
        }
        if (DefeatMe != null && DefeatMe.Health > 0 && DefeatMe.gameObject.activeSelf)
            return false;
        return true;
    }

    void KillAll()
    {
        if (DefeatMe != null && DefeatMe.Health > 0 && DefeatMe.gameObject.activeSelf)
            DefeatMe.SetCharHealth(null, 0);
        if (DefeatGroup != null && DefeatGroup.Count > 0)
        {
            foreach (CharacterData c in DefeatGroup)
            {
                if (c != null && c.Health > 0 && c.gameObject.activeSelf)
                    c.SetCharHealth(null, 0);
            }
        }
    }
}
