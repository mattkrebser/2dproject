﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

/// <summary>
/// Contains many quest compatible functions. Can also call other arbitrary functions for a quest event
/// </summary>
public class FunctionCall : QuestEvent {

    /// <summary>
    /// It is not safe to have multiple function calls on this event. One call per event is the maximum
    /// </summary>
    public UnityEvent Function;

    public override void StartEvent()
    {
        Function.Invoke();
    }

    /// <summary>
    /// Make a single Dialogue above an NPC.
    /// The quest will continue in a few seconds after the dialogue dissapears
    /// (unless Automatic advance is enabled)
    /// </summary>
    /// <param name="d"></param>
    public void SinlgeDialogue(Dialogue d)
    {
        Dialogue.SingleDialogue(d, this);
    }

    /// <summary>
    /// Make a Narrator Dialogue Box, not attatched to any player.
    /// This dialogue interacts with the player, the quest will continue after the player presses [SPACE]
    /// (unless Automatic advance is enabled, if it is, then the quest will continue immediatly)
    /// </summary>
    /// <param name="d"></param>
    public void NarratorDialogue(Dialogue d)
    {
        Dialogue.NarratorSingleDialogue(d, this);
    }

    /// <summary>
    /// Make a Narrator Dialogue Box, not attatched to any player.
    /// The quest will continue in a few seconds after the dialogue dissapears
    /// (unless Automatic advance is enabled)
    /// </summary>
    /// <param name="d"></param>
    public void NarratorSmallDialogue(Dialogue d)
    {
        Dialogue.NarratorSingleSmallDialogue(d, this);
    }

    /// <summary>
    /// Make a Narrator Dialogue Box, not attatched to any player.
    /// The quest will continue in a few seconds after the dialogue dissapears
    /// (unless Automatic advance is enabled)
    /// </summary>
    /// <param name="d"></param>
    public void NarratorSmallDialogue2secdelay(Dialogue d)
    {
        StartCoroutine(waitAndCall(d, 2));
    }
    IEnumerator waitAndCall(Dialogue d, float t)
    {
        yield return new WaitForSeconds(t);
        Dialogue.NarratorSingleSmallDialogue(d, this);
    }

    /// <summary>
    /// Make a single Dialogue above the Player.
    /// The quest will continue in a few seconds after the dialogue dissapears
    /// (unless Automatic advance is enabled)
    /// </summary>
    /// <param name="d"></param>
    public void SinglePlayerDialogue(Dialogue d)
    {
        Dialogue.PlayerSingleDialogue(d, this);
    }

    /// <summary>
    /// The quest will wait 'time' seconds before continuing.
    /// (unless Automatic advance is enabled)
    /// </summary>
    /// <param name="time"></param>
    public void QuestWait(float time)
    {
        StartCoroutine(WaitRoutine(time));
    }
    IEnumerator WaitRoutine(float time)
    {
        yield return new WaitForSeconds(time);
        if (CanRun() && !AutomaticAdvance && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Freeze the players movements..
    /// This also stops interacting options
    /// </summary>
    public void FreezePlayer()
    {
        PlayerDat p = CharacterMove.CharacterTransform.GetComponent<PlayerDat>();
        p.playerInfo2.isFrozen = true;

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }
    /// <summary>
    /// unfreeze player movements...
    /// This also stops interacting options
    /// </summary>
    public void UnFreezePlayer()
    {
        PlayerDat p = CharacterMove.CharacterTransform.GetComponent<PlayerDat>();
        p.playerInfo2.isFrozen = false;
        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// DisAllows the NPC to use random dialogue generation
    /// </summary>
    /// <param name="npc"></param>
    public void DisableSpeaking(NPCAI npc)
    {
        npc.DisabledNormalConversation = true;

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }
    /// <summary>
    /// Allows the NPC to use random dialogue generation
    /// </summary>
    /// <param name="npc"></param>
    public void EnableSpeaking(NPCAI npc)
    {
        npc.DisabledNormalConversation = false;

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Give the player gunammo
    /// </summary>
    /// <param name="g"></param>
    public void AddGunAmmo(GunAmmo g)
    {
        PlayerDat p = CharacterMove.CharacterTransform.GetComponent<PlayerDat>();
        p.SetWeaponAmmo(g.Gun, g.Ammo);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// give the player a gun
    /// </summary>
    /// <param name="g"></param>
    public void AddGun(GunAmmo g)
    {
        PlayerDat p = CharacterMove.CharacterTransform.GetComponent<PlayerDat>();
        p.AddWeapon(g.Gun, g.isRight);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Set the loadout at[0] to have a handgun in the left slot. Sets the active loadout to 0
    /// </summary>
    public void SetPlayerLoadoutDefault()
    {
        PlayerDat p = CharacterMove.CharacterTransform.GetComponent<PlayerDat>();
        p.playerInfo2.LWeaponsLoadOut[0] = "Weapons-Handgun-L";
        p.SetLoadout(0);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Manually defeat a spawner
    /// </summary>
    /// <param name="spawner"></param>
    public void AutoDefeatSpawner(ZoneSettings spawner)
    {
        spawner.AutoDefeatThisSpawner();

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Screen fades to black
    /// </summary>
    public void FadeOut()
    {
        ScreenToBlack.Fadout(this);
    }
    /// <summary>
    /// screen fades from black
    /// </summary>
    public void FadeIn()
    {
        ScreenToBlack.FadeIn(this);
    }

    /// <summary>
    /// Move the player to match the input transform
    /// </summary>
    /// <param name="t"></param>
    public void MovePlayer(Transform t)
    {
        CharacterMove.CharacterTransform.position = t.position;
        CharacterMove.CharacterTransform.eulerAngles = t.eulerAngles;

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Set the zone that the player is in
    /// </summary>
    /// <param name="newZone"></param>
    public void SetPlayerZone(MapData newZone)
    {
        GameSettings.Instance.HibernateMap(MapData.ActiveMap);

        CameraFollow.SetMap(newZone);
        CharacterMove.CharacterTransform.GetComponent<PlayerDat>().playerInfo2.ActiveMap = newZone.ZoneName;
        MapData.ActiveMap = newZone;

        if (MapData.ActiveMap.isAsleep)
            GameSettings.Instance.WakeUpMap(MapData.ActiveMap);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Set the player's immortal value
    /// </summary>
    /// <param name="newVal"></param>
    public void SetPlayerImmortal(bool newVal)
    {
        CharacterMove.CharacterTransform.GetComponent<CharacterData>().IsImmortal = newVal;

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Sets the player to unarmed and doesn't allow them to switch
    /// </summary>
    public void SetPlayerGunsAllowed(bool value)
    {
        CharacterMove.CharacterTransform.GetComponent<PlayerDat>().SetGunsAllowed(value);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Start the input quest
    /// </summary>
    /// <param name="q"></param>
    public void StartQuest(Quest q)
    {
        q.ResetQuest();
        q.Started = true;
        q.Events[q.CurrentEvent].StartEvent();
        if (q.Events[q.CurrentEvent].AutomaticAdvance)
            q.NextEvent();

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Enable the input object
    /// </summary>
    /// <param name="s"></param>
    public void EnableSaveStateObject(SaveStateObject s)
    {
        SaveStateObject.SetActive(s, true);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }
    /// <summary>
    /// Disable the input object
    /// </summary>
    /// <param name="s"></param>
    public void DisableSaveStateObject(SaveStateObject s)
    {
        SaveStateObject.SetActive(s, false);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }

    /// <summary>
    /// Sets the player's health
    /// </summary>
    /// <param name="newValue"></param>
    public void SetPlayerHealth(int newValue)
    {
        CharacterMove.CharacterTransform.GetComponent<CharacterData>().SetCharHealth(null, newValue);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }
    /// <summary>
    /// Set player health packs
    /// </summary>
    /// <param name="newValue"></param>
    public void SetPlayerHealthPacks(int newValue)
    {
        CharacterMove.CharacterTransform.GetComponent<CharacterData>().NumHealthPacks = newValue;

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }
    /// <summary>
    /// active a static characters gameobject, do not use gameobject.setactive
    /// </summary>
    /// <param name="c"></param>
    public void ActivateStaticCharacter(CharacterData c)
    {
        PlaceCharacters.ActivateStaticCharacter(c);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }
    /// <summary>
    /// deactivate a static character's gameobject
    /// </summary>
    /// <param name="c"></param>
    public void DeactivateStaticCharacter(CharacterData c)
    {
        PlaceCharacters.DeactivateStaticCharacter(c);

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }
    /// <summary>
    /// Enable spawning on a zone
    /// </summary>
    /// <param name="z"></param>
    public void ZoneStartSpawning(ZoneSettings z)
    {
        z.IsSpawning = true;

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }
    /// <summary>
    /// Start combat with player ooohhohohohoho!!
    /// </summary>
    /// <param name="boss"></param>
    public void StartCombat(BossAI boss)
    {
        boss.InCombatWithPlayer = true;

        if (!AutomaticAdvance && CanRun() && BaseQuest != null)
            BaseQuest.NextEvent();
    }
}
