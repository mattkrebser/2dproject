﻿using UnityEngine;
using System.Collections;

public class NPCDialogue : QuestEvent {

    public Dialogue dialogue;

    public Interact StartConversationObj;

    /// <summary>
    /// The Dialogue just pressed by the player...
    /// </summary>
    public static Dialogue PlayerSelectedNewDialogue;
    /// <summary>
    /// Set to true every time the player responds, and then set back to false once it has been processed
    /// </summary>
    public static bool PlayerResponded = false;

    [Tooltip("Position of this dialogue, if the character moves too far away, the conversation will end. " +
        "If set to null, this Objects Transform is used")]
    public Transform DialoguePosition;

    [Tooltip("How far away is the character allowed before the dialogue closes?")]
    public float AllowedDistanceFromDialogue = 1.5f;

    [Tooltip("If true, the dialogue will open automatically without the player having to interact with an NPC " +
        " It is recommended to set the 'AllowedDistanceFromDialogue' value to a very high number if this is enabled")]
    public bool IsAutoDialogue = false;

    public override void StartEvent()
    {
        if ((StartConversationObj != null && StartConversationObj.InteractReceived) || IsAutoDialogue)
            Dialogue.MakeQuestDialogueBubble(dialogue, this, DialoguePosition == null ? transform : DialoguePosition);
    }
    public override void ResetEvent()
    {
        StartConversationObj.Reset();
    }
    public override bool IsCurrentEvent()
    {
        return BaseQuest.Events[BaseQuest.CurrentEvent] == this;
    }
}
