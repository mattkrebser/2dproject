﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Interact is used to receive input(e key) from a player and start an event immediatly after receiving input.
/// </summary>
public class Interact : MonoBehaviour
{
    [Tooltip("This value will override attatched sphere collider radius if there is a sphere collider on this gameobject")]
    public float InteractRadius = 0.5f;

    [Tooltip("Current interacted state. default=false. If true, then this interact object will not accept new interactions")]
    public bool InteractReceived = false;

    private GameObject myParticle;

    [Tooltip("The event that this interact object will trigger")]
    public QuestEvent MyEvent;

    SphereCollider MyCollider;

    /// <summary>
    /// If true, this interact will be triggered when the player enters
    /// </summary>
    [Tooltip("If true, this interact will be triggered by the player entering the zone, rather than pressing [e]")]
    public bool EnterMode = false;

    void Start()
    {
        SphereCollider s;
        if ((s = GetComponent<SphereCollider>()) == null)
            s = gameObject.AddComponent<SphereCollider>();
        MyCollider = s;
        s.radius = InteractRadius;
        gameObject.layer = LayerMask.NameToLayer("Interact");
    }

    /// <summary>
    /// Call this to interact with the object
    /// </summary>
    public void Interacted()
    {
        if (!InteractReceived)
        {
            if (MyEvent.CanRun())
            {
                InteractReceived = true;
                MyCollider.enabled = false;
                MyEvent.StartEvent();
                if (myParticle != null)
                    Destroy(myParticle);
            }
        }
    }

    public void Reset()
    {
        InteractReceived = false;
        if (MyCollider != null)
            MyCollider.enabled = true;
    }

    void OnTriggerEnter(Collider c)
    {
        //Enter mode doesn't use the particle FX
        if (EnterMode)
        {
            Interacted();
        }
        else if (MyEvent.CanRun())
        {
            GameObject doorFX = Prefabs.GetNewPrefab("FX/CircleFX");
            doorFX.transform.SetParent(transform);
            doorFX.transform.localPosition = new Vector3(0, 0, 0);
            myParticle = doorFX;
        }

        //can interact
        if (MyEvent.CanRun())
            ZoneLabel.PlayerCanInteract = true;
    }

    void OnTriggerExit(Collider c)
    {
        if (myParticle != null)
        {
            Destroy(myParticle);
        }

        //no longer able to interact
        ZoneLabel.PlayerCanInteract = false;
    }
}
