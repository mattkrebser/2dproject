﻿using UnityEngine;
using System.Collections;

public class DefeatSpawner : QuestEvent {

    [Tooltip("Only applys if the value is greater than 0. This event will auto advance if the amount of time spent " +
        " on this event is greater than the 'Timeout' value")]
    public float Timeout = -1f;

    bool Advanced = false;

    public void OnSpawnerDefeat()
    {
        if (!AutomaticAdvance && !Advanced)
            if (CanRun())
            {
                Advanced = true;
                BaseQuest.NextEvent();
            }
    }

    public override void ResetEvent()
    {
        Advanced = false;
        GetComponent<ZoneSettings>().ResetZone();
    }

    public override void StartEvent()
    {
        if (!CanRun())
            return;

        if (GetComponent<ZoneSettings>() == null)
            Debug.Log("DefeatSpawner event needs Zonesettings to function!");
        else
        {
            if (GetComponent<ZoneSettings>().ZoneReachedSpawnLimit && !AutomaticAdvance)
                BaseQuest.NextEvent();
            else
                StartCoroutine(TimeoutRoutine());
        }
    }

    IEnumerator TimeoutRoutine()
    {
        if (Timeout > 0)
        {
            while (CanRun())
            {
                Timeout -= 0.25f;
                yield return new WaitForSeconds(0.25f);

                if (Timeout <= 0)
                {
                    GetComponent<ZoneSettings>().AutoDefeatThisSpawner();
                    OnSpawnerDefeat();
                }
            }
        }
    }
}
