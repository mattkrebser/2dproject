﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using System.Collections;

public class SetCharacter : QuestEvent{

    /// <summary>
    /// Operations applied to this character
    /// </summary>
    public CharacterData OnThisCharacter;

    /// <summary>
    /// This function can have multiple function calls from this object
    /// </summary>
    public UnityEvent Function;

    public List<CharacterData> OnTheseCharacters;

    [Tooltip("This only applys to dialogue functions, the character will say something every 5 seconds")]
    public float DialougeLoopRate = 5.0f;
    [Tooltip("Randomize the loop rate by this much (seconds)")]
    public float DialogueLoopThreshold = 3.0f;

    public override void StartEvent()
    {
        Function.Invoke();
        if (!AutomaticAdvance)
        {
            BaseQuest.NextEvent();
        }
    }

    /// <summary>
    /// Is a looping dialogue running?
    /// </summary>
    private bool _DialogueIsRunning = false;
    public bool DialogueIsRunning
    {
        get
        {
            return _DialogueIsRunning;
        }
        set
        {
            _DialogueIsRunning = value;
        }
    }

    public void SetCharacterHealth(int newHealth)
    {
        if (OnThisCharacter != null)
        {
            OnThisCharacter.SetCharHealth(null, newHealth);
        }
        if (OnTheseCharacters != null && OnTheseCharacters.Count > 0)
        {
            foreach (CharacterData d in OnTheseCharacters)
            {
                d.SetCharHealth(null, newHealth);
            }
        }
    }
    public void AddCharacterHealth(int amount)
    {
        if (OnThisCharacter != null)
        {
            OnThisCharacter.SetCharHealth(null, OnThisCharacter.Health + amount);
        }
        if (OnTheseCharacters != null && OnTheseCharacters.Count > 0)
        {
            foreach (CharacterData d in OnTheseCharacters)
            {
                d.SetCharHealth(null, d.Health + amount);
            }
        }
    }
    public void SetCharacterImmortal(bool value)
    {
        if (OnThisCharacter != null)
        {
            OnThisCharacter.IsImmortal = value;
        }
        if (OnTheseCharacters != null && OnTheseCharacters.Count > 0)
        {
            foreach (CharacterData d in OnTheseCharacters)
            {
                d.IsImmortal = value;
            }
        }
    }
    public void SetCharacterZone(MapData newZone)
    {
        if (OnThisCharacter != null)
        {
            OnThisCharacter.GetComponent<AIMove>().MyZone = newZone;
        }
        if (OnTheseCharacters != null && OnTheseCharacters.Count > 0)
        {
            foreach (CharacterData d in OnTheseCharacters)
            {
                d.GetComponent<AIMove>().MyZone = newZone;
            }
        }
    }
    public void MoveCharacter(Transform moveHere)
    {
        if (OnThisCharacter != null)
        {
            OnThisCharacter.GetComponent<AIMove>().SetPosition(moveHere.position);
            OnThisCharacter.GetComponent<AIMove>().transform.eulerAngles = moveHere.eulerAngles;
        }
        if (OnTheseCharacters != null && OnTheseCharacters.Count > 0)
        {
            foreach (CharacterData d in OnTheseCharacters)
            {
                d.GetComponent<AIMove>().SetPosition(moveHere.position);
                d.GetComponent<AIMove>().transform.eulerAngles = moveHere.eulerAngles;
            }
        }
    }
    public void MoveCharacterIgnoreRotation(Transform moveHere)
    {
        if (OnThisCharacter != null)
        {
            OnThisCharacter.GetComponent<AIMove>().SetPosition(moveHere.position);
        }
        if (OnTheseCharacters != null && OnTheseCharacters.Count > 0)
        {
            foreach (CharacterData d in OnTheseCharacters)
            {
                d.GetComponent<AIMove>().SetPosition(moveHere.position);
            }
        }
    }
    public void RandomizeCharacter(int thisFaction)
    {
        if (OnThisCharacter != null)
        {
            PeopleData.GetNPC((Factions)thisFaction, OnThisCharacter);
        }
        if (OnTheseCharacters != null && OnTheseCharacters.Count > 0)
        {
            foreach (CharacterData d in OnTheseCharacters)
            {
                PeopleData.GetNPC((Factions)thisFaction, d);
            }
        }
    }

    /// <summary>
    /// This function uses dialogue differently then others, it randomly chooses
    /// statements from the same dialogue for the characters to say. They will continue until stop
    /// dialogue i called on this object.
    /// </summary>
    /// <param name="d"></param>
    public void LoopRandomlyDialogue(Dialogue d)
    {
        DialogueIsRunning = true;
        if (OnThisCharacter != null)
            StartCoroutine(loopDialogueRoutine(d, OnThisCharacter));
        if (OnTheseCharacters != null && OnTheseCharacters.Count > 0)
        {
            foreach (CharacterData c in OnTheseCharacters)
            {
                StartCoroutine(loopDialogueRoutine(d, c));
            }
        }
    }

    IEnumerator loopDialogueRoutine(Dialogue d, CharacterData c)
    {
        yield return new WaitForSeconds(DialougeLoopRate +
            Random.Range(-DialogueLoopThreshold, DialogueLoopThreshold));

        while (DialogueIsRunning)
        {
            //say a dialogue if the player is near
            if (CharacterMove.NearPlayer(c.transform, 49))
                Dialogue.SingleRandomDialogueSelect(d, c);
            yield return new WaitForSeconds(DialougeLoopRate +
                Random.Range(-DialogueLoopThreshold, DialogueLoopThreshold));
        }
    }

    /// <summary>
    /// Stops looping Dialogues for this object only
    /// </summary>
    /// <param name="d"></param>
    public void StopDialogue(SetCharacter s)
    {
        s.DialogueIsRunning = false;
    }
}
