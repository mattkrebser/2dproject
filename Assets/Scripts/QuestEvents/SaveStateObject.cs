﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// These objects save to disk their 'Active' variable, which represents if this gameobject
/// is active/disbaled.
/// </summary>
public class SaveStateObject : MonoBehaviour {

    [Tooltip("This must be a unique name that identifies this object")]
    public string ObjectName = "";

    [Tooltip("How should this object behave? Active/not active. This will disbale this gameObject. Please note, " +
        " all save state objects need to be active at start. Do not modifiy the Unity 'active' value of this object")]
    public bool Active = false;

    private static Dictionary<string, SaveStateObject> objects = new Dictionary<string, SaveStateObject>();

    void Start()
    {
        objects.Add(ObjectName, this);
        StartCoroutine(Deactivate());
    }
    IEnumerator Deactivate()
    {
        //wait a little bit for Star/Awake other initilization tasks.
        //We don't want this object to effect initialization functionality of whatever 
        //game object it is on
        yield return 0; yield return 0;
        yield return 0; yield return 0;
        yield return 0; yield return 0;
        gameObject.SetActive(Active);
    }


    public static void ClearObjects()
    {
        objects.Clear();
    }

    /// <summary>
    /// Set an individual State Object to active/not active. Modifying the gameObject.active property can result
    /// in undefined behaviour for this gameObject
    /// </summary>
    /// <param name="s"></param>
    /// <param name="value"></param>
    public static void SetActive(SaveStateObject s, bool value)
    {
        objects[s.ObjectName].gameObject.SetActive(value);
    }

    public static List<SaveStateDat> GetDat()
    {
        List<SaveStateDat> l = new List<SaveStateDat>();
        foreach(SaveStateObject s in objects.Values)
        {
            SaveStateDat d = new SaveStateDat();
            d.ObjectName = s.ObjectName;
            d.Active = s.gameObject.activeSelf;
            l.Add(d);
        }
        return l;
    }
    public static void SetDat(List<SaveStateDat> l)
    {
        if (l != null && l.Count > 0)
        {
            foreach (SaveStateDat d in l)
            {
                objects[d.ObjectName].gameObject.SetActive(d.Active);
            }
        }
    }
}

[System.Serializable]
public struct SaveStateDat
{
    public bool Active;
    public string ObjectName;
}
