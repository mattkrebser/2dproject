﻿using UnityEngine;
using System.Collections.Generic;

public class PlaceCharacters : QuestEvent
{
    public List<NewSpot> MoveCharacters = new List<NewSpot>();

    public override void StartEvent()
    {
        if (CanRun())
        {
            foreach (NewSpot n in MoveCharacters)
            {
                n.thisCharacter.GetComponent<AIMove>().SetPosition(n.PlaceHere.position);
                n.thisCharacter.RotationEuler = n.PlaceHere.eulerAngles;
                if (n.newZone != null)
                    n.thisCharacter.GetComponent<AIMove>().MyZone = n.newZone;
                if (n.ResetNPC)
                {
                    n.thisCharacter.GetComponent<CharacterData>().ResetStaticNPC();
                    ActivateStaticCharacter(n.thisCharacter);
                }
                if (n.RandomizeNPC)
                {
                    PeopleData.GetNPC(n.RandomizeFaction, n.thisCharacter.GetComponent<CharacterData>());
                }
            }
            if (!AutomaticAdvance)
                BaseQuest.NextEvent();
        }
    }

    /// <summary>
    /// active a static characters gameobject, do not use gameobject.setactive
    /// </summary>
    /// <param name="c"></param>
    public static void ActivateStaticCharacter(CharacterData c)
    {
        AIMove aim = c.GetComponent<AIMove>();
        if (aim != null)
        {
            //if this static character is in the active zone...
            if (aim.MyZone.ZoneID == MapData.ActiveMap.ZoneID)
            {
                c.gameObject.SetActive(true);
            }
            //otherwise, the static character will remain inactive, so serialized data must be changed
            else
            {
                if (aim.MyZone.AwakeStaticPlayers != null && aim.MyZone.AwakeStaticPlayers.Contains(c.gameObject))
                {
                    //no action
                    return;
                }
                else
                {
                    //add to awake list
                    if (aim.MyZone.AwakeStaticPlayers == null)
                        aim.MyZone.AwakeStaticPlayers = new System.Collections.Generic.List<GameObject>();
                    aim.MyZone.AwakeStaticPlayers.Add(c.gameObject);
                }
            }
        }
    }

    /// <summary>
    /// deactivate a static character's gameobject
    /// </summary>
    /// <param name="c"></param>
    public static void DeactivateStaticCharacter(CharacterData c)
    {
        AIMove aim = c.GetComponent<AIMove>();
        if (aim != null)
        {
            //if this static character is in the active zone...
            if (aim.MyZone.ZoneID == MapData.ActiveMap.ZoneID)
            {
                c.gameObject.SetActive(false);
            }
            //otherwise, do stuff
            else
            {
                //if this gameobject is marked as active in its zone..
                if (aim.MyZone.AwakeStaticPlayers != null && aim.MyZone.AwakeStaticPlayers.Contains(c.gameObject))
                {
                    aim.MyZone.AwakeStaticPlayers.Remove(c.gameObject);
                    if (aim.gameObject.activeSelf)
                        aim.gameObject.SetActive(false);
                    return;
                }
                //otherwise... (if it did not appear on the active list)
                else
                {
                    //deactivate the gameobject if it is active
                    if (aim.gameObject.activeSelf)
                        aim.gameObject.SetActive(false);
                    return;
                }
            }
        }
    }
}

[System.Serializable]
public struct NewSpot
{
    [Tooltip("Move this character")]
    public CharacterData thisCharacter;
    [Tooltip("New position for the character")]
    public Transform PlaceHere;
    [Tooltip("If the character is being placed in a new zone, then specifiy it. Otherwise this can be left null")]
    public MapData newZone;
    [Tooltip("Randomize appearance")]
    public bool RandomizeNPC;
    [Tooltip("Resets many properties on the NPC, should almost always be ticked.")]
    public bool ResetNPC;
    [Tooltip("If randomize is on, the character will randomize to this faction. Set to 'None' for random faction")]
    public Factions RandomizeFaction;
}