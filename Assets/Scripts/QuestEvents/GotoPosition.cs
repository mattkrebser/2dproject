﻿using UnityEngine;
using System.Collections;

public class GotoPosition : QuestEvent
{

    public Interact MyInteract;

    public override void StartEvent()
    {
        if (!AutomaticAdvance && MyInteract.InteractReceived)
            BaseQuest.NextEvent();
    }
    public override void ResetEvent()
    {
        MyInteract.Reset();
    }

    public override bool IsCurrentEvent()
    {
        return BaseQuest.Events[BaseQuest.CurrentEvent] == this;
    }
}
