﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicPlayer : MonoBehaviour {

    public List<MusicClip> Music = new List<MusicClip>();
    public float MusicPlayFrequency = 240.0f;

    AudioSource source;
    MusicClip CurrentClip;

    public AudioClip LobbySong;

    bool DonePlaying = true;

    /// <summary>
    /// Total time this object has been existing
    /// </summary>
    private float TimeUp = 0.0f;
    private float timeOnStart = 0.0f;

    static bool Blocked = false;

    static MusicPlayer Instance;
    void Start()
    {
        source = GetFreeSource();
        source.volume = 0;
        StartCoroutine(MusicRoutine());
        timeOnStart = Time.realtimeSinceStartup;
        Instance = this;
    }

    void OnDestroy()
    {
        SoundFX.Reset();
        Instance = null;
    }

    /// <summary>
    /// Stops playing the current track and plays a new one
    /// </summary>
    /// <param name="a"></param>
    public static void PlayTrack(AudioClip a, float PlayTime, float volume)
    {
        if (Blocked)
            return;
        if (PlayTime < 5.0f)
            PlayTime = 5.0f;
        Blocked = true;
        CoroutineHelper.RunCoroutine(TrackRoutine(a, PlayTime, volume));
    }
    /// <summary>
    /// Resumes playing music in the normal mode. This function is used to prematurally end the
    /// music in the PlayTrack function
    /// </summary>
    public static void ResumeNormalPlayer()
    {
        if (!Blocked)
            return;
        if (canceling)
            return;
        if (endTrack)
            return;

        endTrack = true;
    }
    static bool endTrack = false;
    static IEnumerator TrackRoutine(AudioClip a, float PlayTime, float volume)
    {
        if (endTrack)
        {
            Cancel();
            yield break;
        }

        float runningTime = 0.0f;
        //fadeout current
        if (Instance.CurrentClip != null)
            yield return Instance.FadeOut(Instance.CurrentClip);

        if (endTrack)
        {
            Cancel();
            yield break;
        }

        MusicClip newClip = new MusicClip();
        newClip.music = a; newClip.volume = volume;
        Instance.CurrentClip = newClip;

        yield return Instance.FadeIn(newClip);

        if (endTrack)
        {
            Cancel();
            yield break;
        }

        while (runningTime < PlayTime)
        {
            yield return new WaitForSeconds(0.1f);
            runningTime += 0.1f;

            if (endTrack)
            {
                Cancel();
                yield break;
            }
        }

        yield return Instance.FadeOut(newClip);

        if (endTrack)
        {
            Cancel();
            yield break;
        }

        Instance.DonePlaying = true;
        Blocked = false;
    }

    static bool canceling = false;
    static void Cancel()
    {
        if (canceling)
            return;
        canceling = true;
        CoroutineHelper.RunCoroutine(CancelRoutine());
    }
    static IEnumerator CancelRoutine()
    {
        if (Instance.CurrentClip != null)
        {
            yield return Instance.FadeOut(Instance.CurrentClip);
            Instance.CurrentClip = null;
        }

        Instance.DonePlaying = true;
        Blocked = false;
        canceling = false;
        endTrack = false;
    }

    /// <summary>
    /// Returns an object suitable for playing music
    /// </summary>
    /// <returns></returns>
    AudioSource GetFreeSource()
    {
        GameObject newObj = Prefabs.GetNewPrefab("Prefabs/MusicSource");
        AudioSource a;
        a = newObj.GetComponent<AudioSource>();
        newObj.transform.SetParent(transform);
        return a;
    }

    /// <summary>
    /// Play music
    /// </summary>
    /// <returns></returns>
    IEnumerator MusicRoutine()
    {
        bool inLobby = true;
        while (Application.isPlaying)
        {
            if (GameSettings.IsInGame)
            {
                if (inLobby)
                {
                    yield return FadeOut1();
                    inLobby = false;
                }

                //the BlockedTime varibale is here to prevent new music from being played
                if (DonePlaying && !Blocked)
                {
                    DonePlaying = false;
                    StartCoroutine( PlayClip(Music[Random.Range(0, Music.Count)]));
                }
            }
            else if (TimeUp > 0.5f)
            {
                if (!inLobby && source.isPlaying)
                    yield return FadeOut(CurrentClip);
                if (source.clip != LobbySong)
                    source.clip = LobbySong;
                if (source.clip != null && !source.isPlaying)
                    yield return FadeIn();
            }

            yield return 0;
            TimeUp = Time.realtimeSinceStartup - timeOnStart;
        }
    }

    /// <summary>
    /// Play a specific music clip
    /// </summary>
    /// <param name="m"></param>
    /// <returns></returns>
    IEnumerator PlayClip(MusicClip m)
    {
        //assign current clip
        CurrentClip = m;

        //fade in new song
        yield return FadeIn(m);

        if (Blocked)
            yield break;

        //wait for song to end
        yield return new WaitForSeconds(m.repeatTimes * m.music.length);

        if (Blocked)
            yield break;

        //fade out
        yield return FadeOut(m);

        if (Blocked)
            yield break;

        //wait for music frequency
        float noise = MusicPlayFrequency * 0.25f;
        yield return new WaitForSeconds(MusicPlayFrequency + Random.Range(-noise, noise));

        if (Blocked)
            yield break;

        //assign done playing to true (allows for new song to be played)
        DonePlaying = true;
    }

    /// <summary>
    /// Fade into the input music clip
    /// </summary>
    /// <param name="m"></param>
    /// <returns></returns>
    IEnumerator FadeIn(MusicClip m)
    {
        source.clip = m.music;
        source.Play();
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < 20; i++)
        {
            source.volume = (i / 20.0f) * m.volume;
            yield return new WaitForSeconds(0.05f);
        }
        source.volume = m.volume;
    }

    /// <summary>
    /// fade into the lobby music
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeIn()
    {
        source.Play();
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < 20; i++)
        {
            source.volume = (i / 20.0f);
            yield return new WaitForSeconds(0.05f);
        }
        source.volume = 1;
    }

    /// <summary>
    /// Fade out of the input music clip
    /// </summary>
    /// <param name="m"></param>
    /// <returns></returns>
    IEnumerator FadeOut(MusicClip m)
    {
        float v = m.volume;
        for (int i = 0; i < 20; i++)
        {
            source.volume = v * ((20.0f - i) / 20.0f);
            yield return new WaitForSeconds(0.05f);
        }
        source.volume = 0.0f;
        source.Stop();
    }

    /// <summary>
    /// fade out of the lobby music
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeOut1()
    {
        for (int i = 0; i < 20; i++)
        {
            source.volume = ((20.0f - i) / 20.0f);
            yield return new WaitForSeconds(0.05f);
        }
        source.volume = 0.0f;
        source.Stop();
    }
}

/// <summary>
/// Represents a music clip that can be played
/// </summary>
[System.Serializable]
public class MusicClip
{
    public AudioClip music;
    public int repeatTimes;
    public float volume = 1.0f;
}