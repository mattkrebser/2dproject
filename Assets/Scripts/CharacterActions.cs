﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class CharacterActions : MonoBehaviour {

    [System.NonSerialized]
    public Camera cam;

    /// <summary>
    /// Current GameObject under the mouse that is clickable
    /// </summary>
    private GameObject current_clickable;

	// Use this for initialization
	void Start ()
    {
        Action ekey = EKeyAction;
        InputHandler.AddGlobalInput(new KeySet(KeyCode.E, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), ekey);

        InputHandler.AddGlobalInput(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown), ClickHoldDown);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown), RightClickHoldDown);

        InputHandler.AddGlobalInput(new KeySet(KeyCode.Escape, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), OnEscape);

        InputHandler.AddGlobalInput(KeySet.ScrollDown, OnScrollDown);
        InputHandler.AddGlobalInput(KeySet.ScrollUp, OnScrollUp);

        InputHandler.AddGlobalInput(new KeySet(KeyCode.L, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), OnL);

        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha1, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On1);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha2, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On2);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha3, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On3);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha4, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On4);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha5, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On5);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha6, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On6);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha7, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On7);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha8, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On8);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha9, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On9);
        InputHandler.AddGlobalInput(new KeySet(KeyCode.Alpha0, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On0);

        InputHandler.AddGlobalInput(new KeySet(KeyCode.J, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), OnJ);

        InputHandler.AddGlobalInput(new KeySet(KeyCode.Q, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), OnQ);

        cam = GetComponent<CharacterMove>().cam;
	}

    void OnDestroy()
    {
        Action ekey = EKeyAction;
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.E, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), ekey);

        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown), ClickHoldDown);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown), RightClickHoldDown);

        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Escape, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), OnEscape);

        InputHandler.RemoveGlobalInput(KeySet.ScrollDown, OnScrollDown);
        InputHandler.RemoveGlobalInput(KeySet.ScrollUp, OnScrollUp);


        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.L, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), OnL);

        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha1, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On1);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha2, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On2);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha3, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On3);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha4, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On4);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha5, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On5);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha6, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On6);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha7, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On7);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha8, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On8);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha9, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On9);
        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Alpha0, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), On0);

        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.J, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), OnJ);

        InputHandler.RemoveGlobalInput(new KeySet(KeyCode.Q, KeyCode.None, KeyCode.None, GameEventTypes.OnInputKeyDown), OnQ);
    }

    void On1()
    {
        GetComponent<PlayerDat>().SetLoadout(1);
    }
    void On2()
    {
        GetComponent<PlayerDat>().SetLoadout(2);
    }
    void On3()
    {
        GetComponent<PlayerDat>().SetLoadout(3);
    }
    void On4()
    {
        GetComponent<PlayerDat>().SetLoadout(4);
    }
    void On5()
    {
        GetComponent<PlayerDat>().SetLoadout(5);
    }
    void On6()
    {
        GetComponent<PlayerDat>().SetLoadout(6);
    }
    void On7()
    {
        GetComponent<PlayerDat>().SetLoadout(7);
    }
    void On8()
    {
        GetComponent<PlayerDat>().SetLoadout(8);
    }
    void On9()
    {
        GetComponent<PlayerDat>().SetLoadout(9);
    }
    void On0()
    {
        GetComponent<PlayerDat>().SetLoadout(0);
    }

    void OnL()
    {
        if (!GameSettings.IsInGame)
            return;

        CharacterData cd = GetComponent<CharacterData>();

        //if dead, do nothing
        if (cd.IsDead)
            return;

        UIMenus.escapeMenu.SetActive(false);
        UIMenus.SaveGameWindow.SetActive(false);
        UIMenus.NewSaveWindow.SetActive(false);
        UIMenus.ConfirmSaveWindow.SetActive(false);
        UIMenus.QuestWindow.SetActive(false);
        UIMenus.ClothingWindow.SetActive(false);
        UIMenus.ControlsWindow.SetActive(false);
        UIMenus.MedicWindow.SetActive(false);

        bool state = UIMenus.LoadoutWindow.activeSelf || UIMenus.GunWindow.activeSelf;
        UIMenus.LoadoutWindow.SetActive(!state);
        UIMenus.GunWindow.SetActive(false);
    }

    void OnJ()
    {
        if (!GameSettings.IsInGame)
            return;

        CharacterData cd = GetComponent<CharacterData>();

        //if dead, do nothing
        if (cd.IsDead)
            return;

        UIMenus.escapeMenu.SetActive(false);
        UIMenus.SaveGameWindow.SetActive(false);
        UIMenus.NewSaveWindow.SetActive(false);
        UIMenus.ConfirmSaveWindow.SetActive(false);
        UIMenus.LoadoutWindow.SetActive(false);
        UIMenus.GunWindow.SetActive(false);
        UIMenus.ClothingWindow.SetActive(false);
        UIMenus.ControlsWindow.SetActive(false);
        UIMenus.MedicWindow.SetActive(false);

        UIMenus.QuestWindow.SetActive(!UIMenus.QuestWindow.activeSelf);
    }

    void OnQ()
    {
        if (!GameSettings.IsInGame)
            return;

        CharacterData cd = GetComponent<CharacterData>();

        //if dead, do nothing
        if (cd.IsDead)
            return;

        //use a health pack!
        cd.UseHealthPack();
    }

    void OnScrollUp()
    {
        if (!GameSettings.IsInGame)
            return;

        CharacterData cd = GetComponent<CharacterData>();
        //if dead, do nothing
        if (cd.IsDead)
            return;

        GetComponent<PlayerDat>().GetNextLoadoutUp();
    }

    void OnScrollDown()
    {
        if (!GameSettings.IsInGame)
            return;

        CharacterData cd = GetComponent<CharacterData>();
        //if dead, do nothing
        if (cd.IsDead)
            return;

        GetComponent<PlayerDat>().GetNextLoadoutDown();
    }

    void OnEscape()
    {
        if (!GameSettings.IsInGame)
            return;

        CharacterData cd = GetComponent<CharacterData>();

        //if dead, do nothing
        if (cd.IsDead)
            return;

        UIMenus.LoadoutWindow.SetActive(false);
        UIMenus.GunWindow.SetActive(false);
        UIMenus.QuestWindow.SetActive(false);
        UIMenus.ClothingWindow.SetActive(false);
        UIMenus.MedicWindow.SetActive(false);

        bool state = UIMenus.escapeMenu.activeSelf || UIMenus.SaveGameWindow.activeSelf ||
            UIMenus.NewSaveWindow.activeSelf || UIMenus.ConfirmSaveWindow.activeSelf || UIMenus.ControlsWindow.activeSelf;

        UIMenus.escapeMenu.SetActive(!state);
        UIMenus.SaveGameWindow.SetActive(false);
        UIMenus.NewSaveWindow.SetActive(false);
        UIMenus.ConfirmSaveWindow.SetActive(false);
        UIMenus.ControlsWindow.SetActive(false);
    }

    //On click held down
    void ClickHoldDown()
    {
        if (!GameSettings.IsInGame)
            return;

        PlayerDat p = GetComponent<PlayerDat>();
        bool Frozen = p.playerInfo2.isFrozen;
        if (!Frozen && p.CanShoot(false) && !UIMenus.AnyActive && !PlayerDialogueUI.PlayerInChoiceMenu)
            ItemInteract.Shoot(GetComponent<CharacterData>(), false);
    }

    void RightClickHoldDown()
    {
        if (!GameSettings.IsInGame)
            return;

        PlayerDat p = GetComponent<PlayerDat>();
        bool Frozen = p.playerInfo2.isFrozen;
        if (!Frozen && p.CanShoot(true) && !UIMenus.AnyActive && !PlayerDialogueUI.PlayerInChoiceMenu)
            ItemInteract.Shoot(GetComponent<CharacterData>(), true);
    }

    /// <summary>
    /// On press 'E'
    /// </summary>
    void EKeyAction()
    {
        if (!GameSettings.IsInGame)
            return;

        //determine if character is frozen, if the character is frozen then they are
        //disallowed from some interactions
        bool Frozen = GetComponent<PlayerDat>().playerInfo2.isFrozen;

        //try vehicles
        CharacterData cd = GetComponent<CharacterData>();
        
        //if dead, do nothing
        if (cd.IsDead)
            return;

        //if not in a vehicle and not frozen
        if (cd.InVehicle == null && !Frozen)
        {
            VehicleScript min = null;
            float current_distance = 10000000f;
            //Find the closest vehicle that is within 1 distance
            foreach (VehicleScript v in VehicleScript.All_Vehicles.Values)
            {
                if ((v.transform.position - transform.position).magnitude < 0.85f &&
                    (v.transform.position - transform.position).magnitude < current_distance)
                {
                    min = v;
                    current_distance = (v.transform.position - transform.position).magnitude;
                }
            }

            if (min != null)
            {
                min.EnterVehicle(cd);
                return;
            }
        }
        //if in a vehicle
        else if (cd.InVehicle != null)
        {
            cd.InVehicle.LeaveVehicle(cd);
            return;
        }

        //only allow interactions if not frozen
        if (!Frozen)
        {
            //try door enter
            Collider[] Doors = Physics.OverlapSphere(transform.position, 0.01f, 1 << LayerMask.NameToLayer("DoorEnter"));
            if (Doors != null)
            {
                float current_distance = float.MaxValue;
                DoorEnter d = null;
                foreach (Collider c in Doors)
                {
                    DoorEnter curr_door = c.GetComponent<DoorEnter>();
                    if (curr_door != null)
                    {
                        if ((curr_door.transform.position - transform.position).magnitude < current_distance)
                        {
                            d = curr_door;
                            current_distance = (curr_door.transform.position - transform.position).magnitude;
                        }
                    }
                }

                if (d != null)
                {
                    DoorEnter.EnterDoor(d, GetComponent<CharacterData>());
                    return;
                }
            }

            //Do sphere check for high priority quest first
            Collider[] Interacts = Physics.OverlapSphere(transform.position, 0.01f, 1 << LayerMask.NameToLayer("Interact"));
            if (Interacts != null && Interacts.Length > 0)
            {
                float current_distance = float.MaxValue;
                Interact i = null;
                foreach (Collider c in Interacts)
                {
                    Interact curr_i = c.GetComponent<Interact>();
                    if (curr_i != null)
                    {
                        if (curr_i.MyEvent.BaseQuest.isHighPriorityQuest && curr_i.MyEvent.CanRun())
                        {
                            if ((curr_i.transform.position - transform.position).magnitude < current_distance)
                            {
                                i = curr_i;
                                current_distance = (curr_i.transform.position - transform.position).magnitude;
                            }
                        }
                    }
                }

                if (i != null)
                {
                    i.Interacted();
                    return;
                }
            }

            //now do low priority quest interacts
            if (Interacts != null && Interacts.Length > 0)
            {
                float current_distance = float.MaxValue;
                Interact i = null;
                foreach (Collider c in Interacts)
                {
                    Interact curr_i = c.GetComponent<Interact>();
                    if (curr_i != null)
                    {
                        if (!curr_i.MyEvent.BaseQuest.isHighPriorityQuest && curr_i.MyEvent.CanRun())
                        {
                            if ((curr_i.transform.position - transform.position).magnitude < current_distance)
                            {
                                i = curr_i;
                                current_distance = (curr_i.transform.position - transform.position).magnitude;
                            }
                        }
                    }
                }

                if (i != null)
                {
                    i.Interacted();
                    return;
                }
            }
        }
    }

    void Update()
    {
        if (!GameSettings.IsInGame)
            return;

        //sign highlight effect
        RaycastHit hit;
        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 100,
                1 << LayerMask.NameToLayer("Clickable")))
        {
            if (current_clickable == null)
            {
                if (hit.collider.gameObject.GetComponent<SignHighlight>() != null)
                {
                    CharacterData cd = GetComponent<CharacterData>();
                    hit.collider.gameObject.GetComponent<SignHighlight>().Highlight();
                    if (!cd.IsArmed)
                    {
                        SignAction sign = hit.collider.GetComponent<SignAction>();
                        if (sign != null && sign.transform.parent != null)
                        {
                            SignAction.CurrentSign = sign.transform.parent.GetComponent<ZoneSettings>().zone_name;
                        }
                        else
                        {
                            SignAction.CurrentSign = "";
                        }
                    }
                }
            }
            current_clickable = hit.collider.gameObject;
        }
        else
        {
            if (current_clickable != null)
            {
                if (current_clickable.GetComponent<SignHighlight>() != null)
                {
                    current_clickable.GetComponent<SignHighlight>().UnHighlight();
                }
            }
            current_clickable = null;
            SignAction.CurrentSign = null;
        }
    }
}
