﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Note** class is not thread safe
/// </summary>
public static class DoorGraph
{
    private static Dictionary<MapData, MapNode> Graph = new Dictionary<MapData, MapNode>();
    static int StaticRunID = 0;

	public struct MapNode
    {
        public HashSet<DoorEnter> AdjMaps;
        public DoorEnter MarkedDoor;
        public int RunID;
    }

    /// <summary>
    /// Add a door to the graph
    /// </summary>
    /// <param name="thisDoor"></param>
    public static void AddDoor(DoorEnter thisDoor)
    {
        MapNode node;
        if (Graph.TryGetValue(thisDoor.MyMap, out node))
        {
            node.AdjMaps.Add(thisDoor);
        }
        else
        {
            node.AdjMaps = new HashSet<DoorEnter>();
            node.AdjMaps.Add(thisDoor);
            Graph.Add(thisDoor.MyMap, node);
        }
    }

    /// <summary>
    /// Remove a door from the graph
    /// </summary>
    /// <param name="thisDoor"></param>
    public static bool RemoveDoor(DoorEnter thisDoor)
    {
        MapNode node;
        if (Graph.TryGetValue(thisDoor.MyMap, out node))
        {
            if (node.AdjMaps != null)
                return node.AdjMaps.Remove(thisDoor);
        }
        return false;
    }

    /// <summary>
    /// Returns the next foor on thisMap, that will lead to targetMap
    /// </summary>
    /// <param name="thisMap"></param>
    /// <param name="targetMap"></param>
    /// <returns></returns>
    public static DoorEnter GetNextDoor(MapData thisMap, MapData targetMap)
    {
        if (Graph.Count == 0)
            return null;

        //increment run instance ID
        unchecked
        {
            StaticRunID++;
        }

        //Id for this function run
        int RunID = StaticRunID;

        //initialize queue
        Queue<MapData> NodeQueue = new Queue<MapData>();
        NodeQueue.Enqueue(thisMap);

        //initialize start node
        MapNode starter = Graph[thisMap];
        starter.RunID = RunID;

        int count = 0;
        //continue until the graph has been traversed
        while (NodeQueue.Count > 0)
        {
            count++; if (count > 1000) { Debug.Log("oops"); return null; }
            //get element from queue
            MapData p_map = NodeQueue.Dequeue();
            MapNode map_n = Graph[p_map];
            if (map_n.AdjMaps != null)
            {
                //iterate through doors (edges) on this map
                foreach (DoorEnter d in map_n.AdjMaps)
                {
                    MapNode next_n = Graph[d.MyExitsMap];
                    //if the node is unvisited
                    if (next_n.RunID != RunID)
                    {
                        //mark node as processed
                        next_n.RunID = RunID;
                        //assign which door was used (only for the first door from the start map)
                        if (p_map == thisMap)
                            next_n.MarkedDoor = d;
                        //otherwise just reassign the previous door used
                        else
                            next_n.MarkedDoor = map_n.MarkedDoor;

                        //if this node is the target...
                        //due to the nature of BFS, then it is also the closest path
                        //and we can just start to backtrace immediately
                        if (d.MyExitsMap == targetMap)
                        {
                            //no backtracing required, hurrah
                            return next_n.MarkedDoor;
                        }

                        //Enque the node
                        NodeQueue.Enqueue(d.MyExitsMap);
                    }
                }
            }
        }

        //not found from starting point
        return null;
    }
}
