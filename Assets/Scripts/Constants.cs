﻿using UnityEngine;
using System.Collections;

public static class Constants
{
    /// <summary>
    /// In Cooldown time
    /// </summary>
    public static float UZI_fireRate
    {
        get
        {
            return 0.1f;
        }
    }
    public static float UMP_fireRate
    {
        get
        {
            return 0.13f;
        }
    }
    /// <summary>
    /// In Cooldown time
    /// </summary>
    public static float SMG_fireRate
    {
        get
        {
            return 0.2f;
        }
    }
    /// <summary>
    /// In Cooldown time
    /// </summary>
    public static float M16_fireRate
    {
        get
        {
            return 0.175f;
        }
    }
    public static float Handgun_fireRate
    {
        get
        {
            return 0.25f;
        }
    }
    public static float Magnum_fireRate
    {
        get
        {
            return 0.26f;
        }
    }
    public static float Shotgun_fireRate
    {
        get
        {
            return 0.9f;
        }
    }
    public static float Bazooka_fireRate
    {
        get
        {
            return 1.2f;
        }
    }
    public static float Grenade_fireRate
    {
        get
        {
            return 1.4f;
        }
    }
    /// <summary>
    /// Damage Per Shot
    /// </summary>
    public static float UZI_Damage
    {
        get
        {
            return 12.1f;
        }
    }
    public static float UMP_Damage
    {
        get
        {
            return 14.3f;
        }
    }
    public static float SMG_Damage
    {
        get
        {
            return 21;
        }
    }
    public static float M16_Damage
    {
        get
        {
            return 17.5f;
        }
    }
    public static float Handgun_Damage
    {
        get
        {
            return 20.0f;
        }
    }
    public static float Magnum_Damage
    {
        get
        {
            return 27.0f;
        }
    }
    public static float Shotgun_Damage
    {
        get
        {
            return 90.0f;
        }
    }
    public static float Bazooka_Damage
    {
        get
        {
            return 170.0f;
        }
    }
    public static float Grenade_Damage
    {
        get
        {
            return 260.0f;
        }
    }

    /// <summary>
    /// Range before damage decreases (linear)
    /// </summary>
    public static float UMP_Range
    {
        get
        {
            return 2.0f;
        }
    }
    public static float UZI_Range
    {
        get
        {
            return 1.7f;
        }
    }
    public static float Shotgun_Range
    {
        get
        {
            return 1.0f;
        }
    }
    public static float Magnum_Range
    {
        get
        {
            return 2.0f;
        }
    }
    public static float Handgun_Range
    {
        get
        {
            return 2.0f;
        }
    }
    public static float M16_Range
    {
        get
        {
            return 3.2f;
        }
    }
    public static float SMG_Range
    {
        get
        {
            return 3.0f;
        }
    }

    /// <summary>
    /// For explosives, this number is the radius in which the explosion
    /// will hurt enemies
    /// </summary>
    public static float Bazooka_Radius
    {
        get
        {
            return 1.1f;
        }
    }
    public static float Grenade_Radius
    {
        get
        {
            return 1.6f;
        }
    }

    /// <summary>
    /// Shoot speed multiplier. Npc shoot speed will be Constants.NPCShootSpeed * WeaponFireRate.
    /// Higher is slower, lower is faster. 1.0 = normal. Values lower than 1.0 have no effect.
    /// </summary>
    public static float NPCShootSpeed
    {
        get
        {
            float n = 1 / (float)Difficulty * 4;
            return n + 1;
        }
    }
    /// <summary>
    /// Time being in combat until NPC start fighting
    /// </summary>
    public static float NPCStartCombatDelay
    {
        get
        {
            return 0.7f * (1 - Difficulty / 10 + 0.1f);
        }
    }

    /// <summary>
    /// How often do NPC's Miss? Does not apply to bazookas/grenades
    /// </summary>
    public static float NPCMissRate
    {
        get
        {
            float n = 1 / (float)Difficulty * 2;
            return n > 0.9f ? 0.9f : n;
        }
    }
    /// <summary>
    /// 1 to 10, 10 is hardest
    /// </summary>
    public static int Difficulty = 3;
    /// <summary>
    /// How fast the NPC's run.
    /// </summary>
    public static float AIRunSpeed
    {
        get
        {
            return 1.3f + (Difficulty / 10 * 2);
        }
    }
    public static float AIWalkSpeed
    {
        get
        {
            return 0.5f;
        }
    }
    public static float VehicleWalkSpeed
    {
        get
        {
            return 2.4f;
        }
    }
    /// <summary>
    /// How fast each faction tick occurs. In seconds
    /// </summary>
    public static float FactionSimSpeed
    {
        get
        {
            return 30.0f;
        }
    }
    /// <summary>
    /// Amount to deteriorate faction relations by when murdering one of their members
    /// </summary>
    public static float KillFactionMemberPenalty
    {
        get
        {
            return -1f;
        }
    }
    /// <summary>
    /// How much a bribe increases relations by
    /// </summary>
    /// <param name="money"></param>
    /// <returns></returns>
    public static float BribeAmount(int money)
    {
        return money / 50.0f;
    }
    /// <summary>
    /// Maximum range of all guns
    /// </summary>
    public static float MaxGunRange
    {
        get
        {
            return 6.5f;
        }
    }
    /// <summary>
    /// Player's health
    /// </summary>
    public static int PlayerMaxHealth
    {
        get
        {
            return 100;
        }
    }

    /// <summary>
    /// Fx further than this value from the player will be thrown away
    /// </summary>
    public static float RenderFXDistance
    {
        get
        {
            return 8.0f;
        }
    }
    /// <summary>
    /// Returns the firerate for the input guntypes
    /// </summary>
    /// <param name="gun"></param>
    /// <returns></returns>
    public static float FireRate(Guns gun)
    {
        if (gun == Guns.Bazooka)
            return Bazooka_fireRate;
        if (gun == Guns.Grenade)
            return Grenade_fireRate;
        if (gun == Guns.Handgun)
            return Handgun_fireRate;
        if (gun == Guns.M16)
            return M16_fireRate;
        if (gun == Guns.Magnum)
            return Magnum_fireRate;
        if (gun == Guns.Shotgun)
            return Shotgun_fireRate;
        if (gun == Guns.SMG)
            return SMG_fireRate;
        if (gun == Guns.UMP)
            return UMP_fireRate;
        if (gun == Guns.UZI)
            return UZI_fireRate;
        Debug.Log("Bad gun");
        return -1;
    }
    /// <summary>
    /// The amount a player health pack heals by
    /// </summary>
    public static int HealthPackHealAmount
    {
        get
        {
            return 75;
        }
    }
    /// <summary>
    /// Cost per medpack. This does not update the UI
    /// </summary>
    public static int MedPacksCost
    {
        get
        {
            return 10;
        }
    }

}

public enum Guns
{
    UMP,
    UZI,
    M16,
    Handgun,
    Magnum,
    Shotgun,
    SMG,
    Bazooka,
    Grenade
}

public enum Direction
{
    North,
    East,
    South,
    West
}