﻿using UnityEngine;
using System.Collections;

/// <summary>
/// An optional component that allows more customization of what a spawner spawns
/// </summary>
public class ZoneConfiguration : MonoBehaviour {

    [Tooltip("Override LWeapon, set to 'None' for no weapon (case sensitive)")]
    public string LWeapon;
    [Tooltip("Override RWeapon, set to 'None' for no weapon (case sensitive)")]
    public string RWeapon;

    public string FirstName;
    public string LastName;

    public string Hair;
    public string Hat;
    public string Shirt;
    public string Shoes;
    public string Face;
    public string Arms;
    public string legs;
    public string OuterEye;
    public string EyeColor;
    public string Glasses;

    public bool AttacksPlayerOnSpawn;

    void Awake()
    {
        GetComponent<ZoneSettings>().AttackPlayer = AttacksPlayerOnSpawn;
    }
}
