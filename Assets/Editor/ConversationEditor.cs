﻿
using UnityEditor;
using UnityEngine;
using System;

[CustomEditor(typeof(Dialogue)), CanEditMultipleObjects]
public class TextAreaEditor : Editor
{
    public SerializedProperty longStringProp;
    public SerializedProperty responsearr;
    public SerializedProperty enumprop;
    public SerializedProperty facttype;
    public SerializedProperty ptype;
    public SerializedProperty randProp;
    public SerializedProperty Character;

    void OnEnable()
    {
        longStringProp = serializedObject.FindProperty("ThisDialogue");
        responsearr = serializedObject.FindProperty("ThisResponses");
        enumprop = serializedObject.FindProperty("ThisDialogueEventType");
        facttype = serializedObject.FindProperty("FactionDialogue");
        ptype = serializedObject.FindProperty("PersonalityType");
        Character = serializedObject.FindProperty("SaysThisDialogue");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        ArrayGUI(serializedObject, longStringProp);
        ArrayGUINormal(serializedObject, responsearr);
        EditorGUILayout.PropertyField(enumprop);
        EditorGUILayout.PropertyField(facttype);
        EditorGUILayout.PropertyField(ptype);
        EditorGUILayout.PropertyField(Character);
        serializedObject.ApplyModifiedProperties();
    }

    void ArrayGUI(SerializedObject obj, SerializedProperty _property)
    {
        int size = _property.arraySize;
        EditorGUI.indentLevel = 0;
        int newSize = EditorGUILayout.IntField(_property.name + " Size", size);

        if (newSize != size)
        {
            _property.arraySize = newSize;
        }

        EditorGUI.indentLevel = 3;

        for (int i = 0; i < newSize; i++)
        {
            SerializedProperty p = longStringProp.GetArrayElementAtIndex(i);
            p.stringValue = EditorGUILayout.TextArea(p.stringValue, GUILayout.MaxHeight(75));
        }
        EditorGUI.indentLevel = 0;
    }
    void ArrayGUINormal(SerializedObject obj, SerializedProperty _property)
    {
        int size = _property.arraySize;
        EditorGUI.indentLevel = 0;
        int newSize = EditorGUILayout.IntField(_property.name + " Size", size);
        EditorGUI.indentLevel = 3;
        if (newSize != size)
        {
            _property.arraySize = newSize;
        }

        for (int i = 0; i < newSize; i++)
        {
            var prop = _property.GetArrayElementAtIndex(i);
            EditorGUILayout.PropertyField(prop);
        }
        EditorGUI.indentLevel = 0;
    }
}
