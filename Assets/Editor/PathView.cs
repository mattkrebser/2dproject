﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class PathView : EditorWindow {

    public static GameObject new_obj;

    [MenuItem("Window/Build Paths")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        PathView window = (PathView)EditorWindow.GetWindow(typeof(PathView));
        window.Show();
        START();
    }

    // Use this for initialization
    public static void START()
    {
        func();
	}
	
    static void func()
    {
        Transform[] list = GameObject.FindObjectsOfType<Transform>();
        foreach (Transform t in list)
        {
            AIPath ai;
            if ((ai = t.GetComponent<AIPath>()) != null)
            {
                foreach (Transform child in ai.GetComponentsInChildren<Transform>())
                {
                    if (child != ai.transform)
                    {
                        if (!Contains(child, ai.path))
                        {
                            ai.path.Add(child);
                        }
                    }
                }
            }
        }
        foreach (Transform t in list)
        {
            AIPath ai;
            if ((ai = t.GetComponent<AIPath>()) != null)
            {
                if (ai.path != null && ai.path.Count > 1 && ai.show)
                {
                    Vector3 first = Vector3.zero;
                    Vector3 previous = Vector3.zero;
                    for(int i = 0; i < ai.path.Count; i++)
                    {
                        if (ai.path[i] != null)
                        {
                            if (i == 0)
                            {
                                first = ai.path[i].position; previous = ai.path[i].position; continue;
                            }

                            Debug.DrawLine(previous, ai.path[i].position, Color.blue, 38.0f, false);

                            previous = ai.path[i].position;
                        }
                    }
                    Debug.DrawLine(ai.path[ai.path.Count - 1].position, first, Color.white, 38.0f, false);
                }
            }
        }
    }

    static bool Contains(Transform t, List<Transform> arr)
    {
        if (arr == null || arr.Count == 0)
            return false;
        foreach (Transform trans in arr)
        {
            if (trans == t)
                return true;
        }
        return false;
    }
}
