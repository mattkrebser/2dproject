# README #

Unity Game!

### What is this repository for? ###

* A 2D GTA-like game. Players can drive around and shoot things. Also has quests and different events.
* Version 1.0

### How do I get set up? ###

* This repository is a Unity project folder
* Download this repository and open it in Unity3D
* Works as of Unity 5.3.3f1 **Not** any later version
* **Included a build (build1.exe and buil1_data) if you just want to test run** (Download the entire repository, then run the executable)

### Build Notes ###
* Build in Unity like normal.
* After building, copy/paste the 'LobbySave' folder from the Assets folder into the output 'build_data' folder. Otherwise the game will crash.
* Alternatively, run inside the editor.
* Game has a few minor bugs, I am not currently supporting this game
* Your browser may accuse the build as being a virus, ignore it
* Only tested on windows 10

![2DProjScrnshot.png](https://bitbucket.org/repo/okpkja/images/95302449-2DProjScrnshot.png)